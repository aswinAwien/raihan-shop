package uk.co.senab.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.support.v4.widget.ImageViewCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

public class FrontPhotoView extends android.support.v7.widget.AppCompatImageView implements IPhotoView  {

    private final PhotoViewAttacher mAttacher;

    private ScaleType mPendingScaleType;

    public FrontPhotoView(Context context) {
        this(context, null);
    }

    public FrontPhotoView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public FrontPhotoView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        super.setScaleType(ScaleType.MATRIX);
        mAttacher = new PhotoViewAttacher(this);

        if (null != mPendingScaleType) {
            setScaleType(mPendingScaleType);
            mPendingScaleType = null;
        }
    }

    @Override
    public boolean canZoom() {
        return false;
    }

    @Override
    public RectF getDisplayRect() {
        return null;
    }

    @Override
    public float getMinScale() {
        return 0;
    }

    @Override
    public float getMidScale() {
        return 0;
    }

    @Override
    public float getMaxScale() {
        return 0;
    }

    @Override
    public float getScale() {
        return 0;
    }

    @Override
    public void setAllowParentInterceptOnEdge(boolean allow) {

    }

    @Override
    public void setMinScale(float minScale) {
        mAttacher.setMinScale(minScale);
    }

    @Override
    public void setMidScale(float midScale) {
        mAttacher.setMidScale(midScale);
    }

    @Override
    public void setMaxScale(float maxScale) {
        mAttacher.setMaxScale(maxScale);
    }

    @Override
    public void setOnMatrixChangeListener(PhotoViewAttacher.OnMatrixChangedListener listener) {

    }

    @Override
    public void setOnPhotoTapListener(PhotoViewAttacher.OnPhotoTapListener listener) {
        mAttacher.setOnPhotoTapListener(listener);
    }

    @Override
    public void setOnViewTapListener(PhotoViewAttacher.OnViewTapListener listener) {
        mAttacher.setOnViewTapListener(listener);
    }

    @Override
    public void setZoomable(boolean zoomable) {
        mAttacher.setZoomable(zoomable);
    }

    @Override
    public void zoomTo(float scale, float focalX, float focalY) {

    }
}
