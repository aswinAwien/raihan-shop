package com.raihangroup.raihanshop;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.WrapperListAdapter;

import com.raihangroup.raihanshop.activity.SingleModeActivity;
import com.raihangroup.raihanshop.adapter.LayoutPagerAdapter;
import com.raihangroup.raihanshop.adapter.ModelExpandableAdapter;
import com.raihangroup.raihanshop.adapter.SimpleExpandableAdapter;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment.Photo;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.helper.Utility.ReturnInterface;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.novoda.imageloader.core.model.ImageTagFactory;
import com.viewpagerindicator.PageIndicator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

/**
 * Fragment for channels screen.
 * @author Eduar Napoleon
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public abstract class ModelChannel implements Serializable, Comparable<ModelChannel>{
    private static final long serialVersionUID = 4372102976625158929L;
	private static final Map<View, Pair<ModelChannel, Integer>> modelViewMap = Collections.synchronizedMap(new WeakHashMap<View, Pair<ModelChannel, Integer>>());
    public static final int MAX_QUALITY = 100;
	private static final String TAG = ModelChannel.class.getSimpleName();

	public static Drawable background;
	public static int circleMask;
	
	protected transient String currentURL;
	protected transient boolean notInitiated = true;
	protected transient View currentView;

	public interface MethodInterface<A,B>{
		A invoke(B obj);
	}
	
	public boolean isInitiated(){
		return !notInitiated;
	}

	protected void onUpdateFinished() {}

	protected boolean onUpdateStart(String url) {
		return false;
	}

	protected void saveComplete(String obj) {}

	public List<String> savedField() {
		return new ArrayList<String>();
	}

	public Map<String, Object> getSaveData() {
		Map<String, Object> temp = getFieldMap();
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> field = savedField();
		if(field.size() > 0){
			for(String s:field){
				map.put(s, temp.get(s));
			}
		} else {
			map = temp;
		}
		return map;
	}

	protected String updateURL() {
		return null;
	}

	public boolean match(String string) {
		if(getTitle() == null)	{
			return (string == null || string.length() == 0);
		}
		return getTitle().toString().toLowerCase(Locale.getDefault()).contains(string.toLowerCase(Locale.getDefault())) ||
			   (getDescription() != null && getDescription().toLowerCase(Locale.getDefault()).contains(string.toLowerCase(Locale.getDefault())));
	}

	public View getListView(View convertView, int layout, int position, final ViewGroup parent) {
		if(convertView == null || !((Integer)layout).equals(convertView.getTag(R.id.content))) {
			LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			convertView.setTag(R.id.content, layout);
		}
		getData(convertView, position);
		return convertView;
	}
	
	@Override
	public String toString() {
		CharSequence title = getTitle();
		return title!=null?title.toString():super.toString();
	}
	
	public Collection<View> getViews(){
		HashSet<View> views = new HashSet<View>();
		for(Entry<View, Pair<ModelChannel, Integer>> e:modelViewMap.entrySet()){
			if(e.getValue().first == this)	{
				views.add(e.getKey());
			}
		}
		return views;
	}
	
	public Collection<View> getViews(int id){
		HashSet<View> views = new HashSet<View>();
		for(Entry<View, Pair<ModelChannel, Integer>> e:modelViewMap.entrySet()){
			if(e.getValue().first == this)	{
				View v = e.getKey().findViewById(id);
				if(v != null)	views.add(v);
			}
		}
		return views;
	}
	
	public Collection<Activity> getActivitys(){
		HashSet<Activity> activitys = new HashSet<Activity>();
		for(Entry<View, Pair<ModelChannel, Integer>> e:modelViewMap.entrySet()){
			if(e.getValue().first == this && e.getKey().getContext() instanceof Activity)	{
				activitys.add((Activity) e.getKey().getContext());
			}
		}
		return activitys;
	}
	
	public void updateView(){
		try{
			final ArrayList<Pair<View, Integer>> list = new ArrayList<Pair<View, Integer>>();
			for(Entry<View, Pair<ModelChannel, Integer>> e:modelViewMap.entrySet()){
				if(e.getValue().first == this || (isPersistance() && getId().equals(e.getValue().first.getId())))	{
					list.add(new Pair<View, Integer>(e.getKey(), e.getValue().second));
				}
			}
//	        log("Model:Update", getTitle()+":"+list);
			Utility.mainThread.post(new Runnable() {
				@Override
				public void run() {
					for(Pair<View, Integer> e:list)	{
						getData(e.first, e.second);
					}
				}
			});
		}catch(Exception e){}
	}
	
	public void updateView(final int... ids){
		updateView(true, ids);
	}
	
	public void updateView(final boolean clear, final int... ids){
		try{
			final ArrayList<Pair<View, Integer>> list = new ArrayList<Pair<View, Integer>>();
			for(Entry<View, Pair<ModelChannel, Integer>> e:modelViewMap.entrySet()){
				if(e.getValue().first == this || (isPersistance() && getId().equals(e.getValue().first.getId())))	{
					list.add(new Pair<View, Integer>(e.getKey(), e.getValue().second));
				}
			}
	        log("Model:Update", getTitle()+":"+list);
			Utility.mainThread.post(new Runnable() {
				@Override
				public void run() {
					for(Pair<View, Integer> e:list)	{
						for(int id:ids){
							View v = e.first.findViewById(id);
							if(clear)	clearData(v);
							getViewsData(v, e.second);
						}
					}
				}
			});
		}catch(Exception e){}
	}

	public void clearData(View v) {
		v.setTag(R.id.model_content, null);	
		v.setTag(R.id.methods, null);	
		if(v instanceof ViewGroup){
			for(View vi:Utility.getAllView((ViewGroup) v)){
				vi.setTag(R.id.model_content, null);	
				vi.setTag(R.id.methods, null);	
			}
		}
	}

	public boolean validate(Context ctx, HashMap<String, Object> item, List<Pair<String, String>> notEmpty, Map<String, String> regex, Map<String, Integer> length) {
		for(Pair<String, String> i:notEmpty){
			Object o = item.get(i.first);
			if(o == null)	continue;
			if(o.toString().length()==0){
				Utility.toast(ctx, i.second+" tidak boleh kosong");
				return false;
			} else if(length != null && o instanceof String && length.containsKey(i.first)){
				if(o.toString().length() < length.get(i.first)){
					Utility.toast(ctx, i.second+" minimal "+length.get(i.first)+" karakter");
					return false;
				}
			}
			if(regex != null && o instanceof String && regex.containsKey(i.first)){
				if(!o.toString().matches(regex.get(i.first))){
					Utility.toast(ctx, "Input "+i.second+" tidak valid");
					return false;
				}
			}
			
		}
		return true;
	}
	
	public boolean validate(Context ctx, HashMap<String, Object> item, Map<String, String> name, String[] notEmpty, Map<String, String> regex, Map<String, Integer> length) {
		for(String i:notEmpty){
			Object o = item.get(i);
			if(o == null)	continue;
			if(o.toString().length()==0){
				Utility.toast(ctx, name.get(i)+" tidak boleh kosong");
				return false;
			}			
		}
		for(Entry<String, Integer> key:length.entrySet()){
			Object o = item.get(key.getKey());
			if(o == null)	continue;
			if(o.toString().length() < key.getValue()){
				Utility.toast(ctx, name.get(key.getKey())+" minimal "+key.getValue()+" karakter");
				return false;
			}
		}
		for(Entry<String, String> key:regex.entrySet()){
			Object o = item.get(key.getKey());
			if(o == null)	continue;
			if(!o.toString().matches(key.getValue())){
				Utility.toast(ctx, "Input "+name.get(key.getKey())+" tidak valid");
				return false;
			}
		}
		return true;
	}
	
	public boolean validate(Context ctx, HashMap<String, Object> item, Collection<String> notNull) {
		for(String i:notNull){
			if(item.get(i) == null){
				Utility.toast(ctx, i+" tidak boleh kosong");
				return false;
			}
		}
		for(Entry<String, Object> i:item.entrySet()){
			if(i.getValue() == null)	{
				Utility.toast(ctx, "Kesalahan pada "+i.getKey());
				return false;
			}
		}
		return true;
	}
	
	public boolean validate(Context ctx, HashMap<String, Object> item) {
		return validate(ctx, item, getFieldMap().keySet());
	}

	protected void finish(View v) {
		((Activity) v.getContext()).finish();
	}
	
	private static class Click implements OnClickListener{
		private List<RadioButton> list = new ArrayList<RadioButton>();

		public void add(RadioButton b) {
			list.add(b);
		}
		
		@Override
		public void onClick(View v) {
			for(RadioButton b:list){
				if(b == v)	{
					b.setChecked(true);
					b.setClickable(false);
				} else {
					b.setChecked(false);
					b.setClickable(true);
				}
			}
		}
	}
	
	public boolean getData(View v, int position){
//		if(notInitiated){
//			loadOffline();
//			if(notInitiated)	return false;
//		}

		getSimple(v, position);
		return getViewsData(v, position);
	}
	
	public Object gettext1(){
		CharSequence i = getTitle();
		return isEmpty(i)?false:i;
	}
	
	public Object gettext2(){
		CharSequence i = getDescription();
		return isEmpty(i)?false:i;
	}
	
	public Object geticon(){
		Object i = getIcon();
		return isEmpty(i)?false:i;
	}
	
	public boolean isEmpty(Object i) {
		return i==null;
	}

	public void getSimple(View v, int position){
		currentView = v;
		addView(v, position);
	}
	
	public boolean getViewsData(View v, int position) {
		HashMap<String, List<View>> result = (HashMap<String, List<View>>) v.getTag(R.id.views);
		if(result == null) {
			result = Utility.getViewsID(v);
			v.setTag(R.id.views, result);
		}
//		log("Model:"+getTitle(), ""+result.keySet());
		for(Entry<String, List<View>> view:result.entrySet()){
			for(View vi:view.getValue()){
				Object value = getField(view.getKey(), vi, v);
				if(value != null)	setField(vi, vi.getId(), value);
			}
		}
		return false;
	}

	protected OnClickListener getOnClick(final View view, String key) {
		try {
			final Method m = getClass().getMethod("click"+key, View.class);
			if(m != null){
				return new OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							m.setAccessible(true);
							Object result = m.invoke(ModelChannel.this, view);
							if(result != null && result instanceof Boolean && (Boolean) result)
								updateView();
						} catch (Exception e) {}
					}
				};
			}
		} catch (Exception e1) {}
		return null;
	}

	protected Object getIcon() {
		return null;
	}

	public void form(final View vi){
		View temp = vi.findViewById(R.id.send);
		if(temp != null){
			temp.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					HashMap<String, Object> data = getItem(vi);
					if(validate(vi, data)){
						send(vi, data);
					}
				}
			});
		}
	}

	public static void clearForm(View v){
		HashMap<String, List<View>> views = Utility.getViewsID(v);
		for(Entry<String, List<View>> i:views.entrySet()){
			for(View vi:i.getValue()){
				if(vi instanceof EditText)	((EditText) vi).setText("");
			}
		}
	}
	
	protected void send(View vi, HashMap<String, Object> data) {}

	protected boolean validate(View vi, HashMap<String, Object> data) {
		return validateNotEmpty(data);
	}

	public static boolean validateNotEmpty(HashMap<String, Object> data) {
		for(Object o:data.values()){
			if(o.toString().length() ==0)	return false;
		}
		return true;
	}

	protected void addView(View v, int position) {
		modelViewMap.put(v, new Pair<ModelChannel, Integer>(this, position));
	}

	public static class SetFieldWrapper implements MethodInterface<Object, ModelChannel>{
		Field field;
		Object item;
		
		public SetFieldWrapper(Field field, Object item){
			this.field = field;
			this.item = item;
			field.setAccessible(true);
		}
		
		@Override
		public Object invoke(ModelChannel o) {
			o.log("Invoke:Field", ""+this);
			try {
				field.set(o, item);
			} catch (Exception e) {
				o.log("Invoke:0:", Log.getStackTraceString(e));
			}
			return null;
		}
		
		@Override
		public String toString() {
			return field.getName()+":"+item;
		}
	}

	public static class FieldWrapper implements MethodInterface<Object, ModelChannel>{
		Field field;
		
		public FieldWrapper(Field field){
			this.field = field;
			field.setAccessible(true);
		}
		
		@Override
		public Object invoke(ModelChannel o) {
			o.log("Invoke:Field", ""+this);
			try {
				return field.get(o);
			} catch (Exception e) {
				o.log("Invoke:Failed:", Log.getStackTraceString(e));
			}
			return null;
		}
		
		@Override
		public String toString() {
			return field.getName();
		}
	}
	
	public String getTag(){
		return null;
	}
	
	public static class MethodWrapper implements MethodInterface<Object, ModelChannel>{
		Method method;
		Object[] item;
		
		public MethodWrapper(Method method, Object... item){
			this.method = method;
			this.item = item;
			method.setAccessible(true);
		}
		
		@Override
		public Object invoke(ModelChannel o) {
			o.log("Invoke:Method", ""+MethodWrapper.this+":"+Arrays.asList(item));
			try {
				return method.invoke(o, item);
			} catch (Exception e) {
				o.log("Invoke:1:", this+":"+Log.getStackTraceString(e));
			}
			return null;
		}

		@Override
		public String toString() {
			return method.getName()+"("+Arrays.asList(method.getParameterTypes())+")";
		}
	}
	
	public static class OnClickWrapper implements MethodInterface<Object, ModelChannel>{
		Method method;
		View view;
		Object[] item;
		
		public OnClickWrapper(Method method, View view, Object... item){
			this.method = method;
			this.item = item;
			this.view = view;
			method.setAccessible(true);
		}
		
		@Override
		public String toString() {
			return method.getName()+"("+Arrays.asList(method.getParameterTypes())+")";
		}
		
		@Override
		public Object invoke(final ModelChannel o) {
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					o.log("Invoke:Click:", ""+OnClickWrapper.this+":"+Arrays.asList(item));
					try {
						Object result = method.invoke(o, item);
						if(result != null && result instanceof Boolean && (Boolean) result)
							o.updateView(view);
					} catch (Exception e) {
						o.log("Invoke:2:", Log.getStackTraceString(e));
					}
				}
			});
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public void updateView(final View v){
		final List<MethodInterface<Object, ModelChannel>> methods = (List<MethodInterface<Object, ModelChannel>>) v.getTag(R.id.methods);
		if(methods != null){
			v.post(new Runnable() {
				@Override
				public void run() {
					Object value = invoke(methods);
					if(value != null)	
						setField(v, v.getId(), value);
				}
			});
		}
	}
	
	public Object invoke(List<MethodInterface<Object, ModelChannel>> methods){
		Object result = null;
		log("Invoke:List:", ""+methods);
		for(MethodInterface<Object, ModelChannel> m:methods){
			result = m.invoke(this);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private Object getField(String key, final View view, View root) {
		Class<? extends ModelChannel> cls = getClass();
		
		List<Class<? extends ModelChannel>> classes= (List<Class<? extends ModelChannel>>) view.getTag(R.id.model_content);
		if(classes == null)	classes = new ArrayList<Class<? extends ModelChannel>>();
		
		List<MethodInterface<Object, ModelChannel>> methods = (List<MethodInterface<Object, ModelChannel>>) view.getTag(R.id.methods);
		if(methods != null && classes.contains(cls)){
			return invoke(methods);
		}
		
		classes.add(cls);
		methods = new ArrayList<MethodInterface<Object, ModelChannel>>();
		try{
			methods.add(new MethodWrapper(cls.getMethod(key, View.class), view));
		}catch(Exception e){}
		try{
			methods.add(new MethodWrapper(cls.getMethod(key, View.class, View.class), view, root));
		}catch(Exception e){}
		Field fi = null;
		try{
			fi = cls.getField(key);
			if(fi != null && fi.getType().isInstance(view)){
				methods.add(new SetFieldWrapper(fi, view));
				fi = null;
			}
		}catch(Exception f){}
		try{
			methods.add(new OnClickWrapper(cls.getMethod("click"+key, View.class), view, view));
		}catch(Exception e){}
		try{
			methods.add(new MethodWrapper(cls.getMethod("get"+key)));
			return invoke(methods);
		}catch(Exception e){
			try{
				methods.add(new MethodWrapper(cls.getMethod("get"+key, View.class), view));
			}catch(Exception e1){
				if(fi != null){
					try{
						methods.add(new FieldWrapper(fi));
					}catch(Exception f){}
				}
			}
		}
		Object result = invoke(methods);
		view.setTag(R.id.model_content, classes);
		view.setTag(R.id.methods, methods);
		
		return result;
	}

	public Map<String, Object> getFieldMap() {
		HashMap<String, Object>	result = new HashMap<String, Object>();
		for(Field f:getFields(getClass())){
			try {
				Object obj = f.get(this);
				if(obj != null)	result.put(f.getName(), f.get(this));
			} catch (Exception e) {}
		}		
		return result;
	}

	public void setField(View view, int id, Object item){
		setView(view, id, item, this);
	}
	
	@SuppressWarnings({ "rawtypes" })
	protected void setPager(ViewPager pager, List l) {
		if(l.isEmpty()){
			setDefault(pager);
		} else if(pager.getTag(R.id.list) != l){
			pager.setTag(R.id.list, l);
			setAdapter(pager, l);
			setPagerLimit(pager, l);
		} else {
			pager.getAdapter().notifyDataSetChanged();
			setPagerLimit(pager, l);
		}
		ViewParent parent = pager.getParent();
		if(parent != null && parent instanceof ViewGroup){
			View indicator = ((ViewGroup)parent).findViewById(R.id.indicator);
			if(indicator != null && indicator instanceof PageIndicator){
				((PageIndicator) indicator).setViewPager(pager);
			}
		}
	}

	protected void setPagerLimit(ViewPager pager, List l) {
		pager.setOffscreenPageLimit(l.size());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void setAdapter(ViewPager pager, List l) {
		pager.setVisibility(View.VISIBLE);
		Object i = l.get(0);
		if(i instanceof ModelChannel){
			int layout = getLayoutFromTag(pager);
			LayoutPagerAdapter adapter = layout>0?new LayoutPagerAdapter<ModelChannel>(layout, l):new LayoutPagerAdapter<ModelChannel>(l);
			pager.setAdapter(adapter);
		}
	}

	public static int getLayoutFromTag(View v) {
		if(v.getTag() == null)	return 0;
		try{
			return Integer.valueOf(v.getTag().toString().substring(1));
		}catch(Exception e){}
		return 0;
	}

	protected void setDefault(ViewPager pager) {
		pager.setVisibility(View.GONE);
	}
	
	public int getHeader(){
		return 0;
	}
	
	public int getFooter(){
		return 0;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected ArrayAdapter setList(AdapterView list, List l) {
		ArrayAdapter adapter = null;
		list.setEnabled(true);
		
		if(list.getEmptyView() == null)	{
			list.setEmptyView(getEmptyList(list));
		}
		if(l.isEmpty()){
			list.setTag(R.id.list, null);
			setDefault(list, l);
		} else if(list.getTag(R.id.list) != l){
			adapter = getAdapter(list, l);
			if(adapter != null)	list.setAdapter(adapter);
		} else {
			Adapter temp = list.getAdapter();
			if(temp instanceof WrapperListAdapter)	temp = ((WrapperListAdapter) temp).getWrappedAdapter();
			if(temp instanceof ArrayAdapter){
				adapter = (ArrayAdapter) temp;
				adapter.notifyDataSetChanged();
			} else {
				adapter = getAdapter(list, l);
				if(adapter != null)	list.setAdapter(adapter);
			}
		}
		return adapter;
	}

	@SuppressWarnings("rawtypes")
	protected View getEmptyList(AdapterView list) {
		LayoutInflater inflater = (LayoutInflater) list.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return inflater.inflate(R.layout.loading, list, false);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayAdapter getAdapter(final View list, final List l) {
		list.setTag(R.id.list, l);
		Object i = l.get(0);
		if(i instanceof ModelChannel){
			int layout = getLayoutFromTag(list);
//			log("Tag:"+getTitle(), ""+list.getTag());
			ArrayAdapter<ModelChannel> adapter = null;
			ViewChanger vc = new ViewChanger() {
				@Override
				public boolean getData(View v, int position) {
					if(position < l.size()){
						return overrideChild(v, position, (ModelChannel) l.get(position), list.getId());
					} else return false;
				}
			};
			
			if(layout > 0)
				adapter = ModelChannel.getAdapter(list.getContext(), layout, vc,(List<ModelChannel>) l);	
			else 
				adapter = ModelChannel.getAdapter(list.getContext(), vc, (List<ModelChannel>) l);
			return adapter;
		}
		else if(i instanceof Pair){
			int layout = getLayoutFromTag(list);
			ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(list.getContext(), layout>0?layout:android.R.layout.simple_list_item_1, l);
			return adapter;
		}
		else {
			int layout = getLayoutFromTag(list);
			ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(list.getContext(), layout>0?layout:android.R.layout.simple_list_item_1, l);
			return adapter;
		}
	}
	
	protected boolean overrideChild(View v, int position, ModelChannel object, int id) {
		return false;
	}

	public String getShareImage(){
		return null;
	}
	
	public static void share(final Context ctx, final Intent shareIntent){
		PackageManager pm = ctx.getPackageManager();
		List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
		List<ActivityModel> models = new ArrayList<ActivityModel>();
		for (final ResolveInfo app : activityList) {
			models.add(new ActivityModel(ctx, app));
		}
		Utility.chooseDialog2(ctx, "Pilih Aplikasi", new ReturnInterface<ActivityModel>() {
			@Override
			public void get(ActivityModel obj) {
				final ResolveInfo app = obj.app;
				final ActivityInfo activity = app.activityInfo;
					final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
					shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
					shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
					shareIntent.setComponent(name);
					ctx.startActivity(shareIntent);
			}
		}, models.toArray(new ActivityModel[0]));
	}
	
	public void onResume(View v){}
	
	public void onPause(View v){}
	
	public void onDestroy(View v){}

	public void share(final View v){
		v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				final Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Tertib Lantas");
				intent.putExtra(Intent.EXTRA_TEXT, getDescription());
				final String image = getShareImage();
				if(image != null){
					if(image.startsWith("http")){
						File img = ImageLoaderApplication.getImageLoader().getFileManager().getFile(image);
//						File img = ImageLoader.getInstance().getDiscCache().get(image);
						if(img != null && img.exists()){
							Bitmap m = BitmapFactory.decodeFile(img.getAbsolutePath());
							
							intent.setType("image/*");
							ByteArrayOutputStream bytes = new ByteArrayOutputStream();
							m.compress(Bitmap.CompressFormat.JPEG, MAX_QUALITY, bytes);
							m.recycle();
							File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
							try {
								f.createNewFile();
								FileOutputStream fo = new FileOutputStream(f);
								fo.write(bytes.toByteArray());
								fo.close();
								if(Build.VERSION.SDK_INT>=24){
										Uri fileUri = FileProvider.getUriForFile((Activity)v.getContext(),
												BuildConfig.APPLICATION_ID +
														".provider",f);
										intent.putExtra(Intent.EXTRA_STREAM,fileUri);
								}else {
									intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							intent.putExtra(Intent.EXTRA_TEXT, getDescription()+" "+Uri.parse(image));
						}
					} else {
						try {
							Bitmap m = BitmapFactory.decodeStream(v.getContext().getContentResolver().openInputStream(Uri.parse(image)));
							
							intent.setType("image/*");
							ByteArrayOutputStream bytes = new ByteArrayOutputStream();
							m.compress(Bitmap.CompressFormat.JPEG, MAX_QUALITY, bytes);
							m.recycle();
							File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
							try {
								f.createNewFile();
								FileOutputStream fo = new FileOutputStream(f);
								fo.write(bytes.toByteArray());
								fo.close();
								if(Build.VERSION.SDK_INT >= 24){
									intent.putExtra(Intent.EXTRA_STREAM,FileProvider.getUriForFile((Activity)v.getContext(),BuildConfig.APPLICATION_ID +
											".provider",f));
								}else {
									intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}	
				log("Share:", intent.getType()+":"+intent.getExtras());
				share(view.getContext(),  intent);
//				v.getContext().startActivity(Intent.createChooser(intent, "Share Ke..."));
			}
		});
	}
	
	public static void shareItem(Context ctx, final String text, String imageUrl){
		final Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT, ctx.getResources().getString(R.string.app_name));
		intent.putExtra(Intent.EXTRA_TEXT, text);
		final String image = imageUrl;
		if(image != null){
			if(image.startsWith("http")){
				File img = ImageLoaderApplication.getImageLoader().getFileManager().getFile(image);
//				File img = ImageLoader.getInstance().getDiscCache().get(image);
				if(img != null && img.exists()){
					Bitmap m = BitmapFactory.decodeFile(img.getAbsolutePath());
					
					intent.setType("image/*");
					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					m.compress(Bitmap.CompressFormat.JPEG, MAX_QUALITY, bytes);
					m.recycle();
					File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
					try {
						f.createNewFile();
						FileOutputStream fo = new FileOutputStream(f);
						fo.write(bytes.toByteArray());
						fo.close();
						if(Build.VERSION.SDK_INT>=24){
							intent.putExtra(Intent.EXTRA_STREAM,FileProvider.getUriForFile((Activity)ctx,BuildConfig.APPLICATION_ID+
							".provider",f));
						}else {
							intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					intent.putExtra(Intent.EXTRA_TEXT, text+" "+Uri.parse(image));
				}
			} else {
				try {
					Bitmap m = BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(Uri.parse(image)));
					
					intent.setType("image/*");
					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					m.compress(Bitmap.CompressFormat.JPEG, MAX_QUALITY, bytes);
					m.recycle();
					File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
					try {
						f.createNewFile();
						FileOutputStream fo = new FileOutputStream(f);
						fo.write(bytes.toByteArray());
						fo.close();
						if(Build.VERSION.SDK_INT >= 24){
							intent.putExtra(Intent.EXTRA_STREAM,FileProvider.getUriForFile((Activity)ctx,BuildConfig.APPLICATION_ID
							+".provider",f));
						}else {
							intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}	
		share(ctx,  intent);
//		v.getContext().startActivity(Intent.createChooser(intent, "Share Ke..."));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void setDefault(AdapterView list, List l) {
		if(list instanceof Spinner){
			list.setEnabled(false);
		} else {
			list.setAdapter(getEmptyAdapter(list, l));
		}
	}
	
	public void log(String tag, String msg){
		Log.v(tag, msg);
	}

	@SuppressWarnings("rawtypes")
	protected ArrayAdapter getEmptyAdapter(View v, List l) {
		return null;//new EmptyAdapter(v.getContext(), R.layout.empty);
	}

	protected void setDefault(ImageView iv) {}

	public HashMap<String, Object> getProperties() {
		return new HashMap<String, Object>();
	}
	
	public int getLayout(){
		return android.R.layout.simple_list_item_1;
	}

	public static final String tag_regex = "^\\?[1-9][0-9]*$";
	
	public int getLayout(Object tag){
		if(tag != null && tag.toString().matches(tag_regex))	return Integer.valueOf(tag.toString().substring(1));
		else return getLayout();
	}
	
	public abstract CharSequence getTitle();
	
	public ModelChannel merge(ModelChannel model){
		add(model.getFieldMap());
		return this;
	}
	
	public String getDescription() {
		return null;
	}
	
	public String getId(){
		return ""+getTitle().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if(getId() == null)	return super.equals(o);
		if(o instanceof ModelChannel)
			return getId().equals(((ModelChannel) o).getId());
		else if(o instanceof String)
			return o.equals(getId()) || o.equals(getTitle());
		else
			return false;
	}
		
	@Override
	public int hashCode() {
		if(getId() != null)
			return getId().hashCode();
		else return super.hashCode();
	}
	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater, Context ctx){}

	public void onImageCreated(Bitmap bm) {}
	
	public boolean onOptionsItemSelected(MenuItem item, View view) {return false;}
	
	public boolean createActionMode(ActionMode mode,Menu menu, Context ctx){return false;}

	public void add(Map<String, Object> map) {
		for(Field f:getFields(getClass())){
			Object obj = map.get(f.getName());
			if(obj != null){
				if(!(obj instanceof ModelChannel && updateModel((ModelChannel) obj, f))){
//					log("Model:ADD:"+getTitle(), f.getName()+":"+obj.toString());
					try {
						f.setAccessible(true);
						f.set(this, obj);
					} catch (Exception e) {
//						log("Locs:"+f.getName(), f.getType()+":"+obj.getClass());
					}
				}
			}
		}
	}
	
	protected boolean updateModel(ModelChannel obj, Field f) {
		try {
			if(obj.isPersistance()){
				Object o = f.get(this);
				if(o != null && o instanceof ModelChannel) {
					ModelChannel m = ((ModelChannel) o).merge(obj);
					if(m != null){				
						m.updateView();
					}
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	protected boolean isPersistance() {
		return false;
	}

	public HashMap<String, Object> getItem(View v){
		HashMap<String, List<View>> views = Utility.getViewsID(v);
		HashMap<String, Object> result = new HashMap<String, Object>();
		for(Entry<String, List<View>> i:views.entrySet()){
			for(View vi:i.getValue()){
				Object item = extract(vi);
				if(item != null)	result.put(i.getKey(), item);
			}
		}
		return result;		
	}
	
	protected Object extract(View v) {
		if(v instanceof EditText){
			return ((EditText)v).getText().toString();
		} else if(v instanceof Spinner){
			Object obj = ((Spinner)v).getSelectedItem();
			if(obj instanceof ModelChannel)
				return ((ModelChannel) obj).getId();
			else {
				return obj.toString();
			}
		} else if(v instanceof RadioButton){
			ViewGroup parent = ((ViewGroup)v.getParent());
			for(int x=0; x< parent.getChildCount(); ++x){
				View child = parent.getChildAt(x);
				if(child instanceof RadioButton){
					if(((RadioButton) child).isChecked())	return x+1;
				}
			}
		} else if(v instanceof TimePicker){
			GregorianCalendar g = new GregorianCalendar();
			g.set(GregorianCalendar.HOUR_OF_DAY, ((TimePicker) v).getCurrentHour());
			g.set(GregorianCalendar.MINUTE, ((TimePicker) v).getCurrentMinute());
			return g;
		} else if(v instanceof DatePicker){
			GregorianCalendar g = new GregorianCalendar();
			g.set(GregorianCalendar.DAY_OF_MONTH, ((DatePicker) v).getDayOfMonth());
			g.set(GregorianCalendar.MONTH, ((DatePicker) v).getMonth());
			g.set(GregorianCalendar.YEAR, ((DatePicker) v).getYear());
			return g;
		}
		return null;
	}

	@Override
	public int compareTo(ModelChannel another) {
		return getTitle().toString().compareTo(another.getTitle().toString());
	}
	
	public static <E extends ModelChannel> ArrayAdapter<E> getAdapter(Context ctx,final List<E> models) {
		return getAdapter(ctx, models, null, false);
	}
	
	public static <E extends ModelChannel> ArrayAdapter<E> getAdapter(Context ctx, ViewChanger listener,final List<E> models) {
		return getAdapter(ctx, models, listener, false);
	}
	
	public static <E extends ModelChannel> ArrayAdapter<E> getAdapter(Context ctx, final int layout,final List<E> models) {
		return getAdapter(ctx, layout, models, null, false);
	}
	
	public static <E extends ModelChannel> ArrayAdapter<E> getAdapter(Context ctx, final int layout, final ViewChanger listener,final List<E> models) {
		return getAdapter(ctx, layout, models, listener, false);
	}

	public static <E extends ModelChannel> ArrayAdapter<E> getAdapter(Context ctx, final List<E> models, final ViewChanger listener, final boolean isInfinite) {
		ArrayAdapter<E> result = new ArrayAdapter<E>(ctx, android.R.layout.simple_list_item_1, models){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if(models.isEmpty())	return convertView;
				E model = models.get(position%models.size());
				if(model.getLayout() > 0){
					convertView = model.getListView(convertView, model.getLayout(), position, parent);
					if(listener != null)	listener.getData(convertView, position);
				}else {
					convertView = super.getView(position, convertView, parent);
				}
				return convertView;
			}
			
			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent) {
				return getView(position, convertView, parent);
			}

			@Override
			public int getCount() {
				if(!isInfinite)
					return models.size();
				if(models.isEmpty())	return 0;
				return Integer.MAX_VALUE;
			}

			@Override
			public E getItem(int position) {
				if(models.isEmpty())	return null;
				return super.getItem(position%models.size());
			}

			@Override
			public long getItemId(int position) {
				if(models.isEmpty())	return 0;
				return getItem(position%models.size()).hashCode();
			}
        };
        if(models instanceof ListConnector)	((ListConnector<E>)models).setAdapter(result);
        return result;
	}
	
	public static <E extends ModelChannel> ArrayAdapter<E> getAdapter(Context ctx, final int layout, final List<E> models, final ViewChanger listener, final boolean isInfinite) {
		ArrayAdapter<E> result = new ArrayAdapter<E>(ctx, android.R.layout.simple_list_item_1, models){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if(models.isEmpty())	return convertView;
				if(layout > 0){
					convertView = models.get(position%models.size()).getListView(convertView, layout, position, parent);
					if(listener != null)	listener.getData(convertView, position);
					return convertView;
				} 
				return	super.getView(position, convertView, parent);
			}

			@Override
			public int getCount() {
				if(!isInfinite)
					return models.size();
				if(models.isEmpty())	return 0;
				return Integer.MAX_VALUE;
			}

			@Override
			public E getItem(int position) {
				if(models.isEmpty())	return null;
				return super.getItem(position%models.size());
			}

			@Override
			public long getItemId(int position) {
				if(models.isEmpty())	return 0;
				return getItem(position%models.size()).hashCode();
			}
        };
        if(models instanceof ListConnector)	((ListConnector<E>)models).setAdapter(result);
        return result;
	}
	
	public static final Map<Class<?>, List<Field>> fieldCache = new HashMap<Class<?>, List<Field>>();
	
	public static List<Field> getFields(Class<?> tmpClass){
	    List<Field> fieldList = fieldCache.get(tmpClass);
	    if(fieldList != null)	return fieldList;
	    fieldList = new ArrayList<Field>();
	    while (tmpClass != null) {
	    	for(Field f:tmpClass.getDeclaredFields()){
				if(!Modifier.isStatic(f.getModifiers()) && !Modifier.isTransient(f.getModifiers())) fieldList.add(f);
	    	}
	        tmpClass = tmpClass.getSuperclass();
	    }
		return fieldList;
	}
	
	public static void setView(View view, int id, Object item){
		setView(view, id, item, null);
	}

	@SuppressWarnings({ "rawtypes", "deprecation" })
	public static void setView(View view, int id, Object item, final ModelChannel m){
		View v = view;
		if(view != null && id != 0)	v = view.findViewById(id);
		if(v != null)	{
			if(m != null) m.setUpView(v);
			v.setVisibility(View.VISIBLE);
			
			if(item.getClass().isArray()){
				item = Arrays.asList((Object[])item);
			}
			
			if(item != null && item instanceof Boolean){
				if(v instanceof RadioButton){
					((RadioButton) v).setChecked((Boolean) item);
				} else {
//					if(!((Boolean) item))	v.setVisibility(View.GONE);
//					if(prevVis == View.VISIBLE && v.getVisibility() == View.GONE){
//						Utility.toogleAnimation(v, false);
//					} else if(prevVis == View.GONE && v.getVisibility() == View.VISIBLE){
//						Utility.toogleAnimation(v, true);
//					}
					v.setVisibility(((Boolean) item)?View.VISIBLE:View.GONE);
				}
			} else if(item != null && item instanceof ModelChannel){
				final ModelChannel  m1 = (ModelChannel) item;
				final View  v1 = v;
				v.postDelayed(new Runnable() {
					@Override
					public void run() {
						m1.getData(v1, -1);
					}
				}, 30);
			}  else if(item != null && item instanceof Map){
				final Map  m1 = (Map) item;
				final View  v1 = v;
				v.postDelayed(new Runnable() {
					@Override
					public void run() {
						setData(v1, m1, m);
					}
				}, 30);
			} else if(item != null && item instanceof Fragment && v.getContext() instanceof FragmentActivity){
				// Attach to the activity
				FragmentTransaction t = ((FragmentActivity) v.getContext()).getSupportFragmentManager().beginTransaction();
				t.replace(v.getId(), (Fragment) item);
				t.commit();
			} else if(v instanceof ProgressBar){
				ProgressBar pb = (ProgressBar) v;
				if(item != null && item instanceof Integer){
					pb.setProgress((Integer) item);
				}
			} else if(item == null || (item.toString().length() == 0 && !(v instanceof EditText)) || (item instanceof Integer && ((Integer) item) <= 0) && !(v instanceof TextView))	{
				v.setVisibility(View.GONE);
			} else if(v instanceof TextView){
				TextView text = (TextView) v;
				if(item != null){
					if(item instanceof CharSequence){
						text.setText((CharSequence) item);
					} else {
						text.setText(item.toString());
					}
				}
			} else if(v instanceof ImageView){
				ImageView iv = (ImageView) v;
				iv.setVisibility(View.VISIBLE);
				if(item.equals(iv.getTag(R.id.image)))	return;
				if(m != null)	{
					m.setDefault(iv);
					if(m.hasPhoto(iv.getId())){
						m.setPhotoView(iv);
					}
				}
				if(item instanceof String){
//					log("Model:Image:", Utility.getFileName(item.toString())+":"+item);
//					try{
////						ImageLoader.getInstance().displayImage(item.toString(), iv, new LoadListener(Utility.getFileName(item.toString())));
//						ImageLoader.getInstance().displayImage(item.toString(), iv, m!=null?m:null);
//					}catch(Exception e){}
					ImageTagFactory imageTagFactory = ImageTagFactory.newInstance(v.getContext(), R.drawable.default_product);
			        imageTagFactory.setErrorImageId(R.drawable.default_product);
			       
			        v.setTag(imageTagFactory.build(item.toString(), v.getContext()));
					
					ImageLoaderApplication.getImageLoader().getLoader().load(((ImageView)v));
				} else if(item instanceof Integer){
					iv.setImageResource((Integer) item);
				} else if(item instanceof File){
					if(((File) item).isFile() && ((File) item).exists()){
						ImageTagFactory imageTagFactory = ImageTagFactory.newInstance(v.getContext(), R.drawable.default_product);
				        imageTagFactory.setErrorImageId(R.drawable.default_product);
				       
				        v.setTag(imageTagFactory.build(item.toString(), v.getContext()));
						
						ImageLoaderApplication.getImageLoader().getLoader().load(((ImageView)v));
					}
					else iv.setVisibility(View.GONE);
				} else if(item instanceof Bitmap){
					iv.setImageBitmap((Bitmap) item);
				} if(item instanceof Drawable){
					iv.setImageDrawable((Drawable) item);
				}
				iv.setTag(R.id.image, item);
			} else if(v instanceof ExpandableListView){
				ExpandableListAdapter temp = ((ExpandableListView) v).getExpandableListAdapter();
				if(temp != null && v.getTag(R.id.list) == item && temp instanceof BaseExpandableListAdapter){
					((BaseExpandableListAdapter) temp).notifyDataSetChanged();
				} else {
					if(item instanceof List){
						List l = (List) item;
						if(m != null)	m.setExpandableList((ExpandableListView) v, l);
					}
				}
			} else if(v instanceof AdapterView){
				AdapterView list = (AdapterView) v;
				if(item instanceof List){
					List l = (List) item;
					if(m != null)	m.setList(list, l);
				}
			} else if(v instanceof ViewPager){
				ViewPager pager = (ViewPager) v;
				if(item != null && item instanceof List){
					List l = (List) item;
					if(m != null)	m.setPager(pager, l);
				}
			} else if(v instanceof ViewGroup){
				ViewGroup list = (ViewGroup) v;
				if(item instanceof List){
					List l = (List) item;
					if(m != null)	m.setList(list, l);
				} else if(item instanceof Bitmap){
					list.setBackgroundDrawable(new BitmapDrawable(list.getResources(), (Bitmap) item));
				} else if(item instanceof Drawable){
					list.setBackgroundDrawable((Drawable) item);
				}
			}
			if(m != null) m.finishUpView(v);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void setData(View v, Map m1, ModelChannel m) {
		HashMap<String, List<View>> result = (HashMap<String, List<View>>) v.getTag(R.id.views);
		if(result == null) {
			result = Utility.getViewsID(v);
			v.setTag(R.id.views, result);
		}
		for(Entry<String, List<View>> view:result.entrySet()){
			Object value = m1.get(view.getKey());
			for(View vi:view.getValue()){
				if(value != null)	setView(vi, vi.getId(), value, m);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	protected void setList(ViewGroup list, List l) {
		ArrayAdapter adapter = null;
		list.setEnabled(true);
		
		if(l.isEmpty()){
			list.setTag(R.id.list, null);
			adapter = getEmptyAdapter(list, l);
		} else {
			adapter = getAdapter(list, l);
		}
		for(int x=0; x < adapter.getCount(); ++x){
			View old = list.getChildAt(x);
			View v = adapter.getView(x, old, list);
			if(v != old){
				if(old != null)	list.removeViewAt(x);
				list.addView(v, x);
			}
		}
		for(int x=adapter.getCount(); x < list.getChildCount(); ++x){
			list.removeViewAt(x);
		}
	}

	protected void finishUpView(View v) {}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void setExpandableList(ExpandableListView v, Object obj) {
		if(obj instanceof List){
			List l = (List) obj;
			if(l.isEmpty()){
				v.setAdapter((ExpandableListAdapter) null);
				return;
			}
			v.setTag(R.id.list, obj);
			Object i = l.get(0);
			if(i instanceof ModelChannel){
				ModelExpandableAdapter<ModelChannel> adapter = null;
				int layout = getLayoutFromTag(v);
				if(layout > 0)
					adapter = new ModelExpandableAdapter<ModelChannel>(v.getContext(), l, -1, layout);
				else adapter = new ModelExpandableAdapter<ModelChannel>(v.getContext(), l);
				v.setAdapter(adapter);
				if(!isFixedGroup(v.getId())) {
					adapter.setListener(new OnGroupClickListener() {
						@Override
						public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
							if(!parent.expandGroup(groupPosition))	parent.collapseGroup(groupPosition);
							return true;
						}
					});
				}
				for(int x = 0; x < adapter.getGroupCount(); ++x)
					v.expandGroup(x);
			}
		}
		if(obj instanceof Map){
			try {
				Map<Object, List<Object>> map = (Map<Object, List<Object>>) obj;
				SimpleExpandableAdapter adapter = null;
				int layout = getLayoutFromTag(v);
				if(layout > 0)
					adapter = new SimpleExpandableAdapter<Object, Object>(v.getContext(), map, -1, layout);
				else adapter = new SimpleExpandableAdapter<Object, Object>(v.getContext(), map);
				
				v.setAdapter(adapter);
				if(!isFixedGroup(v.getId())) {
					adapter.setListener(new OnGroupClickListener() {
						@Override
						public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
							if(!parent.expandGroup(groupPosition))	parent.collapseGroup(groupPosition);
							return true;
						}
					});
				} else {
					for(int x = 0; x < adapter.getGroupCount(); ++x)
						v.expandGroup(x);
				}
			}catch(Exception e){}
		}
//		for(int x=0;x < v.getCount(); ++x)
//			v.expandGroup(x);
//		adapter = new SimpleExpandableAdapter<A, B>(ctx, data, groupLayout, childLayout)
	}

	public boolean isFixedGroup(int id) {
		return false;
	}

	@SuppressWarnings("rawtypes")
	public List getChild() {
		return new ArrayList<String>();
	}

	public boolean hasPhoto(int id) {
		return false;
	}

	protected void setUpView(View v) {}
	
	public void end(View v){
		log("Finish:", "true");
		v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				log("Finish:", "onClick");
				((Activity) v.getContext()).finish();
			}
		});
	}

	protected void setPhotoView(final ImageView iv) {
		iv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Photo p = getPhoto(iv.getId());
				if(p == null) return;
				List<Photo> photos = getPhotoList(iv);
				Bundle b = null;
				log("Photo:", photos+":"+p);
				if(photos != null)	{
					int index = photos.indexOf(p);
					b = PhotoViewFragment.init(v.getContext(),index>0?index:0, photos.toArray(new Photo[0])).getArguments();
				}
				else b = PhotoViewFragment.init(v.getContext(), 0, p).getArguments();
				
				b.putInt(SingleModeActivity.THEME, R.style.Theme_AppCompat);
				SingleModeActivity.initFragment(v.getContext(), b,PhotoViewFragment.class);
			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Photo> getPhotoList(ImageView iv) {
		View parent = Utility.getParentWithTag(iv, R.id.list);
		if(parent != null){
			List l = (List) parent.getTag(R.id.list);
			if(!l.isEmpty()){
				Object o = l.get(0);
				if(getClass().isInstance(o))	{
					List<Photo> p = new ArrayList<Photo>();
					for(ModelChannel m:(List<ModelChannel>) l){
						Photo photo = m.getPhoto(iv.getId());
						if(photo != null)	p.add(photo);
					}
					return p;
				}
			}
		}
		return null;
	}

	public PhotoViewFragment.Photo getPhoto(int id){
		return null;
	}
	
	public void onClose(){}
	
	public static boolean isEmpty(CharSequence item){
		return item == null || item.length() ==0;
	}
	
	public static  boolean isEmpty(Object[] item) {
		return item==null?true:item.length==0;
	}
	

	public static  <T> boolean isEmpty(Collection<T> item) {
		return item==null?true:item.size()==0;
	}
	
	public static ModelChannel getModel(View v){
		Pair<ModelChannel, Integer> r = modelViewMap.get(v);
		if(r == null)	return null;
		else return r.first;
	}
	
	
	public boolean hasMenu(int layout){return true;}

	public boolean validate(Context ctx, HashMap<String, Object> item, List<Pair<String, String>> notEmpty) {
		return validate(ctx, item, notEmpty, null, null);
	}
	
	public transient ReturnInterface<ModelChannel> onResultSend;
	
	protected void sendResult(Context ctx, boolean isSuccess){
		if(onResultSend != null)	{
			onResultSend.get(this);
			onResultSend = null;
		}
		if(isSuccess && ctx instanceof Activity){
//			log("OnReceive:Send:", getClass().getName()+":"+hashCode());
//			SimpleReceiver.broadcast(ctx, getClass().getName()+":"+hashCode(), this);
			((Activity) ctx).finish();
		}
	}
	
	public static <T extends ModelChannel> List<T> dummy(Class<T> cls, int size){
		ArrayList<T> result = new ArrayList<T>();
		try{
			for(int x =0; x < size;++x){
				result.add(cls.newInstance());
			}
		}catch(Exception e){}
		return result;
	}
	
	public void setUpDialog(Context ctx, Dialog dialog){}
	
	public int getFrame(){return 0;}
	
	public PopupWindow getPopup(Context ctx){
		View item = LayoutInflater.from(ctx).inflate(getLayout(), null);
		getData(item, -1);
		
		PopupWindow window = new PopupWindow(item, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		window.setBackgroundDrawable(new BitmapDrawable());
		return window;
	}


	public PopupWindow getPopup(Context ctx, int width, int height){
		View item = LayoutInflater.from(ctx).inflate(getLayout(), null);
		getData(item, -1);

		PopupWindow window = new PopupWindow(item, width, height, true);
		window.setBackgroundDrawable(new BitmapDrawable());
		return window;
	}
	

	public PopupWindow getPopup(Context ctx, int layout, int width, int height){
		View item = LayoutInflater.from(ctx).inflate(layout, null);
		getData(item, -1);
		
		PopupWindow window = new PopupWindow(item, width, height, true);
		window.setBackgroundDrawable(new BitmapDrawable());
		return window;
	}
	
	public int getTheme() {
		return 0;
	}
}
