package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.OnNavigationListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ShoppingCartAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.CartItem;
import com.raihangroup.raihanshop.model.UserRsvAdmin;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCartActivity extends AppCompatActivity implements OnNavigationListener {

    private static final String TAG = ShoppingCartActivity.class.getSimpleName();
    private AQuery aq;
    private ArrayList<CartItem> listOfCartItems;
    private ListView listview;
    protected ShoppingCartAdapter adapter;
    private ProgressDialog progDialog;
    protected List<UserRsvAdmin> uRsv;
    private int idChoose;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);

        aq = new AQuery(this);
        listOfCartItems = new ArrayList<CartItem>();

        toolbar = (Toolbar) findViewById(R.id.tb_shop_cart);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
        ab.setDisplayHomeAsUpEnabled(true);

        listview = (ListView) findViewById(R.id.listview);
        listview.setOnItemClickListener(null); // no item click on listview

        PrefStorage.newInstance(this);

        viewVisibilityInProgressState();
        if (Utility.isAdmin()) {
            aq.id(R.id.llReferalIsi).visible();

            Map<String, String> params_a = new HashMap<String, String>();
            params_a.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));

            progDialog = new ProgressDialog(this);
            progDialog.setTitle("Mengambil Daftar Pengguna");
            progDialog.setMessage("Mohon Tunggu");
            progDialog.setCancelable(false);

            aq.progress(progDialog).ajax(Constants.URL_ADMGETRESERVED_API, params_a, JSONArray.class, new AjaxCallback<JSONArray>() {
                @Override
                public void callback(String url, JSONArray object, AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        Gson gs = new Gson();
                        uRsv = gs.fromJson(object.toString(),
                                new TypeToken<List<UserRsvAdmin>>() {
                                }.getType());
                        supportInvalidateOptionsMenu();
                    }
                }
            });
        } else {
            loadCart(PrefStorage.instance.getUserId());
        }

        aq.id(R.id.btnCariOrang).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aq.id(R.id.llReferal).visible();
                aq.id(R.id.pgRefer).visible();
                aq.id(R.id.tvNamaref).gone();
                aq.id(R.id.tvLvlref).gone();

                Map<String, String> params = new HashMap<String, String>();
                params.put("reseller_id", aq.id(R.id.etRReferNumber).getText().toString());

                aq.progress(R.id.pgRefer).ajax(Constants.URL_REFERAL_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object,
                                         AjaxStatus status) {
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                            try {
                                aq.id(R.id.tvNamaref).text(object.getString("referal_name")).visible();
                                aq.id(R.id.tvLvlref).text(object.getString("referal_level")).visible();
                            } catch (JSONException e) {
                                Log.d("exception", String.valueOf(e));
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.booking_list, menu);

        if (uRsv != null) menu.findItem(R.id.menu_spinner).setVisible(true);

        if (menu.findItem(R.id.menu_spinner).isVisible()) {
            Spinner sp = (Spinner) MenuItemCompat.getActionView(menu.findItem(R.id.menu_spinner));

            ArrayAdapter<UserRsvAdmin> mSpinnerAdapter =
                    new ArrayAdapter<UserRsvAdmin>(this, R.layout.spinnercustombar, uRsv);

            sp.setAdapter(mSpinnerAdapter); // set the adapter
            sp.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    UserRsvAdmin ua = (UserRsvAdmin) parent.getItemAtPosition(position);
                    idChoose = ua.getId_member();
                    loadCart(idChoose);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion < android.os.Build.VERSION_CODES.HONEYCOMB) {
                sp.setBackgroundResource(R.drawable.spinnersupport_d);
                mSpinnerAdapter.setDropDownViewResource(R.layout.spinnercustombar_black);
            }
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getTitle().equals("Lanjut")) {
            if (listOfCartItems.isEmpty()) {
                Utility.toast(this, "Keranjang belanja kosong");
                return true;
            }
            // lock the cart
            HashMap<String, Object> params = new HashMap<>();
            params.put("id_member", PrefStorage.instance.getUserId());
            aq.ajax(Constants.URL_LOCK_CART, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    super.callback(url, object, status);
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        Intent intent = new Intent(ShoppingCartActivity.this, CartBookingActivity.class);
                        Gson gs = new Gson();
                        String json = gs.toJson(listOfCartItems);
                        intent.putExtra(Constants.INTENT_CART_2_BOOK, json);
                        Log.i(TAG, "callback: ");

                        if (Utility.isAdmin()) {
                            intent.putExtra(Constants.INTENT_CART_ADMIN_RESELLERID, idChoose);
                        }

                        startActivity(intent);
                    } else {
                        DialogHelper.showMessageDialog(ShoppingCartActivity.this, "Terjadi Kesalahan", "Tidak berhasil melakukan booking. Silakan mencoba kembali",
                                "OK", null);
                    }
                }
            });

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {

            for (int j = 0; j < adapter.getCount(); j++) {
                CartItem ci = adapter.getItem(j);
                if (ci.getId() == data.getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, -1)) {
                    ci.setColor(data.getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL));
                    ci.setQty(data.getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, 0));
                    ci.setSize(data.getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ));
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onNavigationItemSelected(int itemPos, long idItem) {
        return true;
    }

    // support methods

    private void loadCart(int userId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("reseller_id", String.valueOf(userId));

        if (Utility.isAdmin()) {
            params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
            viewVisibilityInProgressState();
            if (listOfCartItems != null) listOfCartItems.clear();
        }

        aq.progress(R.id.pg).ajax(Constants.URL_GETCART_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                viewVisibilityInContentState();
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    listOfCartItems = gs.fromJson(object.toString(),
                            new TypeToken<List<CartItem>>() {
                            }.getType());
                    adapter = new ShoppingCartAdapter(ShoppingCartActivity.this, R.layout.cart_item, listOfCartItems);

                    listview.setAdapter(adapter);

                    PrefStorage.instance.resetCart();
                    PrefStorage.instance.changeCart(listOfCartItems.size());
                    if (listOfCartItems.size() == 0) {
                        viewVisibilityInInfoState();
                    }
                } else {
                    viewVisibilityInInfoState();
                }
            }
        });
    }

    private void viewVisibilityInContentState() {
        aq.id(R.id.iv_step).visible();
        aq.id(R.id.pg).gone();
        aq.id(R.id.llInfo).gone();
    }

    private void viewVisibilityInProgressState() {
        aq.id(R.id.iv_step).gone();
        aq.id(R.id.pg).visible();
        aq.id(R.id.llInfo).gone();
    }

    private void viewVisibilityInInfoState() {
        aq.id(R.id.iv_step).gone();
        aq.id(R.id.pg).gone();
        aq.id(R.id.llInfo).visible();
    }
}
