package com.raihangroup.raihanshop.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.PerformanceViewPagerAdapter;
import com.raihangroup.raihanshop.fragments.PieChartPerformanceFragment;
import com.raihangroup.raihanshop.helper.DialogHelper;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PerformanceChartActivity extends AppCompatActivity {

    public static String[] TITLE = new String[]{"Taruna", "Prajurit 1", "Prajurit 2", "Prajurit 3",
            "Prajurit 4", "Komandan 1", "Komandan 2", "Komandan 3", "Komandan 4", "Kapten 1", "Kapten 2",
            "Kapten 3", "Kapten 4", "Mayor 1", "Mayor 2", "Mayor 3",
            "Mayor 4", "Jenderal 1", "Jenderal 2", "Jenderal 3", "Jenderal 4", "Panglima 1",
            "Panglima 2", "Panglima 3", "Panglima 4", "Gubernur Muda 1", "Gubernur Muda 2",
            "Gubernur Muda 3", "Gubernur Muda 4", "Gubernur Besar 1",
            "Gubernur Besar 2", "Gubernur Besar 3", "Gubernur Besar 4",
            "Menteri 1", "Menteri 2", "Menteri 3", "Menteri 4", "Presiden"};

    public static String[] SUBTITLE = new String[]{"Daftar/Registrasi", "PS 15 VOL + PT 10 VOL", "PS 15 VOL + PT 25 VOL",
            "PS 15 VOL + PT 40 VOL", "PS 15 VOL + PT 75 VOL", "PS 10 VOL + PT 100 VOL + 2 PRAJURIT",
            "PS 10 VOL + PT 150 VOL + 4 PRAJURIT", "PS 10 VOL + PT 200 VOL + 6 PRAJURIT", "PS 10 VOL + PT 250 VOL + 8 PRAJURIT",
            "PS 10 VOL + PT 300 VOL + 2 KOMANDAN", "PS 10 VOL + PT 350 VOL + 3 KOMANDAN", "PS 10 VOL + PT 400 VOL + 4 KOMANDAN",
            "PS 10 VOL + PT 450 VOL + 5 KOMANDAN", "PS 10 VOL + PT 500 + 2 KAPTEN", "PS 10 VOL + PT 500 + 3 KAPTEN",
            "PS 10 VOL + PT 500 + 4 KAPTEN", "PS 10 VOL + PT 500 + 5 KAPTEN", "PS 5 VOL + PT 500  + 2 MAYOR",
            "PS 5 VOL + PT 500 + 3 MAYOR", "PS 5 VOL + PT 500 + 4 MAYOR", "PS 5 VOL + PT 500 + 5 MAYOR",
            "PS 5 VOL + PT 500  + 2 JENDRAL", "PS 5 VOL + PT 500  + 3 JENDRAL", "PS 5 VOL + PT 500  + 4 JENDRAL",
            "PS 5 VOL + PT 500  + 5 JENDRAL", "PT 500  + 2 PANGLIMA", "PT 500 + 3 PANGLIMA", "PT 500  + 4 PANGLIMA",
            "PT 500  + 5 PANGLIMA", "PT 500  + 2 GUBERNUR MUDA", "PT 500 + 3 GUBERNUR MUDA", "PT 500  + 4 GUBERNUR MUDA",
            "PT 500  + 5 GUBERNUR MUDA", "PT 500  + 2 GUBERNUR BESAR", "PT 500 + 3 GUBERNUR BESAR", "PT 500  + 4 GUBERNUR BESAR",
            "PT 500  + 5 GUBERNUR BESAR", "3 MENTERI"
    };

    public static String KOMISI[] = new String[]{"KOMISI + FEE", "KOMISI + FEE", "KOMISI + FEE", "KOMISI + FEE", "KOMISI + FEE", "KOMISI + SALARY 1.000.000", "KOMISI + SALARY 1.300.000",
            "KOMISI + SALARY 1.700.000", "KOMISI + SALARY 2.000.000", "KOMISI + SALARY 3.000.000", "KOMISI + SALARY 3.500.000",
            "KOMISI + SALARY 4.000.000", "KOMISI + SALARY 4.500.000", "KOMISI + SALARY 6.000.000", "KOMISI + SALARY 6.500.000", "KOMISI + SALARY 7.000.000",
            "KOMISI + SALARY 7.500.000", "KOMISI + SALARY 8.000.000", "KOMISI + SALARY 8.500.000", "KOMISI + SALARY 9.000.000", "KOMISI + SALARY 9.500.000",
            "KOMISI + SALARY 12.000.000", "KOMISI + SALARY 13.000.000", "KOMISI + SALARY 14.000.000", "KOMISI + SALARY 15.000.000",
            "SALARY 20.000.000", "SALARY 25.000.000", "SALARY 30.000.000", "SALARY 35.000.000", "SALARY 50.000.000", "SALARY 75.000.000",
            "SALARY 100.000.000", "SALARY 150.000.000", "SALARY 200.000.000", "SALARY 225.000.000", "SALARY 250.000.000", "SALARY 300.000.000",
            "SALARY 500.000.000"};
    int year;
    int month;
    @BindView(R.id.tabLayout_performance)
    TabLayout tabLayoutPerformance;
    @BindView(R.id.viewpage_performance)
    ViewPager viewpagePerformance;
    Fragment pieChartPerformanceFragment;
    @BindView(R.id.toolbar_performance)
    Toolbar toolbarPerformance;
    PerformanceViewPagerAdapter adapter;
    Bundle b;
    Context context;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_chart);
        ButterKnife.bind(this);
        context = this;
        pieChartPerformanceFragment = new PieChartPerformanceFragment();
        setSupportActionBar(toolbarPerformance);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Selling Performance");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        b = getIntent().getExtras();
        DialogHelper.showMessageDialog(context, "Peringatan", "Mohon Maaf Fitur ini masih under-development", "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        setupViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.performance_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_info:
                Intent performanceinfoIntent = new Intent(this, PerformanceListInfoActivity.class);
                startActivity(performanceinfoIntent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager() {
        List<String> bulanList = Arrays.asList("Jan","Feb","Mar","Apr","Mei","Jun","Jul",
                "Agt","Sep","Okt","Nov","Des");
        adapter = new PerformanceViewPagerAdapter(getSupportFragmentManager(),bulanList,b.getInt("userId"));
        viewpagePerformance.setAdapter(adapter);
        tabLayoutPerformance.setupWithViewPager(viewpagePerformance);
    }
}
