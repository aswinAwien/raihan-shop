package com.raihangroup.raihanshop.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.BankAdapter;
import com.raihangroup.raihanshop.adapter.CityProvAdapter;
import com.raihangroup.raihanshop.adapter.CitySpinAdapter;
import com.raihangroup.raihanshop.adapter.ProvinceAdapter;
import com.raihangroup.raihanshop.adapter.ProvinceSpinAdapter;
import com.raihangroup.raihanshop.adapter.SubDistrictCityAdapter;
import com.raihangroup.raihanshop.adapter.SubDistrictSpinAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.City;
import com.raihangroup.raihanshop.model.Province;
import com.raihangroup.raihanshop.model.SubDistrict;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends FragmentActivity {

    @BindView(R.id.textInputLayoutEtRPass)
    TextInputLayout textInputLayoutEtRPass;
    @BindView(R.id.textInputLayoutEtRPassConf)
    TextInputLayout textInputLayoutEtRPassConf;
    private Context context;
    private static AQuery aq;
    private ProgressDialog progDialog;
    private DBHelper dbh;
    private ProvinceAdapter provACTVAdapter;
    private CityProvAdapter cityACTVAdapter;
    private SubDistrictCityAdapter subdistrictACTVAdapter;
    private ProvinceSpinAdapter provinceSpinAdapter;
    private CitySpinAdapter citySpinAdapter;
    private SubDistrictSpinAdapter subDistrictSpinAdapter;
    private final static int SPINNER_INIT = 0;
    private final static int SPINNER_REFRESH_AGAIN = 1;
    private final static int SPINNER_FETCHING_DATA = 2;
    private final static String TAG = "Register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        context = this;
        PrefStorage.newInstance(this);
        aq = new AQuery(this);
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setMessage("Mendaftarkan akun baru");
        progDialog.setCancelable(false);

        ((RadioGroup) aq.id(R.id.rgChannel).getView()).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rMTrue) {
                    aq.id(R.id.llMchannel).visible();
                } else {
                    aq.id(R.id.llMchannel).gone();
                }
            }
        });
        ((RadioGroup) aq.id(R.id.rgRefer).getView()).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rRfTrue) {
                    aq.id(R.id.llReferalIsi).visible();
                } else {
                    aq.id(R.id.llReferalIsi).gone();
                    aq.id(R.id.llReferal).gone();
                }
            }
        });
        aq.id(R.id.cbRFb).getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                aq.id(R.id.etRfb).visibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        aq.id(R.id.cbRTw).getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                aq.id(R.id.etRTw).visibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        aq.id(R.id.cbRBlogWeb).getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                aq.id(R.id.etRBlogWeb).visibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        // for autocomplete
        dbh = new DBHelper(this);
        cityACTVAdapter = new CityProvAdapter(this, dbh.initCursor("dor"));
        subdistrictACTVAdapter = new SubDistrictCityAdapter(this,dbh.initCursor("dor"));
        provACTVAdapter = new ProvinceAdapter(this, dbh.initCursor("duer"));

        provinceSpinAdapter = new ProvinceSpinAdapter(this, new ArrayList<Province>(), false);
        provinceSpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clearProvinceSpinAdapter(SPINNER_INIT);
        aq.id(R.id.spinProvince).adapter(provinceSpinAdapter);
        ((Spinner) aq.id(R.id.spinProvince).getView()).setOnItemSelectedListener(new ProvinceSpinListener());

        citySpinAdapter = new CitySpinAdapter(this, new ArrayList<City>(), false);
        citySpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clearCitySpinAdapter(SPINNER_INIT);
        aq.id(R.id.spinCity).adapter(citySpinAdapter);
        ((Spinner) aq.id(R.id.spinCity).getView()).setOnItemSelectedListener(new CitySpinListener());

        subDistrictSpinAdapter = new SubDistrictSpinAdapter(this,new ArrayList<SubDistrict>(),false);
        subDistrictSpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clearSubDistrictAdapter(SPINNER_INIT);
        aq.id(R.id.spinSubDistrict).adapter(subDistrictSpinAdapter);

        // start from getting provinces
        getProvinces();
        ((AutoCompleteTextView) findViewById(R.id.etRProvince)).setAdapter(provACTVAdapter);
        ((AutoCompleteTextView) findViewById(R.id.etRProvince)).setOnItemClickListener(new ProvinceACTVListener());
        ((AutoCompleteTextView) findViewById(R.id.etRCity)).setAdapter(cityACTVAdapter);
        ((AutoCompleteTextView) findViewById(R.id.etRCity)).setOnItemClickListener(new CityACTVListener());
        ((AutoCompleteTextView) findViewById(R.id.etRSubDistrict)).setAdapter(subdistrictACTVAdapter);
        ((AutoCompleteTextView) findViewById(R.id.etRSubDistrict)).setOnItemClickListener(new SubDistrictACTVListener());
        BankAdapter ba = new BankAdapter(this, dbh.findBanks("Bank"), aq.id(R.id.etRBank).getEditText());
        ((AutoCompleteTextView) findViewById(R.id.etRBank)).setAdapter(ba);
        ((AutoCompleteTextView) findViewById(R.id.etRBank)).setOnItemClickListener(ba);
        CheckBox cb = aq.id(R.id.cbSyarat).getCheckBox();
        cb.setText(Html.fromHtml(
                "Saya telah membaca dan menyetujui <a href=\"http://raihanshop.com/terms.pdf\">syarat dan ketentuan</a> yang berlaku"
        ));
        cb.setMovementMethod(LinkMovementMethod.getInstance());
    }

    static int y = 0;
    static int m;
    static int d;

    @SuppressLint("ValidFragment")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if (y == 0) {
                final Calendar c = Calendar.getInstance();
                y = c.get(Calendar.YEAR);
                m = c.get(Calendar.MONTH);
                d = c.get(Calendar.DAY_OF_MONTH);
            }
            return new DatePickerDialog(getActivity(), this, y, m, d);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            y = year;
            m = month;
            d = day;
            aq.id(R.id.etLahTanggal).text(year + "-" + Utility.pad(month + 1) + "-" + Utility.pad(day));
        }

        public DatePickerFragment() {

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }

    // support methods

    private String getMedia() throws JSONException {
        JSONObject jo = new JSONObject();
        if (aq.id(R.id.cbRFb).isChecked()) jo.put("facebook", aq.id(R.id.etRfb).getText());
        if (aq.id(R.id.cbRTw).isChecked()) jo.put("twitter", aq.id(R.id.etRTw).getText());
        if (aq.id(R.id.cbRBlogWeb).isChecked()) jo.put("site", aq.id(R.id.etRBlogWeb).getText());
        return jo.toString();
    }

    private void getProvinces() {
        clearProvinceSpinAdapter(SPINNER_FETCHING_DATA);
        Map<String, String> unusedParams = new HashMap<String, String>();
        aq.ajax(Constants.URL_PROVINCE_API, unusedParams, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    clearProvinceSpinAdapter(SPINNER_REFRESH_AGAIN);
                    return;
                }

                Gson gs = new Gson();
                ArrayList<Province> listOfProvs = gs.fromJson(object.toString(),
                        new TypeToken<List<Province>>() {
                        }.getType());
                provinceSpinAdapter.clear();
                for (Province prov : listOfProvs) {
                    provinceSpinAdapter.add(prov);
                }
                clearCitySpinAdapter(SPINNER_INIT);

                if (listOfProvs.size() > 0) {
                    // here we have selected item
                    Province prov = (Province) aq.id(R.id.spinProvince).getSelectedItem();
                    // then get cities
                    getCities(prov.id);
                }
            }
        });
    }

    private void getCities(String provinceId) {
        clearCitySpinAdapter(SPINNER_FETCHING_DATA);
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_province", provinceId);
        aq.ajax(Constants.URL_CITY_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    clearCitySpinAdapter(SPINNER_REFRESH_AGAIN);
                    return;
                }
                Gson gs = new Gson();
                ArrayList<City> listOfCities = gs.fromJson(object.toString(),
                        new TypeToken<List<City>>() {
                        }.getType());
                citySpinAdapter.clear();
                for (City city : listOfCities) {
                    citySpinAdapter.add(city);
                }

                clearSubDistrictAdapter(SPINNER_INIT);
                if (listOfCities.size() >= 0) {
                    // here we have selected item
                    City city = (City) aq.id(R.id.spinCity).getSelectedItem();
                    // then get cities
                    getSubDistrict(city.id);
                }

            }
        });
    }

    private void getSubDistrict(String cityId){
        clearSubDistrictAdapter(SPINNER_FETCHING_DATA);
        Map<String,String> params = new HashMap<>();
        params.put("id_city",cityId);
        aq.ajax(Constants.URL_SUB_DISTRICT_API,params,JSONArray.class,new AjaxCallback<JSONArray>(){
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if(status.getCode()!= Constants.HTTP_RESPONSE_OK){
                    clearSubDistrictAdapter(SPINNER_REFRESH_AGAIN);
                    return;
                }
                Gson gs = new Gson();
                ArrayList<SubDistrict> listOfSubDistricts = gs.fromJson(object.toString(),
                        new TypeToken<List<SubDistrict>>(){
                        }.getType());
                subDistrictSpinAdapter.clear();
                for (SubDistrict subDistrict:listOfSubDistricts) {
                    subDistrictSpinAdapter.add(subDistrict);
                }
            }
        });
    }

    private void clearProvinceSpinAdapter(int mode) {
        provinceSpinAdapter.clear();
        if (mode == SPINNER_INIT) {
            provinceSpinAdapter.add(new Province("-1", "Pilihan provinsi"));
        } else if (mode == SPINNER_REFRESH_AGAIN) {
            provinceSpinAdapter.add(new Province("-1", "Silakan refresh"));
        } else if (mode == SPINNER_FETCHING_DATA) {
            provinceSpinAdapter.add(new Province("-1", "Memuat data..."));
        }
    }

    private void clearCitySpinAdapter(int mode) {
        citySpinAdapter.clear();
        if (mode == SPINNER_INIT) {
            citySpinAdapter.add(new City("-1", "-1", "Pilihan kota"));
        } else if (mode == SPINNER_REFRESH_AGAIN) {
            citySpinAdapter.add(new City("-1", "-1", "Silakan refresh"));
        } else if (mode == SPINNER_FETCHING_DATA) {
            citySpinAdapter.add(new City("-1", "-1", "Memuat data..."));
        }
    }

    private void clearSubDistrictAdapter(int mode){
        subDistrictSpinAdapter.clear();
        if(mode == SPINNER_INIT){
            subDistrictSpinAdapter.add(new SubDistrict("-1","-1","Pilih Kecamatan"));
        } else if (mode == SPINNER_REFRESH_AGAIN) {
            subDistrictSpinAdapter.add(new SubDistrict("-1","-1","Silakan refresh"));
        }else if(mode == SPINNER_FETCHING_DATA){
            subDistrictSpinAdapter.add(new SubDistrict("-1","-1","Memuat data..."));
        }
    }

//     click methods

    public void onDateChoose(View v) {
        DatePickerFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "datePicker");
    }



    public void onClickFindReferalName(View v) {
        Utility.clearSoftInputFromView(context, aq.id(R.id.etRReferNumber).getView());
        final String referredId = aq.id(R.id.etRReferNumber).getText().toString();
        if (referredId.length() == 0) {
            DialogHelper.showMessageDialog(context, "Isian Kosong", "Silakan masukkan nomor ID yang mengajak", "OK", null);
            return;
        }
        aq.id(R.id.llReferal).visible();
        aq.id(R.id.pg).visible();
        aq.id(R.id.tvNamaref).gone();
        aq.id(R.id.tvLvlref).gone();
        Map<String, String> params = new HashMap<String, String>();
        params.put("reseller_id", referredId);
        aq.progress(R.id.pg).ajax(Constants.URL_REFERAL_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llReferal).invisible();
                    DialogHelper.showMessageDialog(context, "ID " + referredId, "ID tidak ditemukan", "OK", null);
                    return;
                }
                try {
                    aq.id(R.id.tvNamaref).text(object.getString("referal_name")).visible();
                    aq.id(R.id.tvLvlref).text(object.getString("referal_level")).visible();
                } catch (JSONException e) {
                    Log.d("exception", String.valueOf(e));
                }
            }
        });
    }

    public void onClickRegister(View v) throws JSONException, NoSuchAlgorithmException, UnsupportedEncodingException {
        if (!aq.id(R.id.cbSyarat).isChecked()) {
            Utility.showDialogError(this, "Anda harus menyetujui syarat dan ketentuan", "Terjadi Kesalahan");
            return;
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", aq.id(R.id.etREmail).getText().toString());
        if (!aq.id(R.id.etRPassConf).getText().toString().equals(aq.id(R.id.etRPass).getText().toString())) {
            textInputLayoutEtRPass.setError("Password harus sesuai");
            textInputLayoutEtRPassConf.setError("Password harus sesuai");
        } else {
            params.put("password", aq.id(R.id.etRPass).getText().toString());
        }
        params.put("identity_number", aq.id(R.id.etRIdNumber).getText().toString());
        params.put("fullname", aq.id(R.id.etRName).getText().toString());
        params.put("birth_date", aq.id(R.id.etLahTanggal).getText().toString());
        params.put("birth_place", aq.id(R.id.etRTempatLahir).getText().toString());
        params.put("address", aq.id(R.id.etRAddress).getText().toString());
        params.put("job", aq.id(R.id.etRJob).getText().toString());
        params.put("bank_id", String.valueOf(dbh.findIdBank(aq.id(R.id.etRBank).getText().toString())));
        params.put("bank_account", aq.id(R.id.etRNorek).getText().toString());
        params.put("phonenumber", aq.id(R.id.etRPhoneNumber).getText().toString());
        params.put("referal_number", aq.id(R.id.etRReferNumber).getText().toString());
        params.put("sales_media", getMedia());
        Province selectedProv = (Province) ((Spinner) aq.id(R.id.spinProvince).getView()).getSelectedItem();
        City selectedCity = (City) ((Spinner) aq.id(R.id.spinCity).getView()).getSelectedItem();
        SubDistrict selectedSubDistrict = (SubDistrict) ((Spinner) aq.id(R.id.spinSubDistrict).getView()).getSelectedItem();
        params.put("province", selectedProv.id);
        params.put("city", selectedCity.id);
        params.put("sub_district",selectedSubDistrict.id);

        if (Utility.isNullOrBlank(PrefStorage.instance.getGCMid())) {
            Utility.showDialogError(RegisterActivity.this, "Please Try Again", "GCM Not Ready");
        } else {
            params.put("gcm_regid", "");
            Log.i(TAG, "gcm_regid" + PrefStorage.instance.getGCMid());
            aq.progress(progDialog).ajax(Constants.URL_REGISTER_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        try {
                            if (object.getString("success").equals("true")) {
                                Intent i = new Intent(RegisterActivity.this, InfoActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                switch (object.getInt("error")) {
                                    case 1: //email sama
                                        Utility.toast(RegisterActivity.this, "Email telah digunakan akun lain");
                                        break;
                                    case 2: //GCM udah ada
                                        Utility.toast(RegisterActivity.this, "Device ini telah terdaftar atas akun lain");
                                        break;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "callback json object: " + object.toString());
                    }
                    Log.i(TAG, "callback json object: " + object.toString());
                }
            });
        }
    }

    // inner class listener

    private class ProvinceSpinListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            clearCitySpinAdapter(SPINNER_INIT);
            Province province = (Province) aq.id(R.id.spinProvince).getSelectedItem();
            if (!province.id.equals("-1")) {
                getCities(province.id);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // do nothing
        }
    }

    // inner class listener

    private class CitySpinListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            clearSubDistrictAdapter(SPINNER_INIT);
            City city = (City) aq.id(R.id.spinCity).getSelectedItem();
            if (!city.id.equals("-1")) {
                getSubDistrict(city.id);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // do nothing
        }
    }

    /**
     * Listener of auto complete text view for province.
     */
    private class ProvinceACTVListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> listView, View view,
                                int position, long id) {
            Cursor cursor = (Cursor) listView.getItemAtPosition(position);
            String provinceName = cursor.getString(cursor.getColumnIndexOrThrow("prov"));
            aq.id(R.id.etRProvince).text(provinceName);
            // set selected province at city auto complete text view adapter
            cityACTVAdapter.setRelatedProvinceName(provinceName);
            // clear soft input from province auto complete
            Utility.clearSoftInputFromView(RegisterActivity.this, aq.id(R.id.etRProvince).getView());
            Log.d(TAG, "Province ACTV works. It gets " + provinceName);
        }
    }

    /**
     * Listener of auto complete text view for city.
     */
    private class CityACTVListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
            Cursor cursor = (Cursor) listView.getItemAtPosition(position);
            String cityName = cursor.getString(cursor.getColumnIndexOrThrow("kota"));
            String provinceName = aq.id(R.id.etRProvince).getText().toString();
            aq.id(R.id.etRCity).text(cityName);
            // set selected province at city auto complete text view adapter
            subdistrictACTVAdapter.setRelatedCityName(cityName);
            // clear soft input from city auto complete
            Utility.clearSoftInputFromView(RegisterActivity.this, aq.id(R.id.etRCity).getView());
            Log.d(TAG, "City ACTV works. It gets " + cityName + " - " + provinceName);
        }
    }

    /**
     * Listener of auto complete text view for city.
     */
    private class SubDistrictACTVListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
            Cursor cursor = (Cursor) listView.getItemAtPosition(position);
            String subDistrictName = cursor.getString(cursor.getColumnIndexOrThrow("keca"));
            String cityName = aq.id(R.id.etRCity).getText().toString();
            aq.id(R.id.etRCity).text(subDistrictName);
            // clear soft input from city auto complete
            Utility.clearSoftInputFromView(RegisterActivity.this, aq.id(R.id.etRCity).getView());
            Log.d(TAG, "City ACTV works. It gets " + subDistrictName + " - " + cityName);
        }
    }

}


