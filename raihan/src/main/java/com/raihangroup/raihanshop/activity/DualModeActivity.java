package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class DualModeActivity extends SingleModeActivity{
	public static void initFragment(Context ctx, Bundle b,Class<? extends Fragment> cls){
		b.putString("fragment", cls.getName());
		Intent intent = new Intent(ctx, DualModeActivity.class).putExtras(b);
		ctx.startActivity(intent);
	}
	
	public static Intent initFragmentIntent(Context ctx, Class<? extends Fragment> cls){
		Bundle b = new Bundle();
		b.putString("fragment", cls.getName());
		return new Intent(ctx, DualModeActivity.class).putExtras(b);

	}
}
