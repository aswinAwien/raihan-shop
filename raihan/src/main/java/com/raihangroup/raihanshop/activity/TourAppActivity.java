package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.TourAppEndFragment;
import com.raihangroup.raihanshop.fragments.TourAppFragment;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.TourContent;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

public class TourAppActivity extends AppCompatActivity {

    private Context context;
    private ViewPager vp;
    private CirclePageIndicator vpIndicator;
    private CheckBox cbIUnderstand;
    private int contentListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_vp);

        context = this;
        PrefStorage.newInstance(this);
        contentListId = Constants.TOUR_LIST_ID_MAIN;

        // hide the action bar
        getSupportActionBar().hide();

        Bundle b = getIntent().getExtras();
        if(b != null) {
            contentListId = b.getInt("contentListId");
        }

        vp = (ViewPager) findViewById(R.id.pager);
        vpIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        cbIUnderstand = (CheckBox) findViewById(R.id.cbIUnderstand);

        // set view pager
        vp.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        vp.setCurrentItem(0);
        vpIndicator.setViewPager(vp);

        // set check box
        cbIUnderstand.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                PrefStorage.newInstance(context).setIUnderstandTourAppStatus(cb.isChecked());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isCbChecked = PrefStorage.newInstance(context).getUnderstandingOfTourApp();
        cbIUnderstand.setChecked(isCbChecked);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private ArrayList<TourContent> getMainTourContents() {
        ArrayList<TourContent> list = new ArrayList<TourContent>();

        TourContent tcProductCatalogue = new TourContent(
                Constants.TOUR_PAGE_ID_PRODUCT_CATALOGUE, "KATALOG PRODUK",
                "Anda dapat mencari produk yang diinginkan melalui Katalog Produk disertai dengan fitur Search atau berdasarkan kategori",
                R.drawable.img_01_kategori);
        TourContent tcCart = new TourContent(
                Constants.TOUR_PAGE_ID_CART, "KERANJANG BELANJA",
                "Pilih produk yang Anda inginkan. Klik tombol Tambahkan ke Keranjang. Anda dapat memilih lebih dari satu produk",
                R.drawable.img_02_keranjang);
        TourContent tcProductBooking = new TourContent(
                Constants.TOUR_PAGE_ID_PRODUCT_BOOKING, "BOOKING PRODUK",
                "Jika Anda sudah memilih beberapa produk, silakan klik tombol Selesai untuk melanjutkan Order",
                R.drawable.img_03_booking);
        TourContent tcPaymentConfirm = new TourContent(
                Constants.TOUR_PAGE_ID_PAYMENT_CONFIRM, "KONFIRMASI PEMBAYARAN",
                "Lakukan konfirmasi pembayaran di menu Daftar Booking. Klik tombol Konfirm Bayar. Admin akan mengkonfirmasi pembayaran Anda",
                R.drawable.img_04_bayar);
        TourContent tcDeliveryConfirm = new TourContent(
                Constants.TOUR_PAGE_ID_DELIVERY_CONFIRM, "KONFIRMASI PENGIRIMAN",
                "Aktifkan menu Status Pemesanan, lalu klik tombol Konfirmasi. Jika ada komentar ketidakpuasan dapat disampaikan melalui konfirmasi tersebut",
                R.drawable.img_05_pengiriman);

        list.add(tcProductCatalogue);
        list.add(tcCart);
        list.add(tcProductBooking);
        list.add(tcPaymentConfirm);
        list.add(tcDeliveryConfirm);

        return list;
    }

    private ArrayList<TourContent> getBookingTourContents() {
        ArrayList<TourContent> list = new ArrayList<TourContent>();

        TourContent tcBooking1 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_BOOKING_1, "BOOKING",
                "Tampil halaman Detail Pemesanan berisi form pengiriman serta opsi metode pengiriman yang tersedia. Klik tombol Booking jika Anda sudah melengkapi dan menentukan metode pengiriman",
                R.drawable.img_03_booking_flow_01);
        TourContent tcBooking2 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_BOOKING_2, "BOOKING",
                "Jika proses Booking Anda berhasil, akan muncul Nomor Pesanan Anda. Proses selanjutnya Anda dapat melihat pada menu Daftar Booking",
                R.drawable.img_03_booking_flow_02);

        list.add(tcBooking1);
        list.add(tcBooking2);

        return list;
    }

    private ArrayList<TourContent> getPaymentTourContents() {
        ArrayList<TourContent> list = new ArrayList<TourContent>();

        TourContent tcPayment1 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_1, "KONFIRMASI PEMBAYARAN",
                "Jika Anda sudah menekan tombol Konfirmasi Bayar, akan tampil halaman Form Pemesanan dan Opsi Komisi, lalu tekan tombol Lanjut",
                R.drawable.img_04_bayar_flow_01);
        TourContent tcPayment2 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_2, "KONFIRMASI PEMBAYARAN",
                "Lalu akan tampil halaman Metode Pembayaran berisi Metode Pembayaran dan Informasi Pembayaran. Jika Anda sudah melengkapi form tersebut lalu tekan tombol Lanjut",
                R.drawable.img_04_bayar_flow_02);
        TourContent tcPayment3 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_3, "KONFIRMASI PEMBAYARAN",
                "Silakan Anda memeriksa detail pemesanan dan pengiriman pada halaman Cek Ulang. Jika detail sudah sesuai, silakan tekan tombol Order",
                R.drawable.img_04_bayar_flow_03);
        TourContent tcPayment4 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_4, "KONFIRMASI PEMBAYARAN",
                "Jika konfirmasi pembayaran Anda berhasil, akan muncul Nomor Pesanan Anda. Proses selanjutnya Anda dapat melihat pada menu Status Pesanan",
                R.drawable.img_04_bayar_flow_04);

        list.add(tcPayment1);
        list.add(tcPayment2);
        list.add(tcPayment3);
        list.add(tcPayment4);

        return list;
    }

    private ArrayList<TourContent> getDeliveryTourContents() {
        ArrayList<TourContent> list = new ArrayList<TourContent>();

        TourContent tcDelivery1 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_DELIVERY_1, "KONFIRMASI PENGIRIMAN",
                "Setelah Anda menekan tombol Konfirmasi, silakan lengkapi form Review/Rating. Berikan testimoni terhadap barang yang Anda pesan dan berikan rating berdasarkan Kualitas dan Pelayanan",
                R.drawable.img_05_pengiriman_flow_01);
        TourContent tcDelivery2 = new TourContent(
                Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_DELIVERY_2, "KONFIRMASI PENGIRIMAN",
                "Anda juga dapat memberikan Komplain dan Retur produk yang kurang memuaskan (tidak sesuai dengan keinginan)",
                R.drawable.img_05_pengiriman_flow_02);

        list.add(tcDelivery1);
        list.add(tcDelivery2);

        return list;
    }

    private class PagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> listOfFragments;

        public PagerAdapter(FragmentManager fm) {
            super(fm);
            listOfFragments = new ArrayList<>();

            ArrayList<TourContent> contentList = null;
            if(contentListId == Constants.TOUR_LIST_ID_MAIN) {
                contentList = getMainTourContents();
            } else if(contentListId == Constants.TOUR_LIST_ID_BOOKING) {
                contentList = getBookingTourContents();
            } else if(contentListId == Constants.TOUR_LIST_ID_PAYMENT) {
                contentList = getPaymentTourContents();
            } else if(contentListId == Constants.TOUR_LIST_ID_DELIVERY) {
                contentList = getDeliveryTourContents();
            }

            for(TourContent tc : contentList) {
                TourAppFragment tourFragment = new TourAppFragment();
                Bundle b = new Bundle();
                b.putInt("tourId", tc.tourId);
                b.putString("title", tc.title);
                b.putString("description", tc.description);
                b.putInt("imageRes", tc.imageRes);
                tourFragment.setArguments(b);
                listOfFragments.add(tourFragment);
            }

            if(contentListId == Constants.TOUR_LIST_ID_MAIN) {
                // add end of fragment
                listOfFragments.add(new TourAppEndFragment());
            }
        }

        @Override
        public int getCount() {
            return listOfFragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return listOfFragments.get(position);
        }
    }
}
