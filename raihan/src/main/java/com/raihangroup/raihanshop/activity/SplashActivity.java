package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.LoginActivity;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.Helper;
import com.raihangroup.raihanshop.helper.PrefStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SplashActivity extends AppCompatActivity {
	
	private Context context;
	private AQuery aq;
	
	private final static String TAG = "Splash";
	AlertDialog.Builder builder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		context = this;
		aq = new AQuery(this);
		builder = new AlertDialog.Builder(this);
		builder.setTitle("Permission Required!");
		builder.setMessage("You need to approve the permission in order to run this app").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
			}
		}).setCancelable(false).create();

		if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			builder.show();
		} else {
			ImageLoaderApplication.normalImageManagerSettings(context);
			checkActiveVersionApp();

		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		boolean permission = PrefStorage.newInstance(this).getPermission();
		Log.i(TAG, "onResume: freaking" + permission);
	}

	// support methods
	
	private void checkActiveVersionApp() {
		// app version in device
		final String deviceVersion = Helper.getAppVersionName(this);
		Log.d(TAG, "app version in device " + deviceVersion);
		
		// needed for aq so that the call is treated as POST method
		HashMap<String, String> unusedParams = new HashMap<String, String>();
		aq.ajax(Constants.URL_VERSION_APP_API, unusedParams, JSONObject.class, new AjaxCallback<JSONObject>() {
			
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status) {
				if(status.getCode() != Constants.HTTP_RESPONSE_OK) {
					checkLoginStatus();
					return;
				}
				
				// app version on server
				String serverVersion = null;
				
				try {
					// get app version on server
					int major = object.getInt("major");
					int minor = object.getInt("minor");
					int built = object.getInt("built");


					serverVersion = major + "." + minor + "." + built;
					Log.d(TAG, "app version on server " + serverVersion);
					
				} catch (JSONException e) {
					Log.d("exception", String.valueOf(e));
				}
				
				if(deviceVersion.equals(serverVersion)) {
					checkLoginStatus();
				} else {
					Intent intent = new Intent(context, UpdateAppActivity.class);
					intent.putExtra("deviceVersion", deviceVersion);
					intent.putExtra("serverVersion", serverVersion);
					startActivity(intent);
					finish();
				}
			}
		});
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case 1: {

				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					ImageLoaderApplication.normalImageManagerSettings(context);
					checkActiveVersionApp();
					// permission was granted, yay! Do the
					// contacts-related task you need to do.
				} else {

					// permission denied, boo! Disable the
					// functionality that depends on this permission.
					Toast.makeText(SplashActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}
	
	private void checkLoginStatus() {
		// check login
		if(PrefStorage.instance.getUserId() == -1) {
			// not logged in, so go to login page
			Intent intent = new Intent(context,MainActivity.class);
			startActivity(intent);
			finish();
		} else {
			// already logged in case
			Intent intent = new Intent(context, MainActivity.class);
			startActivity(intent);
			finish();
		}

//		Intent intent = new Intent(context, HomeActivity.class);
//			startActivity(intent);
//			finish();

	}
}
