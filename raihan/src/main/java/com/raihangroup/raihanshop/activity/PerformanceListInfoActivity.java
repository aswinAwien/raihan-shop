package com.raihangroup.raihanshop.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ChartPerformanceAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.raihangroup.raihanshop.activity.PerformanceChartActivity.KOMISI;
import static com.raihangroup.raihanshop.activity.PerformanceChartActivity.SUBTITLE;
import static com.raihangroup.raihanshop.activity.PerformanceChartActivity.TITLE;

public class PerformanceListInfoActivity extends AppCompatActivity {

    Toolbar toolbarInfoPerformance;
    RecyclerView rvPerformanceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_list_info);
        toolbarInfoPerformance = findViewById(R.id.toolbar_info_performance);
        rvPerformanceList = findViewById(R.id.rv_performance_list);
        setSupportActionBar(toolbarInfoPerformance);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Performance List Info");
        ChartPerformanceAdapter adapter = new ChartPerformanceAdapter(TITLE,SUBTITLE,KOMISI);
        rvPerformanceList.setHasFixedSize(true);
        rvPerformanceList.setLayoutManager(new LinearLayoutManager(this));
        rvPerformanceList.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        rvPerformanceList.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
