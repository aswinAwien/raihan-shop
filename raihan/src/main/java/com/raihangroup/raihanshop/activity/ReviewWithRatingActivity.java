package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DatePickerDialogWithMaxMinRange;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.CartItem;
import com.novoda.imageloader.core.model.ImageTagFactory;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewWithRatingActivity extends AppCompatActivity {
    @BindView(R.id.toobal_review_rating)
    Toolbar toobalReviewRating;
    private boolean returnProduct = false;
    private CartItem cartItem;
    private AQuery aq;
    private Date date;
    private float kualitas;
    private float pelayanan;
    private String review;
    Bundle bundle;
    private ProgressDialog progDialog;
    boolean valid = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_rating);
        ButterKnife.bind(this);
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setMessage("Mengirimkan Konfirmasi");
        progDialog.setCancelable(false);
        aq = new AQuery(this);
        toobalReviewRating.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalReviewRating);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            cartItem = bundle.getParcelable("cart_item");
            aq.id(R.id.pg).gone();
            aq.id(R.id.tvTitle).text(cartItem.getName());
            final RatingBar rbQuality = aq.id(R.id.ratingBarQuality).getRatingBar();
            final RatingBar rbService = aq.id(R.id.ratingBarService).getRatingBar();
            ImageView ivProduct = aq.id(R.id.ivProduct).getImageView();
            ImageTagFactory imageTagFactory = ImageTagFactory.newInstance(this, R.drawable.default_product);
            imageTagFactory.setErrorImageId(R.drawable.default_product);
            if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
                ivProduct.setTag(imageTagFactory.build(Constants.UPLOAD_URL + cartItem.getImage1(), this));
                ImageLoaderApplication.getImageLoader().getLoader().load(ivProduct);
            }
            rbQuality.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    String[] texts = new String[]{"Sangat Buruk", "Buruk", "Biasa", "Baik", "Sangat Baik"};
                    String[] colors = new String[]{"#ed272f", "#e56133", "#f29e30", "#91aa35", "#3eae49"};
                    if (rating != 0) {
                        aq.id(R.id.tvTextQuality).text(texts[(int) Math.ceil((rating - 1))]);
                        aq.id(R.id.tvTextQuality).textColor(Color.parseColor(colors[(int) Math.ceil((rating - 1))]));
                    } else {
                        aq.id(R.id.tvTextQuality).text("berikan penilaian");
                        aq.id(R.id.tvTextQuality).textColor(Color.parseColor("#ed272f"));
                    }
                }
            });

            rbService.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    String[] texts = new String[]{"Sangat Buruk", "Buruk", "Biasa", "Baik", "Sangat Baik"};
                    String[] colors = new String[]{"#ed272f", "#e56133", "#f29e30", "#91aa35", "#3eae49"};
                    if (rating != 0) {
                        aq.id(R.id.tvTextService).text(texts[(int) Math.ceil((rating - 1))]);
                        aq.id(R.id.tvTextService).textColor(Color.parseColor(colors[(int) Math.ceil((rating - 1))]));
                    } else {
                        aq.id(R.id.tvTextService).text("berikan penilaian");
                        aq.id(R.id.tvTextService).textColor(Color.parseColor("#ed272f"));
                    }
                }
            });
            aq.id(R.id.btnLanjut).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    review = aq.id(R.id.etReview).getText().toString();
                    kualitas = rbQuality.getRating();
                    pelayanan = rbService.getRating();
                    if (date == null) {
                        aq.id(R.id.etTanggal).getView().requestFocus();
                        ((EditText) aq.id(R.id.etTanggal).getView()).setError("wajib diisi");
                        valid = false;
                        return;
                    } else if (review.isEmpty()) {
                        aq.id(R.id.etReview).getView().requestFocus();
                        ((EditText) aq.id(R.id.etReview).getView()).setError("wajib diisi");
                        valid = false;
                        return;
                    }
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("id_order_items", "" + cartItem.getId());
                    params.put("id_member", "" + PrefStorage.instance.getUserId());
                    params.put("date_received", "" + date);
                    Map<String, String> rating = new HashMap<String, String>();
                    rating.put("id_supplier", cartItem.id_supplier);
                    rating.put("quality", "" + kualitas);
                    rating.put("service", "" + pelayanan);
                    rating.put("testimonial", review);
                    JSONObject JSONRating = new JSONObject(rating);
                    params.put("ratings", JSONRating.toString());
                    Log.d("params", "" + params);
                    if (returnProduct) {
                        Bundle b = new Bundle();
                        b.putString("ratings", JSONRating.toString());
                        b.putParcelable("cart_item", cartItem);
                        b.putString("date_received", date.toString());
                        // go to activity return product
                        Intent i = new Intent(ReviewWithRatingActivity.this, ReturnProductActivity.class).putExtras(b);
                        startActivityForResult(i, HomeActivity.REQ_CODE_LIST_ORDER);
                    } else {
                        aq.progress(progDialog).ajax(Constants.URL_RATE_SUPPLIER_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                                    Log.d("Review", "false");
                                    DialogHelper.showMessageDialog(ReviewWithRatingActivity.this, "Terjadi Kesalahan", "Konfirmasi gagal dikirimkan, silahkan coba beberapa saat lagi", "ok", null);
                                    return;
                                }
                                DialogHelper.showMessageDialog(ReviewWithRatingActivity.this, "Terima kasih", "Konfirmasi dan review Anda sudah kami terima", "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        setResult(Activity.RESULT_OK);
                                        finish();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        } else {
            // handle emptiness
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_with_rating, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void dateChoose(View v) {
        DatePickerDialog.OnDateSetListener datePickerOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                Calendar c = Calendar.getInstance(new Locale("in"));
                c.set(year, monthOfYear, dayOfMonth);
                String s = new SimpleDateFormat("dd MMMM yyyy", new Locale("in")).format(c.getTime());
                aq.id(R.id.etTanggal).text(s);
                aq.id(R.id.etTanggal).tag(c);
                date = c.getTime();
            }
        };

        Calendar now = Calendar.getInstance();
        DatePickerDialogWithMaxMinRange datePickerDialog = new DatePickerDialogWithMaxMinRange(
                this,
                datePickerOnDateSetListener,
                now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH) - 1, //29 juni
                now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)); //28 juli
        datePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == HomeActivity.REQ_CODE_LIST_ORDER && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onRadioButtonClicked(View v) {
        boolean checked = ((RadioButton) v).isChecked();
        // Check which radio button was clicked
        switch (v.getId()) {
            case R.id.radioBtnYes:
                if (checked) {
                    aq.id(R.id.radioBtnNo).checked(false);
                    aq.id(R.id.btnLanjut).text("Lanjut");
                    aq.id(R.id.tvStep).text("langkah 9/10");
                    returnProduct = true;
                }
                break;
            case R.id.radioBtnNo:
                if (checked) {
                    aq.id(R.id.radioBtnYes).checked(false);
                    aq.id(R.id.btnLanjut).text("Selesai");
                    aq.id(R.id.tvStep).text("langkah 9/9");
                    returnProduct = false;
                }
                break;
        }
    }
}