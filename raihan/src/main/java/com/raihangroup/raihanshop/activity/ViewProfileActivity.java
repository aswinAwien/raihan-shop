package com.raihangroup.raihanshop.activity;

/**
 * Created by ocit on 8/25/15.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ListNetworkAdapter;
import com.raihangroup.raihanshop.adapter.ListStatusAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.UserProfile;
import com.raihangroup.raihanshop.model.UserStatus;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.novoda.imageloader.core.model.ImageTagFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.reflect.TypeToken;

public class ViewProfileActivity extends AppCompatActivity {

    private AQuery aq;
    private int userId;
    private boolean editable;
    private UserProfile profile;

    private final static String TAG = "View Profile";
    Context context;
    public static final int REQ_CODE_VIEW_PROFILE = 3;
    private int is_mentor;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ArrayList<UserStatus> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_view_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.viewToolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
        ab.setDisplayHomeAsUpEnabled(true);

        aq = new AQuery(this);
        aq.id(R.id.tvMessageTitle).text("Tidak Ada Profile");
        aq.id(R.id.tvMessageContent).text("Silakan mencoba kembali");
        context = this;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.right_drawer);

        ListNetworkAdapter adapter = new ListNetworkAdapter(getApplicationContext());
        mDrawerList.setAdapter(adapter);

/*
        aq.id(R.id.imgMenuNetwork).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });*/

        Bundle b = getIntent().getExtras();
        if (b != null) {
            userId = b.getInt("userId");
            editable = b.getBoolean("editable");
            is_mentor = b.getInt("is_mentor", 1);
            Log.d(TAG, "user id " + userId);

            aq.id(R.id.btnMember).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putInt("type", Constants.GROUP_DOWNLINE);
                    b.putInt("member_id", userId);
                    b.putString("name", profile.fullname);
                    startActivity(new Intent(context, ListMemberActivity.class).putExtras(b));
                }
            });
            if (userId == PrefStorage.instance.getUserId()) {
                aq.id(R.id.btnMember).gone();
            }
            if (is_mentor == 1) {
                aq.id(R.id.btnMember).gone();
            }
        }

        refreshData();
//        getStatusProfile();

/*
        aq.id(R.id.imgBackProfile).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

//        aq.id(R.id.btnUpdateStatus).clicked(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                EditText et = (EditText) findViewById(R.id.etUpdateStatus);
//                updateStatus(et);
//                et.clearComposingText();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (editable) {
            getMenuInflater().inflate(R.menu.view_profile, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_edit:
                if (profile == null) {
                    DialogHelper.showMessageDialog(this, "Terjadi Kesalahan", "Data profile tidak tersedia untuk diubah", "OK", null);
                } else {
                    Bundle b = new Bundle();
                    b.putParcelable("userProfile", profile);
                    Intent i = new Intent(this, EditProfileActivity.class).putExtras(b);
                    startActivityForResult(i, REQ_CODE_VIEW_PROFILE);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_CODE_VIEW_PROFILE) {
                refreshData();
            }
        }
    }

    // handle view visibility
    private void viewVisibilityInProgressState() {
        aq.id(R.id.pg).visible();
        aq.id(R.id.content).gone();
        aq.id(R.id.llInfo).gone();
    }

    private void viewVisibilityInInfoState() {
        aq.id(R.id.pg).gone();
        aq.id(R.id.content).gone();
        aq.id(R.id.llInfo).visible();
    }

    private void viewVisibilityInContentState() {
        aq.id(R.id.pg).gone();
        aq.id(R.id.content).visible();
        aq.id(R.id.llInfo).gone();
    }

    public void refreshData() {
        viewVisibilityInProgressState();
        String id = String.valueOf(userId);
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    viewVisibilityInInfoState();
                    return;
                }

                viewVisibilityInContentState();

                Gson gs = new Gson();
                profile = gs.fromJson(object.toString(), UserProfile.class);

                String birthDateText = profile.birth_date;
                try {
                    Date birthDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(profile.birth_date);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    birthDateText = sdf.format(birthDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!profile.image.equals("img_default_user.png")) {
                    aq.id(R.id.detProfileImage).image(Constants.BASE_URL + profile.image);

                }

                aq.id(R.id.tvFullName).text(profile.fullname);
                aq.id(R.id.tvAddress).text(profile.address + ", " + profile.city_name);
                aq.id(R.id.tvLevel).text(profile.level_name);
                aq.id(R.id.txIdProfile).text(profile.id);

            }
        });
    }

//    public void updateStatus(EditText editText) {
//        String id = String.valueOf(userId);
//        String content = editText.getText().toString();
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String strDate = sdf.format(c.getTime());
//
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("id_creator", id);
//        params.put("isi_status", content);
//        params.put("created", strDate);
//        aq.ajax(Constants.URL_UPDATE_STATUS, params, JSONArray.class, new AjaxCallback<JSONArray>() {
//            @Override
//            public void callback(String url, JSONArray object, AjaxStatus status) {
//                getStatusProfile();
//            }
//        });
//    }

//    public void getStatusProfile() {
//        String id = String.valueOf(userId);
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("id_creator", id);
//        aq.ajax(Constants.URL_USER_PROFILE_STATUS_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
//            @Override
//            public void callback(String url, JSONArray object, AjaxStatus status) {
//
//                Gson gson = new Gson();
//                Type collectionType = new TypeToken<ArrayList<UserStatus>>() {
//                }.getType();
//                data = gson.fromJson(object.toString(), collectionType);
//
//                ListStatusAdapter adapter = new ListStatusAdapter(ViewProfileActivity.this, data, aq);
//                ListView lv = (ListView) findViewById(R.id.lvStatusView);
//                lv.setAdapter(adapter);
//                setListViewHeightBasedOnChildren(lv);
//
//            }
//        });
//    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private class MorePerformClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            context.startActivity(new Intent(context, PerformanceViewActivity.class));
        }
    }
}