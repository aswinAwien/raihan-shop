package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.SelfPerformanceAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.SelfPerformance;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamad on 2/9/2015.
 * Activity to show list of member
 */
public class ListMemberActivity extends AppCompatActivity {
    @BindView(R.id.toobal_list_member)
    Toolbar toobalListMember;
    private Context context;
    private ListView listView;
    private AQuery aq;
    private SelfPerformanceAdapter memberAdapter;
    Bundle b;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = getIntent().getExtras();
        setContentView(R.layout.activity_list_member);
        ButterKnife.bind(this);
        context = this;
        PrefStorage.newInstance(context);
        aq = new AQuery(this);
        listView = (ListView) aq.id(R.id.listMember).getView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelfPerformance selected = (SelfPerformance) parent.getItemAtPosition(position);
                Bundle b = new Bundle();
                b.putInt("userId", selected.getId());
                b.putBoolean("editable", false);
                b.putInt("is_mentor", selected.getIs_mentor());
                Intent i = new Intent(context, ViewProfileActivity.class).putExtras(b);
                startActivity(i);
            }
        });
        aq.id(R.id.tvMessageTitle).text("Tidak Ada Member");
        aq.id(R.id.tvMessageContent).text("Kesalahan mungkin terjadi pada server");
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMemberList();
            }
        });
        toobalListMember.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalListMember);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (b != null && b.getInt("type") == 1) {
            getSupportActionBar().setTitle("Mentor Anda");
            if (b.getInt("member_id") != PrefStorage.instance.getUserId()) {
                getSupportActionBar().setTitle("Mentor dari " + b.getString("name"));
            }
        } else {
            getSupportActionBar().setTitle("Member Anda");
            if (b.getInt("member_id") != PrefStorage.instance.getUserId()) {
                getSupportActionBar().setTitle("Member dari " + b.getString("name"));
            }
        }
        getMemberList();
    }

    private void getMemberList() {
        int month = new GregorianCalendar().get(GregorianCalendar.MONTH) + 1;/* java starts from 0*/
        int year = new GregorianCalendar().get(GregorianCalendar.YEAR);
        HashMap<String, String> params = new HashMap<>();
        params.put("month", String.valueOf(month));
        params.put("year", String.valueOf(year));
        if (b != null) {
            params.put("member_id", "" + b.getInt("member_id"));
            params.put("type", "" + b.getInt("type"));
        }
        Log.d("tag", "params =" + params);
        aq.id(R.id.llInfo).gone();
        aq.progress(R.id.pg).ajax(Constants.URL_TTSPERF_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray jsonArray, AjaxStatus status) {
                super.callback(url, jsonArray, status);
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    Log.d("tag", "json = " + jsonArray.toString());
                    ArrayList<SelfPerformance> members = gs.fromJson(jsonArray.toString(), new TypeToken<List<SelfPerformance>>() {
                    }.getType());
                    if (members.isEmpty()) {
                        aq.id(R.id.llInfo).visible();
                        if (b.getInt("type") == 1) {
                            aq.id(R.id.tvMessageTitle).text("Tidak Ada Mentor");
                            aq.id(R.id.tvMessageContent).text("Anda tidak memiliki mentor");
                        } else {
                            aq.id(R.id.tvMessageTitle).text("Tidak Ada Member");
                            aq.id(R.id.tvMessageContent).text("Anda belum memiliki member");
                        }
                    } else {
                        memberAdapter = new SelfPerformanceAdapter(ListMemberActivity.this,
                                R.layout.s_performance_item, members, 1);//mode tts = 1
                        listView.setAdapter(memberAdapter);
                    }
                } else {
                    aq.id(R.id.llInfo).visible();
                    if (b.getInt("type") == 1) {
                        DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mendapatkan Mentor",
                                "OK", null);
                    } else {
                        DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mendapatkan Member",
                                "OK", null);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}