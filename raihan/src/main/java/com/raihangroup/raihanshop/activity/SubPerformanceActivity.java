package com.raihangroup.raihanshop.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.SubKoordinat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubPerformanceActivity extends AppCompatActivity {

    private AQuery aq;
    private ProgressDialog progDialog;
    private List<SubCoor> objects;
    private LayoutInflater inflater;
    private LinearLayout ll;
    private String currentClass;
    private int currentClassInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_performance);

        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mengambil Data");
        progDialog.setMessage("Mohon Tunggu..");
        progDialog.setCancelable(false);

        Map<String, String> params = new HashMap<String, String>();
        params.put("month", String.valueOf(getIntent().getIntExtra(Constants.INTENT_PERFORMANCE_MONTH, 0)));
        params.put("year", String.valueOf(getIntent().getIntExtra(Constants.INTENT_PERFORMANCE_YEAR, 0)));
        params.put("member_id", String.valueOf(getIntent().getIntExtra(Constants.INTENT_PERFORMANCE_ID, 0)));

        aq = new AQuery(this);

        currentClassInt = getIntent().getIntExtra(Constants.INTENT_TPERFORMANCE_CLASS, 0) - 1;
        currentClass = Utility.idKelastoName(currentClassInt);

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ll = (LinearLayout) findViewById(R.id.llSubParent);
        aq.progress(progDialog).ajax(Constants.URL_TPERF_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    objects = new Gson().fromJson(object.toString(),
                            new TypeToken<List<SubCoor>>() {
                            }.getType());

                    for (int i = 0; i < objects.size(); i++) {
                        generateView(ll, objects.get(i), i);
                    }

                    if (objects.size() == 0) aq.id(R.id.tvErr).visible();

                } else {
                    aq.id(R.id.tvErr).visible();
                }
            }
        });


        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void generateView(LinearLayout ll, SubCoor sc, int count) {
        //bikin textnya dulu
        TextView tv = new TextView(this);
        tv.setBackgroundColor(getResources().getColor(R.color.raihan_main));
        tv.setTextColor(Color.WHITE);
        tv.setText(currentClass + " " + (count + 1) + " : " + sc.getFullname());

        int px = Utility.dipToPx(10, getResources());
        tv.setPadding(px, px, px, px);

        ll.addView(tv, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // PS Sendiri
        TableLayout tl = (TableLayout) inflater.inflate(R.layout.item_dir_ps_bar, ll, false);
        ll.addView(tl);
        aq.recycle(tl);

        ProgressBar pb = aq.id(R.id.progress_PS).getProgressBar();
        int max = getIntent().getIntExtra(Constants.INTENT_SPERFORMANCE_PS_END + "_" + sc.getId_sub_coordinate(), 0);
        int cur = Math.round(getIntent().getFloatExtra(
                Constants.INTENT_SPERFORMANCE_PS_START + "_" + sc.getId_sub_coordinate(), 0));
        pb.setMax(max);
        pb.setProgress(cur);
        aq.id(R.id.tvLevelDesc_PS).text(cur + "/" + max);
        aq.id(R.id.tr_PS).getView().setBackgroundResource(0);

        //bikin per subkoor
        for (SubKoordinat sk : sc.getSub_coordinate()) {
            // untuk high class
            if (currentClassInt > 3) {
                TableRow tr = (TableRow) inflater.inflate(R.layout.item_dir_ex_bar, tl, false);
                tl.addView(tr);
                tr.setBackgroundResource(0);

                aq.recycle(tr);

                aq.id(R.id.tvlabel).text(sk.getLabel());

                switch (sk.getCurrent_state()) {
                    case 1:
                        aq.id(R.id.imJumlahOrang).image(R.drawable.ic_point1);
                        break;
                    case 2:
                        aq.id(R.id.imJumlahOrang).image(R.drawable.ic_point2);
                        break;
                    case 3:
                        aq.id(R.id.imJumlahOrang).image(R.drawable.ic_point3);
                        break;
                    default:
                        aq.id(R.id.imJumlahOrang).image(R.drawable.ic_point0);
                        break;
                }

                ProgressBar pbR = aq.id(R.id.progress).getProgressBar();
                pbR.setMax(sk.getTarget_ps());
                pbR.setProgress(sk.getCurrent_ps());

                aq.id(R.id.tvLevelDesc).text(sk.getLabel_target() +
                        " (" + sk.getCurrent_state() + "/" + sk.getTarget_state() + ")");
            } else {
                tv = new TextView(this);
                tv.setText(sk.getLabel());
                tv.setPadding(Utility.dipToPx(10, getResources()), 0, 0, 0);

                tl.addView(tv);

                TableRow tr = (TableRow) inflater.inflate(R.layout.item_dir_ps_bar, ll, false).findViewById(R.id.tr_PS);
                pb = (ProgressBar) tr.findViewById(R.id.progress_PS);
                pb.setMax(sk.getTarget_ps());
                pb.setProgress(sk.getCurrent_ps());
                ((TextView) tr.findViewById(R.id.tvLevelDesc_PS)).setText(sk.getCurrent_ps() + "/" + sk.getTarget_ps());
                tr.setBackgroundResource(0);

                tl.addView(tr);

                tr = (TableRow) inflater.inflate(R.layout.item_dir_ps_bar, ll, false).findViewById(R.id.tr_PS);
                ((TextView) tr.findViewById(R.id.tvPerf_PS)).setText("TTS");
                pb = (ProgressBar) tr.findViewById(R.id.progress_PS);
                pb.setMax(sk.getTarget_state());
                pb.setProgress(sk.getCurrent_state());
                ((TextView) tr.findViewById(R.id.tvLevelDesc_PS)).setText(sk.getCurrent_state() + "/" + sk.getTarget_ps());
                tr.setBackgroundResource(0);

                tl.addView(tr);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sub_performance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    class SubCoor {
        private int id_sub_coordinate;
        private String fullname;
        private List<SubKoordinat> sub_coordinate;

        public int getId_sub_coordinate() {
            return id_sub_coordinate;
        }

        public void setId_sub_coordinate(int id_sub_coordinate) {
            this.id_sub_coordinate = id_sub_coordinate;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public List<SubKoordinat> getSub_coordinate() {
            return sub_coordinate;
        }

        public void setSub_coordinate(List<SubKoordinat> sub_coordinate) {
            this.sub_coordinate = sub_coordinate;
        }
    }
}
