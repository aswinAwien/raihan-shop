package com.raihangroup.raihanshop.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.LoginActivity;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.FeedbackListFragment;
import com.raihangroup.raihanshop.fragments.FrontMainFragment;
import com.raihangroup.raihanshop.fragments.NotificationListFragment;
import com.raihangroup.raihanshop.fragments.OrderListFragment;
import com.raihangroup.raihanshop.fragments.ProductListFragment;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.raihangroup.raihanshop.activity.ProductActivity.getResourceIdByName;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = MainActivity.class.getSimpleName();
    BottomNavigationViewEx navigation;
    AQuery aq;
    Map<String, String> params;
    Toolbar toolbar;
    Menu menu;
//    SearchView searchView;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    FrontMainFragment frontMainFragment;
    NotLoggedInFragment notLoggedInFragment;
    NotificationListFragment notificationListFragment;
    OrderListFragment orderListFragment;
    FeedbackListFragment feedBackFragment;
//    ProductListFragment productListFragment;
    @BindView(R.id.toolbar_logo)
    ImageView toolbarLogo;
    private BottomNavigationViewEx.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = frontMainFragment;
                    loadFragment(fragment);
                    return true;
//                case R.id.navigation_search:
//                    Intent searchIntent = new Intent(MainActivity.this,SearchResultActivity.class);
//                    startActivity(searchIntent);
//                    return true;
                case R.id.navigation_status_resi:
                    fragment = orderListFragment;
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_notification:
                    fragment = notificationListFragment;
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_profile:
                    if (PrefStorage.newInstance(MainActivity.this).getIsLoggedIn()) {
                        Bundle b = new Bundle();
                        b.putInt("userId",PrefStorage.instance.getUserId());
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class).putExtras(b);
                        startActivity(profileIntent);
                    } else {
                      Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                      startActivity(loginIntent);
                    }
                    return true;
            }
            return false;
        }
    };

    public void switchToOrderFragment() {
        loadFragment(orderListFragment);
    }


    public void switchToFeedbackFragment() {
        loadFragment(feedBackFragment);
    }

//    public void switchHomeFragment() {
//        loadFragment(frontMainFragment);
//    }

    @Override
    protected void onResume() {
        supportInvalidateOptionsMenu();
        super.onResume();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (PrefStorage.instance.getCartAmount() == 0) {
            menu.findItem(R.id.menu_shopping_cart).setIcon(R.drawable.ic_cart);
        } else {
            menu.findItem(R.id.menu_shopping_cart).setIcon(R.drawable.ic_cart_notif);
            if (PrefStorage.instance.getCartAmount() >= 1 && PrefStorage.instance.getCartAmount() <= 10) {
                menu.findItem(R.id.menu_shopping_cart).setIcon(getResourceIdByName(this, "ic_cart_notif_" +
                        PrefStorage.instance.getCartAmount(), "drawable"));
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem cartMenuItem = menu.findItem(R.id.menu_shopping_cart);
        MenuItem chatMenuItem = menu.findItem(R.id.menu_chat);
        if(PrefStorage.instance.getUserId()==-1){
            cartMenuItem.setVisible(false);
            chatMenuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_shopping_cart:
                Intent intent = new Intent(MainActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_chat:
                Intent chatIntent = new Intent(MainActivity.this,ChattingActivity.class);
                startActivity(chatIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        aq = new AQuery(this);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        params = new HashMap<>();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        Fresco.initialize(this);
        frontMainFragment = new FrontMainFragment();
        notificationListFragment = new NotificationListFragment();
        notLoggedInFragment = new NotLoggedInFragment();
        orderListFragment = new OrderListFragment();
        feedBackFragment = new FeedbackListFragment();
//        productListFragment = new ProductListFragment();
        navigation = (BottomNavigationViewEx) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.enableItemShiftingMode(false);
        navigation.enableShiftingMode(false);
        loadFragment(frontMainFragment);
    }


    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
        if (fragment == frontMainFragment) {
            toolbarTitle.setText("Cari produk apa yang dijual");
            toolbarLogo.setVisibility(View.GONE);
            toolbarTitle.setTypeface(Typeface.DEFAULT);
        } else if (fragment == notificationListFragment) {
            toolbarTitle.setText("Notifikasi");
            toolbarLogo.setVisibility(View.GONE);
            toolbarTitle.setTypeface(Typeface.DEFAULT);
        } else {
            toolbarLogo.setVisibility(View.VISIBLE);
            toolbarTitle.setText("Raihanshop");
            toolbarTitle.setTypeface(Typeface.DEFAULT_BOLD);
        }

    }

    //onBack Dialog Log Out
    @Override
    public void onBackPressed() {
        DialogHelper.showYesNoDialog(this, "Konfirmasi", "Apakah Anda ingin keluar dari aplikasi?", "Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, "Tidak", null);
    }
}
