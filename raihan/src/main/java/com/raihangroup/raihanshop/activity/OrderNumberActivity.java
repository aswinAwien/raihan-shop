package com.raihangroup.raihanshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;

/**
 * Activity to display order number
 */
public class OrderNumberActivity extends AppCompatActivity {

    private AQuery aq;
    private String ord_numb;
    private Toolbar toolbarOrderNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_number);
        toolbarOrderNumber = (Toolbar)findViewById(R.id.toobal_order_number);
        setSupportActionBar(toolbarOrderNumber);
        ord_numb = getIntent().getStringExtra(Constants.INTENT_ORDER_NUMBER);
        aq = new AQuery(this);
        aq.id(R.id.tvOrdNumber).text(ord_numb);
        aq.id(R.id.btnAction).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OrderNumberActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_ORD);
        startActivity(intent);
    }
}