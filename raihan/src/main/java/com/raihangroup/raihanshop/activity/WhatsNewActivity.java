package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.PrefStorage;

/**
 * Created by Alh on 4/17/15.
 */
public class WhatsNewActivity extends AppCompatActivity {

    private Context context;
    private AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_new);

        context = this;
        PrefStorage.newInstance(this);

        aq = new AQuery(this);

        // set listener to next time app tour
        aq.id(R.id.btnToTourNextTime).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // finish to back to home
                finish();
            }
        });

        // set listener to app tour right now
        aq.id(R.id.btnToTourNow).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(context, TourAppActivity.class));
            }
        });
    }
}
