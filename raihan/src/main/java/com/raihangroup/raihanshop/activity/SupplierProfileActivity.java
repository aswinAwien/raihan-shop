package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.ProductListFragment;
import com.raihangroup.raihanshop.fragments.SupplierProfileFragment;
import com.raihangroup.raihanshop.fragments.SupplierReviewListFragment;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SupplierProfileActivity extends AppCompatActivity {

    public static final int VP_LIMIT = 3;
    public static final int VP_INITIATE = 0;
    @BindView(R.id.tb_vp_supplier)
    Toolbar tbVpSupplier;
    private Context context;
    private ViewPager vp;
    private TabLayout vpIndicator;

    private String supplierId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vp_supplier_detail);
        ButterKnife.bind(this);

        tbVpSupplier.setTitleTextColor(Color.WHITE);
        setSupportActionBar(tbVpSupplier);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Tenant");

        context = this;
        PrefStorage.newInstance(this);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            supplierId = b.getString("supplierId");
        }

        vp = (ViewPager) findViewById(R.id.pager);
        vpIndicator = (TabLayout) findViewById(R.id.indicator);

        vp.setAdapter(new SupplierDetailPagerAdapter(getSupportFragmentManager()));
        vp.setOffscreenPageLimit(VP_LIMIT);
        vp.setCurrentItem(VP_INITIATE);
        vpIndicator.setupWithViewPager(vp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private class SupplierDetailPagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> listOfFragments;
        private List<String> listOfFrTitles;

        public SupplierDetailPagerAdapter(FragmentManager fm) {
            super(fm);
            listOfFragments = new ArrayList<Fragment>();
            listOfFrTitles = new ArrayList<String>();

            listOfFrTitles.add("Info");
            listOfFrTitles.add("Produk");
            listOfFrTitles.add("Review");

            Bundle b = new Bundle();
            b.putString("supplierId", supplierId);
            b.putInt("mode", ProductActivity.PRODUCT_BY_SUPPLIER_MODE);

            ProductListFragment productFragment = new ProductListFragment();
            productFragment.setArguments(b);
            SupplierProfileFragment profileFragment = new SupplierProfileFragment();
            profileFragment.setArguments(b);
            SupplierReviewListFragment reviewFragment = new SupplierReviewListFragment();
            reviewFragment.setArguments(b);

            listOfFragments.add(profileFragment);
            listOfFragments.add(productFragment);
            listOfFragments.add(reviewFragment);
        }

        @Override
        public int getCount() {
            return listOfFragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return listOfFragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listOfFrTitles.get(position);
        }
    }
}
