package com.raihangroup.raihanshop.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.BookReference;
import com.raihangroup.raihanshop.model.CartItem;
import com.raihangroup.raihanshop.model.ShippingInfo;
import com.raihangroup.raihanshop.model.User;
import com.google.gson.Gson;
import com.novoda.imageloader.core.model.ImageTagFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * step 6 of 9
 */
public class OrderReviewActivity extends AppCompatActivity {
    @BindView(R.id.toobal_order_review)
    Toolbar toobalOrderReview;
    private AQuery aq;
    private String paymentInfo;
    private String commissionInfo;
    private String sender = "";
    private Gson gs;
    private ImageTagFactory imageTagFactory;
    int total = 0, weight = 0, commission = 0;
    private ProgressDialog progDialog;
    private BookReference bookReference;
    private String rekOwner;
    private String norek;
    private int bankId;
    private DBHelper dbh;
    private String paid;
    private Calendar c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_review);
        ButterKnife.bind(this);
        aq = new AQuery(this);
        imageTagFactory = ImageTagFactory.newInstance(this, R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
        PrefStorage.newInstance(this);
        toobalOrderReview.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalOrderReview);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bookReference = getIntent().getParcelableExtra(Constants.INTENT_BOOK_2_ORD);
        sender = getIntent().getStringExtra(Constants.INTENT_ALTERNATE_SENDER);
        paymentInfo = getIntent().getStringExtra(Constants.INTENT_PAYMENT_INFO);
        bankId = getIntent().getIntExtra(Constants.INTENT_BANK_INFO, 0);
        commissionInfo = getIntent().getStringExtra(Constants.INTENT_COMISSION_INFO);
        rekOwner = getIntent().getStringExtra(Constants.INTENT_REKENINGOWNER_INFO);
        norek = getIntent().getStringExtra(Constants.INTENT_NOREK_INFO);
        paid = getIntent().getStringExtra(Constants.INTENT_JUMLAH_PAYMENT);
        c = (Calendar) getIntent().getSerializableExtra(Constants.INTENT_DATE_PAID);
        final ShippingInfo si = bookReference.getShipping();
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Menyelesaikan Order");
        progDialog.setMessage("Mohon Tunggu...");
        progDialog.setCancelable(false);

        gs = new Gson();
        dbh = new DBHelper(this);

        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 20, 0, 0);

        for (int i = 0; i < bookReference.getProducts().size(); i++) {
            ((LinearLayout) findViewById(R.id.llReview)).addView(
                    generateItemReView(bookReference.getProducts().get(i)));
        }

        int totalOngkir = getIntent().getIntExtra(Constants.INTENT_ONGKIR_TOTAL_INFO, 0);
        aq.id(R.id.tvBeratSum).text(weight + " gram");
        aq.id(R.id.tvPriceOngkir).text(Utility.formatPrice(totalOngkir));
        aq.id(R.id.tvNama).text(si.getName());
        aq.id(R.id.tvAddress).text(si.getAddress());
        aq.id(R.id.tvKecamatan).text(si.getSub_district());
        aq.id(R.id.tvZipCode).text(si.getZipcode());
        aq.id(R.id.tvKota).text(si.getCity());
        aq.id(R.id.tvProvince).text(si.getProvince());
        aq.id(R.id.tvPhone).text(si.getPhone());

        User user = new Gson().fromJson(PrefStorage.instance.getUserData(), User.class);
        aq.id(R.id.tvSender).text(user.getFullname());
        if (!si.getAlternate_sender().equals("")) {
            aq.id(R.id.tvSender).text(si.getAlternate_sender());
        }
        aq.id(R.id.tvPhoneSender).text(user.getPhonenumber());

        Log.d("debug", "telepon" + user.getPhonenumber());
        String pay = "Transfer ke Bank ";
        if (!paymentInfo.equals("0")) {
            switch (Integer.valueOf(paymentInfo)) {
                case 1:
                    pay += "BCA";
                    break;
                case 2:
                    pay += "BNI";
                    break;
                case 3:
                    pay += "BRI";
                    break;
                case 4:
                    pay += "Mandiri";
                    break;
                default:
                    break;
            }
            pay += "\na/n " + rekOwner;
            pay += " (" + dbh.findBankName(bankId) + ")";
            pay += "\nNomor Rekening " + norek;
        } else {
            pay = "Membayar Tunai";
            bankId = bankId == -1 ? 0 : bankId;
        }

        pay += "\nMembayar senilai : Rp. " + String.format(Locale.GERMAN, "%,d", Integer.parseInt(paid)) + " ,-";
        pay += "\nDibayar tertanggal : " + new SimpleDateFormat("dd MMMM yyyy", new Locale("in")).format(c.getTime());
        aq.id(R.id.tvPayment).text(pay);

        switch (Integer.valueOf(commissionInfo)) {
            case 1:
                aq.id(R.id.tvKomisi).text("Dengan Komisi");
                aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(total + totalOngkir));
                break;
            case 2:
                aq.id(R.id.tvKomisi).text("Tanpa Komisi");
                aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(total - commission + totalOngkir));
                break;
            default:
                break;
        }

        aq.id(R.id.btnNextAction).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PrefStorage.instance.getStokStat()) {
                    Utility.showDialogError(OrderReviewActivity.this,
                            "Mohon maaf Anda tidak dapat melakukan order karena toko sedang melakukan stock opname",
                            "Stock Opname");
                }

                JSONObject jo = new JSONObject();
                try {
                    jo.put("bank_id", bankId);
                    jo.put("account_number", rekOwner);
                    jo.put("account_owner", norek);
                    jo.put("raihan_bank_id", paymentInfo);
                    jo.put("total_paid", paid);
                    jo.put("date_paid", new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()));
                } catch (JSONException e1) {
                    Log.d("exception", String.valueOf(e1));
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("order_id", bookReference.getBooking_id());
                params.put("sender_name", si.getAlternate_sender());
                params.put("address", si.getAddress());
                params.put("phone", si.getPhone());
                params.put("zipcode", si.getZipcode());
                params.put("with_commission", String.valueOf(Integer.valueOf(commissionInfo) % 2));
                params.put("payment", jo.toString());

                aq.progress(progDialog).ajax(Constants.URL_ORDER_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object,
                                         AjaxStatus status) {
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                            String tmp = "000000";
                            try {
                                tmp = object.getString("order_number");
                            } catch (JSONException e) {
                            }

                            final String ord_numb = tmp;
                            new AlertDialog.Builder(OrderReviewActivity.this).setTitle("Sukses").
                                    setMessage("Terima kasih. Order Anda sedang diproses. Nomor ordernya " + tmp)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            Intent intent = new Intent(OrderReviewActivity.this, OrderNumberActivity.class);
                                            intent.putExtra(Constants.INTENT_ORDER_NUMBER, ord_numb);
                                            startActivity(intent);
                                        }
                                    }).create().show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private View generateItemReView(CartItem ci) {
        View r = getLayoutInflater().inflate(R.layout.package_item_tv, null);
        AQuery a = new AQuery(r);
        total += (ci.getPrice() * ci.getQty());
        commission += (ci.getCommission() * ci.getQty());
        weight += (ci.getWeight() * ci.getQty());
        a.id(R.id.tvTitle).text(ci.getName());
        a.id(R.id.tvHargaMasingProd).text(Utility.formatPrice(ci.getPrice()));
        a.id(R.id.tvBukuran).text(ci.getSize());
        a.id(R.id.tvBwarna).text(ci.getColor());
        a.id(R.id.tvBJumlah).text(String.valueOf(ci.getQty()) + " item");
        a.id(R.id.tvBberat).text(String.valueOf(ci.getQty() * ci.getWeight()) + " gram");
        a.id(R.id.tvSubTotal).text(Utility.formatPrice(ci.getPrice() * ci.getQty()));
        if (ImageLoaderApplication.getImageLoader().getLoader() != null) { //ambil image pertama
            a.id(R.id.imageProduct).getImageView().setTag(imageTagFactory.build(
                    Constants.UPLOAD_URL + ci.getImage1(), this));
            ImageLoaderApplication.getImageLoader().getLoader().load(a.id(R.id.imageProduct).getImageView());
        }
        return r;
    }
}