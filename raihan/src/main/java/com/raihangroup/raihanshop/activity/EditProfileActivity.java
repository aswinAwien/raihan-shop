package com.raihangroup.raihanshop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.BuildConfig;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.BankAdapter;
import com.raihangroup.raihanshop.adapter.CitySpinAdapter;
import com.raihangroup.raihanshop.adapter.ProvinceSpinAdapter;
import com.raihangroup.raihanshop.adapter.SubDistrictSpinAdapter;
import com.raihangroup.raihanshop.fragments.MediaSelectorFragment;
import com.raihangroup.raihanshop.fragments.MediaSelectorFragment.MediaSelectorListener;
import com.raihangroup.raihanshop.helper.BitmapUtil;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.ImageHelper;
import com.raihangroup.raihanshop.helper.ImageRenderer;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.City;
import com.raihangroup.raihanshop.model.Province;
import com.raihangroup.raihanshop.model.SubDistrict;
import com.raihangroup.raihanshop.model.UserProfile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity untuk Edit profile
 */
public class EditProfileActivity extends AppCompatActivity implements MediaSelectorListener {

    @BindView(R.id.toobal_edit_status)
    Toolbar toobalEditStatus;
    private Context context;
    private AQuery aq;
    private DBHelper dbh;
    private ProgressDialog progDialog;

    private ProvinceSpinAdapter provinceSpinAdapter;
    private CitySpinAdapter citySpinAdapter;
    private SubDistrictSpinAdapter subDistrictSpinAdapter;

    private UserProfile profile;

    private final static int CAMERA_REQUEST = 1101;
    private final static int GALLERY_REQUEST = 1102;
    private final static int SPINNER_INIT = 0;
    private final static int SPINNER_REFRESH_AGAIN = 1;
    private final static int SPINNER_FETCHING_DATA = 2;
    private final static String TAG = "Edit Profile";
    private Intent pictureActionIntent;
    private String imagePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        toobalEditStatus.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalEditStatus);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        aq = new AQuery(this);
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setMessage("Menyimpan profil baru");
        progDialog.setCancelable(false);

        dbh = new DBHelper(this);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            profile = b.getParcelable("userProfile");
            if (!profile.image.equals("img_default_user.png")) {
                aq.id(R.id.profileImage).image(Constants.BASE_URL + profile.image);
            }
            // when imageiew clicked call listener to show select media fragment
            aq.id(R.id.profileImage).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaSelectorFragment MSF = new MediaSelectorFragment();
                    MSF.show(getSupportFragmentManager(), "imageChooser");
                }
            });
            aq.id(R.id.etFullname).text(profile.fullname);
            aq.id(R.id.etBirthPlace).text(profile.birth_place);
            aq.id(R.id.etBirthDate).text(profile.birth_date);
            try {
                Date birthDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(profile.birth_date);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                aq.id(R.id.etBirthDate).text(sdf.format(birthDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            aq.id(R.id.etPhoneNumber).text(profile.phonenumber);
            aq.id(R.id.etJob).text(profile.job);

            aq.id(R.id.etAddress).text(profile.address);
            aq.id(R.id.etZipCode).text(profile.zipcode);

            aq.id(R.id.actvBank).text(profile.bank_name);
            aq.id(R.id.etBankAccountNumber).text(profile.account_bank);

        }

        provinceSpinAdapter = new ProvinceSpinAdapter(this, new ArrayList<Province>(), true);
        provinceSpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clearProvinceSpinAdapter(SPINNER_INIT);
        aq.id(R.id.spinProvince).adapter(provinceSpinAdapter);
        ((Spinner) aq.id(R.id.spinProvince).getView()).setOnItemSelectedListener(new ProvinceSpinListener());

        citySpinAdapter = new CitySpinAdapter(this, new ArrayList<City>(), true);
        citySpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clearCitySpinAdapter(SPINNER_INIT);
        aq.id(R.id.spinCity).adapter(citySpinAdapter);
        ((Spinner) aq.id(R.id.spinCity).getView()).setOnItemSelectedListener(new CitySpinListener());

        subDistrictSpinAdapter = new SubDistrictSpinAdapter(this, new ArrayList<SubDistrict>(), true);
        subDistrictSpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clearSubDistrictSpinAdapter(SPINNER_INIT);
        aq.id(R.id.spinSubDistrict).adapter(subDistrictSpinAdapter);

        // start from getting provinces
        getProvinces();

        BankAdapter ba = new BankAdapter(this, dbh.findBanks("Bank"), aq.id(R.id.actvBank).getEditText());
        ((AutoCompleteTextView) findViewById(R.id.actvBank)).setAdapter(ba);
        ((AutoCompleteTextView) findViewById(R.id.actvBank)).setOnItemClickListener(ba);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
                Log.d(TAG, "save performed");
                saveProfile();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // support methods

    /**
     * Save Profile changes
     */
    private void saveProfile() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id_member", profile.id);
        params.put("fullname", aq.id(R.id.etFullname).getText().toString());
        params.put("identity_number", profile.identity_number);
        params.put("birth_date", aq.id(R.id.etBirthDate).getText().toString());
        params.put("birth_place", aq.id(R.id.etBirthPlace).getText().toString());
        params.put("address", aq.id(R.id.etAddress).getText().toString());
        Province selectedProv = (Province) ((Spinner) aq.id(R.id.spinProvince).getView()).getSelectedItem();
        City selectedCity = (City) ((Spinner) aq.id(R.id.spinCity).getView()).getSelectedItem();
        SubDistrict selectedSubDistrict = (SubDistrict) ((Spinner) aq.id(R.id.spinSubDistrict).getView()).getSelectedItem();
        params.put("province", selectedProv.id);
        params.put("city", selectedCity.id);
        params.put("sub_district", selectedSubDistrict.id);
        params.put("zipcode", aq.id(R.id.etZipCode).getText().toString());
        params.put("job", aq.id(R.id.etJob).getText().toString());
        params.put("bank_id", String.valueOf(dbh.findIdBank(aq.id(R.id.actvBank).getText().toString())));
        params.put("bank_account", aq.id(R.id.etBankAccountNumber).getText().toString());
        params.put("phonenumber", aq.id(R.id.etPhoneNumber).getText().toString());
        if (imagePath != null) {
            params.put("file", ImageHelper.getImageFile(imagePath, context));
        }
        aq.progress(progDialog).ajax(Constants.URL_EDIT_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengubah profile. Silakan mencoba kembali", "OK", null);

                    return;
                }

                try {
                    if (object.getString("success").equals("true")) {
                        Log.d(TAG, "message " + object.toString());
                        Utility.toast(context, "Berhasil mengubah profile");
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengubah profile. Silakan mencoba kembali", "OK", null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getProvinces() {
        clearProvinceSpinAdapter(SPINNER_FETCHING_DATA);

        Map<String, String> unusedParams = new HashMap<String, String>();
        aq.ajax(Constants.URL_PROVINCE_API, unusedParams, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    clearProvinceSpinAdapter(SPINNER_REFRESH_AGAIN);
                    return;
                }

                Gson gs = new Gson();
                ArrayList<Province> listOfProvs = gs.fromJson(object.toString(),
                        new TypeToken<List<Province>>() {
                        }.getType());

                int selectedProvIdx = -1;

                provinceSpinAdapter.clear();
                for (int i = 0; i < listOfProvs.size(); i++) {
                    Province prov = listOfProvs.get(i);
                    if (prov.id.equals(profile.province)) {
                        selectedProvIdx = i;
                    }

                    provinceSpinAdapter.add(prov);
                }

                clearCitySpinAdapter(SPINNER_INIT);
                // select first
                if (selectedProvIdx != -1) {
                    ((Spinner) aq.id(R.id.spinProvince).getView()).setSelection(selectedProvIdx);
                }
                // get city from prov selection
                if (listOfProvs.size() > 0) {
                    // here we have selected item
                    Province prov = (Province) aq.id(R.id.spinProvince).getSelectedItem();
                    // then get cities
                    getCities(prov.id);
                }
            }
        });
    }

    private void getCities(String provinceId) {
        clearCitySpinAdapter(SPINNER_FETCHING_DATA);

        Map<String, String> params = new HashMap<String, String>();
        params.put("id_province", provinceId);
        aq.ajax(Constants.URL_CITY_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    clearCitySpinAdapter(SPINNER_REFRESH_AGAIN);
                    return;
                }

                Gson gs = new Gson();
                ArrayList<City> listOfCities = gs.fromJson(object.toString(),
                        new TypeToken<List<City>>() {
                        }.getType());

                int selectedCityIdx = -1;

                citySpinAdapter.clear();
                for (int i = 0; i < listOfCities.size(); i++) {
                    City city = listOfCities.get(i);
                    if (city.id.equals(profile.city)) {
                        selectedCityIdx = i;
                    }
                    citySpinAdapter.add(city);
                }

                clearSubDistrictSpinAdapter(SPINNER_INIT);
                // select first
                if (selectedCityIdx != -1) {
                    ((Spinner) aq.id(R.id.spinCity).getView()).setSelection(selectedCityIdx);
                }
                if (listOfCities.size() > 0) {
                    // here we have selected item
                    City city = (City) aq.id(R.id.spinCity).getSelectedItem();
                    // then get cities
                    getSubDistrict(city.id);
                }
            }
        });
    }

    private void getSubDistrict(String cityId) {
        clearSubDistrictSpinAdapter(SPINNER_FETCHING_DATA);

        Map<String, String> params = new HashMap<String, String>();
        params.put("id_city", cityId);
        aq.ajax(Constants.URL_SUB_DISTRICT_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    clearSubDistrictSpinAdapter(SPINNER_REFRESH_AGAIN);
                    return;
                }

                Gson gs = new Gson();
                ArrayList<SubDistrict> listOfSubDistricts = gs.fromJson(object.toString(),
                        new TypeToken<List<SubDistrict>>() {
                        }.getType());

                subDistrictSpinAdapter.clear();
                for (SubDistrict subDistrict : listOfSubDistricts) {
                    subDistrictSpinAdapter.add(subDistrict);
                }
            }
        });
    }

    private void clearProvinceSpinAdapter(int mode) {
        provinceSpinAdapter.clear();
        if (mode == SPINNER_INIT) {
            provinceSpinAdapter.add(new Province("-1", "Pilihan provinsi"));
        } else if (mode == SPINNER_REFRESH_AGAIN) {
            provinceSpinAdapter.add(new Province("-1", "Silakan refresh"));
        } else if (mode == SPINNER_FETCHING_DATA) {
            provinceSpinAdapter.add(new Province("-1", "Memuat data..."));
        }
    }

    private void clearCitySpinAdapter(int mode) {
        citySpinAdapter.clear();
        if (mode == SPINNER_INIT) {
            citySpinAdapter.add(new City("-1", "-1", "Pilihan kota"));
        } else if (mode == SPINNER_REFRESH_AGAIN) {
            citySpinAdapter.add(new City("-1", "-1", "Silakan refresh"));
        } else if (mode == SPINNER_FETCHING_DATA) {
            citySpinAdapter.add(new City("-1", "-1", "Memuat data..."));
        }
    }

    private void clearSubDistrictSpinAdapter(int mode) {
        subDistrictSpinAdapter.clear();
        if (mode == SPINNER_INIT) {
            subDistrictSpinAdapter.add(new SubDistrict("-1", "-1", "Pilihan kecamatan"));
        } else if (mode == SPINNER_REFRESH_AGAIN) {
            subDistrictSpinAdapter.add(new SubDistrict("-1", "-1", "Silakan refresh"));
        } else if (mode == SPINNER_FETCHING_DATA) {
            subDistrictSpinAdapter.add(new SubDistrict("-1", "-1", "Memuat data..."));
        }
    }

    int y = 0, m, d;


    // click methods

    public void onDateChoose(View v) {
        DatePickerFragment dateFragment = new DatePickerFragment();
        dateFragment.show(getSupportFragmentManager(), "datePicker");
    }


    @Override
    public void onCameraButtonClick(DialogFragment dialog) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST);
        }else {
            pictureActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "reyhan_client.jpg");
            if(Build.VERSION.SDK_INT >= 24){
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        file);
                pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
            }else {
                pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(file));
            }
            startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
        }
    }


    @Override
    public void onGalleryButtonClick(DialogFragment dialog) {
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},GALLERY_REQUEST);
        }else {
            pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
            pictureActionIntent.setType("image/*");
            startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            ImageView ivProfilePict = (ImageView) aq.id(R.id.profileImage).getView();
            if (requestCode == GALLERY_REQUEST) {
                if (data != null) {
                    Uri imageUri = data.getData();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            String id = imageUri.getLastPathSegment()
                                    .split(":")[1];
                            final String[] imageColumns = {MediaStore.Images.Media.DATA};
                            final String imageOrderBy = null;
                            Uri uri = ImageHelper.getMediaStorageUri();
                            Cursor imageCursor = managedQuery(uri,
                                    imageColumns, MediaStore.Images.Media._ID
                                            + "=" + id, null, imageOrderBy);
                            if (imageCursor.moveToFirst()) {
                                imagePath = imageCursor
                                        .getString(imageCursor
                                                .getColumnIndex(MediaStore.Images.Media.DATA));
                            }
                            Log.d("filepath", "kitkat = " + imagePath);
                        } catch (Exception e) {
                            Log.d("filepath", "isi ekspesi uri kitkat = " + imageUri);
                            imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                            Log.d("filepath", "galery path kitkat = " + imagePath);
                        }
                    } else {
                        Log.d("filepath", "isi uri = " + imageUri);
                        imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                        Log.d("filepath", "galery path  = " + imagePath);
                    }
                    new ImageRenderer(ivProfilePict, data, imagePath).execute();
                } else {
                    Toast.makeText(this, "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                Log.d("debug", "on activity camera");
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "reyhan_client.jpg");
                imagePath = Uri.fromFile(file).getPath();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imagePath, options);
                int width = options.outWidth;
                int height = options.outHeight;
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Log.d("debug", "Image.Width = " + width);
                Log.d("debug", "display.getWidth() = " + display.getWidth());
                float scale = (float) display.getWidth() / (float) width;
                Bitmap fixed = BitmapUtil.fixOrientation(context, ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                        (int) (height * scale)), Uri.fromFile(file));
                ivProfilePict.setImageBitmap(fixed);
                ivProfilePict.setVisibility(View.VISIBLE);
            } else {
                ivProfilePict.setVisibility(View.GONE);
            }
        }
    }

    @SuppressLint("ValidFragment")
    class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if (y == 0) {

                final Calendar c = Calendar.getInstance();
                y = c.get(Calendar.YEAR);
                m = c.get(Calendar.MONTH);
                d = c.get(Calendar.DAY_OF_MONTH);
            }

            return new DatePickerDialog(getActivity(), this, y, m, d);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            y = year;
            m = month;
            d = day;
            aq.id(R.id.etBirthDate).text(year + "-" + Utility.pad(month + 1) + "-" + Utility.pad(day));
        }

        public DatePickerFragment() {

        }
    }


    // inner class listener

    private class ProvinceSpinListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            clearCitySpinAdapter(SPINNER_INIT);

            Province province = (Province) aq.id(R.id.spinProvince).getSelectedItem();
            if (!province.id.equals("-1")) {
                getCities(province.id);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // do nothing
        }
    }

    private class CitySpinListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            clearSubDistrictSpinAdapter(SPINNER_INIT);

            City city = (City) aq.id(R.id.spinCity).getSelectedItem();
            if (!city.id.equals("-1")) {
                getSubDistrict(city.id);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // do nothing
        }
    }
}