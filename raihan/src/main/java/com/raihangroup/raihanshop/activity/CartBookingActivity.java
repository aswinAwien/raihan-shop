package com.raihangroup.raihanshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CartItemsProcessor;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.SupplierShipCalc;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.CartItem;
import com.raihangroup.raihanshop.model.ServiceShippingCost;
import com.raihangroup.raihanshop.model.ShippingInfo;
import com.raihangroup.raihanshop.model.SubDistrictJNE;
import com.raihangroup.raihanshop.model.SupplierShippingCost;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.novoda.imageloader.core.model.ImageTagFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity to creating booking to shopping cart
 * steps 2 of 9
 */

public class CartBookingActivity extends AppCompatActivity {
    @BindView(R.id.toobal_cart_booking)
    Toolbar toobalCartBooking;
    private AQuery aq;
    private ImageTagFactory imageTagFactory;
    private List<CartItem> listOfCartItems;
    private Gson gs;
    private ProgressDialog progDialog;
    private String idChooseS;
    private int totalItemPrice;
    private int totalItemWeight;
    private int totalShippingCost;
    private String destinationCode;
    boolean selectedFromJNE;
    AutoCompleteTextView actvSubdirJNE;
    EditText etName;
    private static final String TAG = "CART BOOKING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_booking);
        ButterKnife.bind(this);
        selectedFromJNE = false;
        aq = new AQuery(this);
        imageTagFactory = ImageTagFactory.newInstance(this, R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Book Produk");
        progDialog.setMessage("Mohon Tunggu...");
        progDialog.setCancelable(false);
        toobalCartBooking.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalCartBooking);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String jsonArrCart = getIntent().getStringExtra(Constants.INTENT_CART_2_BOOK);
        if (Utility.isAdmin()) {
            idChooseS = String.valueOf(getIntent().getIntExtra(Constants.INTENT_CART_ADMIN_RESELLERID, -1));
        }
        gs = new Gson();
        // get initial cart items
        listOfCartItems = gs.fromJson(jsonArrCart, new TypeToken<List<CartItem>>() {
        }.getType());
        // add cart item views
        for (int i = 0; i < listOfCartItems.size(); i++) {
            CartItem cartItem = listOfCartItems.get(i);
            LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 10, 0, 0);
            ((LinearLayout) findViewById(R.id.llBookContainer)).addView(
                    generateCartItemView(cartItem), lp);
        }
        calculateWhatMattersInCartItems();

        // init some text views
        aq.id(R.id.tvBeratSum).text(totalItemWeight + " gram");
        // set shipping price
        aq.id(R.id.tvPriceOngkir).text(Utility.formatPrice(totalShippingCost));
        // set total price with total item price
        aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(totalItemPrice));
        etName = (EditText) findViewById(R.id.etBName);
        actvSubdirJNE = ((AutoCompleteTextView) findViewById(R.id.avKecamatanJNE));
        actvSubdirJNE.setTextColor(Color.BLACK);
        actvSubdirJNE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, final int start, int before, int count) {
                selectedFromJNE = false;
                if (s.length() > 2) {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", Constants.JNE_API_USERNAME);
                    params.put("api_key", Constants.JNE_API_KEY);
                    Log.d("JNE", "params = " + params.toString());
                    String url = Constants.URL_JNE_GET_DEST + s;
                    Log.d("JNE", "query = " + url);
                    aq.ajax(url, params, String.class, new AjaxCallback<String>() {
                        @Override
                        public void callback(String url, String object, AjaxStatus status) {
                        if (status.getCode() != Constants.HTTP_RESPONSE_INTERNALSERVERERROR) {
                                Utility.toast(CartBookingActivity.this, "Gagal mendapat data dari" +
                                        " JNE, silakan coba beberapa saat lagi");
                            Log.i(TAG, "callback: " + object.toString());
                                return;
                            }else if(status.getCode()==Constants.HTTP_RESPONSE_PAGENOTFOUND){
                            Utility.toast(CartBookingActivity.this,"Maaf Destinasi Pengiriman tidak diketahui");
                        }

                            SubDistrictJNE sdJNE = new Gson().fromJson(object, SubDistrictJNE.class);
                            ArrayAdapter<SubDistrictJNE.SubDistrict> subDistrictJNEAdapter =
                                    new ArrayAdapter<SubDistrictJNE.SubDistrict>(CartBookingActivity.this,
                                            android.R.layout.simple_dropdown_item_1line, sdJNE.detail) {
                                        public View getView(int position, View convertView, ViewGroup parent) {
                                            convertView = super.getDropDownView(position, convertView, parent);
                                            TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
                                            tv.setTextColor(Color.BLACK);
                                            return convertView;
                                        }
                                    };
                            actvSubdirJNE.setAdapter(subDistrictJNEAdapter);
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        actvSubdirJNE.setOnItemClickListener(new JNESubDistrictACTVListener());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // support methods

    /**
     * Inflate package item view for ordered product.
     *
     * @param ci, cart item
     * @return view
     */
    private View generateCartItemView(CartItem ci) {
        View r = getLayoutInflater().inflate(R.layout.package_item_tv, null);
        AQuery a = new AQuery(r);

        a.id(R.id.tvTitle).text(ci.getName());
        a.id(R.id.tvHargaMasingProd).text(Utility.formatPrice(ci.getPrice()));
        a.id(R.id.tvBukuran).text(ci.getSize());
        a.id(R.id.tvBwarna).text(ci.getColor());
        a.id(R.id.tvBJumlah).text(String.valueOf(ci.getQty()) + " item");
        a.id(R.id.tvBberat).text(String.valueOf(ci.getQty() * ci.getWeight()) + " gram");
        a.id(R.id.tvSubTotal).text(Utility.formatPrice(ci.getPrice() * ci.getQty()));

        // get first image
        if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
            a.id(R.id.imageProduct).getImageView().setTag(imageTagFactory.build(
                    Constants.UPLOAD_URL + ci.getImage1(), this));
            ImageLoaderApplication.getImageLoader().getLoader().load(a.id(R.id.imageProduct).getImageView());
        }
        return r;
    }

    /**
     * Calculate total weight and price.
     */
    private void calculateWhatMattersInCartItems() {
        for (CartItem ci : listOfCartItems) {
            totalItemWeight += (ci.getWeight() * ci.getQty());
            totalItemPrice += (ci.getPrice() * ci.getQty());
        }
    }

    // click methods

    public void onClickBooking(View v) throws JSONException {
        boolean checkPassed = false;
        etName.setError(null);
        actvSubdirJNE.setError(null);
        if (Utility.isNullOrBlank(aq.id(R.id.etBName).getText().toString())) {
            Utility.showDialogError(this, "Isilah Nama Pemesan", "Terjadi Kesalahan");
            etName.setError("Nama Pemesan tidak boleh kosong");
            etName.requestFocus();
        } else if (Utility.isNullOrBlank(aq.id(R.id.avKecamatanJNE).getText().toString())) {
            Utility.showDialogError(this, "Isilah Tujuan Pengiriman", "Terjadi Kesalahan");
            actvSubdirJNE.setError("Tujuan Kecamatan tidak boleh kosong");
            actvSubdirJNE.requestFocus();
        } else if (!selectedFromJNE) {
            Utility.showDialogError(this, "Pilihlah kecamatan sesuai daftar", "Terjadi Kesalahan");
            actvSubdirJNE.setError("Pilihlah kecamatan sesuai daftar");
            actvSubdirJNE.requestFocus();
        } else {
            checkPassed = true;
        }

        if (!checkPassed) return;
        ShippingInfo si = new ShippingInfo();
        si.setName(aq.id(R.id.etBName).getText().toString());
        si.setCode(destinationCode);
        Map<String, String> params = new HashMap<String, String>();
        params.put("reseller_id", String.valueOf(PrefStorage.instance.getUserId()));
        if (Utility.isAdmin()) {
            params.put("reseller_id", idChooseS);
            params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
        }
        params.put("shipping", gs.toJson(si, ShippingInfo.class));
        JSONArray shippingJArr = new JSONArray();
        LinearLayout ll = (LinearLayout) aq.id(R.id.llKirimMethod).getView();
        for (int i = 1; i < ll.getChildCount(); i++) {
            // radio group in first position in bordered linear layout
            RadioGroup rg = ((RadioGroup) ((LinearLayout) ll.getChildAt(i)).getChildAt(1));
            // get POS input view in second position
            View POSInputView = (((LinearLayout) ll.getChildAt(i)).getChildAt(2));
            AQuery a = new AQuery(POSInputView);
            EditText etPOSInput = a.id(R.id.etShippingCost).getEditText();
            int checkedRbId = rg.getCheckedRadioButtonId();
            RadioButton rb = (RadioButton) rg.findViewById(checkedRbId);
            if (checkedRbId == -1) {
                Utility.showDialogError(this, "Isilah jenis pengiriman", "Terjadi Kesalahan");
                return;
            }

            TagOfServiceCostRadioGroup tag = (TagOfServiceCostRadioGroup) rb.getTag();

            JSONObject jo = new JSONObject();
            jo.put("shipping_method_id", String.valueOf(checkedRbId));
            jo.put("post_price", tag.ssc.getServicePrice());
            if (rb.getText().toString().equals("POS")) {
                String pricePOS = etPOSInput.getText().toString().replaceAll("\\.", "");
                if (pricePOS.length() == 0 || Integer.parseInt(pricePOS) == 0) {
                    Utility.showDialogError(this, "Isilah harga total untuk POS", "Terjadi Kesalahan");
                    return;
                }
                jo.put("post_price", pricePOS);
            }

            StringBuilder sb = new StringBuilder();
            for (CartItem ci : listOfCartItems) {
                if (tag.ssc.getSupplierId().equals(ci.id_supplier)) {
                    sb.append(ci.getId()).append(',');
                }
            }
            sb.deleteCharAt(sb.length() - 1); // sb must not be null
            jo.put("id_cart", sb.toString());
            shippingJArr.put(jo);
        }

        params.put("shipping_cost", shippingJArr.toString());
        Log.d("JNE", "params booking " + params);
        int totalShippingPrice = Utility.deformatPrice(
                aq.id(R.id.tvPriceOngkir).getText().toString());
        if (totalShippingPrice == 0) {
            Utility.showDialogError(this, "Ongkos kirim tidak dapat bernilai 0", "Terjadi Kesalahan");
            return;
        }

        aq.progress(progDialog).ajax(Constants.URL_BOOK_API, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Utility.toast(CartBookingActivity.this, "Booking Berhasil!");
                    Log.d("JNE", "Order Code = " + object);
                    PrefStorage.instance.resetCart();
                    Intent intent = new Intent(CartBookingActivity.this, BookingNameActivity.class);
                    intent.putExtra(Constants.INTENT_BOOKING_NAME, aq.id(R.id.etBName).getText().toString());
                    startActivity(intent);
                } else {
                    Utility.showDialogError(CartBookingActivity.this, "Data bermasalah. Tidak berhasil booking. Silakan coba lagi", "Terjadi Kesalahan");
                }
            }
        });
    }

    private class TagOfServiceCostRadioGroup {
        public SupplierShipCalc ssc;
        public int shipSupplierViewPos;
    }

    // item click listener

    private class JNESubDistrictACTVListener implements OnItemClickListener {

        private int[] arrTotalServicePrice;
        private HashMap<String, SupplierShipCalc> hmOfShipCalcPerSupplierId;

        public JNESubDistrictACTVListener() {
            hmOfShipCalcPerSupplierId = CartItemsProcessor.collectWeightPerSupplierId(listOfCartItems, false);
        }

        @Override
        public void onItemClick(AdapterView<?> listView, View view,
                                int position, long id) {
            selectedFromJNE = true;
            SubDistrictJNE.SubDistrict sDistrictJNE = (SubDistrictJNE.SubDistrict) listView.getItemAtPosition(position);
            destinationCode = sDistrictJNE.code;
            Log.d("JNE", sDistrictJNE.label + ";" + sDistrictJNE.code + " choosen");

            ProgressBar progBar = new ProgressBar(CartBookingActivity.this, null, android.R.attr.progressBarStyleSmall);
            //progBar.setId(99); // ???? id si progress bar di keca city adaapter
//            progBar.setPadding(5, 5, 5, 5);
            progBar.setVisibility(View.INVISIBLE);
            ProgressDialog dialog = ProgressDialog.show(CartBookingActivity.this, "",
                    "Kalkulasi Ongkos Pengiriman. Mohon Tunggu...", true);
            LinearLayout shippingChoiceView = (LinearLayout) aq.id(R.id.llKirimMethod).getView();
            shippingChoiceView.removeAllViews();
            shippingChoiceView.addView(progBar, 25, 25);

            // clear soft input from sub-district auto complete
            Utility.clearSoftInputFromView(CartBookingActivity.this, aq.id(R.id.avKecamatanJNE).getView());

            Map<String, String> params = new HashMap<String, String>();
            params.put("sub_district", sDistrictJNE.code);
            params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
            if (!Utility.isNullOrBlank(idChooseS)) {
                params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
                params.put("member_id", idChooseS);
            }

            aq.progress(dialog).ajax(Constants.URL_SHIPPING_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {

                @Override
                public void callback(String url, JSONArray objects, AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        List<SupplierShippingCost> listOfShipSuppliers = new Gson().fromJson(objects.toString(),
                                new TypeToken<List<SupplierShippingCost>>() {
                                }.getType());
                        Log.d(TAG, listOfShipSuppliers.size() + " supplier shipping costs from server");
                        if (listOfShipSuppliers.size() != hmOfShipCalcPerSupplierId.size()) {
                            Log.d(TAG, "!!!! something wrong happens");
                        }
                        // init total service price array
                        arrTotalServicePrice = new int[listOfShipSuppliers.size()];
                        for (int i = 0; i < arrTotalServicePrice.length; i++) {
                            arrTotalServicePrice[i] = -1;
                        }

                        // for each shipping supplier, create related view for user choices
                        for (int i = 0; i < listOfShipSuppliers.size(); i++) {
                            SupplierShippingCost shipSupplier = listOfShipSuppliers.get(i);
                            // find appropriate supplier shipping calculation
                            // this info needed for radio group view
                            SupplierShipCalc appropriateSsc = null;
                            Set<String> keySet = hmOfShipCalcPerSupplierId.keySet();
                            for (String key : keySet) {
                                SupplierShipCalc ssc = hmOfShipCalcPerSupplierId.get(key);
                                if (ssc.getSupplierId().equalsIgnoreCase(shipSupplier.supplier_id)) {
                                    appropriateSsc = ssc;
                                    break;
                                }
                            }
                            TagOfServiceCostRadioGroup tag = new TagOfServiceCostRadioGroup();
                            tag.ssc = appropriateSsc;
                            tag.shipSupplierViewPos = i;

                            LinearLayout supplierView = generateShippingSupplierView(shipSupplier, tag);
                            // find view again here for llKirimMethod
                            LinearLayout shippingChoiceView = (LinearLayout) aq.id(R.id.llKirimMethod).getView();
                            shippingChoiceView.addView(supplierView);
                            aq.id(supplierView).margin(0, 10, 0, 0); // to make supplier view works, set margin after added to parent
                        } // end of for-loop
                    }
                } // end of callback method
            });
        }


        private LinearLayout generateShippingSupplierView(SupplierShippingCost shipSupplier,
                                                          TagOfServiceCostRadioGroup tag) {
            // create view
            LinearLayout llTmp = new LinearLayout(CartBookingActivity.this);
            llTmp.setBackgroundResource(R.drawable.listborder);
            llTmp.setOrientation(LinearLayout.VERTICAL);

            TextView tv = new TextView(CartBookingActivity.this);
            tv.setPadding(10, 10, 10, 10);
            tv.setBackgroundColor(CartBookingActivity.this.getResources().getColor(R.color.gray));
            String addword = "";
            if (shipSupplier.fullname != null) {
                addword = " (" + shipSupplier.fullname + ")";
            }
            tv.setText("Lokasi barang di " + shipSupplier.location + addword);

            llTmp.addView(tv);
            llTmp.addView(generateRadioGroupView(shipSupplier.shipping, tag));

            View POSInputView = LayoutInflater.from(CartBookingActivity.this)
                    .inflate(R.layout.item_pos_input_price, null, false);
            AQuery a = new AQuery(POSInputView);
            EditText etPOSInput = a.id(R.id.etShippingCost).getEditText();
            etPOSInput.addTextChangedListener(new ThousandFormatTextWatcher(etPOSInput, tag));
            POSInputView.setVisibility(View.GONE);
            llTmp.addView(POSInputView);

            return llTmp;
        }

        private RadioGroup generateRadioGroupView(List<ServiceShippingCost> listOfShippingCosts,
                                                  TagOfServiceCostRadioGroup tag) {
            RadioGroup rg = new RadioGroup(CartBookingActivity.this);

            for (ServiceShippingCost serviceShipCost : listOfShippingCosts) {
                RadioButton rb = new RadioButton(CartBookingActivity.this);
                if (serviceShipCost.services.equalsIgnoreCase("POS")) {
                    rb.setText(serviceShipCost.services);
                } else {
                    rb.setText(serviceShipCost.services + " (" +
                            serviceShipCost.etd + " hari) | " + Utility.formatPrice(serviceShipCost.price));
                }

                // each radio button needs specific tag. create the new one
                TagOfServiceCostRadioGroup specificTag = new TagOfServiceCostRadioGroup();
                specificTag.shipSupplierViewPos = tag.shipSupplierViewPos;
                try {
                    specificTag.ssc = (SupplierShipCalc) tag.ssc.clone();
                } catch (CloneNotSupportedException e) {
                    Log.e(TAG, "error", e);
                }
                specificTag.ssc.setServicePrice(serviceShipCost.price);

                rb.setId(serviceShipCost.id); // set radio button id with ship service cost. this id contains many related information

                rb.setTag(specificTag); // this tag so important for onCheckedChanged()
                rg.setOnCheckedChangeListener(new ShippingCostRadioGroupViewListener());
                rg.addView(rb);
            }

            return rg;
        }

        private class ShippingCostRadioGroupViewListener implements OnCheckedChangeListener {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int totalPrice = Utility.deformatPrice(
                        aq.id(R.id.tvPriceTotal).getText().toString());
                int totalShippingPrice = Utility.deformatPrice(
                        aq.id(R.id.tvPriceOngkir).getText().toString());
                RadioButton rb_c = (RadioButton) group.findViewById(checkedId);
                TagOfServiceCostRadioGroup tag = (TagOfServiceCostRadioGroup) rb_c.getTag();
                int shipSupplierViewPos = tag.shipSupplierViewPos;
                SupplierShipCalc appropriateSsc = tag.ssc;
                int totalPriceOfServiceCost = appropriateSsc.calculateShippingPrice();
                if (arrTotalServicePrice[shipSupplierViewPos] != -1) {
                    totalPrice -= arrTotalServicePrice[shipSupplierViewPos];
                    totalShippingPrice -= arrTotalServicePrice[shipSupplierViewPos];
                }
                arrTotalServicePrice[shipSupplierViewPos] = totalPriceOfServiceCost;
                totalPrice += arrTotalServicePrice[shipSupplierViewPos];
                totalShippingPrice += arrTotalServicePrice[shipSupplierViewPos];
                aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(totalPrice));
                aq.id(R.id.tvPriceOngkir).text(Utility.formatPrice(totalShippingPrice));
                //AQuery r = new AQuery(group);
                //RadioButton checkedRb = (RadioButton) r.id(checkedId).getView();
                LinearLayout ll = (LinearLayout) aq.id(R.id.llKirimMethod).getView();
                // get POS input view in second position
                View POSInputView = (((LinearLayout) ll.getChildAt(1 + tag.shipSupplierViewPos))
                        .getChildAt(2));
                AQuery a = new AQuery(POSInputView);
                EditText etPOSInput = a.id(R.id.etShippingCost).getEditText();
                if (rb_c.getText().equals("POS")) {
                    POSInputView.setVisibility(View.VISIBLE);
                } else {
                    POSInputView.setVisibility(View.GONE);
                    // reset text when it is gone
                    etPOSInput.setText("");
                }
            }
        }

        private class ThousandFormatTextWatcher implements TextWatcher {

            /**
             * edittext yang akan dipengaruhi watcher ini
             */
            private EditText et;
            private TagOfServiceCostRadioGroup tag;

            public ThousandFormatTextWatcher(EditText et, TagOfServiceCostRadioGroup tag) {
                this.et = et;
                this.tag = tag;
            }

            @Override
            public void afterTextChanged(Editable st) {
                // just found this mechanism in the net :p ???
                et.removeTextChangedListener(this);
                int inilen, endlen;
                inilen = et.getText().length();

                String s = null;
                try { // german karena kita pingin separatornya pake titik. kebetulan sama dengan INA
                    s = String.format(Locale.GERMANY, "%,d", Long.parseLong(st.toString().replaceAll("\\.", "")));

                } catch (NumberFormatException e) {
                }

                int cp = et.getSelectionStart();
                et.setText(s);
                endlen = et.getText().length();

                int sel = (cp + (endlen - inilen));
                if (sel > 0 && sel <= et.getText().length()) {
                    et.setSelection(sel);
                } else {
                    et.setSelection(et.getText().length());
                }

                et.addTextChangedListener(this);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                LinearLayout ll = (LinearLayout) aq.id(R.id.llKirimMethod).getView();
                RadioGroup rg = ((RadioGroup) ((LinearLayout) ll.getChildAt(1 + tag.shipSupplierViewPos)).getChildAt(1));
                int checkedRbId = rg.getCheckedRadioButtonId();
                RadioButton rb = (RadioButton) rg.findViewById(checkedRbId);
                if (!rb.getText().toString().equals("POS")) {
                    return;
                }

                // perform this when you are in POS radio button
                int totalPrice = Utility.deformatPrice(
                        aq.id(R.id.tvPriceTotal).getText().toString());
                int totalShippingPrice = Utility.deformatPrice(
                        aq.id(R.id.tvPriceOngkir).getText().toString());

                int shipSupplierViewPos = tag.shipSupplierViewPos;

                if (arrTotalServicePrice[shipSupplierViewPos] != -1) {
                    totalPrice -= arrTotalServicePrice[shipSupplierViewPos];
                    totalShippingPrice -= arrTotalServicePrice[shipSupplierViewPos];
                }

                String text = s.toString().replaceAll("\\.", "");
                int priceOfPOSCost = 0;
                if (text.length() != 0) {
                    priceOfPOSCost = Integer.parseInt(text);
                }
                arrTotalServicePrice[shipSupplierViewPos] = priceOfPOSCost;

                totalPrice += arrTotalServicePrice[shipSupplierViewPos];
                totalShippingPrice += arrTotalServicePrice[shipSupplierViewPos];

                aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(totalPrice));
                aq.id(R.id.tvPriceOngkir).text(Utility.formatPrice(totalShippingPrice));
            }
        }
    }
}