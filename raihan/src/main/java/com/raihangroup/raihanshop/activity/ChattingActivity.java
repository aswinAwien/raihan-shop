package com.raihangroup.raihanshop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources.Theme;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.BuildConfig;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ChatReplyAdapter;
import com.raihangroup.raihanshop.fragments.GroupChatFragment;
import com.raihangroup.raihanshop.fragments.MediaSelectorFragment;
import com.raihangroup.raihanshop.helper.BitmapUtil;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.ImageHelper;
import com.raihangroup.raihanshop.helper.ImageRenderer;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.TimeAgo;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.ChatItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChattingActivity extends AppCompatActivity {


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Setup spinner
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(new MyAdapter(
                toolbar.getContext(),
                new String[]{
                        "Group Chat Upline",
                        "Group Chat Downline",
                }));

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // When the given dropdown item is selected, show its contents in the
                // container view.
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, PlaceholderFragment.newInstance(position + 1, ChattingActivity.this))
                        .commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chatting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            PlaceholderFragment.refreshData(true);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private static class MyAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {
        private final Helper mDropDownHelper;

        public MyAdapter(Context context, String[] objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new Helper(context);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                // Inflate the drop down using the helper's LayoutInflater
                LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(getItem(position));

            return view;
        }

        @Override
        public Theme getDropDownViewTheme() {
            return mDropDownHelper.getDropDownViewTheme();
        }

        @Override
        public void setDropDownViewTheme(Theme theme) {
            mDropDownHelper.setDropDownViewTheme(theme);
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements MediaSelectorFragment.MediaSelectorListener {
        private final static int CAMERA_REQUEST = 1101;
        private final static int GALLERY_REQUEST = 1102;

        private static ListView listView;
        private EditText etReplyMessage;
        private ImageView ivReplyMessage;
        private ImageView ivAttachImage;
        private ImageView ivAttachment;
        private ProgressDialog progDialog;
        static ArrayList<ChatItem> listOfConversation;
        private static Button btnLoadMore;
        private static AQuery aq;
        private static ChatReplyAdapter chatReplyAdapter;
        static boolean endOfList;
        public static int page;
        public static String mode;
        static Context context;
        private String imagePath;
        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, Context context) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
//            args.putInt("mode", sectionNumber);
            mode = String.valueOf(sectionNumber);
            fragment.setArguments(args);
            context = context;

            aq = new AQuery(context);
            PrefStorage.newInstance(context);
            PrefStorage.newInstance(context);
            listOfConversation = new ArrayList<>();
            return fragment;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            progDialog = new ProgressDialog(getActivity());
            progDialog.setTitle("Loading");
            progDialog.setMessage("Mengirim Pesan");
//            mode = String.valueOf(getArguments().get("mode"));
            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }
                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (firstVisibleItem != 0 || visibleItemCount == 0) {
                        // The first child VIEW is not the first ITEM
                        btnLoadMore.post(new Runnable() {
                            @Override
                            public void run() {
                                btnLoadMore.setVisibility(View.GONE);
                            }
                        });
                    } else if (view.getChildCount() != 0 && view.getChildAt(0).getTop() == 0) {
                        // The first child VIEW is the first ITEM and it has child and the first ITEM/VIEW position is at 0
                        if (!endOfList) {
                            btnLoadMore.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            btnLoadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getChatList(++page, true);
                }
            });

            ivReplyMessage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String message = aq.id(R.id.etReplyMessage).getText().toString();
                    if (message.length() > 0) {
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                        params.put("type", "" + mode);
                        params.put("message", message);
                        if (imagePath != null) {
                            params.put("file", ImageHelper.getImageFile(imagePath, getActivity()));
                        }
                        aq.progress(progDialog).ajax(Constants.URL_REPLY_CHAT, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                Log.d("debug", "stats =" + status.getCode() + " " +  status.getError());
                                etReplyMessage.setText("");
                                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                    Utility.toast(getActivity(), "Pesan berhasil dikirim");
                                    ivAttachment.setImageDrawable(null);
                                    ivAttachment.setVisibility(View.GONE);
                                    refreshData(false);
                                } else {
                                    DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengirimkan pesan . Silakan mencoba kembali",
                                            "OK", null);
                                    Utility.toast(context,"Terjadi kesalahan, silakan coba lagi");
                                }
                            }
                        });
                    } else {
                        EditText etReplyMessage = (EditText) aq.id(R.id.etReplyMessage).getView();
                        etReplyMessage.setError("Masukkan Pesan");
                    }
                }
            });

            ivAttachImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaSelectorFragment MSF = new MediaSelectorFragment();
                    MSF.setTargetFragment(PlaceholderFragment.this, 0);
                    MSF.show(getFragmentManager(), "imageChooser");
                }
            });
            if(listOfConversation.isEmpty()) {
                refreshData(true);
            }
            else{
                refreshData(false);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_group_chat, container, false);
            aq.recycle(rootView);
            listView = (ListView) aq.id(R.id.listView).getView();
            chatReplyAdapter = new ChatReplyAdapter(getContext(), listOfConversation);
            listView.setAdapter(chatReplyAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ChatItem selected = (ChatItem) listView.getItemAtPosition(position);
                    if (!selected.id_product.equals("0")) {
                        Log.d("product id =", selected.id_product);
                        Intent i = new Intent(getActivity(), ProductDetailActivity.class);
                        i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_MODE, -1);
                        i.putExtra(Constants.INTENT_PRODUCT_ID, Integer.parseInt(selected.id_product));
                        startActivity(i);
                    }
                }
            });

            aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshData(true);
                }
            });
            refreshData(true);
            etReplyMessage = (EditText) aq.id(R.id.etReplyMessage).getView();
            ivReplyMessage = (ImageView) aq.id(R.id.ivReplyMessage).getView();
            ivAttachImage = (ImageView) aq.id(R.id.ivAttachImage).getView();
            ivAttachment = (ImageView) aq.id(R.id.iv_attachment).getView();
            ivAttachment.setVisibility(View.GONE);
            btnLoadMore = (Button) aq.id(R.id.btnLoadMore).getView();
            btnLoadMore.setVisibility(View.GONE);
            endOfList = false;
            page = 0;
            etReplyMessage.requestFocus();
            return rootView;
        }


        // public methods

        public static void refreshData(boolean loading) {
            aq.id(R.id.llInfo).gone();
            getChatList(0, loading);
        }
        // get chat list
        private static void getChatList(final int page, boolean loading) {
            Log.d("debug", "page" + page);
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
            param.put("type", "" + mode);
            param.put("page", "" + page);
            if (loading) {
                aq.progress(R.id.pg);
            }
            aq.ajax(Constants.URL_LIST_GROUP_CHAT, param, JSONArray.class, new AjaxCallback<JSONArray>() {
                @Override
                public void callback(String url, JSONArray array, AjaxStatus status) {
                    // if
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        Gson gs = new Gson();
                        ArrayList<ChatItem> list = gs.fromJson(array.toString(),
                                new TypeToken<List<ChatItem>>() {
                                }.getType());

                        // empty chat
                        if (list.size() == 0 && page == 0) {
                            aq.id(R.id.tvMessageTitle).text("Tidak Ada Pesan");
                            aq.id(R.id.tvMessageContent).text("Silahkan memulai percakapan");
                            aq.id(R.id.llInfo).visible();
                        }
                        // reach end of list
                        else if (list.size() < 10) {
                            endOfList = true;
                            btnLoadMore.setVisibility(View.GONE);
                        }
                        // if refresh or initial get
                        if (page == 0) {
                            listOfConversation.clear();
                        }
                        listOfConversation.addAll(0, list);
                        chatReplyAdapter.notifyDataSetChanged();

                        //SCROLL TO BOTTOM OF THE LIST
                        if (page == 0) {
                            listView.post(new Runnable() {
                                @Override
                                public void run() {
                                    listView.setSelection(listView.getCount());
                                }
                            });
                        }

                        //mark as read to server
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                        params.put("type", "" + mode);
                        aq.ajax(Constants.URL_READ_NOTIFICATION, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                    try {
                                        if (object.getString("Success").equals("true"))
                                            Log.d("debug", "notif read");
                                    } catch (Exception e) {
                                        Log.d("Exception", String.valueOf(e));
                                    }
                                }
                            }
                        });
                    }
                    // something wrong from server (unauthenticated etc.)
                    else {

                        if (page == 0) {
                            aq.id(R.id.llInfo).visible();
                            // end of list in edge of paging
                        } else {
                            endOfList = true;
                            btnLoadMore.setVisibility(View.GONE);
                        }
                    }
                }
            });
        }

        @Override
        public void onCameraButtonClick(DialogFragment dialog) {
            Log.d("debug", "wokokok camera clicked");
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED){
                ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST);
            }else {
                Intent pictureActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "reyhan_client.jpg");
                if(Build.VERSION.SDK_INT >= 24){
                 Uri photoURI = FileProvider.getUriForFile(getActivity(),
                         BuildConfig.APPLICATION_ID + ".provider",
                         file);
                    pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            photoURI);
                }else {
                    pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(file));
                }
                startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
            }
        }

        @Override
        public void onGalleryButtonClick(DialogFragment dialog) {
            if(ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},GALLERY_REQUEST);
            }else {
                Intent pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
                pictureActionIntent.setType("image/*");
                startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == GALLERY_REQUEST) {
                    if (data != null) {
                        ivAttachment.setVisibility(View.VISIBLE);
                        //ambil uri buat dikirim ke server
                        Uri imageUri = data.getData();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            try {
                                String id = imageUri.getLastPathSegment()
                                        .split(":")[1];
                                final String[] imageColumns = {MediaStore.Images.Media.DATA};
                                final String imageOrderBy = null;
                                Uri uri = ImageHelper.getMediaStorageUri();
                                Cursor imageCursor = getActivity().managedQuery(uri,
                                        imageColumns, MediaStore.Images.Media._ID
                                                + "=" + id, null, imageOrderBy);
                                if (imageCursor.moveToFirst()) {
                                    imagePath = imageCursor
                                            .getString(imageCursor
                                                    .getColumnIndex(MediaStore.Images.Media.DATA));
                                }
                                Log.d("filepath", "kitkat = " + imagePath);
                            } catch (Exception e) {
                                Log.d("filepath", "isi uri = " + imageUri);
                                imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                                Log.d("filepath", "galery path = " + imagePath);
                            }
                        } else {
                            Log.d("filepath", "isi uri = " + imageUri);
                            imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                            Log.d("filepath", "galery path  = " + imagePath);
                        }
                        //render to ivAttachment
                        new ImageRenderer(ivAttachment, data, imagePath).execute();
                    } else {
                        Toast.makeText(getActivity(), "Cancelled",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                if (requestCode == CAMERA_REQUEST) {
                    if (resultCode == RESULT_OK) {
                        Log.d("debug", "on activity camera");
                        File file = new File(Environment.getExternalStorageDirectory()
                                + File.separator + "reyhan_client.jpg");
                        imagePath = Uri.fromFile(file).getPath();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(imagePath, options);
                        int width = options.outWidth;
                        int height = options.outHeight;
                        WindowManager wm = (WindowManager) getActivity()
                                .getSystemService(Context.WINDOW_SERVICE);
                        Display display = wm.getDefaultDisplay();
                        Log.d("debug", "Image.Width = " + width);
                        Log.d("debug", "display.getWidth() = " + display.getWidth());
                        float scale = (float) display.getWidth() / (float) width;
                        Bitmap fixed = BitmapUtil.fixOrientation(getActivity(), ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                                (int) (height * scale)), Uri.fromFile(file));
                        ivAttachment.setImageBitmap(fixed);
                        ivAttachment.setVisibility(View.VISIBLE);
                    }
                }else {
                    ivAttachment.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override

        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

            if (requestCode == CAMERA_REQUEST) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("debug", "on activity camera");
                    File file = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "reyhan_client.jpg");
                    imagePath = Uri.fromFile(file).getPath();
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePath, options);
                    int width = options.outWidth;
                    int height = options.outHeight;
                    WindowManager wm = (WindowManager) getActivity()
                            .getSystemService(Context.WINDOW_SERVICE);
                    Display display = wm.getDefaultDisplay();
                    Log.d("debug", "Image.Width = " + width);
                    Log.d("debug", "display.getWidth() = " + display.getWidth());
                    float scale = (float) display.getWidth() / (float) width;
                    Bitmap fixed = BitmapUtil.fixOrientation(context, ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                            (int) (height * scale)), Uri.fromFile(file));
                    ivAttachment.setImageBitmap(fixed);
                    ivAttachment.setVisibility(View.VISIBLE);

                } else {
                    ivAttachment.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "camera permission denied", Toast.LENGTH_LONG).show();
                }
            }else if (requestCode == CAMERA_REQUEST) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("debug", "on activity camera");
                    File file = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "reyhan_client.jpg");
                    imagePath = Uri.fromFile(file).getPath();
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePath, options);
                    int width = options.outWidth;
                    int height = options.outHeight;
                    WindowManager wm = (WindowManager) getActivity()
                            .getSystemService(Context.WINDOW_SERVICE);
                    Display display = wm.getDefaultDisplay();
                    Log.d("debug", "Image.Width = " + width);
                    Log.d("debug", "display.getWidth() = " + display.getWidth());
                    float scale = (float) display.getWidth() / (float) width;
                    Bitmap fixed = BitmapUtil.fixOrientation(getActivity(), ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                            (int) (height * scale)), Uri.fromFile(file));
                    ivAttachment.setImageBitmap(fixed);
                    ivAttachment.setVisibility(View.VISIBLE);
                }
            }else {
                ivAttachment.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }

        }//end onRequestPermissionsResult
    }

}
