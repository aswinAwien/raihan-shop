package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.LoginActivity;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Helper;
import com.raihangroup.raihanshop.helper.PrefStorage;

public class UpdateAppActivity extends Activity {

    private Context context;
    private AQuery aq;

    private final static String TAG = "Update App";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_update_app);

        context = this;
        aq = new AQuery(this);

        String deviceVersion = getIntent().getStringExtra("deviceVersion");
        String serverVersion = getIntent().getStringExtra("serverVersion");

        aq.id(R.id.tvInfo).text("Versi aplikasi tidak sesuai. Beberapa fungsi kemungkinan tidak bekerja dengan baik");
        aq.id(R.id.tvDeviceVersion).text("Versi perangkat: " + deviceVersion);
        aq.id(R.id.tvServerVersion).text("Versi server: " + serverVersion);
    }

    // support methods

    private void checkLoginStatus() {
        // check login
        if (PrefStorage.instance.getUserId() == -1) {
            // not logged in, so go to login page
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            // already logged in case
            Intent intent = new Intent(context, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    // click methods

    public void onClickUpdateApp(View v) {
        Helper.goToGooglePlay(context, "com.raihangroup.raihanshop");
    }

    public void onClickNotNow(View v) {
        checkLoginStatus();
    }
}
