package com.raihangroup.raihanshop.activity;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.BankAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.DatePickerDialogWithMaxMinRange;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.BookReference;
import com.raihangroup.raihanshop.model.CartItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Steps 5/9
 */
public class OrderPaymentActivity extends AppCompatActivity {
    @BindView(R.id.toobal_order_payment)
    Toolbar toobalOrderPayment;
    private AQuery aq;
    private String paymentInfo = "1";//, commissionInfo="1";
    private DBHelper dbh;
    int total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_payment);
        ButterKnife.bind(this);
        setSupportActionBar(toobalOrderPayment);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Metode Pembayaran");
        aq = new AQuery(this);
        dbh = new DBHelper(this);
        ((RadioGroup) aq.id(R.id.rgMethod).getView()).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                aq.id(R.id.tvObca).gone();
                aq.id(R.id.tvObni).gone();
                aq.id(R.id.tvObri).gone();
                aq.id(R.id.tvOmandiri).gone();
                aq.id(R.id.etOName).visible();
                aq.id(R.id.etONorek).visible();
                aq.id(R.id.etOBank).visible();

                switch (checkedId) {
                    case R.id.rbObca:
                        paymentInfo = "1";
                        aq.id(R.id.tvObca).visible();
                        break;
                    case R.id.rbObni:
                        paymentInfo = "2";
                        aq.id(R.id.tvObni).visible();
                        break;
                    case R.id.rbObri:
                        paymentInfo = "3";
                        aq.id(R.id.tvObri).visible();
                        break;
                    case R.id.rbOmandiri:
                        paymentInfo = "4";
                        aq.id(R.id.tvOmandiri).visible();
                        break;
                    case R.id.rbOtunai:
                        paymentInfo = "0";
                        aq.id(R.id.etOName).gone();
                        aq.id(R.id.etONorek).gone();
                        aq.id(R.id.etOBank).gone();
                        break;
                }
            }
        });
        final BookReference bp = getIntent().getParcelableExtra(Constants.INTENT_BOOK_2_ORD);
        int commission = 0;
        for (CartItem ci : bp.getProducts()) {
            total += (ci.getPrice() * ci.getQty());
            if (getIntent().getStringExtra(Constants.INTENT_COMISSION_INFO).equals("2"))
                commission += (ci.getCommission() * ci.getQty());
        }
        total += getIntent().getIntExtra(Constants.INTENT_ONGKIR_TOTAL_INFO, 0);
        total -= commission;
        aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(total));
        aq.id(R.id.btnNextAction).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrderPaymentActivity.this, OrderReviewActivity.class);
                i.putExtra(Constants.INTENT_BOOK_2_ORD, bp);
                i.putExtra(Constants.INTENT_PAYMENT_INFO, paymentInfo);
                i.putExtra(Constants.INTENT_COMISSION_INFO, getIntent().getStringExtra(Constants.INTENT_COMISSION_INFO));
                i.putExtra(Constants.INTENT_BANK_INFO, dbh.findIdBank(aq.id(R.id.etOBank).getText().toString()));
                i.putExtra(Constants.INTENT_ALTERNATE_SENDER, getIntent().getStringExtra(Constants.INTENT_ALTERNATE_SENDER));
                String narek = aq.id(R.id.etOName).getText().toString();
                String norek = aq.id(R.id.etONorek).getText().toString();
                if (!paymentInfo.equals("0")) {
                    if (i.getIntExtra(Constants.INTENT_BANK_INFO, 0) == -1) {
                        Utility.showDialogError(OrderPaymentActivity.this, "Pilihlah Bank sesuai daftar", "Terjadi Kesalahan");
                        return;
                    }
                    if (Utility.isNullOrBlank(narek)) {
                        Utility.showDialogError(OrderPaymentActivity.this, "Isilah Pemilik Rekening", "Terjadi Kesalahan");
                        return;
                    } else if (Utility.isNullOrBlank(norek)) {
                        Utility.showDialogError(OrderPaymentActivity.this, "Isilah Nomor Rekening", "Terjadi Kesalahan");
                        return;
                    }
                }
                String paid = aq.id(R.id.etOJumlahPaid).getText().toString().replaceAll("\\.", "");
                i.putExtra(Constants.INTENT_JUMLAH_PAYMENT, paid);
                if (total > Integer.parseInt(paid)) {
                    Utility.showDialogError(OrderPaymentActivity.this, "Jumlah yang ditransfer tidak mencukupi", "Terjadi Kesalahan");
                    return;
                }
                i.putExtra(Constants.INTENT_DATE_PAID, (Calendar) aq.id(R.id.etLahTanggal).getTag());
                i.putExtra(Constants.INTENT_REKENINGOWNER_INFO, narek);
                i.putExtra(Constants.INTENT_NOREK_INFO, norek);
                i.putExtra(Constants.INTENT_ONGKIR_TOTAL_INFO, getIntent().getIntExtra(Constants.INTENT_ONGKIR_TOTAL_INFO, 0));
                startActivity(i);
            }
        });
        BankAdapter ba = new BankAdapter(this, dbh.findBanks("Bank"), aq.id(R.id.etOBank).getEditText());
        ((AutoCompleteTextView) findViewById(R.id.etOBank)).setAdapter(ba);
        ((AutoCompleteTextView) findViewById(R.id.etOBank)).setOnItemClickListener(ba);
        aq.id(R.id.etOJumlahPaid).getEditText().addTextChangedListener(new ThousandWatcher(aq.id(R.id.etOJumlahPaid).getEditText()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void dateChoose(View v) {
        OnDateSetListener datePickerOnDateSetListener = new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                Calendar c = Calendar.getInstance(new Locale("in"));
                c.set(year, monthOfYear, dayOfMonth);
                String s = new SimpleDateFormat("dd MMMM yyyy", new Locale("in")).format(c.getTime());
                aq.id(R.id.etLahTanggal).text(s);
                aq.id(R.id.etLahTanggal).tag(c);
            }
        };
        Calendar now = Calendar.getInstance();
        DatePickerDialogWithMaxMinRange datePickerDialog = new DatePickerDialogWithMaxMinRange(
                this,
                datePickerOnDateSetListener,
                now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH) - 1, //29 juni
                now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)); //28 juli
        datePickerDialog.show();
    }

    /**
     * watcher untuk ngasih titik di setiap ribuan
     *
     * @author badr
     */
    class ThousandWatcher implements TextWatcher {

        /**
         * edittext yang akan dipengaruhi watcher ini
         */
        private EditText etw;

        public ThousandWatcher(EditText etw) {
            this.etw = etw;
        }

        @Override
        public void afterTextChanged(Editable st) {
            // just found this mechanism in the net :p
            etw.removeTextChangedListener(this);
            int inilen, endlen;
            inilen = etw.getText().length();

            String s = null;
            try { // german karena kita pingin separatornya pake titik. kebetulan sama dengan INA
                s = String.format(Locale.GERMANY, "%,d", Long.parseLong(st.toString().replaceAll("\\.", "")));

            } catch (NumberFormatException e) {
            }

            int cp = etw.getSelectionStart();
            etw.setText(s);
            endlen = etw.getText().length();

            int sel = (cp + (endlen - inilen));
            if (sel > 0 && sel <= etw.getText().length()) {
                etw.setSelection(sel);
            } else {
                etw.setSelection(etw.getText().length());
            }
            etw.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }
}