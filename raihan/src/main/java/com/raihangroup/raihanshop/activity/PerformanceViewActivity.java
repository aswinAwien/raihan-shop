package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.PerformanceFragment;
import com.raihangroup.raihanshop.helper.PrefStorage;

/**
 * Created by Mohamad on 2/6/2015.
 * Activity to show team and self performance
 */
public class PerformanceViewActivity extends AppCompatActivity {

    private Context context;
    private PerformanceFragment performanceFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        context = this;
        PrefStorage.newInstance(context);
        performanceFragment = new PerformanceFragment();

        Toolbar toolbar = (Toolbar) findViewById(R.id.viewToolbar);
        toolbar.setTitle("Kinerja Diri dan Tim");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
        ab.setDisplayHomeAsUpEnabled(true);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, performanceFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}