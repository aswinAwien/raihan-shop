package com.raihangroup.raihanshop.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.raihangroup.raihanshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RatingOfferActivity extends AppCompatActivity {

    @BindView(R.id.toobal_rating_offer)
    Toolbar toobalRatingOffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_offer);
        ButterKnife.bind(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            String title = b.getString("title");
            if (title != null) {
                toobalRatingOffer.setTitleTextColor(Color.WHITE);
                setSupportActionBar(toobalRatingOffer);
                getSupportActionBar().setTitle(title);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    // handle click

    public void onClickNoThanks(View v) {
        finish();
    }

    public void onClickYesPlease(View v) {
        Bundle b = getIntent().getExtras();
        Intent i = new Intent(this, RatingListActivity.class).putExtras(b);
        startActivity(i);
        finish();
    }
}
