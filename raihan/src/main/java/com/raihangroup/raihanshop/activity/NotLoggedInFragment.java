package com.raihangroup.raihanshop.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.raihangroup.raihanshop.LoginActivity;
import com.raihangroup.raihanshop.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotLoggedInFragment extends Fragment {


    Button loginBtn;
    Context context;
    public NotLoggedInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_not_logged_in, container, false);
        loginBtn = (Button)view.findViewById(R.id.btnLogin);

        context = getActivity();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }

}
