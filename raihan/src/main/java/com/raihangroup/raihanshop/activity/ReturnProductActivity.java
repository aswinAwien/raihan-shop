package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.MediaSelectorFragment;
import com.raihangroup.raihanshop.fragments.MediaSelectorFragment.MediaSelectorListener;
import com.raihangroup.raihanshop.helper.BitmapUtil;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.ImageHelper;
import com.raihangroup.raihanshop.helper.ImageRenderer;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.CartItem;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReturnProductActivity extends AppCompatActivity implements MediaSelectorListener {
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    @BindView(R.id.toobal_return_product)
    Toolbar toobalReturnProduct;
    private int reason_type = 1; //default di description not match
    private String reason_type_text;
    private int resolve_type = 0;
    private CartItem cartItem;
    ProgressDialog progDialog;
    AQuery aq;
    Bundle bundle;
    private Intent pictureActionIntent;
    private String imagePath;
    private Context context;
    private String JSONRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_product);
        ButterKnife.bind(this);
        context = this;
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setMessage("Mengirimkan komplain retur");
        progDialog.setCancelable(false);
        aq = new AQuery(this);
        bundle = getIntent().getExtras();
        JSONRating = bundle.getString("ratings");
        cartItem = bundle.getParcelable("cart_item");
        toobalReturnProduct.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalReturnProduct);

        List<Integer> dropdown = new ArrayList<Integer>();
        for (int i = 1; i <= cartItem.getQty(); i++) {
            dropdown.add(i);
        }
        final Spinner spinnerQty = aq.id(R.id.spinnerQty).getSpinner();
        ArrayAdapter<Integer> qtyAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, dropdown);
        qtyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerQty.setAdapter(qtyAdapter);
        aq.id(R.id.ivComplain).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaSelectorFragment MSF = new MediaSelectorFragment();
                MSF.show(getSupportFragmentManager(), "imageChooser");
            }
        });
        aq.id(R.id.btnReturnProduct).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("id_order_items", "" + cartItem.getId());
                params.put("id_member", "" + PrefStorage.instance.getUserId());
                params.put("date_received", "" + bundle.getString("date_received"));
                params.put("ratings", JSONRating);
                Map<String, String> retur = new HashMap<String, String>();
                retur.put("qty", spinnerQty.getSelectedItem().toString());
                retur.put("reason_type", "" + reason_type);
                retur.put("reason_type_text", reason_type_text);
                retur.put("resolve_type", "" + resolve_type);
                JSONObject JSONRetur = new JSONObject(retur);
                params.put("retur", JSONRetur.toString());
                Log.d("Retur", "" + params);
                if (imagePath != null) {
                    params.put("file", ImageHelper.getImageFile(imagePath, context));
                }
                aq.progress(progDialog).ajax(Constants.URL_RATE_SUPPLIER_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        Log.d("retur", "" + status.getCode());
                        if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                            Log.d("Retur", "false");
                            DialogHelper.showMessageDialog(ReturnProductActivity.this, "Terjadi Kesalahan", "Permohonan Retur gagal dikirimkan, silahkan coba beberapa saat lagi", "OK", null);
                            return;
                        }
                        Log.d("Retur", "success");
                        DialogHelper.showMessageDialog(ReturnProductActivity.this, "Terima kasih", "Komplain anda sudah diterima. Silakan tunggu konfirmasi dari Admin Raihan", "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setResult(Activity.RESULT_OK);
                                finish();
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_return_product, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onReasonTypeClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();
        switch (v.getId()) {
            case R.id.radioBtnNotMatchDescription:
                if (checked) {
                    reason_type = 1;
                    Log.d("retur", "" + reason_type);
                    aq.id(R.id.etReasonText).enabled(false);
                    aq.id(R.id.etReasonText).text("");
                }
                break;
            case R.id.radioBtnDefectProduct:
                if (checked) {
                    reason_type = 2;
                    Log.d("retur", "" + reason_type);
                    aq.id(R.id.etReasonText).enabled(false);
                    aq.id(R.id.etReasonText).text("");
                }
                break;
            case R.id.radionBtnOther:
                if (checked) {
                    reason_type = 3;
                    Log.d("retur", "" + reason_type);
                    aq.id(R.id.etReasonText).enabled(true);
                    aq.id(R.id.etReasonText).getView().requestFocus();
                }
                break;
        }
    }

    public void onResolveTypeClick(View v) {
        switch (v.getId()) {
            case R.id.radioBtnReturnMoney:
                resolve_type = 1;
                break;
            case R.id.radioBtnChangeAsOrder:
                resolve_type = 2;
                break;
        }
    }

    @Override
    public void onCameraButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "retur_product.jpg");
        pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(file));
        startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
    }

    @Override
    public void onGalleryButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            ImageView ivComplain = (ImageView) aq.id(R.id.ivComplain).getView();
            if (requestCode == GALLERY_REQUEST) {
                if (data != null) {
                    Uri imageUri = data.getData();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            String id = imageUri.getLastPathSegment().split(":")[1];
                            final String[] imageColumns = {MediaStore.Images.Media.DATA};
                            final String imageOrderBy = null;
                            Uri uri = ImageHelper.getMediaStorageUri();
                            Cursor imageCursor = managedQuery(uri,
                                    imageColumns, MediaStore.Images.Media._ID + "=" + id, null, imageOrderBy);
                            if (imageCursor.moveToFirst()) {
                                imagePath = imageCursor
                                        .getString(imageCursor
                                                .getColumnIndex(MediaStore.Images.Media.DATA));
                            }
                            Log.d("filepath", "kitkat = " + imagePath);
                        } catch (Exception e) {
                            Log.d("filepath", "isi ekspesi uri kitkat = " + imageUri);
                            imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                            Log.d("filepath", "galery path kitkat = " + imagePath);
                        }
                    } else {
                        Log.d("filepath", "isi uri = " + imageUri);
                        imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                        Log.d("filepath", "galery path  = " + imagePath);
                    }
                    //render to ivAttachment)
                    new ImageRenderer(ivComplain, data, imagePath).execute();
                } else {
                    Toast.makeText(this, "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                Log.d("debug", "on activity camera");
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "retur_product.jpg");
                imagePath = Uri.fromFile(file).getPath();
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imagePath, options);
                int width = options.outWidth;
                int height = options.outHeight;
                Log.d("debug", "Image.Width = " + width);
                Log.d("debug", "display.getWidth() = " + display.getWidth());
                float scale = (float) display.getWidth() / (float) width;
                Bitmap fixed = BitmapUtil.fixOrientation(context, ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                        (int) (height * scale)), Uri.fromFile(file));
                ivComplain.setImageBitmap(fixed);
            }
        }
    }
}
