package com.raihangroup.raihanshop.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.SelfPerformanceAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.DownloadPdfProcess;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.SelfPerformance;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelfPerformanceActivity extends AppCompatActivity {
    @BindView(R.id.toobal_self_performance)
    Toolbar toobalSelfPerformance;
    private AQuery aq;
    private ProgressDialog progDialog;
    private ListView listview;
    private Context context;
    private List<SelfPerformance> objects;
    protected SelfPerformanceAdapter adapter;
    private int mode;
    private int memberId;
    private int month;
    private int year;
    private float startPS;
    private int endPS;
    public static final int MODE_PS = 0;
    public static final int MODE_TTS = 1;
    private static final String TAG = "Self Performance";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_performance);
        ButterKnife.bind(this);

        toobalSelfPerformance.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalSelfPerformance);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mode = -1;
        aq = new AQuery(this);
        PrefStorage.newInstance(this);
        // init using Aquery
        listview = aq.id(R.id.lsPerfP).getListView();
        // init from intent extra
        mode = getIntent().getIntExtra(Constants.INTENT_SELFPERFORMANCE_MODE, 0);
        memberId = getIntent().getIntExtra(Constants.INTENT_PERFORMANCE_ID, 0);
        month = getIntent().getIntExtra(Constants.INTENT_PERFORMANCE_MONTH, 0);
        year = getIntent().getIntExtra(Constants.INTENT_PERFORMANCE_YEAR, 0);
        startPS = getIntent().getFloatExtra(Constants.INTENT_SPERFORMANCE_PS_START, 0f);
        endPS = getIntent().getIntExtra(Constants.INTENT_SPERFORMANCE_PS_END, 0);
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, 1);
        aq.id(R.id.tvPBulan).text(new SimpleDateFormat("MMMM yyyy", new Locale("in")).
                format(c.getTime()));
        aq.id(R.id.tvLevelPSDesc).text(startPS + "/" + endPS + " V");
        aq.id(R.id.psSelfP).getProgressBar().setMax(endPS);
        aq.id(R.id.psSelfP).getProgressBar().setProgress(Math.round(startPS));
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Fetching Data");
        progDialog.setMessage("Please wait a moment");
        String link = null;
        if (mode == MODE_TTS) {
            aq.id(R.id.tvLevelPS).text("TTS");
            link = Constants.URL_TTSPERF_API;
        } else {
            aq.id(R.id.tvLevelPS).text("PS");
            link = Constants.URL_SPERF_API;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("member_id", String.valueOf(memberId));
        params.put("month", String.valueOf(month));
        params.put("year", String.valueOf(year));
        aq.progress(progDialog).ajax(link, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                Log.d(TAG, "callback: ");
                Log.d(TAG, "Status code " + status.getCode() + " with JSON array " + object);
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    objects = gs.fromJson(object.toString(), new TypeToken<List<SelfPerformance>>() {
                    }.getType());
                    adapter = new SelfPerformanceAdapter(SelfPerformanceActivity.this,
                            R.layout.s_performance_item, objects, mode);
                    listview.setAdapter(adapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            SelfPerformance selected = (SelfPerformance) parent.getItemAtPosition(position);
                            Bundle b = new Bundle();
                            b.putInt("userId", selected.getId());
                            b.putBoolean("editable", false);
                            Intent i = new Intent(context, ViewProfileActivity.class).putExtras(b);
                            startActivity(i);
                        }
                    });
                    if (objects.size() == 0) {
                        aq.id(R.id.llInfo).visible();
                    }
                } else {
                    aq.id(R.id.llInfo).visible();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void onClickDownloadReport(View v) {
        String link = "http://raihanshop.com/index.php/adminmember/printSales";
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_member", String.valueOf(memberId));
        params.put("type", String.valueOf(mode));
        params.put("month", String.valueOf(month));
        params.put("year", String.valueOf(year));
        params.put("is_mobile", "1");

        aq.progress(progDialog).ajax(link, params, String.class, new AjaxCallback<String>() {

            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    String modeString = null;
                    if (mode == MODE_TTS) {
                        modeString = "TTS";
                    } else {
                        modeString = "PS";
                    }
                    // this is our format name for downloaded file
                    // WARNING: for the pdf viewer to work properly, don't use space in name
                    String fileName = "Raihan_" + modeString + "_Self-Performance-Report_" + year + "-" + month + ".pdf";

                    try {
                        JSONObject jo = new JSONObject(object);
                        String link = jo.getString("link");
                        new DownloadPdfProcess().performDownload(SelfPerformanceActivity.this,
                                fileName, link, true);
                    } catch (JSONException e) {
                        DialogHelper.showMessageDialog(SelfPerformanceActivity.this, "Error", "Failed to get data. " + e.getMessage(), "OK", null);
                        Log.d("exception", String.valueOf(e));
                    }
                } else {
                    DialogHelper.showMessageDialog(SelfPerformanceActivity.this, "Error", "Failed to get data. Please check your internet connection", "OK", null);
                }
            }
        });
    }
}
