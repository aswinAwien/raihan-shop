package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ProductListAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultActivity extends AppCompatActivity implements PullToRefreshBase.OnRefreshListener2<ListView>{
    private static final String TAG = SearchResultActivity.class.getSimpleName();
    String urlString;
    Map<String, String> params;
    AQuery aq;
    @BindView(R.id.sv_product)
    SearchView svProduct;
    List<Product> productList;
    @BindView(R.id.pb_list)
    ProgressBar pbList;
    @BindView(R.id.toolbar_search)
    Toolbar toolbarSearch;
    Context context;
    @BindView(R.id.result_product_list)
    PullToRefreshListView resultProductList;
    @BindView(R.id.tvMessageTitle)
    TextView tvMessageTitle;
    @BindView(R.id.tvMessageContent)
    TextView tvMessageContent;
    @BindView(R.id.llInfo)
    LinearLayout llInfo;
    ArrayList<Product> adaptor;
    ArrayList<Product> listProductSearch;
    private ProductListAdapter adapter;
    int page;
    private String searchString;
    private int preLast;
    Gson gs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        params = new HashMap<>();
        aq = new AQuery(this);
        setContentView(R.layout.activity_search_result);
        listProductSearch = new ArrayList<>();
        ButterKnife.bind(this);
        setSupportActionBar(toolbarSearch);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        context = this;
        gs = new Gson();
        resultProductList.setMode(PullToRefreshBase.Mode.DISABLED);
        resultProductList.setOnRefreshListener(this);
        resultProductList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastItem = firstVisibleItem + visibleItemCount;
                if(lastItem == totalItemCount){
                    if(preLast!=lastItem){
                        preLast = lastItem;
                        Utility.toast(context, "memuat produk lainnya ...");
                        loadNextPage();
                    }
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void handleSearch() {
        page = 0;
        Map<String, String> param = new HashMap<>();
        param.put("keyword", searchString);
        param.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
        param.put("page", String.valueOf(page));
        param.put("mode", String.valueOf(1));
        aq.progress(pbList).ajax(Constants.URL_PRODSEARCH_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    Utility.toast(context, "Produk tidak tersedia dalam pencarian");
                    aq.id(llInfo).visible();
                    aq.id(resultProductList).gone();
                    aq.id(tvMessageTitle).text("Ops sepertinya ada yang salah!");
                    aq.id(tvMessageContent).text("Silakan klik disini untuk memuat ulang");
                }
                    aq.id(resultProductList).visible();
                ArrayList<Product> products = gs.fromJson(object.toString(),new TypeToken<List<Product>>(){}.getType());
                setProductsRead(products);
                listProductSearch = products;

                adaptor = new ArrayList<>();
                adaptor.addAll(products);

                if(adaptor.size() == 0){
                    aq.id(llInfo).visible();
                    aq.id(tvMessageContent).text("Produk tidak ada!");
                }else {
                    aq.id(llInfo).gone();
                }

                adapter = new ProductListAdapter(SearchResultActivity.this,adaptor);
                resultProductList.setAdapter(adapter);
                resultProductList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent productIntent = new Intent(SearchResultActivity.this,ProductDetailActivity.class);

                        productIntent.putExtra(Constants.INTENT_PRODUCT_DETAIL,(Product) adapter.getItem(position));
                        productIntent.putExtra(Constants.INTENT_PRODUCT_ID,((Product)adapter.getItem(position)).getId());
                        startActivity(productIntent);
                    }
                });

            }
        });
    }

    @Override
    protected void onStart() {
        svProduct.setOnQueryTextListener(new SearchProductsListener());
        super.onStart();
    }

    private void setProductsRead(ArrayList<Product> products) {
        Log.d(TAG, "set products read");

        JSONArray jsonArr = new JSONArray();
        for (Product product : products) {
            // the 0 means product is unread
            if (product.is_read == 0) {
                jsonArr.put(product.getId());
            }
        }
        Log.d(TAG, "collected product ids " + jsonArr.toString());

        // needed for aq so that the call is treated as POST method
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
        params.put("id_product", jsonArr.toString());

        aq.ajax(Constants.URL_SET_PRODUCTS_READ_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    return;
                }
            }
        });
    }

    private void loadNextPage(){
        Map<String,String> params = new HashMap<>();
        params.put("mode",String.valueOf(1));
        page++;
        params.put("page",String.valueOf(page));
        params.put("keyword",searchString);
        params.put("id_member",String.valueOf(PrefStorage.instance.getUserId()));

        aq.ajax(Constants.URL_SET_PRODUCTS_READ_API,params,JSONArray.class,new AjaxCallback<JSONArray>(){
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if(status.getCode()!= Constants.HTTP_RESPONSE_OK){
                    page--;
                    preLast = 0;
                    return;
                }

                ArrayList<Product> products = gs.fromJson(object.toString(),
                        new TypeToken<List<Product>>(){}.getType());

                setProductsRead(products);

                adaptor.addAll(products);
                listProductSearch.addAll(products);

                adapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

    }

    public void quitSearchMode() {
        aq.id(resultProductList).visible();

        // we are from search mode
        this.searchString = "";

        // reset products in list view
        adaptor.clear();
        adaptor.addAll(listProductSearch);
        // adapter maybe null
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void prepareSearchMode() {
        aq.id(resultProductList).gone();
    }

    public void enterSearchMode(String searchString) {
        this.searchString = searchString;
        handleSearch();
    }

    private class SearchProductsListener implements SearchView.OnQueryTextListener {

        @Override
        public boolean onQueryTextChange(String s) {
            if(s.isEmpty()){
                quitSearchMode();
            }
            return true;
        }

        @Override
        public boolean onQueryTextSubmit(String s) {
            SearchView searchView = svProduct;
            Utility.clearSoftInputFromView(context, searchView);
            enterSearchMode(s);
            return true;
        }
    }

    public static int getResourceIdByName(Context context, String resName, String resType) {
        int resId = context.getResources().getIdentifier(resName, resType,
                context.getPackageName());
        return resId;
    }
}
