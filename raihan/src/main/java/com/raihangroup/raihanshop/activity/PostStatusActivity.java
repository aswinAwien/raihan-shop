package com.raihangroup.raihanshop.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.UserProfile;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostStatusActivity extends AppCompatActivity {

    private static final String TAG = PostStatusActivity.class.getSimpleName();
    @BindView(R.id.toolbar_post)
    Toolbar toolbarPost;
    @BindView(R.id.appbar_post)
    AppBarLayout appbarPost;
    @BindView(R.id.civ_post)
    CircleImageView civPost;
    @BindView(R.id.et_post)
    EditText etPost;
    AQuery aq;
    Context context;
    UserProfile profile;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_status);
        ButterKnife.bind(this);
        context = this;
        aq = new AQuery(context);
        setSupportActionBar(toolbarPost);
        PrefStorage.newInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Post A Status");

        Map<String, String> params = new HashMap<>();
        params.put("id", String.valueOf(PrefStorage.instance.getUserId()));
        aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    Utility.toast(PostStatusActivity.this, status.getMessage());
                }

                Gson gs = new Gson();
                profile = gs.fromJson(object.toString(), UserProfile.class);
                if(!profile.image.equals("img_default_user.png")) {
                    aq.id(civPost).image(Constants.BASE_URL +profile.image);
                }else {
                    aq.id(civPost).image(context.getResources().getDrawable(R.drawable.img_default_user));
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_post,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_post:
//        String id = String.valueOf(PrefStorage.instance.getUserId());
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());
                Map<String,String> field = new HashMap<>();
                field.put("id_creator", String.valueOf(PrefStorage.instance.getUserId()));
                field.put("isi_status",etPost.getText().toString().trim());
                field.put("created",strDate);
                aq.ajax(Constants.URL_UPDATE_STATUS,field, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        if(status.getCode()==Constants.HTTP_RESPONSE_OK){
                            Utility.toast(context,status.getMessage());
                        }
                        finish();
                    }
                });
        }
        return true;
    }
}
