package com.raihangroup.raihanshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;

/**
 * Activity for showing booking name
 * Step 3 of 9
 */
public class BookingNameActivity extends AppCompatActivity {

    private AQuery aq;
    private String book_name;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_name);
        book_name = getIntent().getStringExtra(Constants.INTENT_BOOKING_NAME);
        aq = new AQuery(this);
        toolbar = (Toolbar)findViewById(R.id.toolbar_booking_name);
        setSupportActionBar(toolbar);
        aq.id(R.id.tvBookName).text(book_name);
        aq.id(R.id.btnAction).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BookingNameActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_BOOK);
        startActivity(intent);
    }
}
