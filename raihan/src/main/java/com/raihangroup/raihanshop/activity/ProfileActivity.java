package com.raihangroup.raihanshop.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ViewStatusListAdapter;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.PerformanceObject;
import com.raihangroup.raihanshop.model.SellingUser;
import com.raihangroup.raihanshop.model.UserProfile;
import com.raihangroup.raihanshop.model.UserStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {


    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private static final String TAG = ProfileActivity.class.getSimpleName();
    @BindView(R.id.appbar_profile)
    AppBarLayout appbarProfile;
    @BindView(R.id.toolbar_profile)
    Toolbar toolbarProfile;
    @BindView(R.id.rv_status_display)
    RecyclerView rvStatusDisplay;
    @BindView(R.id.tvOmset)
    TextView tvOmset;
    @BindView(R.id.tvKomisi)
    TextView tvKomisi;
    @BindView(R.id.tvPSendiri)
    TextView tvPSendiri;
    @BindView(R.id.tvPTim)
    TextView tvPTim;
    @BindView(R.id.tv_profile_id)
    TextView tvProfileId;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.financial_display)
    LinearLayout financialDisplay;
    @BindView(R.id.tv_profile_address)
    TextView tvProfileAddress;
    @BindView(R.id.materialup_title_container)
    LinearLayout materialupTitleContainer;
    @BindView(R.id.cairkan_btn)
    Button cairkanBtn;
    @BindView(R.id.post_status)
    FloatingActionButton postStatus;
    @BindView(R.id.emptyView)
    TextView emptyView;
    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;
    @BindView(R.id.tv_profile_name)
    TextView tvProfileName;
    @BindView(R.id.tv_profile_title)
    TextView tvProfileTitle;
    @BindView(R.id.materialup_profile_image)
    CircleImageView materialupProfileImage;
    AQuery aq;
    public static UserProfile profile;
    String userId;
    Context context;
    ArrayList<UserStatus> data;
    Bundle b;
    private int month;
    private int year;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbarProfile);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        aq = new AQuery(this);
        context = this;
        b = getIntent().getExtras();
        PrefStorage.newInstance(this);
        if (b != null) {
            userId = String.valueOf(b.getInt("userId"));
        }

        if (userId.equals(String.valueOf(PrefStorage.instance.getUserId()))) {
            financialDisplay.setVisibility(View.VISIBLE);
            postStatus.setVisibility(View.VISIBLE);
        } else {
            financialDisplay.setVisibility(View.GONE);
            postStatus.setVisibility(View.GONE);
            Log.i(TAG, "userId: " + PrefStorage.instance.getUserId());
        }

        postStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, PostStatusActivity.class);
                startActivity(intent);
            }
        });
        appbarProfile.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarProfile.getTotalScrollRange();

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        Map<String,String> params = new HashMap<>();
        params.put("month",String.valueOf(month));
        params.put("year",String.valueOf(year));
        params.put("member_id",String.valueOf(PrefStorage.instance.getUserId()));
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Memuat");
        progressDialog.setMessage("Memuat Detail User.....");
        aq.progress(progressDialog).ajax(Constants.URL_PERFSUM_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(status.getCode()==Constants.HTTP_RESPONSE_OK){
                    Gson gs = new Gson();
                    PerformanceObject po = gs.fromJson(object.toString(),PerformanceObject.class);
                    setIconDiluteButton(po);
                    refreshPage();
                }else {
                    Utility.toast(context,status.getMessage());
                    finish();
                }
            }
        });

    }



//    private void setFeeAndCommIcon(int classId, final PerformanceObject po,
//                                   LinearLayout feeCommView) {
//        ImageView icFeeComplete = (ImageView) feeCommView.findViewById(R.id.icFeeComplete);
//
//        if (classId > 2) {
//            icFeeComplete.setVisibility(View.GONE);
//            return;
//        }
//
//        if (po.getPs() >= 15) {
//            icFeeComplete.setImageDrawable(getResources().getDrawable(
//                    R.drawable.ic_fee_complete));
//            icFeeComplete.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Utility.toast(context,
//                            getResources().getString(R.string.msg_fee_complete));
//                }
//            });
//        } else {
//            icFeeComplete.setImageDrawable(getResources().getDrawable(
//                    R.drawable.ic_fee_warning));
//            icFeeComplete.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Utility.toast(context,
//                            getResources().getString(R.string.msg_fee_warning));
//                }
//            });
//        }
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_setting:
                Intent settingIntent = new Intent(context, SettingActivity.class);
                startActivity(settingIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refreshPage() {
        Map<String, String> params = new HashMap<>();
        params.put("id", userId);
        aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    Utility.toast(ProfileActivity.this, status.getMessage());
                }

                Gson gs = new Gson();
                profile = gs.fromJson(object.toString(), UserProfile.class);


                if (!profile.image.equals("img_default_user.png")) {
                    aq.id(materialupProfileImage).image(Constants.BASE_URL + profile.image);
                    Log.i("Profile Image", "callback: " + profile.image);
                }else if(profile.image.equals("")){
                    aq.id(materialupProfileImage).image(context.getResources().getDrawable(R.drawable.img_default_user));
                } else {
                    aq.id(materialupProfileImage).image(context.getResources().getDrawable(R.drawable.img_default_user));
                    Log.i("Profile Image", "callback: " + profile.image);
                }
                aq.id(tvProfileName).text(Utility.toTitleCase(profile.fullname));
                aq.id(tvProfileTitle).text(profile.level_name);
                aq.id(tvProfileAddress).text(profile.address);
                aq.id(tvProfileTitle).clicked(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent performanceChartIntent = new Intent(context, PerformanceChartActivity.class).putExtras(b);
                        context.startActivity(performanceChartIntent);
                    }
                });
                aq.id(tvProfileId).text(profile.id);
                aq.id(tvProfileId).clicked(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            clipboard.setText(profile.id);
                            Utility.snackBar(context, coordinatorLayout, "User ID has been copied to your clipboard");
                        } else {
                            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("Copied Text", profile.id);
                            clipboard.setPrimaryClip(clip);
                            Utility.snackBar(context, coordinatorLayout, "User ID has been copied to your clipboard");
                        }
                    }
                });
                setSelling();
                getStatusProfile();
            }
        });
    }
    private void setIconDiluteButton(PerformanceObject po){
//        Button cairkanBtn = (Button) findViewById(R.id.cairkan_btn);
        if(po.getPs()>=15){
            cairkanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.toast(ProfileActivity.this,getResources().getString(R.string.msg_fee_complete));
                }
            });
        }else{
            cairkanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.toast(ProfileActivity.this,  getResources().getString(R.string.msg_fee_warning));
                }
            });
        }
    }

    public void setSelling() {
        String id = String.valueOf(userId);
        Map<String, String> field = new HashMap<String, String>();
        field.put("id", id);
        aq.ajax(Constants.URL_SELLING_USER, field, JSONObject.class, new AjaxCallback<JSONObject>() {
            SellingUser sellingUser;

            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Gson parsing = new Gson();
                sellingUser = parsing.fromJson(object.toString(), SellingUser.class);
                aq.id(R.id.tvPSendiri).text(sellingUser.getPs());
                aq.id(R.id.tvPTim).text(sellingUser.getPt());
                if (sellingUser.getOmset() != null) {
                    aq.id(R.id.tvOmset).text(currencyConvert(sellingUser.getOmset()));
                } else {
                    aq.id(R.id.tvOmset).text("0");
                }
                if (sellingUser.getComission() != null) {
                    aq.id(R.id.tvKomisi).text(currencyConvert(sellingUser.getComission()));
                } else {
                    aq.id(R.id.tvKomisi).text("0");
                }
            }
        });
    }

    private String currencyConvert(String amount) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance("IDR"));
        String currency = format.format(amount);
        return currency;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            materialupProfileImage.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            materialupProfileImage.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }
    }

    public void getStatusProfile() {
        String id = String.valueOf(userId);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id_creator", id);
        aq.ajax(Constants.URL_USER_PROFILE_STATUS_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                Gson gson = new Gson();
                Type collectionType = new TypeToken<ArrayList<UserStatus>>() {
                }.getType();
                data = gson.fromJson(object.toString(), collectionType);
                if (data.size() != 0) {
                    ViewStatusListAdapter adapter = new ViewStatusListAdapter(data, ProfileActivity.this);
                    rvStatusDisplay.setAdapter(adapter);
                    rvStatusDisplay.addItemDecoration(new DividerItemDecoration(ProfileActivity.this, LinearLayoutManager.VERTICAL));
                    rvStatusDisplay.setLayoutManager(new LinearLayoutManager(ProfileActivity.this));
                    rvStatusDisplay.setHasFixedSize(true);
                    emptyView.setVisibility(View.GONE);
                }else {
                    rvStatusDisplay.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshPage();
    }

    //    private void setListItemMenu() {
//        List<ItemMenuProfileAdapter.ItemMenu> itemMenus = new ArrayList<>();
//        itemMenus.add(new ItemMenuProfileAdapter.ItemMenu(1, "Setting", context.getResources().getDrawable(R.drawable.setting)));
//        itemMenus.add(new ItemMenuProfileAdapter.ItemMenu(2,"Performances and Statistics",context.getResources().getDrawable(R.drawable.ic_teamperformance)));
//        itemMenus.add(new ItemMenuProfileAdapter.ItemMenu(3,"Feedback",context.getResources().getDrawable(R.drawable.ic_grupchat)));
//        rvMenu.setAdapter(new ItemMenuProfileAdapter(itemMenus, context));
//        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
//        rvMenu.setLayoutManager(layoutManager);
//        rvMenu.addItemDecoration(new DividerItemDecoration(context,layoutManager.getOrientation()));
//        rvMenu.setHasFixedSize(true);
//    }
}
