package com.raihangroup.raihanshop.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.CategoryListAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Category;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListActivity extends AppCompatActivity {

    private static final String TAG = CategoryListActivity.class.getSimpleName();
    @BindView(R.id.toolbar_category_list)
    Toolbar toolbarCategoryList;
    @BindView(R.id.recycler_list_category)
    RecyclerView recyclerListCategory;
    AQuery aq;
    @BindView(R.id.pg)
    ProgressBar pg;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.tvMessageTitle)
    TextView tvMessageTitle;
    @BindView(R.id.tvMessageContent)
    TextView tvMessageContent;
    @BindView(R.id.llInfo)
    LinearLayout llInfo;
    List<Category> categoryList;
    CategoryListAdapter categoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);
        int category_id = getIntent().getIntExtra("category_id", 0);
        String category_name = getIntent().getStringExtra("category_name");

        toolbarCategoryList.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbarCategoryList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(category_name);
        Map<String,String> params = new HashMap<>();
        params.put("id_parent",String.valueOf(category_id));
        aq = new AQuery(this);
        aq.progress(pg).ajax(Constants.URL_CATEGORY_LIST_API,params, JSONArray.class,new AjaxCallback<JSONArray>(){
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                aq.id(R.id.llInfo).gone();
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();
                    aq.id(recyclerListCategory).gone();
                }
                Gson gs = new Gson();
                categoryList = gs.fromJson(object.toString(),new TypeToken<List<Category>>(){}.getType());
                categoryListAdapter = new CategoryListAdapter(categoryList,CategoryListActivity.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CategoryListActivity.this);
                recyclerListCategory.setLayoutManager(linearLayoutManager);
                recyclerListCategory.setHasFixedSize(true);
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(CategoryListActivity.this,linearLayoutManager.getOrientation());
                recyclerListCategory.addItemDecoration(dividerItemDecoration);
                recyclerListCategory.setAdapter(categoryListAdapter);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
