package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Activity to showing forget pass activity
 */
public class ForgetPasActivity extends Activity {

    private AQuery aq;
    private ProgressDialog progDialog;

    public static final int FORGET = 1;
    public static final int EDIT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forget_pas);

        aq = new AQuery(this);

        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mengirim permintaan ke server");
        progDialog.setMessage("Mohon Tunggu...");
        progDialog.setCancelable(false);

        int mode = getIntent().getIntExtra(Constants.INTENT_FORGET_P, 0);

        PrefStorage.newInstance(this);

        if (mode == FORGET) {
            aq.id(R.id.btnAction).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String em = aq.id(R.id.etFEmail).getText().toString();
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", em);
                    aq.progress(progDialog).ajax(Constants.URL_LUPAPASS_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                Utility.toast(ForgetPasActivity.this, "Permintaan telah dikirim! \nCek email Anda");
                                finish();
                            } else {
                                Utility.toast(ForgetPasActivity.this, "Terdapat kesalahan");
                            }
                        }
                    });
                }
            });
        } else if (mode == EDIT) {
            aq.id(R.id.tlEPpassawal).visible();
            aq.id(R.id.tlEPpass2).visible();
            aq.id(R.id.tvInfoText).text("Masukkan password lama dan baru anda untuk mengubah password");

            TextInputEditText et = (TextInputEditText) aq.id(R.id.etFEmail).getEditText();
            et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            et.setTransformationMethod(PasswordTransformationMethod.getInstance());

            aq.id(R.id.btnAction).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!aq.id(R.id.etEPpass2).getText().toString().
                            equals(aq.id(R.id.etFEmail).getText().toString())) {
                        Utility.showDialogError(ForgetPasActivity.this, "Password Harus Sama", "Terjadi Kesalahan");
                        return;
                    } else if (Utility.isNullOrBlank(aq.id(R.id.etEPpass2).getText().toString())) {
                        Utility.showDialogError(ForgetPasActivity.this, "Isilah Password baru", "Terjadi Kesalahan");
                        return;
                    } else if (Utility.isNullOrBlank(aq.id(R.id.etEPpessawal).getText().toString())) {
                        Utility.showDialogError(ForgetPasActivity.this, "Isilah Password lama anda", "Terjadi Kesalahan");
                        return;
                    }

                    Map<String, String> params = new HashMap<String, String>();
                    try {
                        params.put("old_password", Utility.toMD5(aq.id(R.id.etEPpessawal).getText().toString()));
                        params.put("new_password", Utility.toMD5(aq.id(R.id.etEPpass2).getText().toString()));
                    } catch (Exception e) {
                        Log.d("exception", String.valueOf(e));
                    }
                    params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));

                    aq.progress(progDialog).ajax(Constants.URL_EDPASS_API, params,
                            JSONObject.class, new AjaxCallback<JSONObject>() {
                                @Override
                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                        Utility.toast(ForgetPasActivity.this, "Pengubahan Password Berhasil!");
                                        finish();
                                    } else {
                                        Utility.toast(ForgetPasActivity.this, "Pengubahan Password Gagal!\nTerdapat kesalahan");
                                    }
                                }
                            });
                }
            });
        }
    }
}