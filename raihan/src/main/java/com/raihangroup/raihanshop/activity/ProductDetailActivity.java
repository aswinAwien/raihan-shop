package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ModelChannel;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.HintAdapter;
import com.raihangroup.raihanshop.adapter.ImageVpAdapter;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.CustomSpinnerAdapter;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.Helper;
import com.raihangroup.raihanshop.helper.IntegerObj;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Price;
import com.raihangroup.raihanshop.model.Product;
import com.raihangroup.raihanshop.model.Stock;
import com.raihangroup.raihanshop.model.SupplierPerformance;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailActivity extends AppCompatActivity {

    public static final int MIN_SUPPLIER_NAME = 15;
    @BindView(R.id.toolbar_detail)
    Toolbar toolbarDetail;
    private Product prod;
    private ViewPager mPager;
    private CirclePageIndicator circleIndicator;
    private AQuery aq;
    private int maxJumlah;
    private ArrayList<String> sizeNameList, colorNameList;
    private HashMap<String, Integer> hmSizeComp, hmPriceComp;
    private DBHelper dbh;

    IntegerObj price = new IntegerObj(0);
    int baseprice = 0;

    private String itemChoose[];
    private ProgressDialog progDialog;
    private boolean firstEdit = false;
    private Button nextButton;
    private boolean book_available = true;
    private ArrayAdapter<String> warnaSpinnerAdapter;
    private ArrayAdapter<String> jumlahSpinnerAdapter;
    private ArrayAdapter<String> ukuranSpinnerAdapter;

    private final static String TAG = "Product Detail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);
        aq = new AQuery(this);
        itemChoose = new String[3];

        productIdInEditMode = getIntent().getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_MODE, -1);
        if (productIdInEditMode == -1) {
            // we are in normal mode
            if (getIntent().getSerializableExtra(Constants.INTENT_PRODUCT_DETAIL) != null) {
                prod = (Product) getIntent().getSerializableExtra(Constants.INTENT_PRODUCT_DETAIL);
                Log.d("product", "" + prod.product_quality);
                Log.d("product", "" + prod.getName());
                init();
                getSupportActionBar().setTitle(prod.getName());
                Log.d("product id = ", "" + prod.getId());
            } else {
                int prod_id = getIntent().getIntExtra(Constants.INTENT_PRODUCT_ID, -1);
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", String.valueOf(prod_id));
                Log.d("product id = ", "" + prod_id);
                aq.progress(progDialog).ajax(Constants.URL_PRODDETAIL_API, params, String.class, new AjaxCallback<String>() {
                    @Override
                    public void callback(String url, String object, AjaxStatus status) {
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                            Gson gs = new Gson();
                            prod = gs.fromJson(object, Product.class);
                            Log.d("product qu", "" + prod.product_quality);
                            Log.d("product name", "" + prod.getName());
                            init();
                        }
                    }
                });
//                getSupportActionBar().setTitle(prod.getName());
            }
        } else {
            // we are in edit mode
            progDialog = new ProgressDialog(this);
            progDialog.setTitle("Mengambil Informasi Produk");
            progDialog.setMessage("Mohon Tunggu...");
            progDialog.setCancelable(false);
            firstEdit = true;
            Map<String, String> params = new HashMap<String, String>();
            params.put("product_id", String.valueOf(productIdInEditMode));
            aq.id(R.id.btnNext).text("Perbaharui " + getIntent().getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_STR));
            aq.progress(progDialog).ajax(Constants.URL_PRODDETAIL_API, params, String.class, new AjaxCallback<String>() {
                @Override
                public void callback(String url, String object, AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        Gson gs = new Gson();
                        prod = gs.fromJson(object, Product.class);
                        init();
                    }
                }
            });
        }

        if(PrefStorage.instance.getUserId() == -1){
            aq.id(R.id.llShareImage).gone();
            aq.id(R.id.rlFee).gone();
            aq.id(R.id.rlKomisi).gone();
            aq.id(R.id.rlVolume).gone();
        }else {
            aq.id(R.id.llShareImage).visible();
        }
    }

    private int productIdInEditMode;
    private String sizeName = "", cl = "";
    private int selectedSizeNameIdxInEditMode = 0, idx_cl = 0, jm;

    private void init() {

        sizeName = getIntent().getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ);
        cl = getIntent().getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL);
        jm = getIntent().getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, 0);


        toolbarDetail.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbarDetail);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new ImageVpAdapter(getSupportFragmentManager(), this, null, prod));
        nextButton = (Button) findViewById(R.id.btnNext);

        circleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        circleIndicator.setViewPager(mPager);
        circleIndicator.setFillColor(Color.WHITE);
        circleIndicator.setSnap(true);

        // *********** START OF SET *********** //
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", "" + prod.id_supplier);
        Log.d("id", "" + prod.id_supplier);
        aq.progress(R.id.pg).ajax(Constants.URL_SUPPLIER_PERFORMANCE_API, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();
                    return;
                }
                Gson gson = new Gson();
                final SupplierPerformance sPerf = gson.fromJson(object, SupplierPerformance.class);
                Log.d("sperf", "" + sPerf.ontime_delivery);
                Log.d("sperf", "" + sPerf.total_order);
                if (sPerf.total_order > 0) {
                    double ontime = 100 * sPerf.ontime_delivery / sPerf.total_order;
                    Log.d("sperf", "" + ontime);
                    double sent = 100 * sPerf.order_success / sPerf.total_order;
                    Log.d("sperf", "" + sent);
                    aq.id(R.id.tvTenantPerf).text("Ketepatan waktu " + String.format("%.0f", ontime) + "%, Pengiriman " + String.format("%.0f", sent) + "%");
                    aq.id(R.id.tvTenantRetur).text("Retur " + String.format("%.0f", (sPerf.broken_retur + sPerf.wrong_retur)) + " pcs");
                }
            }
        });

        if (prod.getCommission() > 0 && prod.getFee() > 0 && Double.parseDouble(prod.getVolume()) > 0) {
            aq.id(R.id.btnNext).clicked(this, "onClickAddToCart");
        } else {
            nextButton.setEnabled(false);
            book_available = false;
        }
        aq.id(R.id.llShareImage).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoViewFragment.Photo a = new PhotoViewFragment.Photo(
                        Constants.UPLOAD_URL + prod.getImages().get(mPager.getCurrentItem()).getImage(),
                        prod.getName(),
                        null);
                ModelChannel.shareItem(ProductDetailActivity.this, a.title, a.photo);
            }
        });
        aq.id(R.id.tvWeight).text(prod.getPrices().get(0).getWeight() + " gram");
        aq.id(R.id.tvSizeSummary).text(sizeComp(prod));
        aq.id(R.id.tvTitle).text(prod.getName());
        if (prod.product_quality != 0) {
            aq.id(R.id.tvProductQuality).text("(" + String.format("%.2f", prod.product_quality) + "/5.0)");
            ((RatingBar) aq.id(R.id.ratingBarQuality).getView())
                    .setRating(prod.product_quality);
        } else {
            aq.id(R.id.tvProductQuality).text("(Belum Ada Rating)");
        }
        // set supplier name
        if (prod.fullname == null) {
            aq.id(R.id.tvSupplier).text("Raihan");
        } else {
            aq.id(R.id.tvSupplier).text(prod.fullname);
        }

        if (sizePrice(prod)) { // ada yang beda
            aq.id(R.id.tvSizeAvailAll).gone();
            aq.id(R.id.spSizeAvail).visible();

            String[] arr = new String[prod.getPrices().size()];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = prod.getPrices().get(i).getSize();
            }

            aq.id(R.id.spSizeAvail).adapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                    arr)).itemSelected(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> arg0,
                                           View arg1, int arg2, long arg3) {
                    aq.id(R.id.tvSizePrice).text(
                            Utility.formatPrice(prod.getPrices().get(arg2).getPrice()));
                    aq.id(R.id.tvSizePriceTitle).text(
                            Utility.formatPrice(prod.getPrices().get(arg2).getPrice()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

        } else { // sama semua
            String ex = "";
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            for (String sz : hmPriceComp.keySet()) {
                sb.append(sz).append('/');
                ex = sz;
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(')');
            aq.id(R.id.tvSizeAvailAll).text(sb.toString());
            aq.id(R.id.tvSizePrice).text(Utility.formatPrice(hmPriceComp.get(ex)));
            aq.id(R.id.tvSizePriceTitle).text(Utility.formatPrice(hmPriceComp.get(ex)));
        }

        aq.id(R.id.tvComm).text(Utility.formatPrice(prod.getCommission()) + "/ pcs");


        sizeNameList = new ArrayList<String>();
        maxJumlah = 0;

        // set contents of size name list
        for (Stock stock : prod.getStock()) {
            // make size name list has unique contents
            if (!sizeNameList.contains(stock.getSize())) {
                sizeNameList.add(stock.getSize());
                Log.d(TAG, "size " + stock.getSize());
                // in edit mode, look for index of product size
                if (productIdInEditMode != -1 && sizeName.equalsIgnoreCase(stock.getSize())) {
                    selectedSizeNameIdxInEditMode = sizeNameList.size() - 1;
                }
            }
        }

        // set size name adapter
        aq.id(R.id.spUkuran).adapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,sizeNameList)).
                itemSelected(new OnItemSelectedListener() {
                    String ukuran;

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View arg1,
                                               int arg2, long id) {

                        ukuran = sizeNameList.get(arg2);
                        itemChoose[0] = ukuran;

                        // set contents of color name list
                        colorNameList = new ArrayList<String>();
                        for (Stock stock : prod.getStock()) {
                            // filter color name by size name
                            if (ukuran.equals(stock.getSize()) && !colorNameList.contains(stock.getColor()))
                                colorNameList.add(stock.getColor());
                        }

                        if (productIdInEditMode != -1 && firstEdit)
                            parent.setSelection(selectedSizeNameIdxInEditMode);
                        if (colorNameList.size() == 0) {
                            aq.id(R.id.tvTotPrice).text("stock habis").typeface(Typeface.DEFAULT_BOLD);
                        }

                        warnaSpinnerAdapter = new ArrayAdapter<String>(ProductDetailActivity.this,android.R.layout.simple_spinner_item,colorNameList);
                        aq.id(R.id.spWarna).adapter(warnaSpinnerAdapter);
                        aq.id(R.id.spWarna).itemSelected(this, "onItemSelectedWarna");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                    }

                    public void onItemSelectedWarna(AdapterView<?> parent, View arg1,
                                                    int arg2, long id) {
                        maxJumlah = 0;
                        for (Stock st : prod.getStock()) {
                            if (ukuran.equals(st.getSize()) && colorNameList.get(arg2).equals(st.getColor()))
                                if (maxJumlah < st.getStock()) maxJumlah = st.getStock();
                        }

                        itemChoose[1] = colorNameList.get(arg2);

                        if (maxJumlah == 0) {
                            aq.id(R.id.tvTotPrice).text("stock habis").typeface(Typeface.DEFAULT_BOLD);
                        }
                        String j[] = new String[maxJumlah];
                        if (productIdInEditMode != -1) {
                            j = new String[maxJumlah > jm ? maxJumlah : jm];
                        }

                        for (int i = 0; i < j.length; i++) {
                            j[i] = String.valueOf(i + 1);
                        }

                        if (productIdInEditMode != -1 && firstEdit) {
                            for (int i = 0; i < colorNameList.size(); i++) {
                                if (cl.equalsIgnoreCase(colorNameList.get(i))) {
                                    parent.setSelection(i);
                                }
                            }
                        }
                        jumlahSpinnerAdapter = new ArrayAdapter<String>(ProductDetailActivity.this,
                                android.R.layout.simple_spinner_item, j);
                        aq.id(R.id.spJumlah).adapter(jumlahSpinnerAdapter).itemSelected(this, "onItemSelectedJumlah");
                    }

                    public void onItemSelectedJumlah(AdapterView<?> parent, View arg1,
                                                     int arg2, long id) {

                        if (productIdInEditMode != -1 && firstEdit) {
                            parent.setSelection(jm - 1);
                        }

                        int amount = arg2 + 1;
                        for (Price p : prod.getPrices()) {
                            if (p.getSize().equals(itemChoose[0])) {
                                if (baseprice != 0) price.red(baseprice);

                                baseprice = amount * p.getPrice();
                                price.add(baseprice);
                            }
                        }
                        if (book_available) {
                            if (baseprice > 0) {
                                nextButton.setEnabled(true);
                            } else {
                                nextButton.setEnabled(false);
                            }
                        }
                        aq.id(R.id.tvTotPrice).text(Utility.formatPrice(price.getO()));
                        firstEdit = false;
                    }
                });



        // *********** END OF SET *********** //

        aq.id(R.id.tvLocation).text(prod.getLocation());
        aq.id(R.id.tvDescDetail).text(prod.getDescription());

        aq.id(R.id.tvFee).text(Utility.formatPrice(prod.getFee()));
        aq.id(R.id.tvVolume).text(prod.getVolume());

        aq.id(R.id.avProvince).gone();
        aq.id(R.id.avKecamatan).gone();
        aq.id(R.id.avKota).gone();
        aq.id(R.id.rlKirimHeader).gone();

        dbh = new DBHelper(this);

//		KecaCityAdapter kecAdapter = new KecaCityAdapter(this, dbh.findKecaByIdCity(1, ""), 
//				aq.id(R.id.avKecamatan).getEditText(), price,
//				aq.id(R.id.llKirimMethod).getView(), aq.id(R.id.tvTotPrice).getTextView());
//		CityProvAdapter cpadpt = new CityProvAdapter(this, dbh.findCityByIdProvince(1, ""), 
//				aq.id(R.id.avKota).getEditText(), kecAdapter);
//		ProvinceAdapter pvadpt = new ProvinceAdapter(this, dbh.findProvince(""), 
//				aq.id(R.id.avProvince).getEditText(),cpadpt);
//
//		((AutoCompleteTextView) findViewById(R.id.avProvince)).setAdapter(pvadpt);
//		((AutoCompleteTextView) findViewById(R.id.avProvince)).setOnItemClickListener(pvadpt);
//		((AutoCompleteTextView) findViewById(R.id.avKota)).setAdapter(cpadpt);
//		((AutoCompleteTextView) findViewById(R.id.avKota)).setOnItemClickListener(cpadpt);
//		((AutoCompleteTextView) findViewById(R.id.avKecamatan)).setAdapter(kecAdapter);
//		((AutoCompleteTextView) findViewById(R.id.avKecamatan)).setOnItemClickListener(kecAdapter);

        if (Utility.isAdmin() && productIdInEditMode == -1) {

            aq.id(R.id.llReferalIsi).visible();

            aq.id(R.id.btnCariOrang).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aq.id(R.id.llReferal).visible();
                    aq.id(R.id.pgRefer).visible();
                    aq.id(R.id.tvNamaref).gone();
                    aq.id(R.id.tvLvlref).gone();

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("reseller_id", aq.id(R.id.etRReferNumber).getText().toString());

                    aq.progress(R.id.pgRefer).ajax(Constants.URL_REFERAL_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject object,
                                             AjaxStatus status) {
                            if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                try {
                                    aq.id(R.id.tvNamaref).text(object.getString("referal_name")).visible();
                                    aq.id(R.id.tvLvlref).text(object.getString("referal_level")).visible();
                                } catch (JSONException e) {
                                    Log.d("exception", String.valueOf(e));
                                }
                            }
                        }
                    });
                }
            });
        }

        aq.id(R.id.btnNext).enabled(PrefStorage.instance.getStokStat());
        Log.d(TAG, "stok " + PrefStorage.instance.getStokStat());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean sizePrice(Product p) {
        hmPriceComp = new HashMap<String, Integer>();
        int start = p.getPrices().get(0).getPrice();

        for (Price pr : p.getPrices()) {
            hmPriceComp.put(pr.getSize(), pr.getPrice());
            if (start != pr.getPrice()) start = -1;
        }

        return start == -1;
    }

    private String sizeComp(Product p) {
        hmSizeComp = new HashMap<String, Integer>();
        for (Stock stok : p.getStock()) {
            hmSizeComp.put(stok.getSize(),
                    hmSizeComp.containsKey(stok.getSize()) ?
                            stok.getStock() + hmSizeComp.get(stok.getSize()) :
                            stok.getStock());
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Integer> ent : hmSizeComp.entrySet()) {
            sb.append(ent.getValue()).append(' ').append(ent.getKey());
            sb.append('|');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    // click methods

    public void onClickAddToCart(View v) throws JSONException {
        itemChoose[2] = String.valueOf(aq.id(R.id.spJumlah).getSpinner().getSelectedItemPosition() + 1);
        Map<String, String> params = new HashMap<String, String>();
        JSONObject jo = new JSONObject();
        progDialog = new ProgressDialog(this);

        String url = Constants.URL_ADDCART_API;

        if (productIdInEditMode == -1) {
            // normal case goes here
            jo.put("product_id", prod.getId());
            progDialog.setTitle("Tambahkan Ke Keranjang");
            if (Utility.isAdmin()) {
                if (Utility.isNullOrBlank(aq.id(R.id.etRReferNumber).getText().toString())) {
                    Utility.showDialogError(this, "Isilah Nomor Reseller", "Galat");
                    return;
                }
                params.put("reseller_id", aq.id(R.id.etRReferNumber).getText().toString());
                params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
            } else {
                params.put("reseller_id", String.valueOf(PrefStorage.instance.getUserId()));
            }
        } else {
            // edit case goes here
            jo.put("id", getIntent().getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, -1));
            progDialog.setTitle("Mengganti..");
            url = Constants.URL_EDCART_API;
        }

        boolean valid = false;
        if (!"".equals(itemChoose[1])) {
            jo.put("color", itemChoose[1]);
            if (!"".equals(itemChoose[0])) {
                jo.put("size", itemChoose[0]);
                if (!"".equals(itemChoose[2])) {
                    jo.put("qty", itemChoose[2]);
                    valid = true;
                } else {
                    Utility.toast(ProductDetailActivity.this, "Terjadi kesalahan pada jumlah Item");
                }
            } else {
                Utility.toast(ProductDetailActivity.this, "Terjadi kesalahan pada ukuran Item ");
            }
        } else {
            Utility.toast(ProductDetailActivity.this, "Terjadi kesalahan pada Warna Item");
        }
        if (valid) {
            params.put("product", jo.toString());
            progDialog.setMessage("Mohon Tunggu...");
            progDialog.setCancelable(false);

            aq.progress(progDialog).ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    Log.d("debug cart", url);
                    Log.d("debug cart", params.toString());

                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        if (productIdInEditMode == -1) {
                            // normal case, increment cart count
                            Utility.toast(ProductDetailActivity.this, "Masuk ke Keranjang");
                            PrefStorage.instance.changeCart(1);
                        } else {
                            Utility.toast(ProductDetailActivity.this, "Sukses Ubah " + getIntent().getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_STR));
                            Intent i = new Intent();
                            i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL, itemChoose[1]);
                            i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, Integer.parseInt(itemChoose[2]));
                            i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ, itemChoose[0]);
                            i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID,
                                    getIntent().getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, -1));
                            setResult(Activity.RESULT_OK, i);
                        }

                        finish();
                    } else {
                        String response = "";
                        try {
                            //response = object.getString("message").toString();
                            Log.d("response", response);
                        } catch (Exception e) {
                            Log.d("exception", String.valueOf(e));
                            Utility.toast(getApplicationContext(), "err " + e.getMessage());
                            response = "Terjadi kesalahan pada Server";
                        } finally {
                            Utility.toast(ProductDetailActivity.this, "Operasi tidak berhasil. Pesan: " + response);
                        }
                    }
                }
            });
        }
    }

    public void onClickCopyToClipboard(View v) {
        Utility.toast(getApplicationContext(), "Deskripsi berhasil disalin ke clipboard");
        Helper.copyToClipboard(this, aq.id(R.id.tvDescDetail).getText().toString());
    }

    public void onClickToSupplierDetail(View v) {
        Bundle b = new Bundle();
        b.putString("supplierId",String.valueOf(prod.id_supplier));
        Intent i = new Intent(this, SupplierProfileActivity.class);
        i.putExtras(b);
        startActivity(i);
    }
}
