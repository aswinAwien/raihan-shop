package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.SupplierRatingAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.Rating;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RatingListActivity extends AppCompatActivity {

    @BindView(R.id.toobal_list_message)
    Toolbar toobalListMessage;
    private Context context;
    private AQuery aq;
    private ListView listView;
    private ProgressDialog progDialog;
    private SupplierRatingAdapter adapter;
    private String orderId;
    private final static String TAG = "Rating List";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_with_message);
        ButterKnife.bind(this);
        toobalListMessage.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalListMessage);
        context = this;
        aq = new AQuery(this);
        PrefStorage.newInstance(context);
        listView = (ListView) aq.id(R.id.listView).getView();
        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setMessage("Melakukan rating");
        progDialog.setCancelable(false);
        listView.setDivider(null);
        listView.setFocusable(false);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            orderId = b.getString("orderId");
        }
        // set message here
        aq.id(R.id.tvMessageTitle).text("Tidak Ada Info Rating");
        aq.id(R.id.tvMessageContent).text("Silakan mencoba kembali");
        refreshData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rating, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:
                performRating();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // support method

    private void performRating() {
        if (adapter == null) {
            DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Daftar rating tidak tersedia. Silakan mencoba kembali", "OK", null);
            return;
        }

        // construct json array for parameter
        JSONArray ratingJsonArr = new JSONArray();
        for (int i = 0; i < adapter.getCount(); i++) {
            Rating rating = (Rating) listView.getAdapter().getItem(i);
            JSONObject ratingJson = new JSONObject();
            try {
                ratingJson.put("id_supplier", rating.id_supplier);
                ratingJson.put("quality", adapter.arrQuality[i]);
                ratingJson.put("service", adapter.arrService[i]);
                ratingJson.put("testimonial", adapter.arrComment[i]);
            } catch (JSONException e) {
                Log.d("exception", String.valueOf(e));
            }
            ratingJsonArr.put(ratingJson);
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_order", orderId);
        params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
        params.put("ratings", ratingJsonArr.toString());
        aq.progress(progDialog).ajax(Constants.URL_RATE_SUPPLIER_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil melakukan rating. Silakan mencoba kembali", "OK", null);
                    return;
                }
                try {
                    if (object.getString("success").equals("true")) {
                        Log.d(TAG, "message " + object.toString());
                        DialogHelper.showMessageDialog(context, "Rating Berhasil", "Terima kasih sudah memberikan rating", "OK", new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setResult(Activity.RESULT_OK);
                                finish();
                            }
                        });
                    } else {
                        DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengubah profile. Silakan mencoba kembali", "OK", null);
                    }
                } catch (JSONException e) {
                    Log.d("exception", String.valueOf(e));
                }
            }
        });
    }

    private void refreshData() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_order", orderId);
        params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
        aq.progress(R.id.pg).ajax(Constants.URL_LIST_SUPPLIER_FOR_RATING_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {

            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                aq.id(R.id.llInfo).gone();
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();

                    return;
                }
                Gson gs = new Gson();
                ArrayList<Rating> listOfRating = gs.fromJson(object.toString(),
                        new TypeToken<List<Rating>>() {
                        }.getType());
                adapter = new SupplierRatingAdapter(context, listOfRating);
                listView.setAdapter(adapter);
            }
        });
    }
}