package com.raihangroup.raihanshop.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment.Photo;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.model.MessageObj;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity to display Message Content
 */
public class MessageActivity extends AppCompatActivity {

    @BindView(R.id.toobal_message)
    Toolbar toobalMessage;
    private AQuery aq;
    private DBHelper dbh;
    private MessageObj message;

    private final static String TAG = "MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);

        aq = new AQuery(this);
        dbh = new DBHelper(this);
        message = getIntent().getParcelableExtra(Constants.INTENT_MESSAGE_OBJ);

        toobalMessage.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalMessage);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(message.getTitle());

        // mark message as read on server
        Map<String, String> params = new HashMap<String, String>();
        params.put("message_id", String.valueOf(message.getId()));
        aq.ajax(Constants.URL_READMESSAGE_API, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                dbh.changeStatReadMessage(message.getId());
                Log.d(TAG, "message " + message.getTitle() + " as read on server");
            }
        });

        aq.id(R.id.tvContentMsg).text(message.getMessage());
        ImageView imPesan = aq.id(R.id.imMsg).getImageView();

        ImageTagFactory imageTagFactory = ImageTagFactory.newInstance(this, R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
        if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
            imPesan.setTag(imageTagFactory.build(Constants.MESSAGE_IMG_URL + message.getImage(), this));
            ImageLoaderApplication.getImageLoader().getLoader().load(imPesan);
        }
        imPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Photo[] p = new Photo[1];
                p[0] = new Photo(Constants.MESSAGE_IMG_URL + message.getImage(), message.getTitle(), null);
                PhotoViewFragment.show(MessageActivity.this, 0, p);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                dbh.close();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
