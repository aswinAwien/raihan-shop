package com.raihangroup.raihanshop.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioGroup;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CartItemsProcessor;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.SupplierShipCalc;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.BookReference;
import com.raihangroup.raihanshop.model.CartItem;
import com.raihangroup.raihanshop.model.ShippingInfo;
import com.raihangroup.raihanshop.model.User;
import com.google.gson.Gson;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity to making order from booking
 * step 4 of 9
 */
public class BookingOrderActivity extends AppCompatActivity {

    @BindView(R.id.toobal_booking_order)
    Toolbar toobalBookingOrder;
    private AQuery aq;
    private ImageTagFactory imageTagFactory;
    private BookReference bookingReference;
    private List<CartItem> listOfCartItems;
    private int totalItemPrice;
    private int totalCommission;
    private int totalItemWeight;
    private int totalShippingCost;
    protected String alternateSender = "";
    protected String commissionInfo = "1";
    private static final String TAG = "BOOKING ORDER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_order);
        ButterKnife.bind(this);
        aq = new AQuery(this);
        imageTagFactory = ImageTagFactory.newInstance(this, R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
        toobalBookingOrder.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toobalBookingOrder);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bookingReference = getIntent().getParcelableExtra(Constants.INTENT_BOOK_2_ORD);

        // get cart items from booking reference
        listOfCartItems = bookingReference.getProducts();

        aq.id(R.id.etBName).text(bookingReference.getShipping().getName());
        Log.d(TAG, "shipping info name " + bookingReference.getShipping().getName());

        ((RadioGroup) aq.id(R.id.rgKomisi).getView()).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbOKons:
                        commissionInfo = "1";
                        break;
                    case R.id.rbODist:
                        commissionInfo = "2";
                        break;
                }
                aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(totalItemPrice +
                        (commissionInfo.equals("1") ? 0 : -totalCommission)
                        + totalShippingCost));
            }
        });

        // add cart item views
        for (int i = 0; i < listOfCartItems.size(); i++) {
            CartItem cartItem = listOfCartItems.get(i);
            LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 10, 0, 0);
            ((LinearLayout) findViewById(R.id.llBookContainer)).addView(
                    generateCartItemView(cartItem), lp);
        }
        calculateWhatMattersInCartItems();

        // init some text views
        aq.id(R.id.tvBeratSum).text(totalItemWeight + " gram");
        // set shipping price
        aq.id(R.id.tvPriceOngkir).text(Utility.formatPrice(totalShippingCost));
        // set total price with total item price
        aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(totalItemPrice));
        // set check box default sender
        if (aq.id(R.id.checkBoxDefaultSender).isChecked()) {
            // hide alternate sender field if checked
            aq.id(R.id.etSender).gone();
        }
        User user = new Gson().fromJson(PrefStorage.instance.getUserData(), User.class);
        aq.id(R.id.checkBoxDefaultSender).text("Kirim Pesanan Sebagai " + user.getFullname());
        CheckBox cb = (CheckBox) (aq.id(R.id.checkBoxDefaultSender).getView());
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (aq.id(R.id.checkBoxDefaultSender).isChecked()) {
                    // hide alternate sender field if checked
                    aq.id(R.id.etSender).gone();
                } else {
                    aq.id(R.id.etSender).visible();
                }
            }
        });

        // calculate total shipping cost
        HashMap<String, SupplierShipCalc> hm = CartItemsProcessor.collectWeightPerSupplierId(listOfCartItems, true);
        for (Map.Entry<String, SupplierShipCalc> i : hm.entrySet()) {
            int shippingPrice = i.getValue().calculateShippingPrice();
            totalShippingCost += shippingPrice;
        }
        aq.id(R.id.tvPriceOngkir).text(Utility.formatPrice(totalShippingCost));
        aq.id(R.id.tvPriceTotal).text(Utility.formatPrice(totalItemPrice +
                (commissionInfo.equals("2") ? -totalCommission : 0)
                + totalShippingCost));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // click methods

    public void onClickOrder(View v) {
        ShippingInfo si = bookingReference.getShipping();
        si.setAddress(aq.id(R.id.etBAddress).getText().toString());
        si.setZipcode(aq.id(R.id.etBZipCode).getText().toString());
        si.setPhone(aq.id(R.id.etBPhone).getText().toString());
        if (aq.id(R.id.checkBoxDefaultSender).isChecked()) {
            alternateSender = aq.id(R.id.etSender).getText().toString();
        }
        si.setAlternate_sender(aq.id(R.id.etSender).getText().toString());
        boolean check = false;
        if (Utility.isNullOrBlank(aq.id(R.id.etBName).getText().toString())) {
            Utility.showDialogError(this, "Isilah Nama Pemesan", "Terjadi Kesalahan");
        } else if (Utility.isNullOrBlank(si.getAddress())) {
            Utility.showDialogError(this, "Isilah Alamat Tujuan", "Terjadi Kesalahan");
        } else if (Utility.isNullOrBlank(si.getPhone())) {
            Utility.showDialogError(this, "Isilah Nomor Telepon Tujuan", "Terjadi Kesalahan");
        } else {
            check = true;
        }
        if (!check) {
            return;
        }
        Intent i = new Intent(this, OrderPaymentActivity.class);
        i.putExtra(Constants.INTENT_BOOK_2_ORD, bookingReference);
        i.putExtra(Constants.INTENT_ONGKIR_TOTAL_INFO, totalShippingCost);
        i.putExtra(Constants.INTENT_COMISSION_INFO, commissionInfo);
        i.putExtra(Constants.INTENT_ALTERNATE_SENDER, alternateSender);
        startActivity(i);
    }

    // support methods

    private View generateCartItemView(CartItem ci) {
        View r = getLayoutInflater().inflate(R.layout.package_item_tv, null);
        AQuery a = new AQuery(r);
        a.id(R.id.tvTitle).text(ci.getName());
        a.id(R.id.tvHargaMasingProd).text(Utility.formatPrice(ci.getPrice()));
        a.id(R.id.tvBukuran).text(ci.getSize());
        a.id(R.id.tvBwarna).text(ci.getColor());
        a.id(R.id.tvBJumlah).text(ci.getQty() + " item");
        a.id(R.id.tvBberat).text((ci.getQty() * ci.getWeight()) + " gram");
        a.id(R.id.tvSubTotal).text(Utility.formatPrice(ci.getPrice() * ci.getQty()));

        // get first image
        if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
            a.id(R.id.imageProduct).getImageView().setTag(imageTagFactory.build(
                    Constants.UPLOAD_URL + ci.getImage1(), this));
            ImageLoaderApplication.getImageLoader().getLoader().load(a.id(R.id.imageProduct).getImageView());
        }
        return r;
    }

    private void calculateWhatMattersInCartItems() {
        // calculate total weight
        for (CartItem ci : listOfCartItems) {
            totalItemWeight += (ci.getWeight() * ci.getQty());
            totalItemPrice += (ci.getPrice() * ci.getQty());
            totalCommission += (ci.getCommission() * ci.getQty());
        }
    }

}
