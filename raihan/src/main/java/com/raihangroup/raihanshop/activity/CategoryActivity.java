package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.CategoryListFragment;
import com.raihangroup.raihanshop.helper.PrefStorage;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * activity to show product from category selected
 */
public class CategoryActivity extends AppCompatActivity {

    Toolbar toolbar;
    private Context context;
    private CategoryListFragment categoryFragment;
    public static final int PRODUCT_BY_CATEGORY_MODE = 0;
    public static final int PRODUCT_BY_SUPPLIER_MODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pilihan Produk");
        context = this;
        PrefStorage.newInstance(this);
        categoryFragment = new CategoryListFragment();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            categoryFragment.setArguments(b);
            String name = b.getString("categoryParentName");
            getSupportActionBar().setSubtitle(name);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, categoryFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (PrefStorage.instance.getCartAmount() == 0) {
            menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart);
        } else {
            menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif);
            if (PrefStorage.instance.getCartAmount() <= 10 && PrefStorage.instance.getCartAmount() >= 1) {
                menu.findItem(R.id.action_cart).setIcon(this.getResources().getIdentifier("ic_cart_notif_" + PrefStorage.instance.getCartAmount(), "drawable", this.getPackageName()));
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem talkToBosMenuItem = menu.findItem(R.id.action_talk_to_bos);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        talkToBosMenuItem.setVisible(false);
        searchMenuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_cart) {
            Intent i = new Intent(this, ShoppingCartActivity.class);
            startActivity(i);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}