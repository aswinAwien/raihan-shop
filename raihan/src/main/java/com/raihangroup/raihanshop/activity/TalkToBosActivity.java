package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.FeedbackReplyListFragment;
import com.raihangroup.raihanshop.fragments.PostToBosFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TalkToBosActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Context context;

    private FeedbackReplyListFragment feedbackReplyListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        context = this;
        feedbackReplyListFragment = new FeedbackReplyListFragment();

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            String subject = b.getString("subject");
            int type = b.getInt("type");

            getSupportActionBar().setTitle(subject);
            getSupportActionBar().setSubtitle(PostToBosFragment.POST_TO_BOS_TYPE[type - 1]);

            feedbackReplyListFragment.setArguments(b);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, feedbackReplyListFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}