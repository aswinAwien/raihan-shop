package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.LoginActivity;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.NotificationObj;

public class InfoActivity extends Activity {

    private AQuery aq;
    private Parcelable obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_info);
        aq = new AQuery(this);
        obj = getIntent().getParcelableExtra(Constants.INTENT_NOTIFICATION_OBJ);
        if (obj != null) {
            if (((NotificationObj) obj).getType().equals("1")) {
                aq.id(R.id.tvInfoText).text("Anda telah terdaftar sebagai member Raihan Online Shop\n\n\nLogin Sekarang?\n");
                aq.id(R.id.tvInfoTitle).visible();
            } else if (((NotificationObj) obj).getType().equals("5") || ((NotificationObj) obj).getType().equals("4")) {
                NotificationObj nop = (NotificationObj) obj;
                aq.id(R.id.tvInfoText).text(nop.getContent());
                if (((NotificationObj) obj).getType().equals("4"))
                    aq.id(R.id.btnAction).text("Cek Order");
                aq.id(R.id.btnAction).clicked(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(InfoActivity.this, HomeActivity.class);
                        //performance
                        if (((NotificationObj) obj).getType().equals("5")) {
                            i = new Intent(InfoActivity.this, PerformanceViewActivity.class);
                            //order
                        } else {
                            i.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_ORD);
                        }
                        startActivity(i);
                        finish();
                    }
                });
                return;
            }
        }
        aq.id(R.id.btnAction).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent i = new Intent(InfoActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}