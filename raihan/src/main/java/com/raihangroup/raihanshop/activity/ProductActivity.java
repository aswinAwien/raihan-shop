package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.ProductListFragment;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductActivity extends AppCompatActivity {

    Toolbar toolbar;
    private Context context;
    private ProductListFragment productFragment;
    private MenuItem searchMenuItem;
    private MenuItem cartMenuItem;
    private MenuItem friendMenuItem;
     public static final int PRODUCT_BY_CATEGORY_MODE = 0;
    public static final int PRODUCT_BY_SUPPLIER_MODE = 1;
    public static final int PRODUCT_BY_SORTING_MODE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        PrefStorage.newInstance(this);
        productFragment = new ProductListFragment();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            productFragment.setArguments(b);
            int mode = b.getInt("mode");
            if (mode == PRODUCT_BY_CATEGORY_MODE) {
                String categoryName = b.getString("categoryName");
                getSupportActionBar().setTitle(categoryName); // set category name as title on action bar
            } else if (mode == PRODUCT_BY_SUPPLIER_MODE) {
                String supplierName = b.getString("supplierName");
                getSupportActionBar().setTitle(supplierName); // set supplier name as title on action bar
            }else if(mode == PRODUCT_BY_SORTING_MODE){
                String sortBy = b.getString("sortName");
                getSupportActionBar().setTitle(sortBy);
            }
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.viewFragment, productFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
            if (PrefStorage.instance.getCartAmount() == 0) {
                menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart);
            } else {
                menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif);
                if (PrefStorage.instance.getCartAmount() >= 1 && PrefStorage.instance.getCartAmount() <= 10) {
                    menu.findItem(R.id.action_cart).setIcon(getResourceIdByName(this, "ic_cart_notif_" +
                            PrefStorage.instance.getCartAmount(), "drawable"));
                }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem talkToBosMenuItem = menu.findItem(R.id.action_talk_to_bos);
        talkToBosMenuItem.setVisible(false); // hide this item
        searchMenuItem = menu.findItem(R.id.action_search);
        cartMenuItem = menu.findItem(R.id.action_cart);
        friendMenuItem = menu.findItem(R.id.action_friend);
        if(PrefStorage.instance.getUserId()==-1){
            cartMenuItem.setVisible(false);
            friendMenuItem.setVisible(false);
        }

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem arg0) {
                productFragment.prepareSearchMode();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem arg0) {
                productFragment.quitSearchMode();
                return true;
            }
        });
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        searchView.setOnQueryTextListener(new SearchProductsListener());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_cart) {
            Intent i = new Intent(this, ShoppingCartActivity.class);
            startActivity(i);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    // inner class listener

    private class SearchProductsListener implements SearchView.OnQueryTextListener {

        @Override
        public boolean onQueryTextChange(String s) {
            // do nothing
            return false;
        }

        @Override
        public boolean onQueryTextSubmit(String s) {
            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            Utility.clearSoftInputFromView(context, searchView);
            productFragment.enterSearchMode(s);
            return false;
        }
    }

    public static int getResourceIdByName(Context context, String resName, String resType) {
        int resId = context.getResources().getIdentifier(resName, resType,
                context.getPackageName());
        return resId;
    }
}
