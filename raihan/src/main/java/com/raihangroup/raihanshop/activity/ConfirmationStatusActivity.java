package com.raihangroup.raihanshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;

public class ConfirmationStatusActivity extends AppCompatActivity {

    private AQuery aq;
    private String confStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_status);
        confStatus = getIntent().getStringExtra(Constants.INTENT_BOOKING_NAME);
        aq = new AQuery(this);
        aq.id(R.id.__tv3).text(confStatus);
        aq.id(R.id.btnAction).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ConfirmationStatusActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_BOOK);
        startActivity(intent);
    }
}
