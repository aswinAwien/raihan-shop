package com.raihangroup.raihanshop.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;

import com.raihangroup.raihanshop.R;

public class SingleModeActivity extends AppCompatActivity {
    public static final String USE_PROGRESS = "useProgress";
    public static final String ORIENTATION = "orientation";
    public static final String THEME = "theme";
    protected Fragment frag;
    protected String title;
    protected String subtitle;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_PROGRESS);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
//		AppEventsLogger.activateApp(this);     

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey(THEME))
                setTheme(b.getInt(THEME));
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame);
        if (getSupportActionBar() != null) {
            setSupportProgressBarIndeterminateVisibility(false);
        }

        if (b != null) {
            if (b.containsKey("fragment")) {
                frag = Fragment.instantiate(this, b.getString("fragment"));
                frag.setArguments(b);
            }
            if (b.containsKey(ORIENTATION)) {
                setRequestedOrientation(b.getInt(ORIENTATION));
            }
        }
        if (frag != null) {
            // set the Behind View Fragment
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, frag)
                    .commit();
        }

        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.content);
    }

//	@Override
//	public void finish() {
//        View v = findViewById(android.R.id.content);
//    	ViewPropertyAnimator.animate(v)
//	        .alpha(0)
//	        .setDuration(500)
//	        .setListener(new AnimatorListenerAdapter() {
//				@Override
//				public void onAnimationEnd(Animator animation) {
//					SingleModeActivity.super.finish();
//				}
//	        });
//	}

//	@Override
//	public void setTitle(CharSequence title) {
//		getSupportActionBar().setSubtitle(title);
//		super.setTitle(getResources().getString(R.string.app_name));
//	}


    @Override
    public void setTitle(CharSequence title) {
        if (this.title == null)
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(title);
            } else {
                super.setTitle(title);
            }
        else if (this.subtitle == null && getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(title);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        findViewById(android.R.id.content).invalidate();
    }

    public static void initFragment(Context ctx, Bundle b, Class<? extends Fragment> cls) {
        b.putString("fragment", cls.getName());
        Intent intent = new Intent(ctx, SingleModeActivity.class).putExtras(b);
        ctx.startActivity(intent);
    }

    public static Intent initFragmentIntent(Context ctx, Class<? extends Fragment> cls) {
        Bundle b = new Bundle();
        b.putString("fragment", cls.getName());
        return new Intent(ctx, SingleModeActivity.class).putExtras(b);
    }

    public static void initFragment(Context ctx, Fragment f) {
        Bundle b = f.getArguments();
        if (b == null) b = new Bundle();
        b.putString("fragment", f.getClass().getName());
        Intent intent = new Intent(ctx, SingleModeActivity.class).putExtras(b);
        ctx.startActivity(intent);
    }
}
