package com.raihangroup.raihanshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.BookingListFragment;
import com.raihangroup.raihanshop.fragments.CategoryParentListFragment;
import com.raihangroup.raihanshop.fragments.FeedbackListFragment;
import com.raihangroup.raihanshop.fragments.FrontMainFragment;
import com.raihangroup.raihanshop.fragments.GroupChatFragment;
import com.raihangroup.raihanshop.fragments.HomeFragmentNew;
import com.raihangroup.raihanshop.fragments.MoreFragment;
import com.raihangroup.raihanshop.fragments.NotificationListFragment;
import com.raihangroup.raihanshop.fragments.OrderListFragment;
import com.raihangroup.raihanshop.fragments.ProductListFragment;
import com.raihangroup.raihanshop.fragments.SupplierListFragment;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.CartItem;
import com.raihangroup.raihanshop.model.Category;
import com.raihangroup.raihanshop.model.User;
import com.raihangroup.raihanshop.model.UserProfile;
import com.raihangroup.raihanshop.model.UserRsvAdmin;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
//import com.mancj.materialsearchbar.MaterialSearchBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    private static final int STATE_FRAGMENT_LIST = 1;
    private static final int STATE_FRAGMENT_BOOK = 2;
    private static final int STATE_FRAGMENT_ORD = 3;
    public static final int REQ_CODE_TALK_TO_BOS = 2;
    public static final int REQ_CODE_LIST_ORDER = 3;

    private int stateFragment = 1;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private Context context;
    private AQuery aq;
    private User user;
    private Fragment activeFragment;
    private ProductListFragment newProductFragment;

    /**
     * Category parent list fragment.
     */
    private FrontMainFragment frontMainFragment;
    private HomeFragmentNew homeFragment;
    private NotificationListFragment notificationListFragment;
    private CategoryParentListFragment catParentListFragment;
    private SupplierListFragment supplierFragment;
    private BookingListFragment bookFragment;
    private OrderListFragment ordListFragment;
    private FeedbackListFragment feedbackListFragment;
    private GroupChatFragment upLineGroupChatFragment;
    private GroupChatFragment downLineGroupChatFragment;
    private MoreFragment moreFragment;
    private FirebaseInstanceId gcm;
    private String regid;
    private MenuItem searchMenuItem;
    private DBHelper dbh;
    private MenuItem spMenuItem;
    private Fragment fragmentDrawer;
    private FloatingSearchView floatingSearchView;

    public int idChooseB, idChooseO;
    private List<UserRsvAdmin> uRsv;
    private ProgressDialog progDialog;

    private final static String TAG = "Home";

    private UserProfile profile;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        context = this;
        aq = new AQuery(this);
        PrefStorage.newInstance(this);
        dbh = new DBHelper(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
//        ab.setDisplayHomeAsUpEnabled(true);

        // check tour app status
        checkTourAppStatus();


        // set new product amount in side menu
        int newProductAmount = PrefStorage.instance.getUnreadNewProductAmount();
        aq.id(R.id.tvNewProductAmount).text(String.valueOf(newProductAmount));
        int unreadNotification = PrefStorage.instance.getUnreadNotification();
        aq.id(R.id.tvNewNotifAmount).text(String.valueOf(unreadNotification));
        int unreadUpLine = PrefStorage.instance.getUnreadUpline();
        aq.id(R.id.tvNewUpline).text(String.valueOf(unreadUpLine));
        int unreadDownLine = PrefStorage.instance.getUnreadDownline();
        aq.id(R.id.tvNewDownline).text(String.valueOf(unreadDownLine));
        int unreadFeedback = PrefStorage.instance.getUnreadFeedback();
        aq.id(R.id.tvNewFeedback).text(String.valueOf(unreadFeedback));
        hideEmptyCounter();


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setFitsSystemWindows(true);
        notificationListFragment = new NotificationListFragment();

        newProductFragment = new ProductListFragment();
        Bundle bundleProduct = new Bundle();
        bundleProduct.putInt("categoryId", 0);
        bundleProduct.putInt("mode", ProductActivity.PRODUCT_BY_CATEGORY_MODE);
        newProductFragment.setArguments(bundleProduct);

        upLineGroupChatFragment = new GroupChatFragment();
        Bundle bundleUpLineChat = new Bundle();
        bundleUpLineChat.putInt("mode", Constants.GROUP_UPLINE);
        upLineGroupChatFragment.setArguments(bundleUpLineChat);

        downLineGroupChatFragment = new GroupChatFragment();
        Bundle bundleDownLineChat = new Bundle();
        bundleDownLineChat.putInt("mode", Constants.GROUP_DOWNLINE);
        downLineGroupChatFragment.setArguments(bundleDownLineChat);

        homeFragment = new HomeFragmentNew();

        frontMainFragment = new FrontMainFragment();

        catParentListFragment = new CategoryParentListFragment();

        supplierFragment = new SupplierListFragment();

        bookFragment = new BookingListFragment();

        ordListFragment = new OrderListFragment();

        feedbackListFragment = new FeedbackListFragment();

        moreFragment = new MoreFragment();

        // set active fragment
//        activeFragment = homeFragment;
        activeFragment = frontMainFragment;
        switchToActiveFragment();

            user = (User) getIntent().getSerializableExtra(Constants.INTENT_USER_OBJ);
            if (user == null) {
                // from splash page
                user = new Gson().fromJson(PrefStorage.instance.getUserData(), User.class);
            }


            String id = String.valueOf(user.getId());
            Map<String, String> params = new HashMap<String, String>();
            params.put("id", id);
            aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {

                    if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                        return;
                    }

                    Gson gs = new Gson();
                    profile = gs.fromJson(object.toString(), UserProfile.class);

                    String birthDateText = profile.birth_date;
                    try {
                        Date birthDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(profile.birth_date);

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        birthDateText = sdf.format(birthDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (!profile.image.equals("img_default_user.png")) {
                        aq.id(R.id.homeProfileImage).image(Constants.BASE_URL + profile.image);
                    }
                }
            });


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
            }

            public void onDrawerOpened(View drawerView) {
            }
        };

        floatingSearchView.attachNavigationDrawerToMenuButton(mDrawerLayout);

        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Fetching list user");
        progDialog.setMessage("Please wait a moment");
        progDialog.setCancelable(false);

//        mDrawerLayout.setDrawerListener(mDrawerToggle);


        aq.id(R.id.tvNameHome).text(user.getFullname());

        // update selected item and title, then close the drawer
        mDrawerLayout.closeDrawers();

        floatingSearchView.setOnQueryChangeListener(new SearchNewProductsListener());
        aq.id(R.id.bLogout).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dbh.clearMessage();
                PrefStorage.instance.clear();
                startActivity(new Intent(HomeActivity.this, unLoggedInHomeActivity.class));
//                try {
//                    gcm.deleteInstanceId();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                finish();

                Utility.toast(HomeActivity.this, "Log out");
            }
        });




        if (checkPlayServices()) {
            gcm = FirebaseInstanceId.getInstance();
            regid = PrefStorage.instance.getGCMid();
        }

        aq.id(R.id.tvNameHome).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToHomeFragment();
                mDrawerLayout.closeDrawers();
            }
        });

        // check status like
        checkStatusInfo();

    }

    @Override
    protected void onResume() {
        super.onResume();
        supportInvalidateOptionsMenu();
        // set unread message in side menu
        setUnreadMessagesNumber(PrefStorage.instance.getUnreadMessages());
        int reload = getIntent().getIntExtra(Constants.INTENT_RELOAD_HOME, 0);

        switch (reload) {
            case Constants.RELOAD_BOOK:
                if (Utility.isAdmin()) {
                    ocBookList(findViewById(R.id.llBookingList));
                    stateFragment = STATE_FRAGMENT_BOOK;
                    break;
                }
                activeFragment = bookFragment;
                switchToActiveFragment();
                mDrawerLayout.closeDrawers();
                stateFragment = STATE_FRAGMENT_BOOK;
                break;
            case Constants.RELOAD_ORD:
                ordListFragment = new OrderListFragment();
                activeFragment = ordListFragment;
                //ordListFragment.getOrderList();
                switchToActiveFragment();

                mDrawerLayout.closeDrawers();
                stateFragment = STATE_FRAGMENT_ORD;
                break;
            case Constants.RELOAD_PRODUCT_LIST:
                stateFragment = STATE_FRAGMENT_LIST;
                activeFragment = newProductFragment;
                switchToActiveFragment();
                newProductFragment.initPage();
                uRsv = null;
                searchMenuItem.setVisible(true);
                spMenuItem.setVisible(false);

                mDrawerLayout.closeDrawers();
                break;
            default:
                Log.d(TAG, "nothing to reload");
                break;
        }
        getIntent().putExtra(Constants.INTENT_RELOAD_HOME, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                for (int i = 0; i < bookFragment.adapter.getGroupCount(); i++) {
                    for (int j = 0; j < bookFragment.adapter.getChildrenCount(i); j++) {
                        CartItem ci = (CartItem) bookFragment.adapter.getChild(i, j);
                        if (ci.getId() == data.getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, -1)) {
                            ci.setColor(data.getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL));
                            ci.setQty(data.getIntExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, 0));
                            ci.setSize(data.getStringExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ));
                        }
                    }
                }
                bookFragment.adapter.notifyDataSetChanged();
            }
        } else if (requestCode == REQ_CODE_TALK_TO_BOS) {
            if (resultCode == Activity.RESULT_OK) {
                // refresh feedback talk to bos fragment
                feedbackListFragment.refreshData();
            }
        } else if (requestCode == REQ_CODE_LIST_ORDER) {
            if (resultCode == Activity.RESULT_OK) {
                // refresh feedback talk to bos fragment
                ordListFragment.getOrderList();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int reload = intent.getIntExtra(Constants.INTENT_RELOAD_HOME, -1);
        setIntent(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    //option menu methods
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem talkToBosMenuItem = menu.findItem(R.id.action_talk_to_bos);
        searchMenuItem = menu.findItem(R.id.action_search);
        spMenuItem = menu.findItem(R.id.menu_spinner);

        if (activeFragment == newProductFragment) {
            searchMenuItem.setVisible(true);
        } else {
            searchMenuItem.setVisible(false);
        }

        if (activeFragment == feedbackListFragment) {
            talkToBosMenuItem.setVisible(true);
        } else {
            talkToBosMenuItem.setVisible(false);
        }

        if (activeFragment == upLineGroupChatFragment || activeFragment == downLineGroupChatFragment) {
            menu.findItem(R.id.action_cart).setVisible(false);
        }

        if (PrefStorage.instance.getCartAmount() == 0) {
            menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart);
        } else {
            switch (PrefStorage.instance.getCartAmount()) {
                case 1:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_1);
                    break;
                case 2:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_2);
                    break;
                case 3:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_3);
                    break;
                case 4:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_4);
                    break;
                case 5:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_5);
                    break;
                case 6:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_6);
                    break;
                case 7:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_7);
                    break;
                case 8:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_8);
                    break;
                case 9:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_9);
                    break;
                case 10:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif_10);
                    break;
                default:
                    menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_notif);
                    break;
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        searchMenuItem = menu.findItem(R.id.action_search);

//        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new OnActionExpandListener() {
//
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem arg0) {
//                newProductFragment.prepareSearchMode();
//                return true;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem arg0) {
//                newProductFragment.quitSearchMode();
//                return true;
//            }
//        });
        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
//        searchView.setOnQueryTextListener(new SearchNewProductsListener());
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        spMenuItem = menu.findItem(R.id.menu_spinner);

        if (uRsv != null) spMenuItem.setVisible(true);

        if (spMenuItem.isVisible()) {
            Spinner sp = (Spinner) MenuItemCompat.getActionView(menu.findItem(R.id.menu_spinner));
            //
            ArrayAdapter<UserRsvAdmin> mSpinnerAdapter =
                    new ArrayAdapter<UserRsvAdmin>(this, R.layout.spinnercustombar, uRsv);

            sp.setAdapter(mSpinnerAdapter); // set the adapter
            sp.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    UserRsvAdmin ua = (UserRsvAdmin) parent.getItemAtPosition(position);
                    Fragment f = bookFragment;
                    if (stateFragment == STATE_FRAGMENT_BOOK) {
                        idChooseB = ua.getId_member();
                    } else if (stateFragment == STATE_FRAGMENT_ORD) {
                        idChooseO = ua.getId_member();
                        f = ordListFragment;
                    } else {
                        return;
                    }

                    activeFragment = f;
                    getSupportFragmentManager().beginTransaction().detach(f).attach(f).commit();
                    getSupportFragmentManager().beginTransaction().replace(R.id.viewFragment, f).commit();

                    searchMenuItem.setVisible(false);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });

            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion < Build.VERSION_CODES.HONEYCOMB) {
                sp.setBackgroundResource(R.drawable.spinnersupport_d);
                mSpinnerAdapter.setDropDownViewResource(R.layout.spinnercustombar_black);
            }
        }
        return true;
    }

    //onOptionmenu selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            checkStatusInfo();
            return true;
        }

        if (item.getItemId() == R.id.action_cart) {
            Intent i = new Intent(this, ShoppingCartActivity.class);
            startActivity(i);
            return true;
        } else if (item.getItemId() == R.id.action_talk_to_bos) {
            Intent i = new Intent(this, StartTalkToBosActivity.class);
            startActivityForResult(i, REQ_CODE_TALK_TO_BOS);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    // generate view methods
    private View generateCategoryNameView(Category category) {
        LinearLayout ll = new LinearLayout(this);
        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ll.setLayoutParams(llParams);
        ll.setId(Utility.generateIdCatChooser(category));
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setGravity(Gravity.CENTER_VERTICAL);

        aq.id(ll).clickable(true).background(R.drawable.clickable).clicked(this, "ocCatChoose");

        int dip_30 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                30, getResources().getDisplayMetrics());
        int d = (int) getResources().getDimension(R.dimen.margin_cat_list);

        LinearLayout.LayoutParams evParams = new LinearLayout.LayoutParams(dip_30, dip_30);
        // just empty view
        View emptyView = new View(this);
        emptyView.setPadding(d, d, d, d);

        TextView tv = new TextView(this);
        tv.setText(category.name);
        tv.setPadding(d, d, d, d);
        tv.setTextColor(Color.WHITE);

        // these are added horizontally
        ll.addView(emptyView, evParams);
        ll.addView(tv);

        return ll;
    }

    public void onClickBroadcast(View v) {
        startActivity(new Intent(HomeActivity.this, InboxActivity.class));
    }

    public void onClickFrontShop(View v) {
        activeFragment = frontMainFragment;
        switchToActiveFragment();

        mDrawerLayout.closeDrawers();
    }

    public void onClickNewProducts(View v) {
        activeFragment = newProductFragment;
        switchToActiveFragment();

        mDrawerLayout.closeDrawers();
    }


    public void onClickTalkToBos(View v) {
        switchToFeedbackFragment();
        mDrawerLayout.closeDrawers();
    }

    public void onClickSuppliers(View v) {
        activeFragment = supplierFragment;
        switchToActiveFragment();

        mDrawerLayout.closeDrawers();
    }

    public void onClickProfile(View v) {
        Bundle b = new Bundle();
        b.putInt("userId", user.getId());
        b.putBoolean("editable", true);
        Intent i = new Intent(this, ViewProfileActivity.class).putExtras(b);
        startActivity(i);
    }

    public void ocBookList(View v) {
        if (Utility.isAdmin()) {
            progDialog.show();

            Map<String, String> params_a = new HashMap<String, String>();
            params_a.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
            aq.progress(progDialog).ajax(Constants.URL_ADMGETRESERVED_API, params_a, JSONArray.class, new AjaxCallback<JSONArray>() {
                @Override
                public void callback(String url, JSONArray object, AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        Gson gs = new Gson();
                        uRsv = gs.fromJson(object.toString(),
                                new TypeToken<List<UserRsvAdmin>>() {
                                }.getType());

                        supportInvalidateOptionsMenu();
                        idChooseB = uRsv.get(0).getId_member();
                    }
                    activeFragment = bookFragment;
                    switchToActiveFragment();

                    mDrawerLayout.closeDrawers();
                }
            });
        } else {
            activeFragment = bookFragment;
            switchToActiveFragment();

            mDrawerLayout.closeDrawers();
        }

        stateFragment = STATE_FRAGMENT_BOOK;
        searchMenuItem.setVisible(false);
    }


    public void onClickOrderStatus(View v) {
        if (Utility.isAdmin()) {
            progDialog.show();

            Map<String, String> params_a = new HashMap<String, String>();
            params_a.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
            aq.progress(progDialog).ajax(Constants.URL_ADMGETRESERVED_API, params_a, JSONArray.class, new AjaxCallback<JSONArray>() {
                @Override
                public void callback(String url, JSONArray object, AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                        Gson gs = new Gson();
                        uRsv = gs.fromJson(object.toString(),
                                new TypeToken<List<UserRsvAdmin>>() {
                                }.getType());

                        supportInvalidateOptionsMenu();

                        idChooseO = uRsv.get(0).getId_member();
                    }
                    activeFragment = ordListFragment;
                    switchToActiveFragment();

                    mDrawerLayout.closeDrawers();
                }
            });
        } else {
            activeFragment = ordListFragment;
            switchToActiveFragment();

            mDrawerLayout.closeDrawers();
        }

        stateFragment = STATE_FRAGMENT_ORD;
    }

    public void onClickMore(View v) {
        activeFragment = moreFragment;
        switchToActiveFragment();

        mDrawerLayout.closeDrawers();
    }

    public void onClickNotification(View v) {
        if (activeFragment == notificationListFragment) {
            notificationListFragment.getNotification(false);
        } else {
            activeFragment = notificationListFragment;
            switchToActiveFragment();
        }
        mDrawerLayout.closeDrawers();
    }

    public void onClickUpLineChat(View v) {
        if (activeFragment == upLineGroupChatFragment) {
            upLineGroupChatFragment.refreshData(false);
        } else {
            activeFragment = upLineGroupChatFragment;
            switchToActiveFragment();
        }
        mDrawerLayout.closeDrawers();
    }

    public void onClickDownLineChat(View v) {
        if (activeFragment == downLineGroupChatFragment) {
            downLineGroupChatFragment.refreshData(false);
        } else {
            activeFragment = downLineGroupChatFragment;
            switchToActiveFragment();
        }
        mDrawerLayout.closeDrawers();
    }

    //onBack Dialog Log Out
    @Override
    public void onBackPressed() {
        DialogHelper.showYesNoDialog(this, "Konfirmasi", "Apakah Anda ingin keluar dari aplikasi?", "Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, "Tidak", null);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    // public methods
    public void switchToHomeFragment() {
        activeFragment = homeFragment;
        switchToActiveFragment();
        mDrawerLayout.closeDrawers();
    }

    public void switchToNewProductFragment() {
        activeFragment = newProductFragment;
        switchToActiveFragment();
//        Intent intent = new Intent(HomeActivity.this,CategoryActivity.class);
//        startActivity(intent);
    }

    public void switchToOrderFragment() {
        activeFragment = ordListFragment;
        switchToActiveFragment();
    }

    public void switchToFeedbackFragment() {
        activeFragment = feedbackListFragment;
        switchToActiveFragment();
    }

    // method for switch active fragment
    private void switchToActiveFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.viewFragment, activeFragment).commit();
        // set action bar title
        final ActionBar bar = getSupportActionBar();
        if (activeFragment == homeFragment) {
            bar.setTitle("Profil");
        } else if (activeFragment == catParentListFragment) {
            bar.setTitle("Kategori Produk");
        } else if (activeFragment == newProductFragment) {
            bar.setTitle("Produk Baru");
        } else if (activeFragment == supplierFragment) {
            bar.setTitle("Tenant");
        } else if (activeFragment == bookFragment) {
            bar.setTitle("Daftar Booking");
        } else if (activeFragment == ordListFragment) {
            bar.setTitle("Status Pesanan");
        } else if (activeFragment == feedbackListFragment) {
            bar.setTitle("Ngobrol Sama Bos");
        } else if (activeFragment == moreFragment) {
            bar.setTitle("Lainnya");
        } else if (activeFragment == upLineGroupChatFragment) {
            bar.setTitle("Chat Mentor Saya");
        } else if (activeFragment == downLineGroupChatFragment) {
            bar.setTitle("Chat Member Saya");
        } else if (activeFragment == notificationListFragment) {
            bar.setTitle("Notifikasi");
        } else {
            bar.setTitle("Raihan");
        }
        supportInvalidateOptionsMenu();
    }

    private void checkStatusInfo() {
        Log.d(TAG, "check status info");
        Map<String, String> params = new HashMap<String, String>();
        params.put("reseller_id", String.valueOf(PrefStorage.instance.getUserId()));

        aq.ajax(Constants.URL_JCART_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                PrefStorage.instance.resetCart();
                if (status.getCode() != 200) {
                    return;
                }

                try {
                    int cartItemAmount = object.getInt("total");
                    boolean openShopStatus = object.getInt("open_shop") == 1;
                    boolean neededReminder = object.getInt("reminder") == 1;
                    boolean isBanned = object.getInt("is_banned") == 1;
                    int newProductAmount = object.getInt("total_new");
                    int unreadMessages = object.getInt("total_unread_message");
                    int unreadNotif = object.getInt("general");
                    int unreadUpline = object.getInt("upline");
                    int unreadDownline = object.getInt("downline");
                    int unreadFeedback = object.getInt("feedback");
                    PrefStorage.instance.changeCart(cartItemAmount);
                    PrefStorage.instance.setStok(openShopStatus);
                    PrefStorage.instance.setUnreadNewProductAmount(newProductAmount);
                    PrefStorage.instance.setUnreadMessages(unreadMessages);
                    PrefStorage.instance.setUnreadNotification(unreadNotif);
                    PrefStorage.instance.setUnreadUpline(unreadUpline);
                    PrefStorage.instance.setUnreadDownline(unreadDownline);
                    PrefStorage.instance.setUnreadFeedback(unreadFeedback);

                    // update new product amount
                    aq.id(R.id.tvNewProductAmount).text(String.valueOf(newProductAmount));
                    aq.id(R.id.tvNewNotifAmount).text(String.valueOf(unreadNotif));
                    aq.id(R.id.tvNewUpline).text(String.valueOf(unreadUpline));
                    aq.id(R.id.tvNewDownline).text(String.valueOf(unreadDownline));
                    aq.id(R.id.tvNewFeedback).text(String.valueOf(unreadFeedback));
                    aq.id(R.id.tvNewBroadcast).text(String.valueOf(unreadMessages));
                    hideEmptyCounter();

                    // set unread messages

                    setUnreadMessagesNumber(unreadMessages);

                    if (!openShopStatus) {
                        Utility.showDialogError(HomeActivity.this,
                                "Mohon maaf Anda tidak dapat melakukan order karena toko sedang melakukan stok opname",
                                "Stok Opname");
                    }
                    if (neededReminder) {
                        Utility.showDialogError(HomeActivity.this,
                                "Anda belum jualan lagi dalam dua bulan ini. Bila Anda belum jualan dalam waktu satu minggu kedepan, akun Anda akan dinon-aktfikan.",
                                "Reminder");
                    }
                    if (isBanned) {
                        Utility.showDialogError(HomeActivity.this,
                                "Anda tidak melakukan order lebih dari dua barang yang sudah dibooking. Mohon maaf akun Anda dinon-aktifkan dalam waktu 24 jam kedepan.",
                                "Banned");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // refresh the action bar
                supportInvalidateOptionsMenu();
            }
        });
    }

    private void hideEmptyCounter() {
        if (PrefStorage.instance.getUnreadMessages() == 0) {
            aq.id(R.id.tvNewBroadcast).gone();
        } else {
            aq.id(R.id.tvNewBroadcast).visible();
        }

        if (PrefStorage.instance.getUnreadNewProductAmount() == 0) {
            aq.id(R.id.tvNewProductAmount).gone();
        } else {
            aq.id(R.id.tvNewProductAmount).visible();
        }

        if (PrefStorage.instance.getUnreadNotification() == 0) {
            aq.id(R.id.tvNewNotifAmount).gone();
        } else {
            aq.id(R.id.tvNewNotifAmount).visible();
        }

        if (PrefStorage.instance.getUnreadUpline() == 0) {
            aq.id(R.id.tvNewUpline).gone();
        } else {
            aq.id(R.id.tvNewUpline).visible();
        }

        if (PrefStorage.instance.getUnreadDownline() == 0) {
            aq.id(R.id.tvNewDownline).gone();
        } else {
            aq.id(R.id.tvNewDownline).visible();
        }

        if (PrefStorage.instance.getUnreadFeedback() == 0) {
            aq.id(R.id.tvNewFeedback).gone();
        } else {
            aq.id(R.id.tvNewFeedback).visible();
        }
    }

    //cek google play services
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        Constants.HTTP_RESPONSE_PAGENOTFOUND).show();
            } else {
                Log.d(TAG, "not supported");
                finish();
            }
            return false;
        }

        return true;
    }

    //Cek tou app
    private void checkTourAppStatus() {
        if (!PrefStorage.instance.getUnderstandingOfTourApp()) {
            startActivity(new Intent(context, WhatsNewActivity.class));
        }
    }

    private void setUnreadMessagesNumber(int unreadMessages) {
        aq.id(R.id.tvNewBroadcast).text("" + unreadMessages);
    }

    // inner class listener
    private class SearchNewProductsListener implements FloatingSearchView.OnQueryChangeListener {
        @Override
        public void onSearchTextChanged(String oldQuery, String newQuery) {
            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            Utility.clearSoftInputFromView(context, searchView);
            newProductFragment.enterSearchMode(newQuery);
        }
    }
}