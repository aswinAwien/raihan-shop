package com.raihangroup.raihanshop.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.InboxAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.MessageObj;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * activity to view inbox
 */
public class InboxActivity extends AppCompatActivity {

    @BindView(R.id.toobal_inbox)
    Toolbar toobalInbox;
    private Context context;
    private AQuery aq;
    private DBHelper dbh;
    private ProgressDialog progDialog;
    private List<MessageObj> listOfLocalMessages;
    private InboxAdapter adapter;
    private ListView listView;

    private final static String TAG = "Inbox";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        ButterKnife.bind(this);
        setSupportActionBar(toobalInbox);
//        getSupportActionBar().setTitle(TAG);
        toobalInbox.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        aq = new AQuery(this);
        dbh = new DBHelper(this);
        listView = (ListView) findViewById(R.id.listView);
        PrefStorage.newInstance(this);
        String url = null;

        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setCancelable(false);

        Map<String, String> params = new HashMap<String, String>();
        params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));

        if (dbh.isMessageDbEmpty()) {
            progDialog.setMessage("Mengambil semua pesan untuk pertama kali..");
            url = Constants.URL_MESSAGE_ALL_API;
            Log.d(TAG, "dbh is empty");
        } else {
            progDialog.setMessage("Mengambil pesan baru");
            Log.d(TAG, "Ambil pesan terbaru");

            // get messages from sqlite
            listOfLocalMessages = dbh.getAllMessages();
            Log.d(TAG, "all local messages " + listOfLocalMessages.size());
            if (listOfLocalMessages.size() == 0) {
                url = Constants.URL_MESSAGE_ALL_API;
            } else {
                url = Constants.URL_MESSAGE_NEWEST_API;
                MessageObj lastMessage = listOfLocalMessages.get(0);
                Log.d(TAG, "timestamp of the last msg " + lastMessage.getTimestamp());
                // add new parameter to params
                params.put("updated_time", lastMessage.getTimestamp());
            }
        }

        aq.progress(progDialog).ajax(url, params, JSONArray.class, new AjaxCallback<JSONArray>() {

            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                List<MessageObj> newMessageList;
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    Utility.toast(context, "Tidak ada pesan baru");
                } else {
                    newMessageList = new Gson().fromJson(object.toString(),
                            new TypeToken<List<MessageObj>>() {
                            }.getType());
                    Log.d(TAG, "we got " + newMessageList.size() + " new messages");
                    dbh.insertNewMessages(newMessageList);
                    // get all messages again because we just performed insert messages
                    listOfLocalMessages = dbh.getAllMessages();
                }

                if (listOfLocalMessages == null || listOfLocalMessages.size() == 0) {
                    aq.id(R.id.llInfo).visible();
                }

                adapter = new InboxAdapter(context, listOfLocalMessages);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view, int position, long id) {
                        // mark message as read in adapter view
                        MessageObj mo = (MessageObj) parent.getItemAtPosition(position);
                        mo.setStatus(1);
                        // mark message as read in local database
                        dbh.changeStatReadMessage(mo.getId());
                        Log.d(TAG, "message " + mo.getTitle() + " as read in adapter and local db");
                        adapter.notifyDataSetChanged();
                        Intent i = new Intent(context, MessageActivity.class);
                        i.putExtra(Constants.INTENT_MESSAGE_OBJ, mo);
                        startActivity(i);
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                dbh.close();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}