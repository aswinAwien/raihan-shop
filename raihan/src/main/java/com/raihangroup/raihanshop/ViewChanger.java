package com.raihangroup.raihanshop;

import android.view.View;

public interface ViewChanger {

	boolean getData(View v, int position);

}