package com.raihangroup.raihanshop;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.cache.LruBitmapCache;

/**
 * Image Loader mirip (copas) dari MKI-Shell buatan ardi
 * @author badr
 *
 */
public class ImageLoaderApplication extends Application {

    /**
     * It is possible to keep a static reference across the
     * application of the image loader.
     */
    private static ImageManager imageManager;

    @Override
    public void onCreate() {
        super.onCreate();
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
////        .cacheInMemory(true)
//	    	.bitmapConfig(Bitmap.Config.RGB_565)
//	        .cacheOnDisc(true)
//	        .build();
//	    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
//	        .defaultDisplayImageOptions(defaultOptions)
//	        .discCacheExtraOptions(300, 300, Bitmap.CompressFormat.JPEG, 90, null)
//	        .tasksProcessingOrder(QueueProcessingType.LIFO)
//	        .memoryCacheExtraOptions(300, 300)
//	        .memoryCacheSizePercentage(50)
//	        .build();
//	    ImageLoader.getInstance().init(config); // Do it on Application start
    }

    /**
     * Normal image manager settings
     */
    public static void normalImageManagerSettings(Context context) {
        imageManager = new ImageManager(context, new SettingsBuilder()
            .withCacheManager(new LruBitmapCache(context))
            .build(context));
    }

    /**
     * There are different settings that you can use to customize
     * the usage of the image loader for your application.
     */
    @SuppressWarnings("unused")
    private void verboseImageManagerSettings() {
        SettingsBuilder settingsBuilder = new SettingsBuilder();
        
        //You can force the urlConnection to disconnect after every call.
        settingsBuilder.withDisconnectOnEveryCall(true);
        
        //We have different types of cache, check cache package for more info
        settingsBuilder.withCacheManager(new LruBitmapCache(this));
        
        //You can set a specific read timeout
        settingsBuilder.withReadTimeout(30000);
        
        //You can set a specific connection timeout
        settingsBuilder.withConnectionTimeout(30000);
        
        //You can disable the multi-threading ability to download image 
        settingsBuilder.withAsyncTasks(false);
        
        //You can set a specific directory for caching files on the sdcard
//        settingsBuilder.withCacheDir(new File("/something"));
        
        
        //Setting this to false means that file cache will use the url without the query part
        //for the generation of the hashname
        settingsBuilder.withEnableQueryInHashGeneration(false);
        
        LoaderSettings loaderSettings = settingsBuilder.build(this);
        imageManager = new ImageManager(this, loaderSettings);
    }

    /**
     * Convenient method of access the imageLoader
     */
    public static ImageManager getImageLoader() {
        return imageManager;
    }
    
	private static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		/* Copied from Android Documentation */
		
		/* Raw height and width of image */
		final int height = options.outHeight;
		final int width = options.outWidth;
		
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {
	
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;
	
			/* Calculate the largest inSampleSize value that is a power of 2 and keeps both height
			 * and width larger than the requested height and width. */
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
	
		return inSampleSize;
	}

	/**
	 * fungsi buatan fikrul supaya thumbnail imageview tidak outofmemory
	 * @param pathName
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap decodeSampledBitmap(String pathName, int reqWidth, int reqHeight) {
		/* Adapted from Android Documentation */
	
		/* First decode with inJustDecodeBounds=true to check dimensions */
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(pathName, options);
		
		/* In case of error */
		if (options.outHeight == -1 || options.outWidth == -1)
			return null;
	
		/* Calculate inSampleSize */
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
	
		/* Decode bitmap with inSampleSize set */
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(pathName, options);
	}

}
