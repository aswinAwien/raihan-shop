package com.raihangroup.raihanshop;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.raihangroup.raihanshop.activity.HomeActivity;
import com.raihangroup.raihanshop.activity.InboxActivity;
import com.raihangroup.raihanshop.activity.InfoActivity;
import com.raihangroup.raihanshop.activity.PerformanceViewActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.NotificationObj;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
//import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentSvc extends FirebaseInstanceIdService {

    private NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        PrefStorage.newInstance(this);
    }

//    public GcmIntentSvc() {
//        super(Constants.GCM_PROJECT_ID);
//    }

//    @Override
//    protected void onHandleIntent(Intent intent) {
//        Bundle b = intent.getExtras();
//        Firebas gcm = GoogleCloudMessaging.getInstance(this);
//
//        String messageType = gcm.getMessageType(intent);
//
//        if (!b.isEmpty()) {
///*			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
////				sendNotification("Send error: " + extras.toString());
//			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
////				sendNotification("Deleted messages on server: " + extras.toString());
//			} else */
//            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//                NotificationObj nop = new NotificationObj();
//                nop.setTitle(b.getString("title"));
//                nop.setType(b.getString("type"));
//                nop.setContent(b.getString("content"));
//
//                // Post notification of received message.
//                sendNotification(nop);
//                Log.i("Notification", "Received: " + b.toString());
//            }
//        }
//        // Release the wake lock provided by the WakefulBroadcastReceiver.
//        GcmBroadcastRecv.completeWakefulIntent(intent);
//    }




    private void sendNotification(NotificationObj no) {
        notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent i = new Intent(this, LoginActivity.class);
        TaskStackBuilder stackBuilder = null;
        switch (Integer.valueOf(no.getType())) {
            case Constants.NOTIFICATION_AFTER_REGISTER:
                i = new Intent(this, InfoActivity.class);
                break;
            case Constants.NOTIFICATION_BROADCAST_INBOX:
                i = new Intent(this, InboxActivity.class);
                stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addNextIntentWithParentStack(i);
                break;
            case Constants.NOTIFICATION_PERFORMANCE:
                i = new Intent(this, PerformanceViewActivity.class); //performance
                //i.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_PERF);
                break;
            case Constants.NOTIFICATION_ORDER:
                // ke info untuk pembayaran komisi, order yang ditolak, dsb
                String[] arr = no.getTitle().split(" ");
                String orderId = arr[0];
                String status = arr[1];
                if (status.equals("dikirim")) {
                    no.setTitle("Pesanan " + orderId + " telah dikirim");
                    i = new Intent(this, HomeActivity.class);
                    i.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_ORD);
                } else {
                    no.setTitle("Pesanan " + orderId + " telah ditolak");
                    i = new Intent(this, InfoActivity.class);
                }
                break;
            case Constants.NOTIFICATION_FEE:
                i = new Intent(this, PerformanceViewActivity.class);
                break;
            case Constants.NOTIFICATION_MEMBER:
                i = new Intent(this, HomeActivity.class); //dashboard
                break;
            case Constants.NOTIFICATION_FEEDBACK_REPLY:
                i = new Intent(this, HomeActivity.class);
                break;
            case Constants.NOTIFICATION_BIRTHDAY:
                i = new Intent(this, HomeActivity.class);
                break;
            case Constants.NOTIFICATION_VOLUME_REQUIRED:
                i = new Intent(this, PerformanceViewActivity.class);
                break;
            case Constants.NOTIFICATION_RETUR_ORDER:
                String[] arrRetur = no.getTitle().split(" ");
                String retur = arrRetur[0]; // Retur
                String orderIdR = arrRetur[1]; // id number
                String statusR = arrRetur[2]; // approved or not
                no.setTitle("Permohonan Retur dengan No. ID " + orderIdR + " telah disetujui");
                i = new Intent(this, HomeActivity.class);
                i.putExtra(Constants.INTENT_RELOAD_HOME, Constants.RELOAD_ORD);
                break;
            case Constants.NOTIFICATION_KOMISI:
                i = new Intent(this, PerformanceViewActivity.class);
                break;
            case Constants.NOTIFICATION_GET_FEE:
                i = new Intent(this, PerformanceViewActivity.class);
                break;
            default:
                break;
        }
        i.putExtra(Constants.INTENT_NOTIFICATION_OBJ, no);

        try {
            PrefStorage.instance.getUserData();
        } catch (Exception e) {
            //check null -> logout
            //kecuali 1, karena 1 register
            if (!no.getType().equals("1")) {
                i = new Intent(this, LoginActivity.class);
            }
        }

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        if (stackBuilder != null) {
            contentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_logodaun)
                .setContentTitle(no.getTitle())
                .setContentText(no.getContent())
                .setAutoCancel(true);

        if (!PrefStorage.instance.getMuteNotification()) {
            builder.setSound(alarmSound);
        }

        builder.setContentIntent(contentIntent);
        notificationManager.notify(0, builder.build());
    }
}
