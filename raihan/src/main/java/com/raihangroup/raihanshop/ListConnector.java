package com.raihangroup.raihanshop;

import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;

import com.raihangroup.raihanshop.helper.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

public class ListConnector<E> extends ArrayList<E> {
    private static final long serialVersionUID = 8582608389792785512L;

    protected transient ArrayAdapter<E> adapter;
    protected transient PagerAdapter pager;
    private transient ListUpdater update;
    private transient Filter<E> filter;
    private boolean isUpdating;
    private String filterString = "";
    private ArrayList<E> fullContent = new ArrayList<E>();
    private static UpdateListener updateListener;
    private static int counter = 0;

    private transient BaseExpandableListAdapter e_adapter;

    public interface ListUpdater {
        void onBackground();

        void onFinish();
    }

    public interface UpdateListener {
        void updateStarted();

        void updateFinished();
    }

    public interface Filter<E> {
        boolean filter(E item, String str);
    }


    public ListConnector(Collection<E> set) {
        addAll(set);
    }

    public ListConnector() {
    }

    public ListConnector(E... list) {
        addAll(Arrays.asList(list));
    }

    @SuppressWarnings("rawtypes")
    public static final Filter DEFAULT_FILTER = new Filter() {
        @Override
        public boolean filter(Object item, String str) {
            if (str.length() > 0) {
                return item.toString().toLowerCase(Locale.ENGLISH).contains(str.toLowerCase(Locale.ENGLISH));
            } else {
                return true;
            }
        }
    };

    public void setAdapter(ArrayAdapter<E> e) {
        adapter = e;
    }

    public void setAdapter(BaseExpandableListAdapter e) {
        e_adapter = e;
    }

    public void setUpdater(ListUpdater update) {
        this.update = update;
    }

    public ListUpdater getUpdater() {
        return update;
    }

    public void update() {
        if (isUpdating) return;
        isUpdating = true;
        counter++;
        if (updateListener != null) updateListener.updateStarted();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Log.w("URL", "doInBackground" + update);
                if (adapter != null) {
                    adapter.getContext();
                }
                if (update != null) update.onBackground();
                counter--;
                if (counter < 0) counter = 0;
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (update != null) {
                    update.onFinish();
                }
                notifyAdapter();
                if (counter == 0 && updateListener != null) {
                    updateListener.updateFinished();
                }
                isUpdating = false;
            }
        }.execute();
    }

    public void setPagerAdapter(PagerAdapter e) {
        pager = e;
    }

    public void setFilter(Filter<E> filter) {
        this.filter = filter;
    }

    public Filter<E> getFilter() {
        return filter;
    }

    public void filter(String str) {
        filterString = str;
        if (filter != null) {
            super.clear();
            for (E item : fullContent) {
                if (filter.filter(item, filterString)) super.add(item);
            }
            if (adapter != null) adapter.notifyDataSetChanged();
            if (pager != null) pager.notifyDataSetChanged();
            if (e_adapter != null) e_adapter.notifyDataSetChanged();
        }
    }

    public void notifyAdapter() {
        Utility.mainThread.post(new Runnable() {
            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                if (e_adapter != null) {
                    e_adapter.notifyDataSetChanged();
                }
                if (pager != null) {
                    pager.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public boolean add(E object) {
        fullContent.add(object);
        if (filter != null) {
            if (filter.filter(object, filterString)) {
                return super.add(object);
            }
            return false;
        } else {
            return super.add(object);
        }
    }

    @Override
    public void add(int index, E object) {
        fullContent.add(index, object);
        if (filter != null) {
            if (filter.filter(object, filterString)) {
                super.add(index, object);
            }
        } else {
            super.add(index, object);
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        fullContent.addAll(collection);
        if (filter != null) {
            for (E i : collection) {
                if (filter.filter(i, filterString)) {
                    super.add(i);
                }
            }
            return true;
        } else {
            return super.addAll(collection);
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> collection) {
        fullContent.addAll(index, collection);
        if (filter != null) {
            for (E i : collection) {
                if (filter.filter(i, filterString)) {
                    super.add(index, i);
                }
            }
            return true;
        } else {
            return super.addAll(index, collection);
        }
    }

    @Override
    public void clear() {
        fullContent.clear();
        super.clear();
    }

    @Override
    public E remove(int index) {
        E object = super.get(index);
        fullContent.remove(object);
        return super.remove(index);
    }

    @Override
    public boolean remove(Object object) {
        fullContent.remove(object);
        return super.remove(object);
    }


    public static void setUpdateListener(UpdateListener listener) {
        updateListener = listener;
        if (updateListener != null) {
            if (counter > 0) {
                updateListener.updateStarted();
            } else {
                updateListener.updateFinished();
            }
        }
    }
}
