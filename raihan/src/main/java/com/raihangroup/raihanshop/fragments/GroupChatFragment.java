package com.raihangroup.raihanshop.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ListMemberActivity;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.adapter.ChatReplyAdapter;
import com.raihangroup.raihanshop.fragments.MediaSelectorFragment.MediaSelectorListener;
import com.raihangroup.raihanshop.helper.BitmapUtil;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.ImageHelper;
import com.raihangroup.raihanshop.helper.ImageRenderer;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.ChatItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Fragment for chat to UpLine or DownLine
 * Created by Mohamad on 1/27/2015.
 */
public class GroupChatFragment extends Fragment implements MediaSelectorListener {
    private Context context;
    private AQuery aq;
    public int mode;
    public int page;
    private ListView listView;
    private EditText etReplyMessage;
    private ImageView ivReplyMessage;
    private ImageView ivAttachImage;
    private ImageView ivAttachment;
    private ProgressDialog progDialog;
    ArrayList<ChatItem> listOfConversation;
    private Button btnLoadMore;
    private ChatReplyAdapter chatReplyAdapter;
    boolean endOfList;
    private Intent pictureActionIntent;
    private final static int CAMERA_REQUEST = 1101;
    private final static int GALLERY_REQUEST = 1102;
    private String imagePath;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.group_chat, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_list_member) {
            Bundle b = new Bundle();
            b.putInt("type", mode);
            b.putInt("member_id", PrefStorage.instance.getUserId());
            context.startActivity(new Intent(context, ListMemberActivity.class).putExtras(b));
            return true;
        }
        if (item.getItemId() == R.id.action_refresh) {
            refreshData(true);
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        context = getActivity();
        aq = new AQuery(getActivity());
        mode = getArguments().getInt("mode");
        PrefStorage.newInstance(context);
        listOfConversation = new ArrayList<>();
        progDialog = new ProgressDialog(context);
        progDialog.setTitle("Mengirim pesan");
        progDialog.setMessage("Mohon Tunggu...");
        progDialog.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_chat, container, false);
        aq.recycle(rootView);
        listView = (ListView) aq.id(R.id.listView).getView();
        chatReplyAdapter = new ChatReplyAdapter(context, listOfConversation);
        listView.setAdapter(chatReplyAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatItem selected = (ChatItem) listView.getItemAtPosition(position);
                if(!selected.id_product.equals("0")){
                    Log.d("product id =",selected.id_product);
                    Intent i = new Intent(getActivity(), ProductDetailActivity.class);
                    i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_MODE, -1);
                    i.putExtra(Constants.INTENT_PRODUCT_ID, Integer.parseInt(selected.id_product));
                    startActivity(i);
                }
            }
        });
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshData(true);
            }
        });
        etReplyMessage = (EditText) aq.id(R.id.etReplyMessage).getView();
        ivReplyMessage = (ImageView) aq.id(R.id.ivReplyMessage).getView();
        ivAttachImage = (ImageView) aq.id(R.id.ivAttachImage).getView();
        ivAttachment = (ImageView) aq.id(R.id.iv_attachment).getView();
        ivAttachment.setVisibility(View.GONE);
        btnLoadMore = (Button) aq.id(R.id.btnLoadMore).getView();
        btnLoadMore.setVisibility(View.GONE);
        endOfList = false;
        page = 0;
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem != 0 || visibleItemCount == 0) {
                    // The first child VIEW is not the first ITEM
                    btnLoadMore.post(new Runnable() {
                        @Override
                        public void run() {
                            btnLoadMore.setVisibility(View.GONE);
                        }
                    });
                } else if (view.getChildCount() != 0 && view.getChildAt(0).getTop() == 0) {
                    // The first child VIEW is the first ITEM and it has child and the first ITEM/VIEW position is at 0
                    if (!endOfList) {
                        btnLoadMore.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getChatList(++page, true);
            }
        });

        ivReplyMessage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String message = aq.id(R.id.etReplyMessage).getText().toString();
                if (message.length() > 0) {
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                    params.put("type", "" + mode);
                    params.put("message", message);
                    if (imagePath != null) {
                        params.put("file", ImageHelper.getImageFile(imagePath, context));
                    }
                    aq.progress(progDialog).ajax(Constants.URL_REPLY_CHAT, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            Log.d("debug", "stats =" + status.getCode());
                            if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                Utility.toast(context, "Pesan berhasil dikirim");
                                etReplyMessage.setText("");
                                ivAttachment.setImageDrawable(null);
                                ivAttachment.setVisibility(View.GONE);
                                refreshData(false);
                            } else {
                                DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengirimkan pesan . Silakan mencoba kembali",
                                        "OK", null);
                            }
                        }
                    });
                } else {
                    EditText etReplyMessage = (EditText) aq.id(R.id.etReplyMessage).getView();
                    etReplyMessage.setError("Masukkan Pesan");
                }
            }
        });

        ivAttachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaSelectorFragment MSF = new MediaSelectorFragment();
                MSF.setTargetFragment(GroupChatFragment.this, 0);
                MSF.show(getFragmentManager(), "imageChooser");
            }
        });
        if(listOfConversation.isEmpty()) {
            refreshData(true);
        }
        else{
            refreshData(false);
        }
    }

    // public methods

    public void refreshData(boolean loading) {
        aq.id(R.id.llInfo).gone();
        getChatList(0,loading);
    }

    // get chat list

    private void getChatList(final int page, boolean loading) {
        Log.d("debug", "page" + page);
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
        param.put("type", "" + mode);
        param.put("page", "" + page);
        if(loading){
            aq.progress(R.id.pg);
        }
        aq.ajax(Constants.URL_LIST_GROUP_CHAT, param, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray array, AjaxStatus status) {
                // if
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    ArrayList<ChatItem> list = gs.fromJson(array.toString(),
                            new TypeToken<List<ChatItem>>() {
                            }.getType());

                    // empty chat
                    if (list.size() == 0 && page == 0) {
                        aq.id(R.id.tvMessageTitle).text("Tidak Ada Pesan");
                        aq.id(R.id.tvMessageContent).text("Silahkan memulai percakapan");
                        aq.id(R.id.llInfo).visible();
                    }
                    // reach end of list
                    else if (list.size() < 10) {
                        endOfList = true;
                        btnLoadMore.setVisibility(View.GONE);
                    }
                    // if refresh or initial get
                    if (page == 0) {
                        listOfConversation.clear();
                    }
                    listOfConversation.addAll(0, list);
                    chatReplyAdapter.notifyDataSetChanged();

                    //SCROLL TO BOTTOM OF THE LIST
                    if (page == 0) {
                        listView.post(new Runnable() {
                            @Override
                            public void run() {
                                listView.setSelection(listView.getCount());
                            }
                        });
                    }

                    //mark as read to server
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                    params.put("type", "" + mode);
                    aq.ajax(Constants.URL_READ_NOTIFICATION, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                try {
                                    if (object.getString("Success").equals("true"))
                                        Log.d("debug", "notif read");
                                } catch (Exception e) {
                                    Log.d("Exception",String.valueOf(e));
                                }
                            }
                        }
                    });
                }
                // something wrong from server (unauthenticated etc.)
                else {

                    if (page == 0) {
                        aq.id(R.id.llInfo).visible();
                    // end of list in edge of paging
                    } else {
                        endOfList = true;
                        btnLoadMore.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onCameraButtonClick(DialogFragment dialog) {
        Log.d("debug", "wokokok camera clicked");
        pictureActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "reyhan_client.jpg");
        pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(file));
        startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
    }

    @Override
    public void onGalleryButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                if (data != null) {
                    ivAttachment.setVisibility(View.VISIBLE);
                    //ambil uri buat dikirim ke server
                    Uri imageUri = data.getData();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            String id = imageUri.getLastPathSegment()
                                    .split(":")[1];
                            final String[] imageColumns = {MediaStore.Images.Media.DATA};
                            final String imageOrderBy = null;
                            Uri uri = ImageHelper.getMediaStorageUri();
                            Cursor imageCursor = getActivity().managedQuery(uri,
                                    imageColumns, MediaStore.Images.Media._ID
                                            + "=" + id, null, imageOrderBy);
                            if (imageCursor.moveToFirst()) {
                                imagePath = imageCursor
                                        .getString(imageCursor
                                                .getColumnIndex(MediaStore.Images.Media.DATA));
                            }
                            Log.d("filepath", "kitkat = " + imagePath);
                        } catch (Exception e) {
                            Log.d("filepath", "isi uri = " + imageUri);
                            imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                            Log.d("filepath", "galery path = " + imagePath);
                        }
                    } else {
                        Log.d("filepath", "isi uri = " + imageUri);
                        imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                        Log.d("filepath", "galery path  = " + imagePath);
                    }
                    //render to ivAttachment
                    new ImageRenderer(ivAttachment, data, imagePath).execute();
                } else {
                    Toast.makeText(getActivity(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                Log.d("debug", "on activity camera");
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "reyhan_client.jpg");
                imagePath = Uri.fromFile(file).getPath();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imagePath, options);
                int width = options.outWidth;
                int height = options.outHeight;
                WindowManager wm = (WindowManager) getActivity()
                        .getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Log.d("debug", "Image.Width = " + width);
                Log.d("debug", "display.getWidth() = " + display.getWidth());
                float scale = (float) display.getWidth() / (float) width;
                Bitmap fixed = BitmapUtil.fixOrientation(context, ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                        (int) (height * scale)), Uri.fromFile(file));
                ivAttachment.setImageBitmap(fixed);
                ivAttachment.setVisibility(View.VISIBLE);
            } else {
                ivAttachment.setVisibility(View.GONE);
            }
        }
    }
}