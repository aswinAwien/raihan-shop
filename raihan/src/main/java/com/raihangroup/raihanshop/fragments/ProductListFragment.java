package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductActivity;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.activity.ProfileActivity;
import com.raihangroup.raihanshop.adapter.ProductListAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductListFragment extends Fragment implements OnRefreshListener2<ListView> {

    private Context context;
    private AQuery aq;
    private Gson gs;

    private ArrayList<Product> adapterProducts;
    private ArrayList<Product> listOfProducts;
    private ArrayList<Product> listOfSearchProducts;
    private ProductListAdapter adapter;
    private PullToRefreshListView listView;
    private Spinner spinnerSortBy;
    private int normalPage;
    private int searchPage;
    private int preLast;
    private int sortBy;
    private int mode;
    private int categoryId;
    private String supplierId; // ubah jadi int ????

    private boolean isSearchMode;
    private String searchString;

    private final static String TAG = "Product Frg";

    private static final String[] SORT_BY = new String[]{"Urutkan", "Terbaru", "Termahal", "Termurah",
            "Stok Terbanyak", "Komisi Terbesar", "Fee Terbesar", "Volume Terbesar", "Terlaris"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();
        aq = new AQuery(getActivity());
        gs = new Gson();

        adapterProducts = new ArrayList<Product>();
        listOfSearchProducts = new ArrayList<Product>();
        listOfProducts = new ArrayList<Product>();

        // must have arguments
        mode = getArguments().getInt("mode");
        if (mode == ProductActivity.PRODUCT_BY_CATEGORY_MODE) {
            categoryId = getArguments().getInt("categoryId");
        } else if (mode == ProductActivity.PRODUCT_BY_SUPPLIER_MODE) {
            supplierId = getArguments().getString("supplierId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);

        aq.recycle(rootView);
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPage();
            }
        });
        spinnerSortBy = (Spinner) aq.id(R.id.SortSpinner).getView();
        if(mode == ProductActivity.PRODUCT_BY_SORTING_MODE){
            spinnerSortBy.setVisibility(View.GONE);
            sortBy = getArguments().getInt("sortBy");
            initPage();
        }
        spinnerSortBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sortBy = position;
                initPage();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listView = (PullToRefreshListView) rootView.findViewById(R.id.listview);
        listView.setMode(Mode.DISABLED);
        listView.setOnRefreshListener(this);

        listView.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (preLast != lastItem) { // to avoid multiple calls for last item
                        preLast = lastItem;
                        Utility.toast(context, "Memuat produk lainya...");
                        loadNextPage();
                    }
                }
            }
        });

        initPage();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        aq.id(R.id.SortSpinner).adapter(new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, SORT_BY));
    }

    // unused yoooooo
    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
        /*
        final String q = ((HomeActivity) getActivity()).searchString;
		String url = "";//urlString;

		Map<String, String> params = new HashMap<String, String>();
		if(categoryId != -1 && categoryId != 0) {
			params.put("id_category", String.valueOf(categoryId));
		}
		if (!Utility.isNullOrBlank(q)) {
			searchPage++;
			System.out.println("loading spage "+searchPage+" with q "+q);
			url = Constants.URL_PRODSEARCH_API;
			params.put("keyword", q);
			params.put("page", String.valueOf(searchPage));
		} else {
			normalPage = 1;
			System.out.println("reloading page "+normalPage);
			params.put("page", String.valueOf(normalPage));
		}

		aq.ajax(url, params ,JSONArray.class, new AjaxCallback<JSONArray>(){
			@Override
			public void callback(String url, JSONArray object, AjaxStatus status) {
				System.out.println("done dari atas");
				if (status.getCode() == 200) {
					List<Product> listTemp = gs.fromJson(object.toString(),
        					new TypeToken<List<Product>>(){}.getType());

					// refresh all
					fullListProduct.clear();
					adapterProducts.clear();

					if (Utility.isNullOrBlank(q)) {
						adapterProducts.addAll(listTemp);
						fullListProduct.addAll(listTemp);
					}
					listView.onRefreshComplete();
					if(listTemp.size() < 10) listView.setMode(Mode.DISABLED);
				} else if (status.getCode() == 500) {
					System.out.println("empty load, do nothing");

					listView.onRefreshComplete();
					listView.setMode(Mode.DISABLED);
				}
			}
		});*/
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
        Log.d(TAG, "pull up");
    }

    // handle page

    public void initPage() {
        if(isSearchMode==false){
            aq.id(R.id.listview).gone();
            aq.id(R.id.llInfo).gone();
        }
        String urlString = null;
        Map<String, String> params = new HashMap<String, String>();
        params.put("mode", "" + sortBy);
        if (isSearchMode) {
            Log.d(TAG, "load init page (search mode)");

            searchPage = 1; // start from 1
            urlString = Constants.URL_PRODSEARCH_API;
            params.put("page", String.valueOf(searchPage));
            params.put("keyword", searchString);
            params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
        } else {
            Log.d(TAG, "load init page (normal mode)");

            normalPage = 1; // start from 1
            params.put("page", String.valueOf(normalPage));
            if (mode == ProductActivity.PRODUCT_BY_CATEGORY_MODE) {
                if (categoryId == 0) {
                    urlString = Constants.URL_GET_NEW_PRODUCTS_API;
                    params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
                } else {
                    urlString = Constants.URL_GET_PRODUCTS_CAT_API;
                    params.put("id_category", String.valueOf(categoryId));
                }
            } else if (mode == ProductActivity.PRODUCT_BY_SUPPLIER_MODE) {
                urlString = Constants.URL_SUPPLIER_DETAIL_API;
                params.put("id_supplier", supplierId);
            }else if(mode == ProductActivity.PRODUCT_BY_SORTING_MODE){
                urlString = Constants.URL_GET_PRODUCTS_API;
            }
        }

        aq.progress(R.id.pg).ajax(urlString, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                // show list view anyway
                // no need to hide pg. pg is automatically handled by this object

                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    if (isSearchMode) {
                        Utility.toast(context, "Produk tidak ditemukan dalam pencarian");
                    }else {
                        Utility.toast(context, "Produk tidak ditemukan");
                    }
                    aq.id(R.id.listView).gone();
                    aq.id(R.id.llInfo).visible();
                }
                    aq.id(R.id.listview).visible();

                ArrayList<Product> listResult = gs.fromJson(object.toString(),
                        new TypeToken<List<Product>>() {
                        }.getType());
                // set products read
                setProductsRead(listResult);

                // set result to corresponding list
                if (isSearchMode) {
                    listOfSearchProducts = listResult;
                } else {
                    listOfProducts = listResult;
                }

                // set products for adapter
                adapterProducts = new ArrayList<Product>();
                adapterProducts.addAll(listResult);

                if (adapterProducts.size() == 0) {
                    aq.id(R.id.llInfo).visible();
                    aq.id(R.id.tvTitle).text("Produk Tidak Tersedia");
                } else {
                    aq.id(R.id.llInfo).gone();
                }
                adapter = new ProductListAdapter(getActivity(), adapterProducts);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View v,
                                            int position, long id) {
                        Intent i = new Intent(getActivity(), ProductDetailActivity.class);

                        i.putExtra(Constants.INTENT_PRODUCT_DETAIL, (Product) adapter.getItemAtPosition(position));
                        i.putExtra(Constants.INTENT_PRODUCT_ID, ((Product) adapter.getItemAtPosition(position)).getId());
                        startActivity(i);
                    }
                });
            }
        });
    }

    public void loadNextPage() {
        String urlString = null;
        Map<String, String> params = new HashMap<String, String>();
        params.put("mode", "" + sortBy);
        if (isSearchMode) {
            searchPage++;
            urlString = Constants.URL_PRODSEARCH_API;
            params.put("page", String.valueOf(searchPage));
            params.put("keyword", searchString);
            params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));

            Log.d(TAG, "load next page (search mode)");
            Log.d(TAG, "loading page " + searchPage + " for " + searchString);
        } else {
            normalPage++;
            params.put("page", String.valueOf(normalPage));

            if (mode == ProductActivity.PRODUCT_BY_CATEGORY_MODE) {
                if (categoryId == 0) {
                    urlString = Constants.URL_GET_NEW_PRODUCTS_API;
                    params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
                } else {
                    urlString = Constants.URL_GET_PRODUCTS_CAT_API;
                    params.put("id_category", String.valueOf(categoryId));
                }
            } else if (mode == ProductActivity.PRODUCT_BY_SUPPLIER_MODE) {
                urlString = Constants.URL_SUPPLIER_DETAIL_API;
                params.put("id_supplier", supplierId);
            }else if(mode == ProductActivity.PRODUCT_BY_SORTING_MODE){
                urlString = Constants.URL_GET_PRODUCTS_API;
            }

            Log.d(TAG, "load next page (normal mode)");
            Log.d(TAG, "loading page " + normalPage);
        }

        aq.ajax(urlString, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    if (isSearchMode) {
                        searchPage--;
                    } else {
                        normalPage--;
                    }

                    // reset pre last
                    preLast = 0;

                    return;
                }

                ArrayList<Product> listResult = gs.fromJson(object.toString(),
                        new TypeToken<List<Product>>() {
                        }.getType());
                // set products read
                setProductsRead(listResult);

                // set result to corresponding list
                if (isSearchMode) {
                    adapterProducts.addAll(listResult);
                    listOfSearchProducts.addAll(listResult);
                } else {
                    adapterProducts.addAll(listResult);
                    listOfProducts.addAll(listResult);
                }

                adapter.notifyDataSetChanged();
            }
        });
    }

    // support methods

    public void prepareSearchMode() {
        aq.id(R.id.listview).gone();
    }

    public void enterSearchMode(String searchString) {
        this.isSearchMode = true;
        this.searchString = searchString;
        initPage();
    }

    public void quitSearchMode() {
        aq.id(R.id.listview).visible();

        // we are from search mode
        this.isSearchMode = false;
        this.searchString = "";

        // reset products in list view
        adapterProducts.clear();
        adapterProducts.addAll(listOfProducts);
        // adapter maybe null
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    private void setProductsRead(ArrayList<Product> products) {
        Log.d(TAG, "set products read");

        JSONArray jsonArr = new JSONArray();
        for (Product product : products) {
            // the 0 means product is unread
            if (product.is_read == 0) {
                jsonArr.put(product.getId());
            }
        }
        Log.d(TAG, "collected product ids " + jsonArr.toString());

        // needed for aq so that the call is treated as POST method
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_member", String.valueOf(PrefStorage.instance.getUserId()));
        params.put("id_product", jsonArr.toString());

        aq.ajax(Constants.URL_SET_PRODUCTS_READ_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    return;
                }
            }
        });
    }
}
