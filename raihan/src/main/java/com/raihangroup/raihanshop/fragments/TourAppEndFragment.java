package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;

/**
 * Fragment for tour app for ending.
 * @author Alh
 *
 */
public class TourAppEndFragment extends Fragment {

	private Context context;
	private AQuery aq;
	
	private final static String TAG = "Tour App End Frg";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
		aq = new AQuery(getActivity());
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {	
		View rootView = inflater.inflate(R.layout.fragment_tour_app_end, container, false);
		
		aq.recycle(rootView);
		
		return rootView;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        aq.id(R.id.btnToHome).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
	}
}
