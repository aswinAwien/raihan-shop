package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ReviewAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.Review;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * List for supplier review for selected id.
 * @author Alh
 *
 */
public class SupplierReviewListFragment extends Fragment {

	private Context context;
	private AQuery aq;
	
	private ListView listView;
	
	private final static String TAG = "Supplier Review Frg";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
		aq = new AQuery(getActivity());
		
		PrefStorage.newInstance(context);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {	
		View rootView = inflater.inflate(R.layout.fragment_list_with_message, container, false);
		
		aq.recycle(rootView);
		listView = (ListView) rootView.findViewById(R.id.listView);
		// set message here
		aq.id(R.id.tvMessageTitle).text("Tidak Ada Review");
		aq.id(R.id.tvMessageContent).text("Klik Untuk Memuat Ulang");
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReview();
                aq.id(R.id.llInfo).gone();
            }
        });
		return rootView;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getReview();
	}

    public void getReview() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_supplier", getArguments().getString("supplierId"));
        aq.progress(R.id.pg).ajax(Constants.URL_SUPPLIER_REVIEW_API, params, JSONArray.class, new AjaxCallback<JSONArray>(){
			@Override
			public void callback(String url, JSONArray object, AjaxStatus status) {
				aq.id(R.id.llInfo).gone();
				if(status.getCode() != Constants.HTTP_RESPONSE_OK) {
					aq.id(R.id.llInfo).visible();
                    return;
				}

				Gson gs = new Gson();
				ArrayList<Review> listOfReviews = gs.fromJson(object.toString(),
    					new TypeToken<List<Review>>(){}.getType());

				ReviewAdapter adapter = new ReviewAdapter(context, listOfReviews);
			    listView.setAdapter(adapter);
			}
		});
    }
}