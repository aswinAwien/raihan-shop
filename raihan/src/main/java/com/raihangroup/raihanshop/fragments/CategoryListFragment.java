package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductActivity;
import com.raihangroup.raihanshop.adapter.CategoryAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.Category;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * List for categories.
 *
 * @author Alh
 */
public class CategoryListFragment extends Fragment implements OnItemClickListener {

    private Context context;
    private AQuery aq;

    private ListView listView;

    private static final String TAG = "Category Frg";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());

        PrefStorage.newInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_list, container, false);

        aq.recycle(rootView);
        listView = (ListView) rootView.findViewById(R.id.listView);
        aq.id(R.id.llInfo).gone();
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCategoryList();
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listView.setOnItemClickListener(this);

        getCategoryList();
    }

    private void getCategoryList() {
        int catParentId = getArguments().getInt("categoryParentId");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_parent", String.valueOf(catParentId));
        aq.progress(R.id.pg).ajax(Constants.URL_CATEGORY_LIST_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {

            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                aq.id(R.id.llInfo).gone();
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();
                    aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            aq.id(R.id.llInfo).gone();
                            getCategoryList();
                        }
                    });
                }

                Gson gs = new Gson();
                ArrayList<Category> listOfCategories = gs.fromJson(object.toString(),
                        new TypeToken<List<Category>>() {
                        }.getType());
                if (listOfCategories.size() == 0) {
                    aq.id(R.id.tvMessageTitle).text("Kategori Kosong");
                    aq.id(R.id.tvMessageContent).text("Terjadi Kesalahan");
                    aq.id(R.id.llInfo).visible();
                    aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getCategoryList();
                        }
                    });
                }
                CategoryAdapter adapter = new CategoryAdapter(context, listOfCategories);
                listView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
        Category cat = (Category) listView.getItemAtPosition(position);
        Log.d(TAG, "selected category " + cat.name + " and " + cat.id);
        Bundle b = new Bundle();
        b.putInt("categoryId", cat.id);
        b.putString("categoryName", cat.name);
        b.putInt("mode", ProductActivity.PRODUCT_BY_CATEGORY_MODE);
        startActivity(new Intent(context, ProductActivity.class).putExtras(b));
    }
}
