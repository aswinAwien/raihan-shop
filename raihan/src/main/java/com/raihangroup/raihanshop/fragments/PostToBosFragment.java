package com.raihangroup.raihanshop.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.BitmapUtil;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.ImageHelper;
import com.raihangroup.raihanshop.helper.ImageRenderer;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Complain;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Fragment for posting message to bos.
 *
 * @author Alh
 * @author Bambolz
 */
public class PostToBosFragment extends Fragment implements MediaSelectorFragment.MediaSelectorListener {

    private static final int CAMERA_REQUEST = 1101;
    private static final int GALLERY_REQUEST = 1102;
    private Context context;
    private AQuery aq;

    private ProgressDialog progDialog;

    public static final String[] POST_TO_BOS_TYPE = new String[]{"Saran", "Pertanyaan", "Komplain", "Usulan Produk"};
    private final static String TAG = "Post to Bos Frg";
    List<Complain> complains = new ArrayList<Complain>();
    Complain.Item selectedItem;
    Complain selectedComplain;
    private Intent pictureActionIntent;
    private String imagePath;
    private ImageView ivAddImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());

        progDialog = new ProgressDialog(context);
        progDialog.setTitle("Mohon Tunggu");
        progDialog.setMessage("Mengirimkan pesan");
        progDialog.setCancelable(false);

        PrefStorage.newInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post_to_bos, container, false);

        aq.recycle(rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        aq.id(R.id.spinTalk).adapter(new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, POST_TO_BOS_TYPE));
        final AutoCompleteTextView actvTransactionID = (AutoCompleteTextView) aq.id(R.id.actvTransactionID).getView();
        final AutoCompleteTextView actvOrderItem = (AutoCompleteTextView) aq.id(R.id.actvItem).getView();
        actvTransactionID.setTextColor(Color.BLACK);
        ivAddImage = (ImageView) aq.id(R.id.ivAddImage).getView();

        actvOrderItem.setTextColor(Color.BLACK);
        Spinner sp = (Spinner) aq.id(R.id.spinTalk).getView();
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2) {
                    aq.id(R.id.layoutTransaction).gone();
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                    Log.d("debug", "member_id = " + String.valueOf(PrefStorage.instance.getUserId()));
                    aq.ajax(Constants.URL_COMPLAIN_DROPDOWN_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
                        @Override
                        public void callback(String url, JSONArray object, AjaxStatus status) {
                            Log.d("debug", url);
                            Log.d("debug", "status code = " + status.getCode());
                            if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                Utility.toast(context, "list order berhasil didapat silahkan ketikan id order");
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<List<Complain>>() {
                                }.getType();
                                complains = gson.fromJson(object.toString(), collectionType);
                                ArrayAdapter<Complain> complainAdapter = new ArrayAdapter<Complain>(context, android.R.layout.simple_dropdown_item_1line, complains) {
                                    @Override
                                    public View getView(int position, View convertView, ViewGroup parent) {
                                        convertView = super.getDropDownView(position, convertView, parent);
                                        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
                                        tv.setTextColor(Color.BLACK);
                                        return convertView;
                                    }
                                };
                                actvTransactionID.setAdapter(complainAdapter);
                                aq.id(R.id.layoutTransaction).visible();
                                aq.id(R.id.actvTransactionID).text("");
                            } else {
                                DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mendapatkan daftar order",
                                        "OK", null);
                            }
                        }
                    });

                    actvTransactionID.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectedComplain = (Complain) parent.getItemAtPosition(position);
                            selectedItem = selectedComplain.items.get(0);
                            actvOrderItem.setText(selectedItem.title);
                            ArrayAdapter<Complain.Item> itemAdapter = new ArrayAdapter<Complain.Item>(context, android.R.layout.simple_dropdown_item_1line, selectedComplain.items) {
                                @Override
                                public View getView(int position, View convertView, ViewGroup parent) {
                                    convertView = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
                                    tv.setTextColor(Color.BLACK);
                                    return convertView;
                                }
                            };
                            itemAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            actvOrderItem.setAdapter(itemAdapter);
                            actvOrderItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    selectedItem = (Complain.Item) parent.getItemAtPosition(position);
                                }
                            });
                            aq.id(R.id.layoutItem).visible();
                        }
                    });


                } else {
                    aq.id(R.id.layoutTransaction).gone();
                    aq.id(R.id.layoutItem).gone();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        aq.id(R.id.btn_show_actv_transaction).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actvTransactionID.showDropDown();
            }
        });
        aq.id(R.id.btn_show_actv_item).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actvOrderItem.showDropDown();
            }
        });

        aq.id(R.id.btnSendMessage).clicked(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String s = (String) ((Spinner) aq.id(R.id.spinTalk).getView()).getSelectedItem();
                String subject = aq.id(R.id.etSubject).getText().toString();
                String message = aq.id(R.id.etMessage).getText().toString();
                if (subject.length() == 0 || message.length() == 0) {
                    DialogHelper.showMessageDialog(context, "Isian Kosong", "Isian teks subyek atau pesan tidak boleh kosong",
                            "OK", null);
                    return;
                }
                // find type
                int type = 0;
                for (int i = 0; i < POST_TO_BOS_TYPE.length; i++) {
                    if (POST_TO_BOS_TYPE[i].equals(s)) {
                        type = i + 1; // type start from 1
                        break;
                    }
                }
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                params.put("type", String.valueOf(type));
                params.put("subject", subject);
                params.put("content", message);
                if (imagePath != null) {
                    params.put("file", ImageHelper.getImageFile(imagePath, context));
                }
                //use 3 as parameter complaints
                if (type == 3) {
                    params.put("id_order_items", " " + selectedItem.id_order_items);
                    Log.d("debug", "selectedItem.id_order_items" + selectedItem.id_order_items);
                }
                aq.progress(progDialog).ajax(Constants.URL_POST_FEEDBACK_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        Log.d("debug", "stats =" + status.getCode());
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                            Utility.toast(context, "Pesan berhasil dikirim ke bos");
                            getActivity().setResult(Activity.RESULT_OK);
                            getActivity().finish();
                        } else {
                            DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengirimkan pesan. Silakan mencoba kembali",
                                    "OK", null);
                        }
                    }
                });
            }
        });

        aq.id(R.id.ivAddImage).clicked(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaSelectorFragment mediaSelectorFragment = new MediaSelectorFragment();
                mediaSelectorFragment.setTargetFragment(PostToBosFragment.this, 0);
                mediaSelectorFragment.show(getFragmentManager(), "imageChooser");
            }
        });

        aq.id(R.id.ivAddImage).longClicked(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
    }

    @Override
    public void onCameraButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "reyhan_client.jpg");
        pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(file));
        startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
    }

    @Override
    public void onGalleryButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                if (data != null) {
                    //try {
                    //ambil uri buat dikirim ke server
                    Uri imageUri = data.getData();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            String id = imageUri.getLastPathSegment()
                                    .split(":")[1];
                            final String[] imageColumns = {MediaStore.Images.Media.DATA};
                            final String imageOrderBy = null;
                            Uri uri = ImageHelper.getMediaStorageUri();
                            Cursor imageCursor = getActivity().managedQuery(uri,
                                    imageColumns, MediaStore.Images.Media._ID
                                            + "=" + id, null, imageOrderBy);
                            if (imageCursor.moveToFirst()) {
                                imagePath = imageCursor
                                        .getString(imageCursor
                                                .getColumnIndex(MediaStore.Images.Media.DATA));
                            }
                            Log.d("filepath", "kitkat = " + imagePath);
                        } catch (Exception e) {
                            Log.d("filepath", "isi uri = " + imageUri);
                            imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                            Log.d("filepath", "galery path = " + imagePath);
                        }
                    } else {
                        Log.d("filepath", "isi uri = " + imageUri);
                        imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                        Log.d("filepath", "galery path  = " + imagePath);
                    }
                    //render to ivAttachment
                    new ImageRenderer(ivAddImage, data, imagePath).execute();

/*                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "fail load image",
                                Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Toast.makeText(getActivity(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                Log.d("debug", "on activity camera");
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "reyhan_client.jpg");
                imagePath = Uri.fromFile(file).getPath();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imagePath, options);
                int width = options.outWidth;
                int height = options.outHeight;
                WindowManager wm = (WindowManager) getActivity()
                        .getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Log.d("debug", "Image.Width = " + width);
                Log.d("debug", "display.getWidth() = " + display.getWidth());
                float scale = (float) display.getWidth() / (float) width;
                Bitmap fixed = BitmapUtil.fixOrientation(context, ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                        (int) (height * scale)), Uri.fromFile(file));
                ivAddImage.setImageBitmap(fixed);
                ivAddImage.setVisibility(View.VISIBLE);
            }
        }
    }
}