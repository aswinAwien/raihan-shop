package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.SelfPerformanceActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.PerformanceObject;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ListPerformanceFragment extends Fragment {
    private Context context;
    private AQuery aq;

    private int month, year;

    private final static int ICON[] = new int[]{R.id.imIconTaruna,
            R.id.imIconPrajurit1, R.id.imIconPrajurit2, R.id.imIconPrajurit3, R.id.imIconPrajurit4,
            R.id.imIconKomandan1, R.id.imIconKomandan2, R.id.imIconKomandan3, R.id.imIconKomandan4,
            R.id.imIconKapten1, R.id.imIconKapten2, R.id.imIconKapten3, R.id.imIconKapten4,
            R.id.imIconMayor1, R.id.imIconMayor2, R.id.imIconMayor3, R.id.imIconMayor4,
            R.id.imIconJenderal1, R.id.imIconJenderal2, R.id.imIconJenderal3, R.id.imIconJenderal4,
            R.id.imIconPanglima1, R.id.imIconPanglima2, R.id.imIconPanglima3, R.id.imIconPanglima4,
            R.id.imIconGubernurMuda1, R.id.imIconGubernurMuda2, R.id.imIconGubernurMuda3, R.id.imIconGubernurMuda4,
            R.id.imIconGubernurBesar1, R.id.imIconGubernurBesar2, R.id.imIconGubernurBesar3, R.id.imIconGubernurBesar4,
            R.id.imIconMenteri1, R.id.imIconMenteri2, R.id.imIconMenteri3, R.id.imIconMenteri4,
            R.id.imIconPresiden};

    private final static int TITLE[] = new int[]{R.id.tvTitleTaruna,
            R.id.tvTitlePrajurit1, R.id.tvTitlePrajurit2, R.id.tvTitlePrajurit3, R.id.tvTitlePrajurit4,
            R.id.tvTitleKomandan1, R.id.tvTitleKomandan2, R.id.tvTitleKomandan3, R.id.tvTitleKomandan4,
            R.id.tvTitleKapten1, R.id.tvTitleKapten2, R.id.tvTitleKapten3, R.id.tvTitleKapten4,
            R.id.tvTitleMayor1, R.id.tvTitleMayor2, R.id.tvTitleMayor3, R.id.tvTitleMayor4,
            R.id.tvTitleJenderal1, R.id.tvTitleJenderal2, R.id.tvTitleJenderal3, R.id.tvTitleJenderal4,
            R.id.tvTitlePanglima1, R.id.tvTitlePanglima2, R.id.tvTitlePanglima3, R.id.tvTitlePanglima4,
            R.id.tvTitleGubernurMuda1, R.id.tvTitleGubernurMuda2, R.id.tvTitleGubernurMuda3, R.id.tvTitleGubernurMuda4,
            R.id.tvTitleGubernurBesar1, R.id.tvTitleGubernurBesar2, R.id.tvTitleGubernurBesar3, R.id.tvTitleGubernurBesar4,
            R.id.tvTitleMenteri1, R.id.tvTitleMenteri2, R.id.tvTitleMenteri3, R.id.tvTitleMenteri4,
            R.id.tvTitlePresiden};

    private final static int SUBTITLE[] = new int[]{R.id.tvSubTitleTaruna,
            R.id.tvSubTitlePrajurit1, R.id.tvSubTitlePrajurit2, R.id.tvSubTitlePrajurit3, R.id.tvSubTitlePrajurit4,
            R.id.tvSubTitleKomandan1, R.id.tvSubTitleKomandan2, R.id.tvSubTitleKomandan3, R.id.tvSubTitleKomandan4,
            R.id.tvSubTitleKapten1, R.id.tvSubTitleKapten2, R.id.tvSubTitleKapten3, R.id.tvSubTitleKapten4,
            R.id.tvSubTitleMayor1, R.id.tvSubTitleMayor2, R.id.tvSubTitleMayor3, R.id.tvSubTitleMayor4,
            R.id.tvSubTitleJenderal1, R.id.tvSubTitleJenderal2, R.id.tvSubTitleJenderal3, R.id.tvSubTitleJenderal4,
            R.id.tvSubTitlePanglima1, R.id.tvSubTitlePanglima2, R.id.tvSubTitlePanglima3, R.id.tvSubTitlePanglima4,
            R.id.tvSubTitleGubernurMuda1, R.id.tvSubTitleGubernurMuda2, R.id.tvSubTitleGubernurMuda3, R.id.tvSubTitleGubernurMuda4,
            R.id.tvSubTitleGubernurBesar1, R.id.tvSubTitleGubernurBesar2, R.id.tvSubTitleGubernurBesar3, R.id.tvSubTitleGubernurBesar4,
            R.id.tvSubTitleMenteri1, R.id.tvSubTitleMenteri2, R.id.tvSubTitleMenteri3, R.id.tvSubTitleMenteri4,
            R.id.tvSubTitlePresiden};

    private final static int KOMISI[] = new int[]{R.id.tvKomisiTaruna,
            R.id.tvKomisiPrajurit1, R.id.tvKomisiPrajurit2, R.id.tvKomisiPrajurit3, R.id.tvKomisiPrajurit4,
            R.id.tvKomisiKomandan1, R.id.tvKomisiKomandan2, R.id.tvKomisiKomandan3, R.id.tvKomisiKomandan4,
            R.id.tvKomisiKapten1, R.id.tvKomisiKapten2, R.id.tvKomisiKapten3, R.id.tvKomisiKapten4,
            R.id.tvKomisiMayor1, R.id.tvKomisiMayor2, R.id.tvKomisiMayor3, R.id.tvKomisiMayor4,
            R.id.tvKomisiJenderal1, R.id.tvKomisiJenderal2, R.id.tvKomisiJenderal3, R.id.tvKomisiJenderal4,
            R.id.tvKomisiPanglima1, R.id.tvKomisiPanglima2, R.id.tvKomisiPanglima3, R.id.tvKomisiPanglima4,
            R.id.tvKomisiGubernurMuda1, R.id.tvKomisiGubernurMuda2, R.id.tvKomisiGubernurMuda3, R.id.tvKomisiGubernurMuda4,
            R.id.tvKomisiGubernurBesar1, R.id.tvKomisiGubernurBesar2, R.id.tvKomisiGubernurBesar3, R.id.tvKomisiGubernurBesar4,
            R.id.tvKomisiMenteri1, R.id.tvKomisiMenteri2, R.id.tvKomisiMenteri3, R.id.tvKomisiMenteri4,
            R.id.tvKomisiPresiden};

    // for general layout container
    private final static int LL[] = new int[]{
            R.id.llTaruna,
            R.id.llPrajurit1, R.id.llPrajurit2, R.id.llPrajurit3, R.id.llPrajurit4,
            R.id.llKomandan1, R.id.llKomandan2, R.id.llKomandan3, R.id.llKomandan4,
            R.id.llKapten1, R.id.llKapten2, R.id.llKapten3, R.id.llKapten4,
            R.id.llMayor1, R.id.llMayor2, R.id.llMayor3, R.id.llMayor4,
            R.id.llJenderal1, R.id.llJenderal2, R.id.llJenderal3, R.id.llJenderal4,
            R.id.llPanglima1, R.id.llPanglima2, R.id.llPanglima3, R.id.llPanglima4,
            R.id.llGubernurMuda1, R.id.llGubernurMuda2, R.id.llGubernurMuda3, R.id.llGubernurMuda4,
            R.id.llGubernurBesar1, R.id.llGubernurBesar2, R.id.llGubernurBesar3, R.id.llGubernurBesar4,
            R.id.llMenteri1, R.id.llMenteri2, R.id.llMenteri3, R.id.llMenteri4,
            R.id.llPresiden};

    // size 14
    private final static int LINE[] = new int[]{R.id.doLineTaruna,
            R.id.upLinePrajurit1, R.id.doLinePrajurit1, R.id.upLinePrajurit2, R.id.doLinePrajurit2,
            R.id.upLinePrajurit3, R.id.doLinePrajurit3, R.id.upLinePrajurit4, R.id.doLinePrajurit4,
            R.id.upLineKomandan1, R.id.doLineKomandan1, R.id.upLineKomandan2, R.id.doLineKomandan2,
            R.id.upLineKomandan3, R.id.doLineKomandan3, R.id.upLineKomandan4, R.id.doLineKomandan4,
            R.id.upLineKapten1, R.id.doLineKapten1, R.id.upLineKapten2, R.id.doLineKapten2,
            R.id.upLineKapten3, R.id.doLineKapten3, R.id.upLineKapten4, R.id.doLineKapten4,
            R.id.upLineMayor1, R.id.doLineMayor1, R.id.upLineMayor2, R.id.doLineMayor2,
            R.id.upLineMayor3, R.id.doLineMayor3, R.id.upLineMayor4, R.id.doLineMayor4,
            R.id.upLineJenderal1, R.id.doLineJenderal1, R.id.upLineJenderal2, R.id.doLineJenderal2,
            R.id.upLineJenderal3, R.id.doLineJenderal3, R.id.upLineJenderal4, R.id.doLineJenderal4,
            R.id.upLinePanglima1, R.id.doLinePanglima1, R.id.upLinePanglima2, R.id.doLinePanglima2,
            R.id.upLinePanglima3, R.id.doLinePanglima3, R.id.upLinePanglima4, R.id.doLinePanglima4,
            R.id.doLineGubernurMuda1, R.id.doLineGubernurMuda1, R.id.doLineGubernurMuda2, R.id.doLineGubernurMuda2,
            R.id.doLineGubernurMuda3, R.id.doLineGubernurMuda3, R.id.doLineGubernurMuda4, R.id.doLineGubernurMuda4,
            R.id.upLineGubernurBesar1, R.id.doLineGubernurBesar1, R.id.upLineGubernurBesar2, R.id.doLineGubernurBesar2,
            R.id.upLineGubernurBesar3, R.id.doLineGubernurBesar3, R.id.upLineGubernurBesar4, R.id.doLineGubernurBesar4,
            R.id.upLineMenteri1, R.id.doLineMenteri1, R.id.upLineMenteri2, R.id.doLineMenteri2,
            R.id.upLineMenteri3, R.id.doLineMenteri3, R.id.upLineMenteri4, R.id.doLineMenteri4,
            R.id.upLinePresiden
    };

    private final static String TAG = "Perform List Frg";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();
        aq = new AQuery(getActivity());

        month = -1;
        year = -1;
        PrefStorage.newInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_perf_true_list,
                container, false);
        aq.recycle(rootView);

        Bundle b = getArguments();
        if (b != null) {
            month = b.getInt(Constants.BUNDLE_PERF_MONTH);
            year = b.getInt(Constants.BUNDLE_PERF_YEAR);
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("month", String.valueOf(month));
        params.put("year", String.valueOf(year));
        params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));

        aq.id(R.id.sv).gone(); // hide all views except progress bar
        aq.progress(R.id.pg).ajax(Constants.URL_PERFSUM_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject object,
                                 AjaxStatus status) {
                if (object != null) {
                    Gson gs = new Gson();
                    PerformanceObject po = gs.fromJson(object.toString(), PerformanceObject.class);
                    Log.d(TAG, "level " + po.getLevel());
                    int classId = kelasToId(po.getLevel());
                    // for debug purpose change from -1 to 37 (leveling position on android)
                    //int classId = 37;
                    aq.id(R.id.tvDate).text(po.getMonth() + " " + po.getYear());
                    aq.id(R.id.sv).visible();

                    if (classId == -1) {
                        DialogHelper.showMessageDialog(context, "Level " + po.getLevel(), "Level tersebut tidak diketahui. Kesalahan mungkin terjadi pada server. Silakan hubungi CS Raihan", "OK", null);
                        return;
                    }

                    setLineIconView(classId);
                    setPSAndPTBarView(classId, po);
                    setFeeCommissionView(classId, po);
                }
            }
        });

        return rootView;
    }

    // support methods for line icon views

    private void setLineIconView(int classId) {
        // only needed class id
        setLine(classId);
        setIcon(classId);
        setTitleAndSubtitle(classId);
    }

    private void setLine(int classId) {
        // formula to get bound of coloring
        // 2 because there is up and down for line
        int bound = classId * 2;
        Log.d(TAG, "line bound " + bound);
        for (int i = 0; i < LINE.length; i++) {
            if (i < bound) {
                aq.id(LINE[i]).backgroundColorId(R.color.raihan_muda);
            } else {
                aq.id(LINE[i]).backgroundColorId(R.color.grayc2);
            }
        }
    }

    private void setIcon(int classId) {
        for (int i = 0; i < ICON.length; i++) {
            if (i < classId) {
                // light pink check
                aq.id(ICON[i]).image(R.drawable.ic_check);
            } else if (i == classId) {
                aq.id(ICON[i]).image(R.drawable.ic_check_pink);
            } else {
                aq.id(ICON[i]).image(R.drawable.ic_check_grey);
            }
        }
    }

    private void setTitleAndSubtitle(int classId) {
        for (int i = 0; i < TITLE.length; i++) {

            // in default, title and subtitle is visible and their color are gray
            if (i < classId) {
                aq.id(TITLE[i]).textColorId(R.color.black);
                aq.id(SUBTITLE[i]).textColorId(R.color.black);
                aq.id(KOMISI[i]).textColorId(R.color.black);
            } else if (i == classId) {
                aq.id(TITLE[i]).textColorId(R.color.black);
                aq.id(SUBTITLE[i]).gone();
                aq.id(KOMISI[i]).gone();
            } else {
                aq.id(TITLE[i]).textColorId(R.color.gray);
                aq.id(SUBTITLE[i]).textColorId(R.color.gray);
                aq.id(KOMISI[i]).textColorId(R.color.gray);
            }
            TextView temp = (TextView) aq.id(KOMISI[i]).getView();
            //TextView temp = (TextView) aq.id(SUBTITLE[i]).getView();
            temp.setPadding(0, 0, 0, 10);
        }
    }

    // support methods for detail views
    private void setFeeCommissionView(int classId, PerformanceObject po) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //@todo remove magic number
        llParams.topMargin = 5;
        LinearLayout ll = (LinearLayout) aq.id(LL[classId]).getView();

        // set fee and commission view
        LinearLayout feeCommView = (LinearLayout) inflater.inflate(R.layout.item_fee_comm,
                null, false);
        setFeeAndCommButton(classId, po, feeCommView);
        setFeeAndCommIcon(classId, po, feeCommView);
        ll.addView(feeCommView, llParams);
    }

    private void setFeeAndCommButton(int classId, final PerformanceObject po,
                                     LinearLayout feeCommView) {
        Button feeButton = (Button) feeCommView.findViewById(R.id.feeButton);
        Button commissionButton = (Button) feeCommView.findViewById(R.id.commissionButton);
        //@todo remove magic number
        if (classId > 2) {
            commissionButton.setText("  Reward  ");
            feeButton.setText("  Bonus  ");
        } else {
            commissionButton.setText("  Komisi  ");
            feeButton.setText("      Fee      ");
        }

        feeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = "Fee bulan lalu (Vol " + po.last_month_vol + "):\n"
                        + Utility.formatPrice(po.last_month_fee) + "\n\n"
                        + "Fee bulan ini (Vol " + po.this_month_vol + "):\n"
                        + Utility.formatPrice(po.this_month_fee);
                DialogHelper.showMessageDialog(getActivity(), "Fee",
                        s, "OK", null);
            }
        });
        commissionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = "Total komisi bulan ini:\n"
                        + Utility.formatPrice(po.all_commission_this_month) + "\n\n"
                        + "Total komisi bulan sebelumnya:\n"
                        + Utility.formatPrice(po.all_commission_before) + "\n\n"
                        + "Komisi bulan ini yang belum diambil:\n"
                        + Utility.formatPrice(po.getCommission());
                DialogHelper.showMessageDialog(getActivity(), "Komisi",
                        s, "OK", null);
            }
        });
    }

    private void setFeeAndCommIcon(int classId, final PerformanceObject po,
                                   LinearLayout feeCommView) {
        ImageView icFeeComplete = (ImageView) feeCommView.findViewById(R.id.icFeeComplete);

        if (classId > 2) {
            icFeeComplete.setVisibility(View.GONE);
            return;
        }

        if (po.getPs() >= 15) {
            icFeeComplete.setImageDrawable(getResources().getDrawable(
                    R.drawable.ic_fee_complete));
            icFeeComplete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Utility.toast(getActivity(),
                            getResources().getString(R.string.msg_fee_complete));
                }
            });
        } else {
            icFeeComplete.setImageDrawable(getResources().getDrawable(
                    R.drawable.ic_fee_warning));
            icFeeComplete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Utility.toast(getActivity(),
                            getResources().getString(R.string.msg_fee_warning));
                }
            });
        }
    }

    private void setPSAndPTBarView(final int classId, final PerformanceObject po) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ProgressBar progBar = null;
        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llParams.topMargin = 5;
        LinearLayout ll = (LinearLayout) aq.id(LL[classId]).getView();

        // add PS Bar
        LinearLayout psBar = (LinearLayout) inflater.inflate(
                R.layout.item_ps_bar, null, false);
        psBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SelfPerformanceActivity.class);
                i.putExtra(Constants.INTENT_PERFORMANCE_ID,
                        PrefStorage.instance.getUserId());
                i.putExtra(Constants.INTENT_PERFORMANCE_MONTH, month);
                i.putExtra(Constants.INTENT_PERFORMANCE_YEAR, year);
                i.putExtra(Constants.INTENT_SELFPERFORMANCE_MODE,
                        SelfPerformanceActivity.MODE_PS);
                i.putExtra(Constants.INTENT_SPERFORMANCE_PS_START,
                        po.getPs());
                i.putExtra(Constants.INTENT_SPERFORMANCE_PS_END,
                        po.getTarget_ps());
                startActivity(i);
            }
        });
        progBar = (ProgressBar) psBar.findViewById(R.id.progressPS);
        progBar.setMax(po.getTarget_ps());
        progBar.setProgress(Math.round(po.getPs()));
        TextView tvLevelPSDesc = (TextView) psBar.findViewById(R.id.tvLevelPSDesc);
        tvLevelPSDesc.setText(po.getPs() + "/" + po.getTarget_ps());
        ll.addView(psBar, llParams); // add PS Bar to container LL with params

        // add PT Bar
        LinearLayout ptBar = (LinearLayout) inflater.inflate(R.layout.item_ttl_bar,
                null, false);
        ptBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),
                        SelfPerformanceActivity.class);
                i.putExtra(Constants.INTENT_PERFORMANCE_ID,
                        PrefStorage.instance.getUserId());
                i.putExtra(Constants.INTENT_PERFORMANCE_MONTH, month);
                i.putExtra(Constants.INTENT_PERFORMANCE_YEAR, year);
                i.putExtra(Constants.INTENT_SELFPERFORMANCE_MODE,
                        SelfPerformanceActivity.MODE_TTS);
                i.putExtra(Constants.INTENT_SPERFORMANCE_PS_START,
                        po.getTts());
                i.putExtra(Constants.INTENT_SPERFORMANCE_PS_END, 100);
                startActivity(i);
            }
        });
        progBar = (ProgressBar) ptBar.findViewById(R.id.progressTTL);
        progBar.setMax(100);
        progBar.setProgress((int) po.getTts());
        TextView tvLevelTTLDesc = (TextView) ptBar.findViewById(R.id.tvLevelTTLDesc);
        tvLevelTTLDesc.setText(po.getTts() + "/100");
        ll.addView(ptBar, llParams); // add PT Bar to container LL with params
    }

    public static int kelasToId(String kelas) {
        int ret = -1;
        if (kelas.equalsIgnoreCase("Taruna")) ret = 0;
        if (kelas.equalsIgnoreCase("Prajurit 1")) ret = 1;
        if (kelas.equalsIgnoreCase("Prajurit 2")) ret = 2;
        if (kelas.equalsIgnoreCase("Prajurit 3")) ret = 3;
        if (kelas.equalsIgnoreCase("Prajurit 4")) ret = 4;
        if (kelas.equalsIgnoreCase("Komandan 1")) ret = 5;
        if (kelas.equalsIgnoreCase("Komandan 2")) ret = 6;
        if (kelas.equalsIgnoreCase("Komandan 3")) ret = 7;
        if (kelas.equalsIgnoreCase("Komandan 4")) ret = 8;
        if (kelas.equalsIgnoreCase("Kapten 1")) ret = 9;
        if (kelas.equalsIgnoreCase("Kapten 2")) ret = 10;
        if (kelas.equalsIgnoreCase("Kapten 3")) ret = 11;
        if (kelas.equalsIgnoreCase("Kapten 4")) ret = 12;
        if (kelas.equalsIgnoreCase("Mayor 1")) ret = 13;
        if (kelas.equalsIgnoreCase("Mayor 2")) ret = 14;
        if (kelas.equalsIgnoreCase("Mayor 3")) ret = 15;
        if (kelas.equalsIgnoreCase("Mayor 4")) ret = 16;
        if (kelas.equalsIgnoreCase("Jenderal 1")) ret = 17;
        if (kelas.equalsIgnoreCase("Jenderal 2")) ret = 18;
        if (kelas.equalsIgnoreCase("Jenderal 3")) ret = 19;
        if (kelas.equalsIgnoreCase("Jenderal 4")) ret = 20;
        if (kelas.equalsIgnoreCase("Panglima 1")) ret = 21;
        if (kelas.equalsIgnoreCase("Panglima 2")) ret = 22;
        if (kelas.equalsIgnoreCase("Panglima 3")) ret = 23;
        if (kelas.equalsIgnoreCase("Panglima 4")) ret = 24;
        if (kelas.equalsIgnoreCase("Gubernur Muda 1")) ret =25;
        if (kelas.equalsIgnoreCase("Gubernur Muda 2")) ret =26;
        if (kelas.equalsIgnoreCase("Gubernur Muda 3")) ret =27;
        if (kelas.equalsIgnoreCase("Gubernur Muda 4")) ret =28;
        if (kelas.equalsIgnoreCase("Gubernur Besar 1")) ret =29;
        if (kelas.equalsIgnoreCase("Gubernur Besar 2")) ret =30;
        if (kelas.equalsIgnoreCase("Gubernur Besar 3")) ret =31;
        if (kelas.equalsIgnoreCase("Gubernur Besar 4")) ret =32;
        if (kelas.equalsIgnoreCase("Menteri 1")) ret =33;
        if (kelas.equalsIgnoreCase("Menteri 2")) ret =34;
        if (kelas.equalsIgnoreCase("Menteri 3")) ret =35;
        if (kelas.equalsIgnoreCase("Menteri 4")) ret =36;
        if (kelas.equalsIgnoreCase("Presiden")) ret = 37;
        return ret;
    }
}
