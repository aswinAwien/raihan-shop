package com.raihangroup.raihanshop.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.HomeActivity;
import com.raihangroup.raihanshop.adapter.BookListAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.BookReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookingListFragment extends Fragment {

    private AQuery aq;
    public ArrayList<BookReference> objects;
    public BookListAdapter adapter;
    private ExpandableListView exListview;

    public BookingListFragment() {
        aq = new AQuery(getActivity());
        objects = new ArrayList<BookReference>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_booking_list, container, false);
        aq.recycle(rootView);
        exListview = (ExpandableListView) rootView.findViewById(R.id.exlv);
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBookingList();
            }
        });
        getBookingList();
        return rootView;
    }

    private void getBookingList() {
        aq.id(R.id.llInfo).gone();
        Map<String, String> params = new HashMap<String, String>();
        params.put("reseller_id", String.valueOf(PrefStorage.instance.getUserId()));
        if (Utility.isAdmin()) {
            params.put("reseller_id", String.valueOf(((HomeActivity) getActivity()).idChooseB));
            params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
        }
        aq.progress(R.id.pg).ajax(Constants.URL_GETBOOK_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    objects = gs.fromJson(object.toString(),
                            new TypeToken<List<BookReference>>() {
                            }.getType());
                    adapter = new BookListAdapter(getActivity(), objects, true);
                    exListview.setAdapter(adapter);
                    if (objects.size() == 0) {
                        aq.id(R.id.tvMessageTitle).text("Anda Belum Melakukan Booking");
                        aq.id(R.id.tvMessageContent).text("Silakan melakukan booking");
                        aq.id(R.id.llInfo).visible();
                    }
                } else {
                    aq.id(R.id.tvMessageTitle).text("Terjadi Kesalahan");
                    aq.id(R.id.llInfo).visible();
                }
            }
        });
    }
}
