package com.raihangroup.raihanshop.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.FeedbackReplyAdapter;
import com.raihangroup.raihanshop.helper.BitmapUtil;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.ImageHelper;
import com.raihangroup.raihanshop.helper.ImageRenderer;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.FeedbackReply;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Fragment for feedback reply list to bos.
 *
 * @author Alh
 */
public class FeedbackReplyListFragment extends Fragment implements MediaSelectorFragment.MediaSelectorListener {

    private Context context;
    private AQuery aq;

    private ListView listView;
    private EditText etReplyMessage;
    private ImageView ivReplyMessage;
    private ImageView ivAttachImage;
    private ImageView ivAttachment;
    private ProgressDialog progDialog;

    private final static String TAG = "Feedback Reply To Bos Frg";
    private String imagePath;
    private Intent pictureActionIntent;
    private final static int GALLERY_REQUEST = 1101;
    private final static int CAMERA_REQUEST = 1102;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());

        progDialog = new ProgressDialog(context);
        progDialog.setTitle("Mengirim pesan");
        progDialog.setMessage("Mohon Tunggu...");
        progDialog.setCancelable(false);

        PrefStorage.newInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_chat, container, false);

        aq.recycle(rootView);
        listView = (ListView) aq.id(R.id.listView).getView();
        // set message here
        etReplyMessage = (EditText) aq.id(R.id.etReplyMessage).getView();
        ivReplyMessage = (ImageView) aq.id(R.id.ivReplyMessage).getView();
        ivAttachImage = (ImageView) aq.id(R.id.ivAttachImage).getView();
        ivAttachment = (ImageView) aq.id(R.id.iv_attachment).getView();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ivReplyMessage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.clearSoftInputFromView(context, etReplyMessage);

                String message = etReplyMessage.getText().toString();

                if (message.length() == 0) {
                    DialogHelper.showMessageDialog(context, "Isian Kosong", "Isian teks balasan tidak boleh kosong",
                            "OK", null);
                    return;
                }

                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                params.put("feedback_id", getArguments().getString("feedbackId"));
                params.put("content", message);
                if (imagePath != null) {
                    params.put("file", ImageHelper.getImageFile(imagePath, context));
                }
                aq.progress(progDialog).ajax(Constants.URL_POST_REPLY_FEEDBACK_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                            Utility.toast(context, "Pesan balasan berhasil dikirim ke bos");
                            etReplyMessage.setText("");
                            ivAttachment.setVisibility(View.GONE);
                            // self refresh
                            refreshData();
                        } else {
                            DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mengirimkan pesan balasan. Silakan mencoba kembali",
                                    "OK", null);
                        }
                    }
                });
            }
        });
        ivAttachImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaSelectorFragment MSF = new MediaSelectorFragment();
                MSF.setTargetFragment(FeedbackReplyListFragment.this, 0);
                MSF.show(getFragmentManager(), "imageChooser");
            }
        });
        refreshData();
    }

    // public methods

    public void refreshData() {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("feedback_id", getArguments().getString("feedbackId"));
        Log.d("debug", "feedback id = " + getArguments().getString("feedbackId"));
        aq.progress(R.id.pg).ajax(Constants.URL_FEEDBACK_REPLY_LIST_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {

            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                aq.id(R.id.llInfo).gone();
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();
                    return;
                }
                Gson gs = new Gson();
                ArrayList<FeedbackReply> listOfFeedbackReplies = gs.fromJson(object.toString(),
                        new TypeToken<List<FeedbackReply>>() {
                        }.getType());
                if (listOfFeedbackReplies.isEmpty()) {
                    aq.id(R.id.tvMessageTitle).text("Tidak Ada Pesan");
                    aq.id(R.id.tvMessageContent).text("Silahkan memulai percakapan");
                    aq.id(R.id.llInfo).visible();
                    return;
                }
                FeedbackReplyAdapter adapter = new FeedbackReplyAdapter(context, listOfFeedbackReplies);
                listView.setAdapter(adapter);
                listView.setSelection(listView.getCount());

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
                params.put("type", "" + 0);
                aq.ajax(Constants.URL_READ_NOTIFICATION, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                            try {
                                if (object.getString("Success").equals("true"))
                                    Log.d("debug", "notif read");
                            } catch (Exception e) {
                                Log.d("exception", String.valueOf(e));
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onCameraButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "reyhan_client.jpg");
        pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(file));
        startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
    }

    @Override
    public void onGalleryButtonClick(DialogFragment dialog) {
        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                if (data != null) {
                    //try {
                    ivAttachment.setVisibility(View.VISIBLE);
                    //ambil uri buat dikirim ke server
                    Uri imageUri = data.getData();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            String id = imageUri.getLastPathSegment()
                                    .split(":")[1];
                            final String[] imageColumns = {MediaStore.Images.Media.DATA};
                            final String imageOrderBy = null;
                            Uri uri = ImageHelper.getMediaStorageUri();
                            Cursor imageCursor = getActivity().managedQuery(uri,
                                    imageColumns, MediaStore.Images.Media._ID
                                            + "=" + id, null, imageOrderBy);
                            if (imageCursor.moveToFirst()) {
                                imagePath = imageCursor
                                        .getString(imageCursor
                                                .getColumnIndex(MediaStore.Images.Media.DATA));
                            }
                            Log.d("filepath", "kitkat = " + imagePath);
                        } catch (Exception e) {
                            Log.d("filepath", "isi uri = " + imageUri);
                            imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                            Log.d("filepath", "galery path = " + imagePath);
                        }
                    } else {
                        Log.d("filepath", "isi uri = " + imageUri);
                        imagePath = ImageHelper.getRealPathFromURI(imageUri, context);
                        Log.d("filepath", "galery path  = " + imagePath);
                    }
                    //render to ivAttachment
                    new ImageRenderer(ivAttachment, data, imagePath).execute();

/*                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "fail load image",
                                Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Toast.makeText(getActivity(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                Log.d("debug", "on activity camera");

                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "reyhan_client.jpg");
                imagePath = Uri.fromFile(file).getPath();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imagePath, options);
                int width = options.outWidth;
                int height = options.outHeight;
                WindowManager wm = (WindowManager) getActivity()
                        .getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Log.d("debug", "Image.Width = " + width);
                Log.d("debug", "display.getWidth() = " + display.getWidth());
                float scale = (float) display.getWidth() / (float) width;
                Bitmap fixed = BitmapUtil.fixOrientation(context, ImageRenderer.decodeSampledBitmapFromFile(file.getAbsolutePath(), display.getWidth(),
                        (int) (height * scale)), Uri.fromFile(file));

                ivAttachment.setImageBitmap(fixed);
                ivAttachment.setVisibility(View.VISIBLE);
            } else {
                ivAttachment.setVisibility(View.GONE);
            }
        }
    }

}
