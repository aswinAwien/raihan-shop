package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductActivity;
import com.raihangroup.raihanshop.adapter.SupplierAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Supplier;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * List for suppliers.
 *
 * @author Alh
 */
public class SupplierListFragment extends Fragment implements OnItemClickListener {

    private Context context;
    private AQuery aq;

    private ListView listView;

    private final static String TAG = "Supplier Frg";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_supplier_list, container, false);
        aq.recycle(rootView);
        listView = (ListView) rootView.findViewById(R.id.listView);
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupplier();
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView.setOnItemClickListener(this);
        getSupplier();
    }

    private void getSupplier() {
        Map<String, String> unusedParams = new HashMap<String, String>();
        aq.progress(R.id.pg).ajax(Constants.URL_SUPPLIER_LIST_API, unusedParams, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                aq.id(R.id.llInfo).gone();
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();
                }
                Gson gs = new Gson();
                ArrayList<Supplier> listOfSuppliers = gs.fromJson(object.toString(),
                        new TypeToken<List<Supplier>>() {
                        }.getType());
                SupplierAdapter adapter = new SupplierAdapter(context, listOfSuppliers);
                listView.setAdapter(adapter);
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
        Supplier supplier = (Supplier) listView.getItemAtPosition(position);
        Log.d(TAG, "selected supplier " + supplier.fullname);
        Bundle b = new Bundle();
        b.putString("supplierId", supplier.id);
        b.putString("supplierName", supplier.fullname);
        b.putInt("mode", ProductActivity.PRODUCT_BY_SUPPLIER_MODE);
        startActivity(new Intent(context, ProductActivity.class).putExtras(b));
    }
}
