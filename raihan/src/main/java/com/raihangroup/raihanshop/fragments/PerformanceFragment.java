package com.raihangroup.raihanshop.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.User;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PerformanceFragment extends Fragment {

	private AQuery aq;
	
	/** Month pointer. 1 is January. */
	private int monthPtr; 
	/** Year pointer. */
	private int yearPtr;
	/** Join month. */
	private int joinMonth;
    /** Join Year. */
    private int joinYear;
	/** Current month from active calendar. */
	private int currentMonth;
    /** Current year from active calendar. */
    private int currentYear;

	private User user;
	
	private final static String TAG = "Perform Frg";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aq = new AQuery(getActivity());
		PrefStorage.newInstance(getActivity());
		user = new Gson().fromJson(PrefStorage.instance.getUserData(), User.class);
		
		Calendar c = Calendar.getInstance();
		currentMonth = c.get(Calendar.MONTH) + 1; // adjust to 1 is January
        currentYear = c.get(Calendar.YEAR);
		monthPtr = currentMonth;
		yearPtr = currentYear;
		Log.d(TAG, "current month " + currentMonth);
		
		try {
			c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(user.getJoined_date()));
			joinMonth = c.get(Calendar.MONTH) + 1; // adjust to 1 is January
            joinYear = c.get(Calendar.YEAR);
			Log.d(TAG, "join month " + joinMonth);
		} catch (Exception e) {
			Log.d("exception", String.valueOf(e));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_performance, container, false);
		
		aq.recycle(rootView);
		
		// set initial visibility
		aq.id(R.id.btnPrevMonth).visibility(View.VISIBLE);
		aq.id(R.id.btnNextMonth).visibility(View.VISIBLE);
		if(joinMonth == monthPtr && joinYear == yearPtr) {
			aq.id(R.id.btnPrevMonth).visibility(View.INVISIBLE);
		}
		if(currentMonth == monthPtr && currentYear == yearPtr) {
			aq.id(R.id.btnNextMonth).visibility(View.INVISIBLE);
		}
		
		aq.id(R.id.tvBulan).text("Bulan " + monthPtr);
		
		aq.id(R.id.btnPrevMonth).clicked(new PrevMonthClickListener());
		aq.id(R.id.btnNextMonth).clicked(new NextMonthClickListener());
		
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		// set current month fragment
		Fragment f = new ListPerformanceFragment();
		Bundle b = new Bundle();
		b.putInt(Constants.BUNDLE_PERF_MONTH, monthPtr);
		b.putInt(Constants.BUNDLE_PERF_YEAR, yearPtr);
		f.setArguments(b);
		getChildFragmentManager().beginTransaction()
			.replace(R.id.container, f).commit();
	}
	
	@Override
	public void onDetach() {
	    super.onDetach();
	    try {
	        Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
	        childFragmentManager.setAccessible(true);
	        childFragmentManager.set(this, null);

	    } catch (NoSuchFieldException e) {
	        throw new RuntimeException(e);
	    } catch (IllegalAccessException e) {
	        throw new RuntimeException(e);
	    }
	}
	
	// inner class listener
	
	private class PrevMonthClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			Fragment f = new ListPerformanceFragment();
			
			monthPtr--;
			if(monthPtr == 0) {
				monthPtr = 12;
				yearPtr--;
			}
			aq.id(R.id.tvBulan).text("Bulan " + monthPtr);
			
			Bundle b = new Bundle();
			b.putInt(Constants.BUNDLE_PERF_MONTH, monthPtr);
			b.putInt(Constants.BUNDLE_PERF_YEAR, yearPtr);
			f.setArguments(b);
			getChildFragmentManager().beginTransaction()
				.replace(R.id.container, f).commit();
			
			// in the beginning of month range, hide prev month button
			if(joinMonth == monthPtr && joinYear == yearPtr) {
				v.setVisibility(View.INVISIBLE);
			}
			// always show next month button 
			aq.id(R.id.btnNextMonth).visibility(View.VISIBLE);
			
			Log.d(TAG, "prev month ptr: " + monthPtr);
		}
	}
	
	private class NextMonthClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			Fragment f = new ListPerformanceFragment();
			
			monthPtr++;
			if(monthPtr == 13) {
				monthPtr = 1;
				yearPtr++;
			}
            aq.id(R.id.tvBulan).text("Bulan " + monthPtr);
			Bundle b = new Bundle();
			b.putInt(Constants.BUNDLE_PERF_MONTH, monthPtr);
			b.putInt(Constants.BUNDLE_PERF_YEAR, yearPtr);
			f.setArguments(b);
			getChildFragmentManager().beginTransaction()
				.replace(R.id.container, f).commit();
			
			// in the end of month range, hide next month button
			if(currentMonth == monthPtr && currentYear == yearPtr) {
				v.setVisibility(View.INVISIBLE);
			}
			// always show prev month button
			aq.id(R.id.btnPrevMonth).visibility(View.VISIBLE);
			
			Log.d(TAG, "next month ptr: " + monthPtr);
		}
		
	}
}
