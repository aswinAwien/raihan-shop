package com.raihangroup.raihanshop.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

/**
 * Created by Mohamad on 1/30/2015.
 */
public class MediaSelectorFragment extends DialogFragment {
    public interface MediaSelectorListener {
        void onCameraButtonClick(DialogFragment dialog);

        void onGalleryButtonClick(DialogFragment dialog);
    }

    private String[] sourceImage = null;
    public MediaSelectorListener mediaSelectorListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the
            // host
            if (getTargetFragment() instanceof MediaSelectorListener) {
                mediaSelectorListener = (MediaSelectorListener) getTargetFragment();
                Log.d("debug", "check");
            } else {
                mediaSelectorListener = (MediaSelectorListener) activity;
            }
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement Media Selector Listener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        sourceImage = new String[2];
        sourceImage[0] = "Camera";
        sourceImage[1] = "Gallery";
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Image Source").setItems(sourceImage,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                mediaSelectorListener.onCameraButtonClick(MediaSelectorFragment.this);
                                break;
                            case 1:
                                mediaSelectorListener.onGalleryButtonClick(MediaSelectorFragment.this);
                                break;
                        }
                    }
                });
        return builder.create();
    }
}
