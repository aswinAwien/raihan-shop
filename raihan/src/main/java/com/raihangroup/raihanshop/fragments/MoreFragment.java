package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.EditProfileActivity;
import com.raihangroup.raihanshop.activity.ForgetPasActivity;
import com.raihangroup.raihanshop.activity.ProfileActivity;
import com.raihangroup.raihanshop.activity.WhatsNewActivity;
import com.raihangroup.raihanshop.adapter.StringAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;

import java.util.ArrayList;

/**
 * List for string list.
 *
 * @author Alh
 */
public class MoreFragment extends Fragment implements OnItemClickListener {

    private Context context;
    private AQuery aq;
    public static final int REQ_CODE_VIEW_PROFILE = 3;

    private ListView listView;

    private final static String TAG = "String List Frg";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());

        PrefStorage.newInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_with_message, container, false);
        View footerMuteView = inflater.inflate(R.layout.footer_mute_notification, null, false);
        SwitchCompat notificationSwitch = (SwitchCompat) footerMuteView.findViewById(R.id.switch1);
        boolean mute = PrefStorage.instance.getMuteNotification();
        notificationSwitch.setChecked(!mute);
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    PrefStorage.instance.setMuteNotification(false);
                } else {
                    PrefStorage.instance.setMuteNotification(true);
                }
                Log.d("Mute = ", "" + PrefStorage.instance.getMuteNotification());
            }
        });

        aq.recycle(rootView);
        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.addFooterView(footerMuteView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        aq.id(R.id.pg).gone();
        aq.id(R.id.llInfo).gone();

        ArrayList<String> listOfString = new ArrayList<String>();
        listOfString.add("Ubah Password");
        listOfString.add("Edit Profile");
        StringAdapter adapter = new StringAdapter(context, listOfString);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
        String s = (String) listView.getItemAtPosition(position);
        Log.d(TAG, "selected string " + s);
        if (position == 0) {
            Intent i = new Intent(getActivity(), ForgetPasActivity.class);
            i.putExtra(Constants.INTENT_FORGET_P, ForgetPasActivity.EDIT);
            startActivity(i);
        } else if (position == 1) {
            Bundle b = new Bundle();
            b.putParcelable("userProfile", ProfileActivity.profile);
            Intent i = new Intent(getActivity(), EditProfileActivity.class).putExtras(b);
            startActivityForResult(i, REQ_CODE_VIEW_PROFILE);
        }
    }
}
