package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.ModelChannel;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.DualModeActivity;
import com.raihangroup.raihanshop.helper.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PhotoViewFragment extends DialogFragment implements OnPageChangeListener {
	public static class Photo extends ModelChannel{
		private static final long serialVersionUID = -2986064397062812162L;

		public String photo;
		public String title;
		public String subtitle;

		public Photo(String photo, String title, String subtitle) {
			this.photo = photo;
			this.title = title;
			this.subtitle = subtitle;
		}

		@Override
		public String getTitle() {
			return photo;
		}

		@Override
		public int getLayout() {
			return R.layout.photo;
		}

	}

	private static final String PHOTOS = "PHOTOS";
	private static final String DEF_POS = "DEF_POS";

	private ViewPager mViewPager;
	private SamplePagerAdapter mAdapter;
	private List<Photo> photos = new ArrayList<Photo>();
	private Bundle mArguments;
	private android.support.v7.widget.Toolbar photoToolbar;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
//		mViewPager.setTransitionEffect(TransitionEffect.Stack);
		View rootView = LayoutInflater.from(container.getContext()).inflate(R.layout.photo, container, false);
        photoToolbar = rootView.findViewById(R.id.toolbar_photo);
		mViewPager = new ViewPager(getActivity());
		mAdapter = new SamplePagerAdapter();
		mViewPager.setAdapter(mAdapter);
		mViewPager.setOnPageChangeListener(this);
		int pos = mArguments.getInt(DEF_POS, 0);
		mViewPager.setCurrentItem(pos);
		((AppCompatActivity)getActivity()).setSupportActionBar(photoToolbar);
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(photos.get(pos).title);
		Log.i("TEST", "onCreateView: " + photos.get(pos).title);
		((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(photos.get(pos).subtitle);
		Log.i("TEST", "onCreateView: " + photos.get(pos).subtitle);
		((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
		((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		return mViewPager;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mArguments = getArguments();
		if(mArguments != null && mArguments.getSerializable(PHOTOS) != null){
			for(Object photo:(Object[]) getArguments().getSerializable(PHOTOS)){
				addPhoto((Photo) photo);
			}
		}
		setHasOptionsMenu(true);
	}

	public void setPosition(int x){
		getArguments().putInt(DEF_POS, x);
	}

	public void addPhoto(Photo bm){
		photos.add(bm);
		if(mAdapter != null)	mAdapter.notifyDataSetChanged();
	}

	public void addPhotos(Collection<Photo> bm){
		photos.addAll(bm);
		if(mAdapter != null)	mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.share, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){

		}
		else if(item.getItemId() == R.id.share){
			Photo i = photos.get(mViewPager.getCurrentItem());
			ModelChannel.shareItem(getActivity(), i.title, i.photo);
			return true;
		}
		else if(item.getItemId() == R.id.save){
			Photo i = photos.get(mViewPager.getCurrentItem());
			File img = ImageLoaderApplication.getImageLoader().getFileManager().getFile(i.photo);
			File root = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
			File file = new File(root, i.title+"_"+mViewPager.getCurrentItem()+".jpg");try {
				Utility.copy(img, file);
				Utility.saveToGallery(file, i.title+"_"+mViewPager.getCurrentItem(), getActivity());
				Utility.toast(getActivity(), "Gambar Produk Berhasil Disimpan");
			} catch (Exception e) {
				Utility.toast(getActivity(), "Gambar Produk Gagal Disimpan!");
			    Log.e("Copy:", Log.getStackTraceString(e));
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public class SamplePagerAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			return photos.size();
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			Photo p = photos.get(position);
			LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflater.inflate(p.getLayout(), container, false);
			container.addView(v);
			p.getData(v, -1);
			return v;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {}

	@Override
	public void onPageSelected(int i) {
		((AppCompatActivity)getActivity()).setSupportActionBar(photoToolbar);
		((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(photos.get(i).title);
		((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(photos.get(i).subtitle);
	}

	public static PhotoViewFragment init(Context ctx, int pos, Photo... photos){
		final Bundle b = new Bundle();
		b.putString("fragment", PhotoViewFragment.class.getName());
		b.putInt(PhotoViewFragment.DEF_POS, pos);
		b.putSerializable(PhotoViewFragment.PHOTOS, photos);
//		b.putInt(SingleModeActivity.THEME, R.style.Theme_AppCompat);

		Fragment frag = Fragment.instantiate(ctx, PhotoViewFragment.class.getName());
		frag.setArguments(b);
		return (PhotoViewFragment) frag;
	}

	public static PhotoViewFragment init(Context ctx, int pos, String... images){
		final Bundle b = new Bundle();
		List<Photo> photos = new ArrayList<Photo>();
		for(String s:images){
			photos.add(new Photo(s, "Raihan", null));
		}
		b.putString("fragment", PhotoViewFragment.class.getName());
		b.putInt(PhotoViewFragment.DEF_POS, pos);
		b.putSerializable(PhotoViewFragment.PHOTOS, photos.toArray(new Photo[0]));
//		b.putInt(SingleModeActivity.THEME, R.style.Theme_AppCompat);

		Fragment frag = Fragment.instantiate(ctx, PhotoViewFragment.class.getName());
		frag.setArguments(b);
		return (PhotoViewFragment) frag;
	}

	public static void show(Context ctx, int pos, String... images){
		PhotoViewFragment f = init(ctx, pos, images);
		DualModeActivity.initFragment(ctx, f);
	}

	public static void show(Context ctx, int pos, Photo... images){
		PhotoViewFragment f = init(ctx, pos, images);
		DualModeActivity.initFragment(ctx, f);
	}
}
