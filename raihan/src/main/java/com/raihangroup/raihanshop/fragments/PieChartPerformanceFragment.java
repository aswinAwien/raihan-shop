package com.raihangroup.raihanshop.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raihangroup.raihanshop.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PieChartPerformanceFragment extends Fragment {


    @BindView(R.id.pieLevelChart)
    PieChart pieLevelChart;
    Unbinder unbinder;

    public PieChartPerformanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pie_chart_performance, container, false);
        unbinder = ButterKnife.bind(this, view);
        setData();
        return view;
    }

    private void setData(){
        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(30,"P.S"));
        entries.add(new PieEntry(20,"P.T"));
        entries.add(new PieEntry(50,"uncompleted"));

        PieDataSet dataSet = new PieDataSet(entries, " ");

        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c: ColorTemplate.MATERIAL_COLORS)
            colors.add(c);

        dataSet.setColors(colors);
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
//        data.setValueTypeface(mTfLight);
        pieLevelChart.setData(data);
        pieLevelChart.animateX(1400);
        pieLevelChart.setCenterText("TARUNA");
        // undo all highlights
        pieLevelChart.highlightValues(null);

        pieLevelChart.invalidate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
