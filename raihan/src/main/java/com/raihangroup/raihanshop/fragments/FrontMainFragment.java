package com.raihangroup.raihanshop.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FrontMainFragment extends Fragment {


    private static final String TAG = FrontMainFragment.class.getSimpleName();
    @BindView(R.id.view_pager_front)
    ViewPager viewPagerFront;
    Unbinder unbinder;
    HomeShopFragment homeShopFragment;

    public FrontMainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_front_home, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        homeShopFragment = new HomeShopFragment();
        setupViewPager();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setupViewPager(){
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(homeShopFragment);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(),fragments);
        viewPagerFront.setAdapter(adapter);
    }
}

