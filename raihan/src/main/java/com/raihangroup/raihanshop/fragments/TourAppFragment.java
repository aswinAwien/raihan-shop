package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.TourAppActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;

/**
 * Fragment for tour app.
 * @author Alh
 *
 */
public class TourAppFragment extends Fragment {

	private Context context;
	private AQuery aq;
	
	private final static String TAG = "Tour App Frg";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
		aq = new AQuery(getActivity());
		
		PrefStorage.newInstance(context);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {	
		View rootView = inflater.inflate(R.layout.fragment_tour_app, container, false);
		
		aq.recycle(rootView);
		
		return rootView;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // specific view and action for specific tour id
        int tourId = getArguments().getInt("tourId");
        if(tourId == Constants.TOUR_PAGE_ID_PRODUCT_BOOKING) {
            aq.id(R.id.btnAction).text("Lihat Cara Booking");
            aq.id(R.id.btnAction).clicked(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, TourAppActivity.class).putExtra("contentListId",
                            Constants.TOUR_LIST_ID_BOOKING));
                }
            });
        } else if(tourId == Constants.TOUR_PAGE_ID_PAYMENT_CONFIRM) {
            aq.id(R.id.btnAction).text("Lihat Cara Konfirmasi Bayar");
            aq.id(R.id.btnAction).clicked(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, TourAppActivity.class).putExtra("contentListId",
                            Constants.TOUR_LIST_ID_PAYMENT));
                }
            });
        } else if(tourId == Constants.TOUR_PAGE_ID_DELIVERY_CONFIRM) {
            aq.id(R.id.btnAction).text("Lihat Cara Konfirmasi Pengiriman");
            aq.id(R.id.btnAction).clicked(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, TourAppActivity.class).putExtra("contentListId",
                            Constants.TOUR_LIST_ID_DELIVERY));
                }
            });
        } else if(tourId==Constants.TOUR_PAGE_ID_HOW_TO_BOOKING_2
                || tourId==Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_4
                || tourId==Constants.TOUR_PAGE_ID_HOW_TO_CONFIRM_DELIVERY_2) {
            aq.id(R.id.btnAction).text("Kembali ke Panduan");
            aq.id(R.id.btnAction).clicked(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // finish
                    getActivity().finish();
                }
            });
        } else {
            aq.id(R.id.btnAction).gone();
        }

        aq.id(R.id.tvTitle).text(getArguments().getString("title"));
        aq.id(R.id.tvDesc).text(getArguments().getString("description"));
        aq.id(R.id.imTourApp).image(getArguments().getInt("imageRes"));
	}
}
