package com.raihangroup.raihanshop.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.HomeActivity;
import com.raihangroup.raihanshop.adapter.OrderAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Order;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderListFragment extends Fragment {

    private AQuery aq;
    private ArrayList<Order> listOfOrders;
    private ExpandableListView exListView;
    private OrderAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_list, container, false);
        aq = new AQuery(getActivity());
        aq.id(R.id.tvMessageTitle).text("Tidak Ada list order");
        aq.id(R.id.tvMessageContent).text("Silakan Mencoba Kembali");
        listOfOrders = new ArrayList<Order>();
        aq.recycle(rootView);
        exListView = (ExpandableListView) rootView.findViewById(R.id.exlv);
        exListView.setIndicatorBounds(15,35);
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewVisibilityInProgressState();
                getOrderList();
            }
        });
        getOrderList();
        return rootView;
    }

    public void getOrderList() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("reseller_id", String.valueOf(PrefStorage.instance.getUserId()));
        if (Utility.isAdmin()) {
            params.put("reseller_id", String.valueOf(((HomeActivity) getActivity()).idChooseO));
            params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
        }
        viewVisibilityInProgressState();
        aq.progress(R.id.pg).ajax(Constants.URL_GETORDER_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    listOfOrders = gs.fromJson(object.toString(),
                            new TypeToken<List<Order>>() {
                            }.getType());
                    adapter = new OrderAdapter(getActivity(), listOfOrders);
                    if (listOfOrders.isEmpty()) {
                        aq.id(R.id.tvMessageTitle).text("Daftar Pemesanan Anda Kosong");
                        aq.id(R.id.tvMessage).text("Silakan melakukan pemesanan / klik untuk memuat ulang");
                        viewVisibilityInInfoState();
                    } else {
                        exListView.setAdapter(adapter);
                        viewVisibilityInContentState();
                    }
                } else {
                    viewVisibilityInInfoState();
                }
            }
        });
    }

    private void viewVisibilityInContentState() {
        aq.id(R.id.pg).gone();
        aq.id(R.id.exlv).visible();
        aq.id(R.id.llInfo).gone();
    }

    private void viewVisibilityInProgressState() {
        aq.id(R.id.pg).visible();
        aq.id(R.id.exlv).gone();
        aq.id(R.id.llInfo).gone();
    }

    private void viewVisibilityInInfoState() {
        aq.id(R.id.iv_step).gone();
        aq.id(R.id.pg).gone();
        aq.id(R.id.exlv).gone();
        aq.id(R.id.llInfo).visible();
    }
}
