package com.raihangroup.raihanshop.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.LoginActivity;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.User;
import com.raihangroup.raihanshop.model.UserProfile;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoggedinDrawer extends Fragment {


    @BindView(R.id.homeProfileImage)
    CircleImageView homeProfileImage;
    @BindView(R.id.tvNameHome)
    TextView tvNameHome;
    Unbinder unbinder;
    @BindView(R.id.bLogout)
    Button bLogout;
    private UserProfile profile;
    private User user;
    private AQuery aq;
    private DBHelper dbh;

    public LoggedinDrawer() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        aq = new AQuery(getActivity());
        user = (User) getActivity().getIntent().getSerializableExtra(Constants.INTENT_USER_OBJ);
        if (user == null) {
            // from splash page
            user = new Gson().fromJson(PrefStorage.instance.getUserData(), User.class);
        }
        tvNameHome.setText(user.getFullname());

        String id = String.valueOf(user.getId());
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {

                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    return;
                }

                Gson gs = new Gson();
                profile = gs.fromJson(object.toString(), UserProfile.class);

                String birthDateText = profile.birth_date;
                try {
                    Date birthDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(profile.birth_date);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    birthDateText = sdf.format(birthDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!profile.image.equals("img_default_user.png")) {
                    aq.id(R.id.homeProfileImage).image(Constants.BASE_URL + profile.image);
                }
            }
        });

        dbh = new DBHelper(getContext());



        aq.id(bLogout).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbh.clearMessage();
                PrefStorage.instance.clear();
                startActivity(new Intent(getActivity(), LoginActivity.class));

                getActivity().finish();
                Utility.toast(getActivity(), "Log out");
                 }
            });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_drawer, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
