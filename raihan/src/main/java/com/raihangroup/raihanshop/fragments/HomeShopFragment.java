package com.raihangroup.raihanshop.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductActivity;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.activity.ProfileActivity;
import com.raihangroup.raihanshop.adapter.FrontShopProductsAdapter;
import com.raihangroup.raihanshop.adapter.GridCategoryAdapter;
import com.raihangroup.raihanshop.adapter.ImgVpAdapter;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.ClickableViewPager;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Category;
import com.raihangroup.raihanshop.model.Product;
import com.raihangroup.raihanshop.model.UserProfile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeShopFragment extends Fragment implements PullToRefreshScrollView.OnRefreshListener2{


    private static final String TAG = HomeShopFragment.class.getSimpleName();
    CircleImageView civProfileHome;
    TextView tvUsernameHome;
    TextView tvUserTitleHome;
    TextView tvUserId;
    ClickableViewPager pager;
    CirclePageIndicator indicator;
    RelativeLayout userInfoBanner;
    ArrayList<Product> newestProductList;
    ArrayList<Product> mostStockedProduct;
    ArrayList<Product> mostBoughtProductList;
    ArrayList<Category> categoryList;
    Context context;
    UserProfile profile;
    String userId;

    AQuery aq;
    RecyclerView gridCatagoryList;
    RecyclerView mostStockedList;
    RecyclerView mostBoughtList;
    ImageButton moreProductBtn;
    ImageButton moreProductBtn2;
    PullToRefreshScrollView pullToRefreshScrollView;


    public HomeShopFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_shop, container, false);
        aq.recycle(view);
        PrefStorage.newInstance(getContext());
        civProfileHome = (CircleImageView)view.findViewById(R.id.civ_profile_home);
        tvUsernameHome = (TextView)view.findViewById(R.id.tv_profile_username);
        tvUserTitleHome = (TextView)view.findViewById(R.id.tv_profile_title);
        tvUserId = (TextView)view.findViewById(R.id.tv_profile_id);
        pager = (ClickableViewPager) view.findViewById(R.id.pager);
        userInfoBanner = (RelativeLayout)view.findViewById(R.id.user_info_banner);
        indicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
        gridCatagoryList = (RecyclerView)view.findViewById(R.id.grid_catagory_list);
        mostStockedList = (RecyclerView)view.findViewById(R.id.most_stocked_product);
        mostBoughtList = (RecyclerView)view.findViewById(R.id.most_bought_list);
        moreProductBtn = (ImageButton)view.findViewById(R.id.btnMoreProduct);
        moreProductBtn2 = (ImageButton)view.findViewById(R.id.btnMoreProduct2);
        pullToRefreshScrollView = (PullToRefreshScrollView) view.findViewById(R.id.pull_to_refresh_scrollview);
        pullToRefreshScrollView.setOnRefreshListener(this);
        if(PrefStorage.instance.getUserId()==-1){
            userInfoBanner.setVisibility(View.GONE);
        }
        newestProductList = new ArrayList<>();
        mostBoughtProductList = new ArrayList<>();
        mostStockedProduct = new ArrayList<>();
        userInfoBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putInt("userId",PrefStorage.instance.getUserId());
                Intent profileIntent = new Intent(getContext(),ProfileActivity.class).putExtras(b);
                startActivity(profileIntent);
            }
        });
        userId = String.valueOf(PrefStorage.instance.getUserId());
        return view;
    }
    private void loadUserInfo(String userId) {
        Map<String, String> params = new HashMap<>();
        params.put("id", userId);
        aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    Utility.toast(getContext(), status.getMessage());
                }

                Gson gs = new Gson();
                profile = gs.fromJson(object.toString(), UserProfile.class);

                if (!profile.image.equals("img_default_user.png")) {
                    aq.id(civProfileHome).image(Constants.BASE_URL + profile.image);
                }
                aq.id(tvUsernameHome).text(Utility.toTitleCase(profile.fullname));
                aq.id(tvUserTitleHome).text(profile.level_name);
                aq.id(tvUserId).text(profile.id);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        refreshPage();
    }

    private void refreshPage(){
        mostBoughtProductList.clear();
        mostStockedProduct.clear();
        newestProductList.clear();
        loadUserInfo(userId);
        getViewPager(1,0);
        getCategoryParent();
        getMostStockedProducts(4,0);
        getMostBoughtItems(6,0);
    }
    private void getViewPager(Integer mode, Integer page) {
        Map<String, Integer> params = new HashMap<>();
        params.put("mode", mode);
        params.put("page", page);
        aq.progress(R.id.pg).progress(R.id.pg).ajax(Constants.URL_GET_PRODUCTS_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                super.callback(url, object, status);
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    Toast.makeText(getActivity(), status.getMessage(), Toast.LENGTH_SHORT).show();
                }
                Gson gson = new Gson();
                List<Product> productList;
                productList = gson.fromJson(object.toString(), new TypeToken<List<Product>>() {
                }.getType());
                newestProductList.addAll(productList);
                Log.i(TAG, "callback: " + newestProductList.size());
                pager.setAdapter(new ImgVpAdapter(newestProductList, context));
                pager.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        Product itemProduct = newestProductList.get(position);
                        Intent intent = new Intent(getActivity(),ProductDetailActivity.class);
                        intent.putExtra(Constants.INTENT_PRODUCT_ID,itemProduct.getId());
                        context.startActivity(intent);
                    }
                });
                indicator.setViewPager(pager);
                indicator.onPageSelected(0);
                indicator.setFillColor(context.getResources().getColor(R.color.accent));
                indicator.setSnap(true);

            }
        });
    }

    private void getMostStockedProducts(final Integer mode, Integer page){
        Map<String,Integer> params = new HashMap<>();
        params.put("mode",mode);
        params.put("page",page);
        aq.progress(R.id.pg_product_1).ajax(Constants.URL_GET_PRODUCTS_API,params,JSONArray.class,new AjaxCallback<JSONArray>(){
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                super.callback(url, object, status);
                if(status.getCode()!=Constants.HTTP_RESPONSE_OK){
                    Utility.toast(getActivity(),status.getMessage());
                }
                Gson gson = new Gson();
                List<Product> productList;
                productList = gson.fromJson(object.toString(),new TypeToken<List<Product>>(){}.getType());
                mostStockedProduct.addAll(productList);
                mostStockedList.setAdapter(new FrontShopProductsAdapter(mostStockedProduct,getContext()));
                mostStockedList.setLayoutManager(new LinearLayoutManager(context,LinearLayout.HORIZONTAL,false));
//                mostAffordableList.addItemDecoration(new DividerItemDecoration(context,LinearLayout.HORIZONTAL));
                mostStockedList.setHasFixedSize(true);
                moreProductBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent terbanyakProductIntent = new Intent(getContext(), ProductActivity.class);
                        terbanyakProductIntent.putExtra("mode",ProductActivity.PRODUCT_BY_SORTING_MODE);
                        terbanyakProductIntent.putExtra("sortName","Produk Terbanyak");
                        terbanyakProductIntent.putExtra("sortBy",mode);
                        startActivity(terbanyakProductIntent);
                    }
                });

            }
        });
    }



    private void getCategoryParent(){
        Map<String,String> params = new HashMap<>();
        aq.progress(R.id.pg).ajax(Constants.URL_CATEGORY_PARENT_LIST_API,params,JSONArray.class,new AjaxCallback<JSONArray>(){
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                super.callback(url, object, status);
                if(status.getCode()!=Constants.HTTP_RESPONSE_OK){
                    Utility.toast(context,status.getMessage());
                    Log.i(TAG, "callback status: " + status.getMessage());
                }
                Gson gson = new Gson();
                categoryList = gson.fromJson(object.toString(),new TypeToken<List<Category>>(){}.getType());
                gridCatagoryList.setAdapter(new GridCategoryAdapter(categoryList,context));
                gridCatagoryList.setLayoutManager(new GridLayoutManager(context,3));
            }
        });
    }

    private void getMostBoughtItems(final Integer mode, Integer page){
        final Map<String,Integer> params = new HashMap<>();
        params.put("mode",mode);
        params.put("page",page);

        aq.progress(R.id.pg_product_2).ajax(Constants.URL_GET_PRODUCTS_API,params,JSONArray.class,new AjaxCallback<JSONArray>(){
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                super.callback(url, object, status);
                if(status.getCode()!=Constants.HTTP_RESPONSE_OK){
                    Utility.toast(context,status.getMessage());
                }
                Gson gson = new Gson();
                List<Product> productList;
                productList = gson.fromJson(object.toString(),new TypeToken<List<Product>>(){}.getType());
                mostBoughtProductList.addAll(productList);
                mostBoughtList.setAdapter(new FrontShopProductsAdapter(mostBoughtProductList,context));
                mostBoughtList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayout.HORIZONTAL,false));
//                mostAffordableList.addItemDecoration(new DividerItemDecoration(context,LinearLayout.HORIZONTAL));
                mostBoughtList.setHasFixedSize(true);
                moreProductBtn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent terlarisProductIntent = new Intent(getContext(), ProductActivity.class);
                        terlarisProductIntent.putExtra("mode",ProductActivity.PRODUCT_BY_SORTING_MODE);
                        terlarisProductIntent.putExtra("sortName","Produk Terlaris");
                        terlarisProductIntent.putExtra("sortBy",mode);
                        startActivity(terlarisProductIntent);
                    }
                });
            }
        });

    }

    @Override
    public void onPullDownToRefresh(PullToRefreshBase refreshView) {
        refreshPage();
        refreshView.onRefreshComplete();
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase refreshView) {

    }
}
