package com.raihangroup.raihanshop.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment.Photo;
import com.novoda.imageloader.core.model.ImageTagFactory;

public final class ImgFragment extends Fragment {
    
	private int index;
	private Photo[] photos;
	private ImageTagFactory imageTagFactory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
	        photos = (Photo[]) getArguments().getSerializable("photos");
	        index = getArguments().getInt("index");
		} catch (Exception e) {getActivity().finish();}
        
        imageTagFactory = ImageTagFactory.newInstance(getActivity(), R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ImageView image = new ImageView(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());

        layout.setGravity(Gravity.CENTER);
        layout.addView(image,new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
        		LinearLayout.LayoutParams.MATCH_PARENT));
        image.setScaleType(ScaleType.FIT_CENTER);
        image.setPadding(10, 0, 10, 0);

    	if(photos == null)	return layout;
        
        if(ImageLoaderApplication.getImageLoader().getLoader() != null) { //ambil image pertama
			image.setTag(imageTagFactory.build(photos[index].photo, getActivity()));
			
			ImageLoaderApplication.getImageLoader().getLoader().load(image);
		}
        image.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PhotoViewFragment.show(getActivity(), index, photos);
			}
		});
        return layout;
    }
}
