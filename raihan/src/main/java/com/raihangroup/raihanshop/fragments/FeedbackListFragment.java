package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.TalkToBosActivity;
import com.raihangroup.raihanshop.adapter.FeedbackAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.Feedback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Fragment for feedback list to bos.
 * @author Alh
 *
 */
public class FeedbackListFragment extends Fragment implements OnItemClickListener {

	private Context context;
	private AQuery aq;
	
	private ListView listView;
	
	private final static String TAG = "Feedback To Bos Frg";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
		aq = new AQuery(getActivity());
		
		PrefStorage.newInstance(context);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {	
		View rootView = inflater.inflate(R.layout.fragment_to_bos_feedback_list, container, false);
		aq.recycle(rootView);
		listView = (ListView) rootView.findViewById(R.id.listView);
		return rootView;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        listView.setOnItemClickListener(this);
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshData();
            }
        });
        refreshData();
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
		Feedback feedback = (Feedback) listView.getItemAtPosition(position);
		Log.d(TAG, "selected feedback " + feedback.id + " and " + feedback.subject);
		
		Bundle b = new Bundle();
		b.putString("feedbackId", feedback.id);
		b.putString("subject", feedback.subject);
		b.putInt("type", feedback.type);
		startActivity(new Intent(context, TalkToBosActivity.class).putExtras(b));
	}
	
	// public methods
	
	public void refreshData() {
        aq.id(R.id.llInfo).gone();
		HashMap<String, String> params = new HashMap<String, String>();
        params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
		aq.progress(R.id.pg).ajax(Constants.URL_FEEDBACK_LIST_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
			
			@Override
			public void callback(String url, JSONArray object, AjaxStatus status) {
				if(status.getCode() != Constants.HTTP_RESPONSE_OK) {
					aq.id(R.id.llInfo).visible();
					return;
				}
				
				Gson gs = new Gson();
				ArrayList<Feedback> listOfFeedbacks = gs.fromJson(object.toString(), 
    					new TypeToken<List<Feedback>>(){}.getType());

				FeedbackAdapter adapter = new FeedbackAdapter(context, listOfFeedbacks);
				listView.setAdapter(adapter);
                if(listOfFeedbacks.size()==0){
                    aq.id(R.id.tvMessageTitle).text("Percakapan Kosong");
                    aq.id(R.id.tvMessageContent).text("Silakan memulai percakapan dengan bos");
                    aq.id(R.id.llInfo).visible();
                }
			}
		});
	}
}
