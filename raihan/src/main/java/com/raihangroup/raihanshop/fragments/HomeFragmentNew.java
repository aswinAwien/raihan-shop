package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.PerformanceViewActivity;
import com.raihangroup.raihanshop.adapter.ListNetworkAdapter;
import com.raihangroup.raihanshop.adapter.ListStatusAdapter;
import com.raihangroup.raihanshop.dialogs.AddMemberDialog;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Friend;
import com.raihangroup.raihanshop.model.Person;
import com.raihangroup.raihanshop.model.SellingUser;
import com.raihangroup.raihanshop.model.UserProfile;
import com.raihangroup.raihanshop.model.UserStatus;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.linkedin.android.spyglass.suggestions.SuggestionsResult;
import com.linkedin.android.spyglass.tokenization.QueryToken;
import com.linkedin.android.spyglass.tokenization.interfaces.QueryTokenReceiver;
import com.linkedin.android.spyglass.ui.RichEditorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ocit on 8/25/15.
 */
public class HomeFragmentNew extends Fragment {

    private Context context;
    private AQuery aq;
    private String userId;
    public static UserProfile profile;

    private final static String TAG = "View Home";
//    private ListView lvStatusView;
    private ArrayList<UserStatus> data;
    private ListView right_drawer;
    private List<Person> dataFriends;
    private Friend.FriendLoader arrMention;
//    private RichEditorView etUpdateStatus;
    private static final String BUCKET = "Members";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());
        Log.d("debug", "home fragment created");
        PrefStorage.newInstance(context);
//        getStatusProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_view_profile, container, false);
        Fresco.initialize(getActivity());

        aq.recycle(rootView);
        userId = String.valueOf(PrefStorage.instance.getUserId());

        Log.i(TAG, "user id " + userId);

        final View actionB = rootView.findViewById(R.id.action_b);
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager fragmentManager = getActivity().getFragmentManager();
                AddMemberDialog addMemberDialog = AddMemberDialog.newIntance("Tambahkan Member",context);
                addMemberDialog.show(fragmentManager,"fragment_edit_name");
            }
        });

        final View actionA = rootView.findViewById(R.id.action_a);
        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.toast(getContext(),"My Wallet");
            }
        });
        final View actionC = rootView.findViewById(R.id.action_c);
        actionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, PerformanceViewActivity.class));
            }
        });

        // load profile info
        refreshData();
//        getStatusProfile();
        setSelling();
//        getFriends();
        getFriends();

//
//        etUpdateStatus = (RichEditorView) rootView.findViewById(R.id.etUpdateStatus);
//        etUpdateStatus.setQueryTokenReceiver(this);
//        etUpdateStatus.setHint("Mention A name");

//        lvStatusView = (ListView) rootView.findViewById(R.id.lvStatusView);
//        aq.id(R.id.btnMorePerformProfile).clicked(new MorePerformClickListener());

        //Right drawer ListNetwork view
        right_drawer = (ListView) rootView.findViewById(R.id.right_drawer);
        rightDrawer();

        return rootView;
    }

    private void rightDrawer() {
        ListNetworkAdapter networkAdapter = new ListNetworkAdapter(getActivity());
        right_drawer.setAdapter(networkAdapter);
    }

    // handle view visibility
    private void viewVisibilityInProgressState() {
        aq.id(R.id.pg).visible();
        aq.id(R.id.content).gone();
        aq.id(R.id.llInfo).gone();
    }

    private void viewVisibilityInInfoState() {
        aq.id(R.id.pg).gone();
        aq.id(R.id.content).gone();
        aq.id(R.id.llInfo).visible();
    }

    private void viewVisibilityInContentState() {
        aq.id(R.id.pg).gone();
        aq.id(R.id.content).visible();
        aq.id(R.id.llInfo).gone();
    }

    //refresh and get data profile
    public void refreshData() {
        viewVisibilityInProgressState();
        String id = String.valueOf(userId);
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        aq.ajax(Constants.URL_USER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    viewVisibilityInInfoState();
                    return;
                }

                viewVisibilityInContentState();

                Gson gs = new Gson();
                profile = gs.fromJson(object.toString(), UserProfile.class);

                String birthDateText = profile.birth_date;
                try {
                    Date birthDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(profile.birth_date);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    birthDateText = sdf.format(birthDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!profile.image.equals("img_default_user.png")) {
                    aq.id(R.id.detProfileImage).image(Constants.BASE_URL + profile.image);
                }

                aq.id(R.id.tvFullName).text(Utility.toTitleCase(profile.fullname));
                aq.id(R.id.tvAddress).text(profile.address + ", " + profile.city_name);
                aq.id(R.id.tvLevel).text(profile.level_name);
                aq.id(R.id.txIdProfile).text(profile.id);
            }
        });
    }

    //Function update status  with parameter EditText
//    public void updateStatus(RichEditorView editText) {
//        String id = String.valueOf(userId);
//        String content = editText.getText().toString();
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String strDate = sdf.format(c.getTime());
//
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("id_creator", id);
//        params.put("isi_status", content);
//        params.put("created", strDate);
//        aq.ajax(Constants.URL_UPDATE_STATUS, params, JSONArray.class, new AjaxCallback<JSONArray>() {
//            @Override
//            public void callback(String url, JSONArray object, AjaxStatus status) {
//                getStatusProfile();
//            }
//        });
//
//    }

    //Fuction for mention a people
//    @Override
//    public List<String> onQueryReceived(final @NonNull QueryToken queryToken) {
//        List<String> buckets = Collections.singletonList(BUCKET);
//        List<Friend> suggestions = arrMention.getSuggestions(queryToken);
//        SuggestionsResult result = new SuggestionsResult(queryToken, suggestions);
//        etUpdateStatus.onReceiveSuggestionsResult(result, BUCKET);
//        return buckets;
//    }

    //set selling user
    public void setSelling() {
        String id = String.valueOf(userId);
        Map<String, String> field = new HashMap<String, String>();
        field.put("id", id);
        aq.ajax(Constants.URL_SELLING_USER, field, JSONObject.class, new AjaxCallback<JSONObject>() {
            SellingUser sellingUser;

            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Gson parsing = new Gson();
                sellingUser = parsing.fromJson(object.toString(), SellingUser.class);
                aq.id(R.id.tvPSendiri).text(sellingUser.getPs());
                aq.id(R.id.tvPTim).text(sellingUser.getPt());
                aq.id(R.id.tvOmset).text(sellingUser.getOmset());
                aq.id(R.id.tvKomisi).text(sellingUser.getComission());
            }
        });
    }

    // get status and display in listview
//    public void getStatusProfile() {
//        String id = String.valueOf(userId);
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("id_creator", id);
//        aq.ajax(Constants.URL_USER_PROFILE_STATUS_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
//            @Override
//            public void callback(String url, JSONArray object, AjaxStatus status) {
//
//                Gson gson = new Gson();
//                Type collectionType = new TypeToken<ArrayList<UserStatus>>() {
//                }.getType();
//                data = gson.fromJson(object.toString(), collectionType);
//
//                ListStatusAdapter adapter = new ListStatusAdapter(getActivity(), data, aq);
//                lvStatusView.setAdapter(adapter);
//                setListViewHeightBasedOnChildren(lvStatusView);
//
//            }
//        });
//    }


    //get status
    public void getFriends() {
        String id = String.valueOf(userId);
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        aq.ajax(Constants.URL_GET_FRIENDS, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                Gson gson = new Gson();
                Type collectionType = new TypeToken<ArrayList<Person>>() {
                }.getType();
                dataFriends = gson.fromJson(object.toString(), collectionType);
                System.out.println(object.toString());
//                returnJSONArray(data);
                arrMention = new Friend.FriendLoader(dataFriends);
            }
        });
    }

    // Set height of listview match with scrollview
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}