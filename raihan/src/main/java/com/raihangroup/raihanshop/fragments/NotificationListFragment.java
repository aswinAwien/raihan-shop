package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ChattingActivity;
import com.raihangroup.raihanshop.activity.HomeActivity;
import com.raihangroup.raihanshop.activity.InboxActivity;
import com.raihangroup.raihanshop.activity.MainActivity;
import com.raihangroup.raihanshop.activity.PerformanceChartActivity;
import com.raihangroup.raihanshop.activity.PerformanceViewActivity;
import com.raihangroup.raihanshop.activity.ProfileActivity;
import com.raihangroup.raihanshop.activity.ViewProfileActivity;
import com.raihangroup.raihanshop.adapter.NotificationAdapter;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.Notification;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mohamad on 2/3/2015.
 */
public class NotificationListFragment extends Fragment {

    private Context context;
    private AQuery aq;
    private ListView listView;
    ArrayList<Notification> notifications;
    private final static String TAG = "Notification List Frg";
    private NotificationAdapter notificationAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());
        PrefStorage.newInstance(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notification_list, container, false);
        aq.recycle(rootView);
        listView = (ListView) aq.id(R.id.listNotification).getView();
        aq.id(R.id.llInfo).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNotification(true);

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Notification notification = (Notification) parent.getItemAtPosition(position);
                switch (Integer.parseInt(notification.type)) {
                    case Constants.NOTIFICATION_AFTER_REGISTER:
                        context.startActivity(new Intent(context,ProfileActivity.class));
                        break;
                    case Constants.NOTIFICATION_BROADCAST_INBOX:
                        context.startActivity(new Intent(context, ChattingActivity.class));
                        break;
                    case Constants.NOTIFICATION_PERFORMANCE:
                        context.startActivity(new Intent(context, PerformanceChartActivity.class));
                        break;
                    case Constants.NOTIFICATION_ORDER:
                        ((MainActivity) getActivity()).switchToOrderFragment();
                        break;
                    case Constants.NOTIFICATION_FEE:
                        context.startActivity(new Intent(context, PerformanceChartActivity.class));
                        break;
                    case Constants.NOTIFICATION_MEMBER:
                        Bundle data = new Bundle();
                        data.putInt("userId", notification.id_member);
                        data.putBoolean("editable", false);
                        Intent i = new Intent(context, ProfileActivity.class).putExtras(data);
                        startActivity(i);
                        break;
                    case Constants.NOTIFICATION_FEEDBACK_REPLY:
                        ((MainActivity) getActivity()).switchToFeedbackFragment();
                        break;
                    case Constants.NOTIFICATION_BIRTHDAY:
                        Bundle b = new Bundle();
                        b.putInt("userId", notification.id_member);
                        b.putBoolean("editable", false);
                        Intent profile = new Intent(context, ProfileActivity.class).putExtras(b);
                        startActivity(profile);
                        break;
                    case Constants.NOTIFICATION_VOLUME_REQUIRED:
                        context.startActivity(new Intent(context,ProfileActivity.class));
                        break;
                    case Constants.NOTIFICATION_RETUR_ORDER:
                        ((MainActivity) getActivity()).switchToOrderFragment();
                        break;
                    case Constants.NOTIFICATION_KOMISI:
                        context.startActivity(new Intent(context, PerformanceChartActivity.class));
                        break;
                    case Constants.NOTIFICATION_GET_FEE:
                        context.startActivity(new Intent(context, PerformanceChartActivity.class));
                        break;
                    default:
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (notifications == null) {
            getNotification(true);
        } else {
            getNotification(false);
        }
    }

    public void getNotification(boolean loading) {
        notifications = new ArrayList<>();
        aq.id(R.id.llInfo).gone();
        HashMap<String, Object> params = new HashMap<>();
        params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));
        if (loading) {
            aq.progress(R.id.pg);
        }
        aq.progress(R.id.pg).ajax(Constants.URL_GET_NOTIFICATION, params, JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray array, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    Gson gs = new Gson();
                    ArrayList<Notification> list = gs.fromJson(array.toString(),
                            new TypeToken<List<Notification>>() {
                            }.getType());
                    Log.d("debug", array.toString());
                    if (list.isEmpty()) {
                        aq.id(R.id.tvMessageTitle).text("Tidak Ada Notifikasi");
                        aq.id(R.id.tvMessageContent).text("Anda tidak memiliki notifikasi baru");
                        aq.id(R.id.llInfo).visible();
                    } else {
                        notificationAdapter = new NotificationAdapter(context, list);
                        listView.setAdapter(notificationAdapter);
                        params.put("type", 3);
                        aq.ajax(Constants.URL_READ_NOTIFICATION, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                                    try {
                                        if (object.getString("Success").equals("true"))
                                            Log.d(TAG, "mark as read");
                                    } catch (Exception e) {
                                        Log.d("exception", String.valueOf(e));
                                    }
                                }
                            }
                        });
                    }
                } else {
                    aq.id(R.id.llInfo).visible();
                    DialogHelper.showMessageDialog(context, "Terjadi Kesalahan", "Tidak berhasil mendapatkan notifikasi",
                            "OK", null);
                }
            }
        });
    }
}
