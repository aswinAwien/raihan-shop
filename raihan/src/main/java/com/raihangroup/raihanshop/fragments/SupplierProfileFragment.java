package com.raihangroup.raihanshop.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.SupplierPerformance;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Fragment for supplier info.
 *
 * @author Alh
 */
public class SupplierProfileFragment extends Fragment {

    private Context context;
    private AQuery aq;

    private final static String TAG = "Supplier Profile Frg";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        aq = new AQuery(getActivity());

        PrefStorage.newInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_supplier_info, container, false);

        aq.recycle(rootView);
        // set message here
        aq.id(R.id.tvMessageTitle).text("Tidak Ada Profil Tenant");
        aq.id(R.id.tvMessageContent).text("Silakan mencoba kembali");

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        aq.id(R.id.pg).visible();
        aq.id(R.id.content).gone();
        aq.id(R.id.llInfo).gone();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", getArguments().getString("supplierId"));
        aq.progress(R.id.pg).ajax(Constants.URL_SUPPLIER_PERFORMANCE_API, params, String.class, new AjaxCallback<String>() {

            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                    aq.id(R.id.llInfo).visible();
                    return;
                }
                Gson gson = new Gson();
                final SupplierPerformance sPerf = gson.fromJson(object, SupplierPerformance.class);
                Log.d("sperf", "" + sPerf.good_product);
                double ontime = 0;
                double successRatio = 0;
                if (sPerf.total_order > 0) {
                    ontime = sPerf.ontime_delivery / sPerf.total_order;
                    successRatio = sPerf.order_success / sPerf.total_order;
                }
                aq.id(R.id.tvOnTime).text(String.format("%.0f", ontime * 100) + "%");
                aq.id(R.id.tvOrderSuccess).text(String.format("%.0f", successRatio * 100) + "%");
                aq.id(R.id.tvDefectOrder).text(String.format("%.0f", sPerf.broken_retur) + " pcs");
                aq.id(R.id.tvWrongOrder).text(String.format("%.0f", sPerf.wrong_retur) + " pcs");
                aq.id(R.id.tvTotalOrder).text("dari total " + String.format("%.0f", sPerf.total_order) + " order");
                aq.id(R.id.tvTotalOrder2).text("dari total " + String.format("%.0f", sPerf.total_order) + " order");
                aq.progress(R.id.pg).ajax(Constants.URL_SUPPLIER_PROFILE_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        if (status.getCode() != Constants.HTTP_RESPONSE_OK) {
                            aq.id(R.id.llInfo).visible();

                            return;
                        }
                        aq.id(R.id.content).visible();
                        try {
                            aq.id(R.id.tvShopName).text(object.getString("fullname"));
                            aq.id(R.id.tvLocation).text(object.getString("location"));
                            aq.id(R.id.tvLastActive).text(object.getString("last_updated"));
                            aq.id(R.id.tvActiveSince).text(object.getString("created"));

                            // set quality service
                            double quality = object.getDouble("quality");
                            if (quality != 0) {
                                aq.id(R.id.tvQuality).text("Kualitas (" + String.format("%.2f", quality) + "/5.0)");
                                ((RatingBar) aq.id(R.id.ratingBarQuality).getView())
                                        .setRating((float) quality);
                            } else {
                                aq.id(R.id.tvQuality).text("Kualitas (Belum Ada Rating)");
                            }
                            // set rating service
//                            double service = object.getDouble("service");
                            double service = 0;
                            if (sPerf.total_order > 0) {
                                // service quality formula 5(stars) * (ontime + good product + (total order - total retur))/3 total order
                                service = 5* (sPerf.ontime_delivery + sPerf.good_product + (sPerf.total_order - (sPerf.broken_retur + sPerf.wrong_retur))) / (3 * sPerf.total_order);
                            }

                            if (service != 0) {
                                aq.id(R.id.tvService).text("Pelayanan (" + String.format("%.2f", service) + "/5.0)");
                                ((RatingBar) aq.id(R.id.ratingBarService).getView())
                                        .setRating((float) service);
                            } else {
                                aq.id(R.id.tvService).text("Pelayanan (Belum Ada Rating)");
                            }
                        } catch (JSONException e) {
                            Log.d("exception", String.valueOf(e));
                        }
                    }
                });
            }
        });
    }
}
