package com.raihangroup.raihanshop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListMap<K,V> extends HashMap<K, List<V>> {
	private static final long serialVersionUID = 6429003011447590868L;
	
	public void add(K key, V value){
		List<V> l = get(key);
		if(l != null){
			l.add(value);
		} else {
			l = new ArrayList<V>();
			l.add(value);
			put(key, l);
		}
	}
}
