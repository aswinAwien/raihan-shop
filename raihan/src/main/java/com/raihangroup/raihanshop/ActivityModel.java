package com.raihangroup.raihanshop;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.view.View;

public class ActivityModel extends ModelChannel {
	private static final long serialVersionUID = 5997140276118341457L;
	public transient ResolveInfo app;
	public transient Context ctx;

	public ActivityModel(Context ctx, ResolveInfo app) {
		this.app = app;		
		this.ctx = ctx;
	}
	
	@Override
	public boolean getData(View v, int position) {
		super.getData(v, position);
		return false;
	}

	@Override
	public CharSequence getTitle() {
		return app.loadLabel(ctx.getPackageManager());
	}
	
	public Drawable geticon(){
		return app.loadIcon(ctx.getPackageManager());
	}
	
	@Override
	public int getLayout() {
		return R.layout.itemlist;
	}
}
