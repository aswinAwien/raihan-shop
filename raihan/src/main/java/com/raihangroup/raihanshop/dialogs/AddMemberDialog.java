package com.raihangroup.raihanshop.dialogs;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddMemberDialog extends DialogFragment {

    private static Context mContext;
    public AddMemberDialog() {
    }

    public static AddMemberDialog newIntance(String title,Context context) {
        AddMemberDialog addMemberDialog = new AddMemberDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        addMemberDialog.setArguments(args);
        mContext = context;
        return addMemberDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_add_member, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title = getArguments().getString("title","Daftarkan member");
        getDialog().setTitle(title);
        TextView textView = view.findViewById(R.id.add_member_title_dialog);
        textView.setText(title);
        TextInputEditText etMemberName = view.findViewById(R.id.et_memberName);
        TextInputEditText etMemberEmail = view.findViewById(R.id.et_memberEmail);
        TextInputEditText etMemberPassword = view.findViewById(R.id.et_memberPasword);
        Button negativeBtn = view.findViewById(R.id.negative_btn);
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        Button positiveBtn = view.findViewById(R.id.positive_btn);
        etMemberName.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

}
