package com.raihangroup.raihanshop.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.github.mikephil.charting.charts.PieChart;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChartPerformanceAdapter extends RecyclerView.Adapter<ChartPerformanceAdapter.ViewHolder> {


    private String[] titles;
    private String[] subtitles;
    private String[] komisiList;

    public ChartPerformanceAdapter(String[] titles, String[] subtitles, String[] komisiList) {
        this.titles = titles;
        this.subtitles = subtitles;
        this.komisiList = komisiList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_chart_performance, null, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String title = titles[position];
        String subtitle = subtitles[position];
        String komisi = komisiList[position];

        holder.tvLevelName.setText(title);
        holder.tvSubtitle.setText(subtitle);
        holder.tvKomisi.setText(komisi);
    }

    @Override
    public int getItemCount() {
            return titles.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_level_name)
        TextView tvLevelName;
        @BindView(R.id.tv_subtitle)
        TextView tvSubtitle;
        @BindView(R.id.piePSchart)
        PieChart piePSchart;
        @BindView(R.id.piePTchart)
        PieChart piePTchart;
        @BindView(R.id.tv_komisi)
        TextView tvKomisi;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
