package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Category;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private List<Category> listOfCategory;
    private Context context;

    public CategoryListAdapter(List<Category> listOfCategory, Context context) {
        this.listOfCategory = listOfCategory;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist_category, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Category category = listOfCategory.get(position);
        holder.tvCategory.setText(category.name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("mode", ProductActivity.PRODUCT_BY_CATEGORY_MODE);
                intent.putExtra("categoryName",category.name);
                intent.putExtra("categoryId",category.id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOfCategory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCategory)
        TextView tvCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
