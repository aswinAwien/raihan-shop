package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.helper.IntegerObj;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.SupplierShipCalc;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.CartItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * helper untuk autocomplete kota dari provinsi
 *
 * @author badr
 */
public class KecaCityAdapter extends CursorAdapter implements OnItemClickListener {

    Context cotx;
    Cursor cu;
    private EditText et;
    private TextView tvTotal;
    private DBHelper dbh;
    private LinearLayout llLoad;
    private AQuery aq;
    private int mode = 0;
    private int currentPrice[];
    public IntegerObj basePrice;
    private int positionOrder;
    private TextView tvShipPrice;
    private ProgressBar pb;
    private String cityS;
    private String provS;
    //private List<LocationWeight> locW;
    private HashMap<String, SupplierShipCalc> hmOfSupplierShippingCalcFromSupplierId;
    private String idChooseS;

    public static final int MODE_PRODUCT_DETAIL = 1;
    public static final int MODE_BOOK_DATA = 2;

    public KecaCityAdapter(Context context, Cursor c, EditText et) {
        super(context, c);
        this.cotx = context;
        this.cu = c;
        this.et = et;
        dbh = new DBHelper(context);

        aq = new AQuery(context);
        pb = new ProgressBar(mContext, null, android.R.attr.progressBarStyleSmall);
        pb.setId(99);//id
    }

//	public KecaCityAdapter(Context context, Cursor c, EditText et, 
//			IntegerObj baseprice, View vLoad, TextView tvTot) {
//		this(context, c, et);
//		this.mode  = MODE_PRODUCT_DETAIL;
//		this.basePrice = baseprice;
//		llLoad = (LinearLayout) vLoad;
//		this.tvTotal = tvTot;
//	}

    public KecaCityAdapter(Context context, Cursor c, EditText et, List<CartItem> listOfCartItems,
                           View vLoad, TextView tvTot, TextView tvShipPrice) {
        this(context, c, et);
        this.mode = MODE_BOOK_DATA;

        hmOfSupplierShippingCalcFromSupplierId = new HashMap<String, SupplierShipCalc>();

        for (CartItem ci : listOfCartItems) {
            if (hmOfSupplierShippingCalcFromSupplierId.containsKey(ci.id_supplier)) {
                // update data in hash map
                SupplierShipCalc sscFromHm = hmOfSupplierShippingCalcFromSupplierId.get(ci.id_supplier);
                sscFromHm.addCartItemWeight(ci);
            } else {
                // add new data to hash map
                SupplierShipCalc ssc = new SupplierShipCalc(ci.id_supplier);
                ssc.addCartItemWeight(ci);

                hmOfSupplierShippingCalcFromSupplierId.put(ci.id_supplier, ssc);
            }
        }

        llLoad = (LinearLayout) vLoad;
        this.tvTotal = tvTot;
        this.tvShipPrice = tvShipPrice;
    }

    public KecaCityAdapter(Context context, Cursor c, EditText et, List<CartItem> listOfCartItems,
                           View vLoad, TextView tvTot, TextView tvShipPrice, String idChooseS) {
        this(context, c, et, listOfCartItems, vLoad, tvTot, tvShipPrice);
        this.idChooseS = idChooseS;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final TextView view = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        String item = createItem(cursor); //ambil judul
        view.setText(item);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String item = createItem(cursor);
        ((TextView) view).setText(item);

    }

    private String createItem(Cursor cursor) {
        return cursor.getString(1);  //title
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        Cursor currentCursor = null;
        if (getFilterQueryProvider() != null) {
            return getFilterQueryProvider().runQuery(constraint);
        }
        String args = "";
        if (constraint != null) {
            args = constraint.toString();
        }

        currentCursor = dbh.findKecaByCityProv(provS, cityS, args); //setiap ngetik, jalanin query
        return currentCursor;
    }

    @Override
    public void onItemClick(AdapterView<?> listView, View view,
                            int position, long arg3) {
        // set edittext kalau udah diklik, dan set kecamatan untuk nanti di kelurahan
        Cursor cursor = (Cursor) listView.getItemAtPosition(position);
        String title = cursor.getString(cursor.getColumnIndexOrThrow("keca"));
        et.setText(title);

        llLoad.removeAllViews();
        llLoad.addView(pb, 30, 30);

//		if (mode == MODE_PRODUCT_DETAIL) {
//			aq.ajax(Constants.URL_SHIPPING_API, JSONArray.class, new AjaxCallback<JSONArray>(){
//				public void callback(String url, JSONArray object_, com.androidquery.callback.AjaxStatus status) {
//					
//					JSONArray object =null;
//					try {
//						object = new JSONArray("[{\"shipping_id\":\"1\",\"text\":\"JNE Regular (1-2 Hari Kerja)\",\"price\":\"10000\"},{\"shipping_id\":\"2\",\"text\":\"JNEKilat\",\"price\":\"17000\"}]");
//					} catch (JSONException e1) {
//						e1.printStackTrace();
//					}
//					llLoad.removeAllViews();
//					RadioGroup rg = new RadioGroup(mContext);
//					for (int i = 0; i < object.length(); i++) {
//						try {
//						JSONObject jo = object.getJSONObject(i);
//						RadioButton rb = new RadioButton(mContext);
//						rb.setText(jo.getString("text") + " | "+Utility.formatPrice(jo.getInt("price")));
//						rb.setId(jo.getInt("shipping_id"));
//						TagKeca tc = new TagKeca();
//						tc.price = jo.getInt("price");
//						tc.zone = jo.getString("zone");
//						rb.setTag((TagKeca) tc);
//						
//						rg.addView(rb);
//						} catch (JSONException e) {e.printStackTrace();}
//					}
//					rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//						@Override
//						public void onCheckedChanged(RadioGroup group, int checkedId) {
////							int priceNow = Utility.deformatPrice(tvTotal.getText().toString());
//							RadioButton rb_c = (RadioButton) group.findViewById(checkedId);
//							TagKeca tc = (TagKeca) rb_c.getTag();
//							int priceSip = tc.price;
//							
//							priceSip = priceSip * (weight < 1200 ? 1 : (int) Math.ceil((weight - 200)/1000));
//							
//							// kalau sudah pernah pilih price, maka kurangi
//							if (currentPrice != -1) {
//								basePrice.red(currentPrice);
//							}
//							
//							currentPrice = priceSip;
//							basePrice.add(priceSip);
//							
//							tvTotal.setText(Utility.formatPrice(basePrice.getO()));
//						}
//					});
//					llLoad.addView(rg);
//				};
//			});
//		} else 
        if (mode == MODE_BOOK_DATA) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("sub_district", title);
            params.put("city", cityS);
            params.put("province", provS);
            params.put("member_id", String.valueOf(PrefStorage.instance.getUserId()));

            if (!Utility.isNullOrBlank(idChooseS)) {
                params.put("id_admin", String.valueOf(PrefStorage.instance.getUserId()));
                params.put("member_id", idChooseS);
            }

            aq.progress(pb).ajax(Constants.URL_SHIPPING_API, params, JSONArray.class, new AjaxCallback<JSONArray>() {
                public void callback(String url, JSONArray object, com.androidquery.callback.AjaxStatus status) {
                    if (status.getCode() == Constants.HTTP_RESPONSE_INTERNALSERVERERROR) return;

                    currentPrice = new int[hmOfSupplierShippingCalcFromSupplierId.size()];
                    for (int i = 0; i < currentPrice.length; i++) {
                        currentPrice[i] = -1;
                    }

                    for (int g = 0; g < object.length(); g++) { //loop sebanyak kombinasi lokasi
                        JSONObject jo = null;

                        String loc = null;
                        String supplierName = null;
                        String supplierId = null;
                        try {
                            jo = object.getJSONObject(g);
                            loc = jo.getString("location");
                            supplierName = jo.isNull("fullname") ? null : jo.getString("fullname");
                            supplierId = jo.getString("supplier_id");
                        } catch (JSONException e1) {
                            Log.d("exception", String.valueOf(e1));
                        }

                        // find appropriate supplier shipping calculation
                        SupplierShipCalc appropriateSsc = null;
                        Set<String> keySet = hmOfSupplierShippingCalcFromSupplierId.keySet();
                        for (String key : keySet) {
                            SupplierShipCalc ssc = hmOfSupplierShippingCalcFromSupplierId.get(key);

                            if (ssc.getSupplierId().equalsIgnoreCase(supplierId)) {
                                appropriateSsc = ssc;
                                break;
                            }
                        }

                        final int g_f = g;

                        LinearLayout llTmp = new LinearLayout(mContext);
                        llTmp.setBackgroundResource(R.drawable.listborder);
                        llTmp.setOrientation(LinearLayout.VERTICAL);

                        TextView tv = new TextView(mContext);
                        tv.setPadding(10, 10, 10, 10);
                        tv.setBackgroundColor(mContext.getResources().getColor(R.color.gray));
                        String addword = "";
                        if (supplierName != null) {
                            addword = " (" + supplierName + ")";
                        }
                        tv.setText("Lokasi barang di " + loc + addword);

                        llTmp.addView(tv);

                        RadioGroup rg = new RadioGroup(mContext);

                        try {
                            for (int i = 0; i < jo.getJSONArray("shipping").length(); i++) {
                                JSONObject joShip = jo.getJSONArray("shipping").getJSONObject(i);
                                RadioButton rb = new RadioButton(mContext);
                                if (joShip.getString("services").equalsIgnoreCase("POS")) {
                                    rb.setText(joShip.getString("services") + " (" +
                                            "Ongkos kirim tanyakan pada CS)");
                                } else {
                                    try {
                                        rb.setText(joShip.getString("services") + " (" +
                                                joShip.getString("etd")
                                                + " hari) | " + Utility.formatPrice(joShip.getInt("price")));
                                    } catch (JSONException e) {
                                        continue;
                                    }
                                }

                                rb.setId(joShip.getInt("id"));

                                TagKeca tc = new TagKeca();
                                tc.price = joShip.getInt("price");
                                tc.low = appropriateSsc;

                                rb.setTag(tc);

                                rg.addView(rb);
                            }
                        } catch (JSONException e) {
                            Log.d("exception", String.valueOf(e));
                        }
                        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group,
                                                         int checkedId) {
                                int totalPrice = Utility.deformatPrice(tvTotal.getText().toString());
                                int totalPriceShipping = Utility.deformatPrice(tvShipPrice.getText().toString());

                                RadioButton rb_c = (RadioButton) group.findViewById(checkedId);

                                TagKeca tc = (TagKeca) rb_c.getTag();
                                int priceSip = tc.price;

//								System.out.println(priceSip+" kali "+(weight < 1200 ? 1 : (int) Math.ceil((float)(weight - 200)/1000)));
                                //LocationWeight lw = tc.low;
                                SupplierShipCalc appropriateSsc = tc.low;
                                priceSip = priceSip *
                                        (appropriateSsc.getTotalWeightInGr() < 1200 ? 1
                                                : (int) Math.ceil((float) (appropriateSsc.getTotalWeightInGr() - 200) / 1000));

                                if (currentPrice[g_f] != -1) {
                                    totalPrice -= currentPrice[g_f];
                                    totalPriceShipping -= currentPrice[g_f];
                                }

                                currentPrice[g_f] = priceSip;

                                totalPrice += currentPrice[g_f];
                                totalPriceShipping += currentPrice[g_f];

                                tvTotal.setText(Utility.formatPrice(totalPrice));
                                tvShipPrice.setText(Utility.formatPrice(totalPriceShipping));
                            }

                        });
                        llTmp.addView(rg);

                        llLoad.addView(llTmp);
                        aq.id(llTmp).margin(0, 10, 0, 0);

                    } // end of loop
                }
            });
        }

        clearAutoCompleteSoftInput();
    }

    private void clearAutoCompleteSoftInput() {
        // dismiss the keyboard
        InputMethodManager imm = (InputMethodManager) cotx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        // clear focus
        et.clearFocus();
    }

    public void setProvince(String prov) {
        this.provS = prov;
    }

    public void setCity(String city) {
        this.cityS = city;
    }


    public static class TagKeca {
        public int price;
        public SupplierShipCalc low;
    }

	/*
    public static class LocationWeight {
		public String location;
		public int tWeight;
		public int cost;
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof String) {
				return (((String)o).equalsIgnoreCase(this.location));	
			}
			return (((LocationWeight) o).location).equalsIgnoreCase(this.location);
		}
		
		@Override
		public int hashCode() {
			return location.hashCode();
		}
	}*/
}
