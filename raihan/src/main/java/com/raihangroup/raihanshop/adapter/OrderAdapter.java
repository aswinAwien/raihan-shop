package com.raihangroup.raihanshop.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.HomeActivity;
import com.raihangroup.raihanshop.activity.ReviewWithRatingActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.Helper;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.CartItem;
import com.raihangroup.raihanshop.model.Order;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.List;

public class OrderAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Order> listOrd;
    private ImageTagFactory imageTagFactory;
    private ProgressDialog progDialog;

    private final static String TAG = "Order List Adapter";
    private Order openedord;

    public OrderAdapter(Context context, List<Order> listOrd) {
        this.context = context;
        this.listOrd = listOrd;

        progDialog = new ProgressDialog(context);
        progDialog.setTitle("Processing");
        progDialog.setMessage("Please wait a moment");

        // selama image belum keload, load gambar default
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.headerlist_exlv_order_status, null);
        }

        openedord = (Order) getGroup(groupPosition);
        int confirmed = 0;
        for (CartItem prod : openedord.getProducts()) {
            if (!prod.is_rated.equals(Constants.STATUS_UNCONFIRMED)) {
                confirmed++;
            }
        }
        int totalOrder = openedord.getProducts().size();

        if (openedord.getStatus().equals("Sudah Terkirim")) {
            convertView.findViewById(R.id.tvConfirmStatus).setVisibility(View.VISIBLE);
            if (confirmed < totalOrder) {
                convertView.findViewById(R.id.headerContainer).setBackgroundResource(R.drawable.listborderpink);
            } else {
                convertView.findViewById(R.id.headerContainer).setBackgroundResource(R.drawable.listborder);
            }
        } else {
            convertView.findViewById(R.id.tvConfirmStatus).setVisibility(View.GONE);
            convertView.findViewById(R.id.headerContainer).setBackgroundResource(R.drawable.listborder);
        }
        ((TextView) convertView.findViewById(R.id.tvTitle)).setText(openedord.getOrder_id());
        ((TextView) convertView.findViewById(R.id.tvSubTitle)).setText(openedord.getStatus());
        ((TextView) convertView.findViewById(R.id.tvConfirmStatus)).setText("(" + confirmed + "/" + totalOrder + ") telah dikonfirmasi");
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        final CartItem ci = (CartItem) getChild(groupPosition, childPosition);

        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (row == null) {
            holder = new ViewHolder();
            row = infalInflater.inflate(R.layout.itemlist_order_status, null);

            holder.tvOPemesan = (TextView) row.findViewById(R.id.tvOPesan);
            holder.tvOJudul = ((TextView) row.findViewById(R.id.tvOJudul));
            holder.tvOWarna = ((TextView) row.findViewById(R.id.tvOWarna));
            holder.tvOUkuran = ((TextView) row.findViewById(R.id.tvOUkuran));
            holder.tvOJumlah = ((TextView) row.findViewById(R.id.tvOJumlah));
            holder.tvOTotHar = ((TextView) row.findViewById(R.id.tvOTotHar));
            holder.imageProduct = (ImageView) row.findViewById(R.id.imageProduct);
            holder.tvOResi = ((TextView) row.findViewById(R.id.tvOResi));
            holder.tvCopyResi = (TextView) row.findViewById(R.id.tvCopyResi);
            holder.tvStatus = (TextView) row.findViewById(R.id.tvStatus);
            holder.btnConfirmation = (Button) row.findViewById(R.id.buttonConfirmation);
            holder.tvStep = (TextView) row.findViewById(R.id.tvStep);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        holder.tvOJudul.setText(ci.getName());
        holder.tvOWarna.setText(ci.getColor());
        holder.tvOUkuran.setText(ci.getSize());
        holder.tvOJumlah.setText(String.valueOf(ci.getQty()));
        holder.tvStatus.setVisibility(View.GONE);
        holder.tvStep.setVisibility(View.GONE);
        holder.tvOTotHar.setText(Utility.formatPrice(ci.getQty() * ci.getPrice()));
        final Order order = (Order) getGroup(groupPosition);
        holder.tvOPemesan.setText(order.getCustomer());

        final String resi = ci.getResi();
        if (Utility.isNullOrBlank(resi)) {
            holder.tvOResi.setText("-");
        } else {
            holder.tvOResi.setText(resi);
        }
        // kalau sudah dikirim tapi belum dirating
        // status 1 = sudah dikirim
        // rated 0 = belum di rating
        //
        if (ci.status.equals("1") && ci.is_rated.equals("0") && (!ci.is_confirmed.equals("2"))) {
            holder.tvStep.setVisibility(View.VISIBLE);
            holder.btnConfirmation.setVisibility(View.VISIBLE);
        } else {
            // retur
            if (ci.is_confirmed.equals("2")) {
                holder.tvStatus.setVisibility(View.VISIBLE);

            }
            holder.tvStep.setVisibility(View.GONE);
            holder.btnConfirmation.setVisibility(View.GONE);
        }

        holder.btnConfirmation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putParcelable("cart_item", ci);
                if (context instanceof HomeActivity) {
                    ((HomeActivity) context).startActivityForResult(
                            (new Intent(context, ReviewWithRatingActivity.class).putExtras(b)),
                            HomeActivity.REQ_CODE_LIST_ORDER);
                }
            }
        });

        holder.tvCopyResi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Utility.toast(context, "Nomor resi berhasil disalin ke clipboard");
                Helper.copyToClipboard(context, resi);
            }
        });

        if (ImageLoaderApplication.getImageLoader().getLoader() != null) { //ambil image pertama
            holder.imageProduct.setTag(imageTagFactory.build(
                    Constants.UPLOAD_URL + ci.getImage1(), context));
            ImageLoaderApplication.getImageLoader().getLoader().load(holder.imageProduct);
        }
        return row;
    }

    @Override
    public Object getChild(int arg0, int arg1) {
        return listOrd.get(arg0).getProducts().get(arg1);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listOrd.get(groupPosition).getProducts().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listOrd.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return listOrd.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private class ViewHolder {
        ImageView imageProduct;
        TextView tvOJudul;
        TextView tvOWarna;
        TextView tvOUkuran;
        TextView tvOJumlah;
        TextView tvOTotHar;
        TextView tvStatus;
        TextView tvCopyResi;
        TextView tvOResi;
        TextView tvStep;
        TextView tvOPemesan;
        Button btnConfirmation;
    }
}

