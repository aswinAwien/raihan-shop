package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.SubDistrict;

import java.util.ArrayList;

public class SubDistrictSpinAdapter extends ArrayAdapter<SubDistrict> {
	private Context context;
	private boolean isTextDark;
	private LayoutInflater inflater;

	public SubDistrictSpinAdapter(Context context, ArrayList<SubDistrict> list, boolean isTextDark) {
		super(context, R.layout.itemlist_spinner, list);
		this.context = context;
		this.isTextDark = isTextDark;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if(convertView == null) {
			convertView = inflater.inflate(R.layout.itemlist_spinner, null);

			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.title);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		SubDistrict subDistrict = getItem(position);
		holder.title.setText(subDistrict.title);
		
		if(isTextDark) {
			holder.title.setTextColor(Color.BLACK);
		} else {
			holder.title.setTextColor(Color.WHITE);
		}

		return convertView;
	}

	private class ViewHolder {
		TextView title;
	}
}