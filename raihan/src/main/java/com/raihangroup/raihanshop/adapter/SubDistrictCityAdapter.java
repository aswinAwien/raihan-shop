package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.helper.DBHelper;
import com.raihangroup.raihanshop.model.SubDistrict;

public class SubDistrictCityAdapter extends CursorAdapter {

    Context cotx;
    Cursor cu;
    private DBHelper dbh;
    private String relatedCityName;

    public SubDistrictCityAdapter(Context context, Cursor c) {
        super(context, c);
        this.cotx = context;
        this.cu = c;
        dbh = new DBHelper(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final TextView view = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        String item = createItem(cursor); //ambil judul
        view.setText(item);
        return view;
    }

    private String createItem(Cursor cursor) {
        return cursor.getString(1);
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        Cursor currentCursor = null;
        if (getFilterQueryProvider() != null) {
            return getFilterQueryProvider().runQuery(constraint);
        }
        String args = "";
        if (constraint != null) {
            args = constraint.toString();
        }
//		currentCursor = dbh.findCityByIdProvince(provId, args); //setiap ngetik, jalanin query
        currentCursor = dbh.findCityByProvince(relatedCityName, args); //setiap ngetik, jalanin query
        return currentCursor;
    }

    public void setRelatedCityName(String city) {
        this.relatedCityName = city;
    }

    public String getRelatedCityName() {
        return this.relatedCityName;
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String item = createItem(cursor);
        ((TextView) view).setText(item);
    }
}
