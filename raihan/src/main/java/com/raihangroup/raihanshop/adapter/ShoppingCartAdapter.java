package com.raihangroup.raihanshop.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.CartItem;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCartAdapter extends ArrayAdapter<CartItem>{


	private Context ctx;
	private int res;
	private List<CartItem> listProd;
	private ImageTagFactory imageTagFactory;
	private AQuery aq;
	private ProgressDialog progDialog;


	public ShoppingCartAdapter(Context context, int resource, List<CartItem> objects) {
		super(context, resource, objects);
		this.ctx = context;
		this.res = resource;
		this.listProd = objects;
		
		aq = new AQuery(context);
		progDialog = new ProgressDialog(ctx);
        progDialog.setTitle("Processing");
        progDialog.setMessage("Please wait a moment");

		// selama image belum keload, load gambar default
		imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.default_product);
		imageTagFactory.setErrorImageId(R.drawable.default_product);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		final int finalpos = position;
		final CartItem item = listProd.get(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
			row = inflater.inflate(res, parent, false);

			holder = new ViewHolder();
			holder.tvOJudul = (TextView) row.findViewById(R.id.tvOJudul);

			holder.imageProduct = (ImageView) row.findViewById(R.id.imageProduct);
			holder.tvOPemesan = (TextView) row.findViewById(R.id.tvOPesan);
			holder.tvOWarna = (TextView) row.findViewById(R.id.tvOWarna);
			holder.tvOUkuran = (TextView) row.findViewById(R.id.tvOUkuran);
			holder.tvOJumlah = (TextView) row.findViewById(R.id.tvOJumlah);
			holder.tvOTotHar = (TextView) row.findViewById(R.id.tvOTotHar);
			holder.tvOLokasi = (TextView) row.findViewById(R.id.tvOLokasi);
//			holder.imMenu = (ImageView) row.findViewById(R.id.imMenu);
			holder.editBtn = (ImageButton)row.findViewById(R.id.action_edit_btn);
			holder.deleteBtn = (ImageButton)row.findViewById(R.id.action_delete_btn);
			
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		((View)holder.tvOPemesan.getParent()).setVisibility(View.GONE);
		holder.tvOJudul.setText(item.getName());
		holder.tvOWarna.setText(item.getColor());
		holder.tvOUkuran.setText(item.getSize());
		holder.tvOJumlah.setText(String.valueOf(item.getQty()));
		holder.tvOTotHar.setText(Utility.formatPrice(item.getPrice() * item.getQty()));
		holder.tvOLokasi.setText(item.getLocation());

		holder.editBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, ProductDetailActivity.class);
				i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_MODE, item.getId_product());
				i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_STR, "Keranjang");
				i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL, item.getColor());
				i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, item.getQty());
				i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ, item.getSize());
				i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, item.getId());
				((Activity)ctx).startActivityForResult(i, 0);
			}
		});
		holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(ctx).setTitle("Konfirmasi").setMessage("Anda akan menghapus item dalam cart, apakah anda yaqin?")
						.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								Map<String, String> params = new HashMap<String, String>();
								params.put("id", String.valueOf(item.getId()));

								aq.progress(progDialog).ajax(Constants.URL_RMCART_API, params,
										String.class, new AjaxCallback<String>(){
											@Override
											public void callback(String url,
																 String object, AjaxStatus status) {
												if (object != null) {
													if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
														Utility.showDialogSuccess(ctx, "Berhasil!", "Sukses");
														listProd.remove(item);
														notifyDataSetChanged();
														PrefStorage.instance.changeCart(-1);

														if (listProd.size() == 0) {
															((Activity) ctx).findViewById(R.id.llInfo).setVisibility(View.VISIBLE);
														} else {
															((Activity) ctx).findViewById(R.id.llInfo).setVisibility(View.GONE);
														}
													}

												}
											}
										});
							}
						}).setNegativeButton("Tidak", null).create().show();
			}
		});
		
//		holder.imMenu.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				PopupMenu pop = new PopupMenu(ctx, v);
//				pop.getMenuInflater().inflate(R.menu.cartitem, pop.getMenu());
//				pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//					@Override
//					public boolean onMenuItemClick(MenuItem arg0) {
//						switch (arg0.getItemId()) {
//						case R.id.action_delete:
//							new AlertDialog.Builder(ctx).setTitle("Konfirmasi").setMessage("Anda akan menghapus item dalam cart, apakah anda yaqin?")
//							.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog, int whichButton) {
//									Map<String, String> params = new HashMap<String, String>();
//									params.put("id", String.valueOf(item.getId()));
//
//									aq.progress(progDialog).ajax(Constants.URL_RMCART_API, params,
//											String.class, new AjaxCallback<String>(){
//										@Override
//										public void callback(String url,
//												String object, AjaxStatus status) {
//											if (object != null) {
//												if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
//													Utility.showDialogSuccess(ctx, "Berhasil!", "Sukses");
//													listProd.remove(item);
//													notifyDataSetChanged();
//													PrefStorage.instance.changeCart(-1);
//
//													if (listProd.size() == 0) {
//														((Activity) ctx).findViewById(R.id.llInfo).setVisibility(View.VISIBLE);
//													} else {
//														((Activity) ctx).findViewById(R.id.llInfo).setVisibility(View.GONE);
//													}
//												}
//
//											}
//										}
//									});
//								}
//							}).setNegativeButton("Tidak", null).create().show();
//
//							return true;
//						case R.id.action_edit:
//							Intent i = new Intent(ctx, ProductDetailActivity.class);
//							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_MODE, item.getId_product());
//							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_STR, "Keranjang");
//							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL, item.getColor());
//							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, item.getQty());
//							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ, item.getSize());
//							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, item.getId());
//							((Activity)ctx).startActivityForResult(i, 0);
//							return true;
//						default:
//							return false;
//						}
//					}
//				});
//				pop.show();
//			}
//		});

		if(ImageLoaderApplication.getImageLoader().getLoader() != null) { //ambil image pertama
			holder.imageProduct.setTag(imageTagFactory.build(
					Constants.UPLOAD_URL + item.getImage1(), ctx));
			ImageLoaderApplication.getImageLoader().getLoader().load(holder.imageProduct);
		}
		
		return row;
	}
	
	static class ViewHolder {
		ImageButton deleteBtn;
		ImageButton editBtn;
		ImageView imageProduct;
		TextView tvOPemesan;
		TextView tvOJudul;
		TextView tvOWarna;
		TextView tvOUkuran;
		TextView tvOJumlah;
		TextView tvOTotHar;
		TextView tvOLokasi;
	}
}
