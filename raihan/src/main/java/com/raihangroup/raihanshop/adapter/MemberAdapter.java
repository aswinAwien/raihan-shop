package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.Member;

import java.util.ArrayList;

/**
 * Created by Mohamad on 2/9/2015.
 */
public class MemberAdapter extends ArrayAdapter<Member> {
    private Context context;
    private LayoutInflater inflater;

    public MemberAdapter(Context context, ArrayList<Member> list) {
        super(context, R.layout.itemlist_member, list);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.itemlist_member, null);
            holder = new ViewHolder();
            holder.fullname = (TextView) convertView.findViewById(R.id.tvName);
            holder.level = (TextView) convertView.findViewById(R.id.tvLevel);
            holder.join_date = (TextView) convertView.findViewById(R.id.tvJoinDate);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Member member = getItem(position);
        holder.fullname.setText(member.fullname);
        holder.level.setText(member.level);
        holder.join_date.setText(member.join_date);
        return convertView;
    }

    private class ViewHolder {
        TextView fullname;
        TextView level;
        TextView join_date;
    }
}
