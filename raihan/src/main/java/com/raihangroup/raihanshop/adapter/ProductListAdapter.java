package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.Image;
import com.raihangroup.raihanshop.model.MImage;
import com.raihangroup.raihanshop.model.Product;
import com.raihangroup.raihanshop.model.Stock;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.List;

public class ProductListAdapter extends ArrayAdapter<Product> {

    private static final String TAG = ProductListAdapter.class.getSimpleName();
    private Context context;
    private LayoutInflater inflater;
    private ImageTagFactory imageTagFactory;

    public ProductListAdapter(Context context, List<Product> objects) {
        super(context, R.layout.list_item_product, objects);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.bg_popup);
        imageTagFactory.setErrorImageId(R.drawable.bg_popup);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_product, parent, false);

            holder = new ViewHolder();
            holder.rectView = (LinearLayout) convertView.findViewById(R.id.rectView);
            holder.imThumbnail = (ImageView) convertView.findViewById(R.id.imThumbnail);
            holder.tvNama = (TextView) convertView.findViewById(R.id.tvNama);
            holder.tvHarga = (TextView) convertView.findViewById(R.id.tvHarga);
            holder.tvSupplier = (TextView) convertView.findViewById(R.id.tvSupplier);
            holder.tvPcs = (TextView) convertView.findViewById(R.id.tvPcs);
            holder.tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
            holder.imNew = (ImageView) convertView.findViewById(R.id.imNew);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Product item = getItem(position);
        holder.tvNama.setText(item.getName());
        //prevent out of bonds because of late fill to adapter
        if (item.getPrices().size() > 0) {
            holder.tvHarga.setText(Utility.formatPrice(item.getPrices().get(0).getPrice()));
        }
        holder.tvLocation.setText(item.getLocation());
        holder.imNew.setVisibility(item.is_read == 1 ? View.VISIBLE : View.INVISIBLE);
        if (item.fullname == null) {
            holder.tvSupplier.setText("Raihan");
        } else {
            holder.tvSupplier.setText(String.valueOf(item.fullname));
        }
        if (item.is_read == 0) {
            // the 0 means product is unread
            holder.rectView.setBackgroundResource(R.drawable.selector_very_pale_blue_clickable);
        } else {
            holder.rectView.setBackgroundResource(R.drawable.selector_very_light_gray_clickable);
        }

        int pcs = 0;
        for (Stock s : item.getStock()) {
            pcs += s.getStock();
        }
        holder.tvPcs.setText(pcs + " pcs");
        if (item.getImages().size() != 0) {
            MImage mImage = item.getImages().get(0);
            if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
                holder.imThumbnail.setTag(imageTagFactory.build(
                        Constants.UPLOAD_URL + mImage.getImage(), context));
                ImageLoaderApplication.getImageLoader().getLoader().load(holder.imThumbnail);
                Log.i(TAG, "getView: " + mImage.getImage().toString());
            }
        } else {
            holder.imThumbnail.setImageResource(R.drawable.default_product);
        }
        return convertView;

    }

    static class ViewHolder {
        LinearLayout rectView;
        ImageView imThumbnail;
        TextView tvNama;
        TextView tvHarga;
        TextView tvSupplier;
        TextView tvPcs;
        TextView tvLocation;
        ImageView imNew;
    }
}
