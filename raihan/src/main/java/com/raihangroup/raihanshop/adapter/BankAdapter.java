package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.raihangroup.raihanshop.helper.DBHelper;

/**
 * helper untuk autocomplete bank
 * @author badr
 */
public class BankAdapter extends CursorAdapter implements OnItemClickListener {

	Context cotx;
	Cursor cu;
	private EditText et;
	private DBHelper dbh;
	
	public BankAdapter(Context context, Cursor c, EditText et) {
		super(context, c);
		this.cotx = context;
		this.cu = c;
		this.et = et;
		dbh = new DBHelper(context);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(context);
        final TextView view = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        
        String item = createItem(cursor); //ambil nama
        view.setText(item);
        return view;
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		String item = createItem(cursor);       
        ((TextView) view).setText(item);  
        
	}
	
	private String createItem(Cursor cursor) {
        return cursor.getString(1);
    }
	
	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        Cursor currentCursor = null;
		if (getFilterQueryProvider() != null) {
			return getFilterQueryProvider().runQuery(constraint);
		}
		String args = "";
		if (constraint != null) {
			args = constraint.toString();       
		}
		currentCursor = dbh.findBanks(args); //setiap ngetik, jalanin query
		return currentCursor;
	}

	@Override
	public void onItemClick(AdapterView<?> listView, View view,
			int position, long arg3) {
		// set edittext kalau udah diklik, dan set prov untuk nanti di cityprov
		Cursor cursor = (Cursor) listView.getItemAtPosition(position);
		String nama = cursor.getString(cursor.getColumnIndexOrThrow("nama"));
		et.setText(nama);
	}
}
