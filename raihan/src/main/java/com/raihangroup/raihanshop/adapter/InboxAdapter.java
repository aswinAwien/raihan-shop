package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.MessageObj;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class InboxAdapter extends ArrayAdapter<MessageObj> {

	private Context context;
	private LayoutInflater inflater;
	private ImageTagFactory imageTagFactory;

	public InboxAdapter(Context context, List<MessageObj> objects) {
		super(context, R.layout.itemlist_inbox, objects);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.default_product);
        this.imageTagFactory.setErrorImageId(R.drawable.default_product);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if(convertView == null) {
			convertView = inflater.inflate(R.layout.itemlist_inbox, parent, false);

			holder = new ViewHolder();
			holder.rectView = (RelativeLayout) convertView;
			holder.imageProduct = (ImageView) convertView.findViewById(R.id.imageProduct);
			holder.tvDari = (TextView) convertView.findViewById(R.id.tvDari);
			holder.tvTanggal = (TextView) convertView.findViewById(R.id.tvTanggal);
			holder.tvMessage = (TextView) convertView.findViewById(R.id.tvMessage);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		MessageObj item = getItem(position);
		holder.rectView.setBackgroundResource(item.getStatus() == 0 ?
				R.drawable.selector_very_pale_blue_clickable : R.drawable.selector_very_light_gray_clickable);
		holder.tvDari.setText(item.getTitle());
		
		Date d = new Date();
		int toYes = -1;
		try {
			d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.getTimestamp());
			Calendar c_t = Calendar.getInstance();
			
			Calendar c_y = Calendar.getInstance();
			c_y.add(Calendar.DAY_OF_YEAR, -1);
			
			Calendar c_s = Calendar.getInstance();
			c_s.setTime(d);
			
			if (c_t.get(Calendar.YEAR) == c_s.get(Calendar.YEAR)
					  && c_t.get(Calendar.DAY_OF_YEAR) == c_s.get(Calendar.DAY_OF_YEAR)){
				toYes = 0;}
			else if (c_y.get(Calendar.YEAR) == c_s.get(Calendar.YEAR)
					  && c_y.get(Calendar.DAY_OF_YEAR) == c_s.get(Calendar.DAY_OF_YEAR)) {
                toYes = 1;
            }
		} catch (Exception e) {
			Log.d("exception", String.valueOf(e));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", new Locale("in"));
		
		holder.tvTanggal.setText(sdf.format(d));
		if (toYes != -1) {
			holder.tvTanggal.setText(toYes == 0 ? "Hari Ini" : "Kemarin" );	
		}
		holder.tvMessage.setText(item.getMessage());
		
		if(ImageLoaderApplication.getImageLoader().getLoader() != null) {
			holder.imageProduct.setTag(imageTagFactory.build(
					Constants.MESSAGE_IMG_URL + item.getImage(), context));
			ImageLoaderApplication.getImageLoader().getLoader().load(holder.imageProduct);
		}
		
		return convertView;
	}

	class ViewHolder {
		RelativeLayout rectView;
		ImageView imageProduct;
		TextView tvDari;
		TextView tvTanggal;
		TextView tvMessage;
	}
}
