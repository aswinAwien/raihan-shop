package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raihangroup.raihanshop.ModelChannel;
import com.raihangroup.raihanshop.R;

import java.util.List;
import java.util.Stack;


public class LayoutPagerAdapter<T extends ModelChannel> extends PagerAdapter{  
	private final List<T> models;
	private final Stack<View> recycle = new Stack<View>();
	private int layout = 0;
	private int maxHeight;


	public LayoutPagerAdapter(int layout, List<T> models) {
		this.layout = layout;
		this.models = models;
	}

	public LayoutPagerAdapter(List<T> models) {
		this.models = models;
	}
	
	@Override
	public int getCount() {
//		return Integer.MAX_VALUE;
		return models.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
		recycle.push((View) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		position = position%models.size();
		ModelChannel m = models.get(position);
		Integer l = layout>0?layout:m.getLayout();
		View convertView;

		LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(l, container, false);
		
		convertView.setTag(R.id.content, layout);
    	m.getData(convertView, position);
		convertView.measure(0, 0);
		maxHeight = Math.max(maxHeight, convertView.getMeasuredHeight());
		
		container.addView(convertView);
		return convertView;
	}
	
	public int getPageHeight() {
		return maxHeight;
	}
	
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
	
	public T getItemAt(int position) {
		position = position%models.size();
		return models.get(position);
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		position = position%models.size();
		return models.get(position).toString();
	}
}
