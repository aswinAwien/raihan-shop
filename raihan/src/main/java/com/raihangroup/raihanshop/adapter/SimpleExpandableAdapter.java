package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;

import com.raihangroup.raihanshop.ModelChannel;
import com.raihangroup.raihanshop.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SimpleExpandableAdapter <A, B> extends BaseExpandableListAdapter {
	private Map<A, List<B>> data;
	private int groupLayout = -1;
	private int childLayout = -1;
	private OnChildClickListener childListener;
	private OnGroupClickListener groupListener;

	public SimpleExpandableAdapter(Context ctx, Map<A, List<B>> data) {
		this.data = data;
	}
	
	public SimpleExpandableAdapter(Context ctx, Map<A, List<B>> data, int groupLayout, int childLayout) {
		this.data = data;
		this.groupLayout = groupLayout;
		this.childLayout  = childLayout;
	}

	@Override
	public B getChild(int groupPosition, int childPosition) {
		return data.get(getGroup(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return getChild(groupPosition, childPosition).hashCode();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return data.get(getGroup(groupPosition)).size();
	}

	@Override
	public A getGroup(int groupPosition) {
		ArrayList<A> group = new ArrayList<A>(data.keySet());
		Log.v("TAG:Group:1:", groupPosition+":"+group);
		return group.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		Log.v("TAG:Group:2:", data.keySet().size()+":"+data.keySet());
		return data.keySet().size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return getGroup(groupPosition).hashCode();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
		Object child = getChild(groupPosition, childPosition);
		int layout_id = android.R.layout.simple_list_item_1;
		if(childLayout > 0){
			layout_id = childLayout;
		} else if(child instanceof ModelChannel){
			layout_id = ((ModelChannel) child).getLayout(getGroup(groupPosition));
		}
		if(convertView == null || !((Integer)layout_id).equals(convertView.getTag(R.id.content))) {
			LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View content = inflater.inflate(layout_id, parent, false);
			convertView = content;
		}
		if(child instanceof ModelChannel){
			((ModelChannel) child).getData(convertView, childPosition);
		} else {
			TextView tf = (TextView) convertView.findViewById(android.R.id.text1);
			if(tf != null)	tf.setText(child.toString());
		}
		if(childListener != null){
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					childListener.onChildClick((ExpandableListView) parent, v, groupPosition, childPosition, getChildId(groupPosition, childPosition));
				}
			});
		} else {
			convertView.setOnClickListener(null);
		}
		return convertView;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, final ViewGroup parent) {
		final A group = getGroup(groupPosition);
		int layout_id = android.R.layout.simple_list_item_1;
		if(groupLayout > 0){
			layout_id = groupLayout;
		} else if(group instanceof ModelChannel){
			layout_id = ((ModelChannel) group).getLayout();
		}
		if(convertView == null || !((Integer)layout_id).equals(convertView.getTag(R.id.content))) {
			LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout_id, parent, false);
		}
		if(group instanceof ModelChannel){
			((ModelChannel) group).getData(convertView, groupPosition);
		} else {
			TextView tf = (TextView) convertView.findViewById(android.R.id.text1);
			if(tf != null)	tf.setText(group.toString());
		}
		if(groupListener != null){
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					groupListener.onGroupClick((ExpandableListView) parent, v, groupPosition, getGroupId(groupPosition));
				}
			});
		} else {
			convertView.setOnClickListener(null);
		}
		return convertView;
	}
	
	public void setListener(OnChildClickListener listener){
		childListener = listener;
	}
	
	public void setListener(OnGroupClickListener listener){
		groupListener = listener;
	}
}
