package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.FeedbackReply;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.ArrayList;

public class FeedbackReplyAdapter extends ArrayAdapter<FeedbackReply> {
    private Context context;
    private LayoutInflater inflater;
    private ImageTagFactory imageTagFactory;
    private final static int ME = 0;
    private final static int OTHER = 1;

    public FeedbackReplyAdapter(Context context, ArrayList<FeedbackReply> list) {
        super(context, R.layout.itemlist_feedback_reply, list);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.bg_popup);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            if (getItemViewType(position) == ME) {
                convertView = inflater.inflate(R.layout.itemlist_feedback_sent, null);
            } else {
                convertView = inflater.inflate(R.layout.itemlist_feedback_reply, null);
            }
            holder = new ViewHolder();
            holder.tvFrom = (TextView) convertView.findViewById(R.id.tvFrom);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tvMessage);
            holder.ivImage = (ImageView) convertView.findViewById(R.id.ivImageChat);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        FeedbackReply feedbackReply = getItem(position);
        String from = feedbackReply.id_from.equals("0") ? "Admin" : "Saya";
        holder.tvFrom.setText(from);
        holder.tvDate.setText(feedbackReply.date);
        holder.tvMessage.setText(feedbackReply.message);
        if (!feedbackReply.image.isEmpty()) {
            if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
                Log.d("debug","lalalala");
                holder.ivImage.setVisibility(View.VISIBLE);
                holder.ivImage.setTag(imageTagFactory.build(
                        Constants.BASE_URL + feedbackReply.image, context));
                ImageLoaderApplication.getImageLoader().getLoader().load(holder.ivImage);
            }
        } else {
            holder.ivImage.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        FeedbackReply reply = getItem(position);
        if (reply.id_from.equals(String.valueOf(PrefStorage.instance.getUserId()))) {
            return ME;
        } else {
            return OTHER;
        }
    }

    private class ViewHolder {
        TextView tvFrom;
        TextView tvDate;
        TextView tvMessage;
        ImageView ivImage;
    }
}