package com.raihangroup.raihanshop.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.BookingOrderActivity;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.BookReference;
import com.raihangroup.raihanshop.model.CartItem;
import com.novoda.imageloader.core.model.ImageTagFactory;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookListAdapter extends BaseExpandableListAdapter {

	private Context ctx;
	private List<BookReference> listBook;
	private ImageTagFactory imageTagFactory;
	private AQuery aq;
	private ProgressDialog progDialog;

	public BookListAdapter(Context ctx, List<BookReference> listBook, boolean buttonOn) {
		this.ctx = ctx;
		this.listBook = listBook;

		aq = new AQuery(ctx);
		progDialog = new ProgressDialog(ctx);
		progDialog.setTitle("Sedang Diproses");
		progDialog.setMessage("Mohon Tunggu...");

		// selama image belum keload, load gambar default
		imageTagFactory = ImageTagFactory.newInstance(ctx, R.drawable.default_product);
		imageTagFactory.setErrorImageId(R.drawable.default_product);
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.header_exlv_booklist, null);
			convertView.findViewById(R.id.tvSubTitle).setVisibility(View.GONE);
			convertView.findViewById(R.id.btnAction).setVisibility(View.VISIBLE);
		}

		final BookReference bp = (BookReference) getGroup(groupPosition);

		((TextView) convertView.findViewById(R.id.tvTitle)).setText(bp.getCustomer());
		(convertView.findViewById(R.id.btnAction)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                HashMap<String,String> params = new HashMap<String, String>();
                params.put("id_member", ""+PrefStorage.instance.getUserId());
                params.put("id_order",""+bp.getBooking_id());
                Log.d("id_order", ""+bp.getBooking_id());
				aq.ajax(Constants.URL_LOCK_CART,params, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        super.callback(url, object, status);
                        if (status.getCode() == Constants.HTTP_RESPONSE_OK){
                            Intent i = new Intent(ctx, BookingOrderActivity.class);
                            i.putExtra(Constants.INTENT_BOOK_2_ORD, bp);
                            ctx.startActivity(i);
                        }else{
                            DialogHelper.showMessageDialog(ctx, "Terjadi Kesalahan", "Tidak berhasil melakukan konfirmasi . Silakan mencoba kembali",
                                    "OK", null);
                        }
                    }
                });
			}
		});
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		final CartItem ci = (CartItem) getChild(groupPosition, childPosition);
		final int gPos = groupPosition;
		
		LayoutInflater infalInflater = (LayoutInflater) this.ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (row == null) {
			holder = new ViewHolder();
			row = infalInflater.inflate(R.layout.cart_item, null);

			holder.tvOPemesan = (TextView) row.findViewById(R.id.tvOPesan);
			holder.tvOJudul = ((TextView) row.findViewById(R.id.tvOJudul));
			holder.tvOWarna = ((TextView) row.findViewById(R.id.tvOWarna));
			holder.tvOUkuran = ((TextView) row.findViewById(R.id.tvOUkuran));
			holder.tvOJumlah = ((TextView) row.findViewById(R.id.tvOJumlah));
			holder.tvOTotHar = ((TextView) row.findViewById(R.id.tvOTotHar));
			holder.imageProduct = (ImageView) row.findViewById(R.id.imageProduct);
			holder.tvOLokasi = (TextView) row.findViewById(R.id.tvOLokasi);
			holder.imMenu = (ImageView) row.findViewById(R.id.imMenu);
			
//			holder.tvOPesan = (TextView) row.findViewById(R.id.tvOPesan);

			row.setTag(holder);
		} else {
            holder = (ViewHolder) row.getTag();
        }

		((View)holder.tvOPemesan.getParent()).setVisibility(View.GONE);
		holder.tvOJudul.setText(ci.getName());
		holder.tvOWarna.setText(ci.getColor());
		holder.tvOUkuran.setText(ci.getSize());
		holder.tvOJumlah.setText(String.valueOf(ci.getQty()));
		holder.tvOTotHar.setText(Utility.formatPrice(ci.getQty()*ci.getPrice()));
		holder.tvOLokasi.setText(ci.getLocation());
		
		holder.imMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu pop = new PopupMenu(ctx, v);
				pop.getMenuInflater().inflate(R.menu.cartitem, pop.getMenu());
				pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						switch (arg0.getItemId()) {
						case R.id.action_delete:
							if (((BookReference)getGroup(gPos)).getProducts().size() == 1) {
								Utility.toast(ctx, "Anda tidak dapat menghapus");
								return true;
							}
							
							new AlertDialog.Builder(ctx).setTitle("Konfirmasi").setMessage("Anda akan menghapus item dalam booking, apakah Anda yakin?")
							.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
									
									Map<String, String> params = new HashMap<String, String>();
									params.put("id", String.valueOf(ci.getId()));
									
									aq.progress(progDialog).ajax(Constants.URL_RMCART_API, params, 
											String.class, new AjaxCallback<String>(){
										@Override
										public void callback(String url,
												String object, AjaxStatus status) {
											if (object != null) {
												if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
													Utility.showDialogSuccess(ctx, "Berhasil!", "Sukses");
													BookReference br = (BookReference) getGroup(gPos);
													br.getProducts().remove(ci);
													notifyDataSetChanged();
												}
											}
										}
									});
								}
							}).setNegativeButton("Tidak", null).create().show();
							
							return true;
						case R.id.action_edit:
							Intent i = new Intent(ctx, ProductDetailActivity.class);
							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_STR, "Booking");
							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_MODE, ci.getId_product());
							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_CL, ci.getColor());
							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_JM, ci.getQty());
							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_SZ, ci.getSize());
							i.putExtra(Constants.INTENT_CHANGEDETAIL_PRODDETAIL_ID, ci.getId());
							((Activity)ctx).startActivityForResult(i, 1);
							return true;
						default:
							return false;
						} 
					}
				});
				pop.show();
			}
		});

		if(ImageLoaderApplication.getImageLoader().getLoader() != null) { //ambil image pertama
			holder.imageProduct.setTag(imageTagFactory.build(
					Constants.UPLOAD_URL + ci.getImage1(), ctx));

			ImageLoaderApplication.getImageLoader().getLoader().load(holder.imageProduct);
		}
		
		return row;
	}


	@Override
	public Object getChild(int arg0, int arg1) {
		return listBook.get(arg0).getProducts().get(arg1);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return listBook.get(groupPosition).getProducts().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return listBook.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return listBook.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	static class ViewHolder{
		ImageView imMenu;
		ImageView imageProduct;
//		TextView tvOPesan;
		TextView tvOJudul;
		TextView tvOWarna;
		TextView tvOUkuran;
		TextView tvOJumlah;
		TextView tvOTotHar;
		TextView tvOLokasi;
		TextView tvOPemesan;
	}
}