package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.Supplier;

import java.util.ArrayList;

public class SupplierAdapter extends ArrayAdapter<Supplier> {
	private Context context;
	private LayoutInflater inflater;

	public SupplierAdapter(Context context, ArrayList<Supplier> values) {
		super(context, R.layout.itemlist_supplier, values);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
        if(convertView == null) {
        	convertView = inflater.inflate(R.layout.itemlist_supplier, null);
        	
        	holder = new ViewHolder();
        	holder.tvSupplierName = (TextView) convertView.findViewById(R.id.tvName);
        	convertView.setTag(holder);
        } else {
        	// view already defined, retrieve view holder
        	holder = (ViewHolder) convertView.getTag();
        }
        
        Supplier category = getItem(position);
        holder.tvSupplierName.setText(category.fullname);
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView tvSupplierName;
	}
}
