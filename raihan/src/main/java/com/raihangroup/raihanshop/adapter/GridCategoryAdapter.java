package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.CategoryActivity;
import com.raihangroup.raihanshop.activity.CategoryListActivity;
import com.raihangroup.raihanshop.activity.ProductActivity;
import com.raihangroup.raihanshop.model.Category;
import com.raihangroup.raihanshop.model.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridCategoryAdapter extends RecyclerView.Adapter<GridCategoryAdapter.ViewHolder> {

    List<Category> categories;
    Context context;

    public GridCategoryAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_category, null, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Category category = categories.get(position);
        holder.textCategory.setText(category.name);
        if(category.id ==1){
            holder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_hijab));
        }else if(category.id ==2){
            holder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_muslim));
        }else if(category.id==3){
            holder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_muslim));
        }else if(category.id == 4){
            holder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_anak2));
        }else if (category.id==5){
            holder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_aksesoris));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,CategoryListActivity.class);
                intent.putExtra("category_id",category.id);
                intent.putExtra("category_name",category.name);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_category)
        ImageView imgCategory;
        @BindView(R.id.text_category)
        TextView textCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
