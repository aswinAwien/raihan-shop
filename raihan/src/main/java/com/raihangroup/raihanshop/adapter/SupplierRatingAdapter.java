package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.Rating;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.ArrayList;

public class SupplierRatingAdapter extends ArrayAdapter<Rating> {
	private Context context;
	private LayoutInflater inflater;
	private ImageTagFactory imageTagFactory;
	
	public String[] arrComment;
	public float[] arrQuality;
	public float[] arrService;

	public SupplierRatingAdapter(Context context, ArrayList<Rating> values) {
		super(context, R.layout.itemlist_rating, values);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.bg_popup);
        imageTagFactory.setErrorImageId(R.drawable.bg_popup);
        
        arrComment = new String[values.size()];
        arrQuality = new float[values.size()];
        arrService = new float[values.size()];
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
        if(convertView == null) {
        	convertView = inflater.inflate(R.layout.itemlist_rating, null);
        	
        	holder = new ViewHolder();
        	holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        	holder.imThumbnail = (ImageView) convertView.findViewById(R.id.imThumbnail);
        	holder.ratingBarQuality = (RatingBar) convertView.findViewById(R.id.ratingBarQuality);
        	holder.ratingBarService = (RatingBar) convertView.findViewById(R.id.ratingBarService);
        	holder.etComment = (EditText) convertView.findViewById(R.id.etComment);
        	convertView.setTag(holder);
        } else {
        	// view already defined, retrieve view holder
        	holder = (ViewHolder) convertView.getTag();
        }
        
        holder.position = position;
        
        Rating ratingModel = getItem(position);
        holder.tvTitle.setText(ratingModel.fullname);
        holder.ratingBarQuality.setRating(arrQuality[holder.position]);
        holder.ratingBarService.setRating(arrService[holder.position]);
        holder.etComment.setText(arrComment[holder.position]);
        
        if(ImageLoaderApplication.getImageLoader().getLoader() != null) {
			holder.imThumbnail.setTag(imageTagFactory.build(ratingModel.image, context));
			ImageLoaderApplication.getImageLoader().getLoader().load(holder.imThumbnail);
		}
        
        holder.etComment.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// nothing to do
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// nothing to do
			}
			
			@Override
			public void afterTextChanged(Editable es) {
				arrComment[holder.position] = es.toString();
			}
		});
        holder.ratingBarQuality.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				arrQuality[holder.position] = rating;
			}
		});
        holder.ratingBarService.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				arrService[holder.position] = rating;
			}
		});
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView tvTitle;
		ImageView imThumbnail;
		RatingBar ratingBarQuality;
		RatingBar ratingBarService;
		EditText etComment;
		
		int position;
	}
}
