package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.Category;

import java.util.ArrayList;

public class CategoryAdapter extends ArrayAdapter<Category> {
	private Context context;
	private LayoutInflater inflater;

	public CategoryAdapter(Context context, ArrayList<Category> values) {
		super(context, R.layout.itemlist_category, values);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
        if(convertView == null) {
        	convertView = inflater.inflate(R.layout.itemlist_category, null);
        	holder = new ViewHolder();
        	holder.tvCategory = (TextView) convertView.findViewById(R.id.tvCategory);
        	convertView.setTag(holder);
        } else {
        	// view already defined, retrieve view holder
        	holder = (ViewHolder) convertView.getTag();
        }
        
        Category category = getItem(position);
        holder.tvCategory.setText(category.name);
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView tvCategory;
	}
}
