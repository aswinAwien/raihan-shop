package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.PostToBosFragment;
import com.raihangroup.raihanshop.model.Feedback;

import java.util.ArrayList;

public class FeedbackAdapter extends ArrayAdapter<Feedback> {
	private Context context;
	private LayoutInflater inflater;

	public FeedbackAdapter(Context context, ArrayList<Feedback> list) {
		super(context, R.layout.itemlist_feedback, list);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if(convertView == null) {
			convertView = inflater.inflate(R.layout.itemlist_feedback, null);

			holder = new ViewHolder();
			holder.tvSubject = (TextView) convertView.findViewById(R.id.tvSubject);
			holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			holder.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Feedback feedback = getItem(position);
		holder.tvSubject.setText(feedback.subject);
		holder.tvDate.setText(feedback.date);
		
		String status = PostToBosFragment.POST_TO_BOS_TYPE[feedback.type-1];
		holder.tvStatus.setText(status);

		return convertView;
	}

	private class ViewHolder {
		TextView tvSubject;
		TextView tvDate;
		TextView tvStatus;
	}
}