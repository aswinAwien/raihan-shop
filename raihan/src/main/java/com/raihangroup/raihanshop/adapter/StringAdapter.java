package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;

import java.util.ArrayList;

public class StringAdapter extends ArrayAdapter<String> {
	private Context context;
	private LayoutInflater inflater;

	public StringAdapter(Context context, ArrayList<String> values) {
		super(context, R.layout.itemlist_string, values);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
        if(convertView == null) {
        	convertView = inflater.inflate(R.layout.itemlist_string, null);
        	
        	holder = new ViewHolder();
        	holder.tvStringItem = (TextView) convertView.findViewById(R.id.tvStringItem);
        	convertView.setTag(holder);
        } else {
        	// view already defined, retrieve view holder
        	holder = (ViewHolder) convertView.getTag();
        }
        
        String s = getItem(position);
        holder.tvStringItem.setText(s);
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView tvStringItem;
	}
}
