package com.raihangroup.raihanshop.adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.DialogHelper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.model.PerformanceObject;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PerformanceViewPagerAdapter extends FragmentPagerAdapter {

    List<String> monthTitle;
    Integer userId;

    public PerformanceViewPagerAdapter(FragmentManager fm, List<String> monthTitle, Integer userid) {
        super(fm);
        this.monthTitle = monthTitle;
        this.userId = userid;
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(position+1, userId);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return monthTitle.get(position);
    }

    @Override
    public int getCount() {
        return monthTitle.size();
    }

    public static class PlaceholderFragment extends Fragment {


        private static final String TAG = PlaceholderFragment.class.getSimpleName();
        PieChart pieChart;
        int month;
        int year;
        int userId;
        PerformanceObject po;
        AQuery aq;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, Integer userId) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            Calendar c = Calendar.getInstance();
            int currentYear = c.get(Calendar.YEAR);
            args.putInt(Constants.BUNDLE_PERF_MONTH, sectionNumber);
            args.putInt(Constants.BUNDLE_PERF_YEAR,currentYear);
            args.putInt(Constants.USER_ID_PERFORMANCE,userId);
            fragment.setArguments(args);
            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_pie_chart_performance,null,false);
            pieChart = rootView.findViewById(R.id.pieLevelChart);
            aq = new AQuery(getActivity());
            setData();
            return rootView;
        }

        private void setData(){
            Bundle b = getArguments();
            if (b != null) {
                month = b.getInt(Constants.BUNDLE_PERF_MONTH);
                year = b.getInt(Constants.BUNDLE_PERF_YEAR);
                userId = b.getInt(Constants.USER_ID_PERFORMANCE);
            }
            Map<String, String> params = new HashMap<String, String>();
            params.put("month", String.valueOf(month));
            params.put("year", String.valueOf(year));
            params.put("member_id", String.valueOf(userId));
            Log.i(TAG, "userId: " + userId);

            aq.progress(R.id.pg).ajax(Constants.URL_PERFSUM_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject object,
                                     AjaxStatus status) {
                    if (object != null) {
                        Gson gs = new Gson();
                        PerformanceObject po = gs.fromJson(object.toString(), PerformanceObject.class);
                        Log.d(TAG, "level " + po.getLevel());
                        pieChart.setCenterText(po.getLevel());
                        ArrayList<PieEntry> entries = new ArrayList<>();
                        entries.add(new PieEntry(100 - ((po.getPs()+po.getTts())*10),"Belum Selesai"));
                        entries.add(new PieEntry(po.getPs() * 10,"P.S"));
                        entries.add(new PieEntry(po.getTts() * 10,"P.T"));
                        Log.i(TAG, "tts = " + po.getTts() + " , ps = " + po.getPs() );
                        PieDataSet dataSet = new PieDataSet(entries, " ");

                        ArrayList<Integer> colors = new ArrayList<Integer>();
                        for (int c: ColorTemplate.MATERIAL_COLORS) colors.add(c);

                        dataSet.setColors(colors);
                        PieData data = new PieData(dataSet);
                        data.setValueFormatter(new PercentFormatter());
                        data.setValueTextSize(11f);
                        data.setValueTextColor(Color.WHITE);
//                        data.setValueTypeface(Typeface.create("sans-serif-condensed",Typeface.NORMAL));
                        pieChart.setData(data);
                        pieChart.animateX(1400);

                        // undo all highlights
                        pieChart.highlightValues(null);

                        pieChart.invalidate();

                    }
                }
            });

        }
    }


}
