package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.raihangroup.raihanshop.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HintAdapter extends ArrayAdapter<Objects> {


    public HintAdapter(@NonNull Context context, ArrayList<String> sizeNameList, int resource) {
        super(context, resource);
    }

    public HintAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Objects> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }
}

