package com.raihangroup.raihanshop.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Product;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FrontShopProductsAdapter extends RecyclerView.Adapter<FrontShopProductsAdapter.ViewHolder> {
    private static final String TAG = FrontShopProductsAdapter.class.getSimpleName();
    List<Product> listOfProducts;
    Context context;

    public FrontShopProductsAdapter(List<Product> listOfProducts, Context context) {
        this.listOfProducts = listOfProducts;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_newest_product, null, false);
        return new ViewHolder(view);
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Product product = listOfProducts.get(position);
//        Picasso.get().load(product.getImages().get(0).getImage()).into(holder.ivNewestProduct);
        Picasso.get().load(Constants.UPLOAD_URL+product.getImages().get(0).getImage()).placeholder(R.drawable.ic_logodaun).into(holder.ivNewestProduct);
        holder.tvNewestProductName.setText(product.getName());
        Locale indo = new Locale("id","ID");
        NumberFormat rupiahFormat = NumberFormat.getCurrencyInstance(indo);
        holder.tvNewestProductPrice.setText(rupiahFormat.format(product.getPrices().get(0).getPrice()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductDetailActivity.class);
                i.putExtra(Constants.INTENT_PRODUCT_DETAIL,  product);
                i.putExtra(Constants.INTENT_PRODUCT_ID, product.getId());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOfProducts.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_newestProduct)
        ImageView ivNewestProduct;
        @BindView(R.id.tv_newestProductName)
        TextView tvNewestProductName;
        @BindView(R.id.tv_newestProductPrice)
        TextView tvNewestProductPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
