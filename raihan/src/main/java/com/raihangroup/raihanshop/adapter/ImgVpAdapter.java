package com.raihangroup.raihanshop.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.fragments.ImgFragment;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Image;
import com.raihangroup.raihanshop.model.MImage;
import com.raihangroup.raihanshop.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImgVpAdapter extends PagerAdapter {

//    Context context;
//    LayoutInflater layoutInflater;
//    List<Product> products;
//
//    public ImgVpAdapter(Context contextz) {
//        this.context = context;
//        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    @Override
//    public int getCount() {
//        return products.size();
//    }
//
//    @Override
//    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
//        return view == ((LinearLayout)object);
//    }
//
//    @NonNull
//    @Override
//    public Object instantiateItem(@NonNull ViewGroup container, int position) {
//        View itemView = layoutInflater.inflate(R.layout.fragment_front_shop,null,false);
//        ImageView imageView = (ImageView)itemView.findViewById(R.id.pager);
//        imageView.setImageURI(Uri.parse(Constants.UPLOAD_URL+products.get(position).getImages().get(0)));
//        return super.instantiateItem(container, position);
//    }

        List<Product> products;
    Context context;
    PhotoViewFragment.Photo[] photos;
    LayoutInflater layoutInflater;

    public ImgVpAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
        this.layoutInflater = (LayoutInflater)this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
//        display = ((Activity)context).getWindowManager().getDefaultDisplay();
//        List<PhotoViewFragment.Photo> list = new ArrayList<>();
//        for (Product product:products) {
//            MImage image = product.getImages().get(0);
//            list.add(new PhotoViewFragment.Photo(Constants.UPLOAD_URL+image.getImage(),product.getName(),null));
//        }
//        photos = list.toArray(new PhotoViewFragment.Photo[0]);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = this.layoutInflater.inflate(R.layout.pager_item,container,false);
        ImageView imageView = (ImageView)view.findViewById(R.id.large_image);
        if(products.get(position).getImages().size()>0) {
            Picasso.get().load(Constants.UPLOAD_URL + products.get(position).getImages().get(0).getImage()).error(R.drawable.ic_logodaun).placeholder(R.drawable.ic_logodaun).into(imageView);
        }else {
            Picasso.get().load(R.drawable.ic_logodaun).placeholder(R.drawable.ic_logodaun).into(imageView);
        }
        container.addView(view);
        return view;
    }

    //    @Override
//    public Fragment getItem(int position) {
//        ImgFragment fg = new ImgFragment();
//        Bundle b = new Bundle();
//        b.putInt("index", position);
//        b.putSerializable("photos", photos);
//        fg.setArguments(b);
//        return fg;
//    }


    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
