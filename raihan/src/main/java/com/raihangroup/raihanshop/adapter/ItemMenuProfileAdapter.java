package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.PerformanceViewActivity;
import com.raihangroup.raihanshop.activity.SelfPerformanceActivity;
import com.raihangroup.raihanshop.activity.SettingActivity;
import com.raihangroup.raihanshop.model.SelfPerformance;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemMenuProfileAdapter extends RecyclerView.Adapter<ItemMenuProfileAdapter.ViewHolder> {

    List<ItemMenu> itemMenuList;
    Context context;

    public ItemMenuProfileAdapter(List<ItemMenu> itemMenuList, Context context) {
        this.itemMenuList = itemMenuList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemMenu itemMenu = itemMenuList.get(position);
        holder.btnListMenuImg.setImageDrawable(itemMenu.imgRes);
        holder.btnListMenuTv.setText(itemMenu.textMenu);
        if(itemMenu.id==1){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent settingIntent = new Intent(context, SettingActivity.class);
                    context.startActivity(settingIntent);
                }
            });
        }else if(itemMenu.id==2){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }else if(itemMenu.id==3){

        }
    }

    @Override
    public int getItemCount() {
        return itemMenuList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btn_list_menu_img)
        ImageView btnListMenuImg;
        @BindView(R.id.btn_list_menu_tv)
        TextView btnListMenuTv;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ItemMenu {
        private Integer id;
        private String textMenu;
        private Drawable imgRes;

        public ItemMenu(Integer id, String textMenu, Drawable imgRes) {
            this.id = id;
            this.textMenu = textMenu;
            this.imgRes = imgRes;
        }
    }
}


