package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.activity.ProfileActivity;
import com.raihangroup.raihanshop.activity.ViewProfileActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Review;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.ArrayList;

public class ReviewAdapter extends ArrayAdapter<Review> {
    private Context context;
    private LayoutInflater inflater;
    private ImageTagFactory imageTagFactory;

    public ReviewAdapter(Context context, ArrayList<Review> values) {
        super(context, R.layout.itemlist_review, values);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.default_product);
        imageTagFactory.setErrorImageId(R.drawable.default_product);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.itemlist_review, null);

            holder = new ViewHolder();
            holder.tvProductName = (TextView) convertView.findViewById(R.id.tvProductName);
            holder.ivProduct = (ImageView) convertView.findViewById(R.id.ivProduct);
            holder.tvFrom = (TextView) convertView.findViewById(R.id.tvMemberName);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tvMessage);
            holder.rbQuality = (RatingBar) convertView.findViewById(R.id.ratingBarQuality);
            convertView.setTag(holder);
        } else {
            // view already defined, retrieve view holder
            holder = (ViewHolder) convertView.getTag();
        }

        final Review review = getItem(position);
        if (review.product_image != null && ImageLoaderApplication.getImageLoader().getLoader() != null) {
            holder.ivProduct.setTag(imageTagFactory.build(
                    Constants.UPLOAD_URL + review.product_image, context));
            ImageLoaderApplication.getImageLoader().getLoader().load(holder.ivProduct);

        } else {
            holder.ivProduct.setImageResource(R.drawable.default_product);
        }
        holder.ivProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductDetailActivity.class);
                i.putExtra(Constants.INTENT_PRODUCT_ID, review.id_product);
                context.startActivity(i);
            }
        });
        holder.tvFrom.setText(review.member_name);
        holder.tvFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putInt("userId", review.id_member);
                b.putBoolean("editable", false);
                b.putInt("is_mentor", 1);
                Intent i = new Intent(context, ProfileActivity.class).putExtras(b);
                context.startActivity(i);
            }
        });
        holder.tvDate.setText(review.date);
        holder.tvMessage.setText(review.testimonial);
        holder.tvProductName.setText(review.product_name);
        holder.rbQuality.setRating(review.quality);

        return convertView;
    }

    private class ViewHolder {
        TextView tvProductName;
        TextView tvFrom;
        TextView tvDate;
        TextView tvMessage;
        RatingBar rbQuality;
        ImageView ivProduct;
    }
}
