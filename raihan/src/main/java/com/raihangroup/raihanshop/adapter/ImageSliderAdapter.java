package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.ProductDetailActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageSliderAdapter extends PagerAdapter {

    private Context mContext;
    private List<Product> imageList;
    private LayoutInflater inflater;

    public ImageSliderAdapter(Context context, List<Product> list) {
        mContext = context;
        imageList = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ViewGroup imageLayout = (ViewGroup) inflater.inflate(R.layout.imagefrontslider, collection, false);
        ImageView imageView = imageLayout.findViewById(R.id.imageView);
        Picasso.get().load(Uri.parse(Constants.UPLOAD_URL+imageList.get(position).getImages().get(0).getImage())).placeholder(R.drawable.ic_logodaun).into(imageView);
        collection.addView(imageLayout);
        return imageLayout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        Intent intent = new Intent(mContext, ProductDetailActivity.class);
        Product product = imageList.get(getItemPosition(object));
        intent.putExtra("product_id",product.getId());
        mContext.startActivity(intent);
        return super.getItemPosition(object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return imageList.get(position).getName();
    }
}