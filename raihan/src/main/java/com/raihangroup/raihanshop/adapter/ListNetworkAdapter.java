package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.raihangroup.raihanshop.R;

/**
 * Created by ocit on 8/11/15.
 */
public class ListNetworkAdapter extends BaseAdapter {

    private LayoutInflater inflate;

    public ListNetworkAdapter(Context context) {
        this.inflate= LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflate.inflate(R.layout.friend_network, null);
        }
        return convertView;
    }
}
