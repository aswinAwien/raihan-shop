package com.raihangroup.raihanshop.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.UserStatus;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by ocit on 8/11/15.
 */
public class ListStatusAdapter extends BaseAdapter {

    private static final String TAG = ListStatusAdapter.class.getSimpleName();
    private LayoutInflater inflanter;
    private ArrayList<UserStatus> array;
    private AQuery aq;
    private Context context;

    class ViewHolder {
        TextView nameProfileStatus, contentStatus, subTimeStatus;
        SimpleDraweeView img;
    }

    public ListStatusAdapter(Context context, ArrayList<UserStatus> array, AQuery aq) {
//        this.inflanter = LayoutInflater.from(context);
        this.context = context;
        this.array = array;
        this.aq = aq;
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        ViewHolder holder;
            if (v == null) {
                v = LayoutInflater.from(context).inflate(R.layout.view_status, null,false);
                holder = new ViewHolder();
                holder.contentStatus = (TextView) v.findViewById(R.id.txBodyTextStatus);
                holder.subTimeStatus = (TextView) v.findViewById(R.id.txSubTimeStatus);
                holder.nameProfileStatus = (TextView) v.findViewById(R.id.txNameProfileStatus);
                holder.img = (SimpleDraweeView) v.findViewById(R.id.imgProfileStatus);
                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            holder.contentStatus.setText(array.get(position).content);
            holder.subTimeStatus.setText(array.get(position).created);
            holder.nameProfileStatus.setText(array.get(position).name_creator);
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
            roundingParams.setBorder(R.color.vpi__background_holo_dark, (float) 1.0);
            roundingParams.setRoundAsCircle(true);
            holder.img.getHierarchy().setRoundingParams(roundingParams);
            Uri uri = Uri.parse(Constants.BASE_URL + array.get(position).image);
            if(uri.equals(Uri.parse("http://raihanshop.com/img_default_user.png"))){
             holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.img_default_user));
            }else {
                holder.img.setImageURI(uri);
            }

            return v;
    }
}