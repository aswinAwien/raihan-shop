package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.TimeAgo;
import com.raihangroup.raihanshop.model.UserStatus;
import com.facebook.drawee.drawable.RoundedBitmapDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewStatusListAdapter extends RecyclerView.Adapter<ViewStatusListAdapter.ViewHolder> {

    ArrayList<UserStatus> userStatuses;
    Context context;
    TimeAgo timeAgo;

    public ViewStatusListAdapter(ArrayList<UserStatus> userStatuses, Context context) {
        this.userStatuses = userStatuses;
        this.context = context;
        timeAgo = new TimeAgo(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_status, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserStatus userStatus = userStatuses.get(position);
        holder.txBodyTextStatus.setText(userStatus.content);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(userStatus.created);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();
        String timeString = timeAgo.timeAgo(millis);
        holder.txSubTimeStatus.setText(timeString);
        holder.txNameProfileStatus.setText(userStatus.name_creator);
        Uri uri = Uri.parse(Constants.BASE_URL + userStatus.image);
        if (uri.equals(Uri.parse("http://raihanshop.com/img_default_user.png"))) {
            Picasso.get().load(R.drawable.img_default_user).placeholder(R.drawable.img_default_user).into(holder.imgProfileStatus);
        } else {
            Picasso.get().load(uri).placeholder(R.drawable.img_default_user).into(holder.imgProfileStatus);
        }
    }

    @Override
    public int getItemCount() {
        return userStatuses.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgProfileStatus)
        CircleImageView imgProfileStatus;
        @BindView(R.id.txNameProfileStatus)
        TextView txNameProfileStatus;
        @BindView(R.id.txSubTimeStatus)
        TextView txSubTimeStatus;
        @BindView(R.id.txBodyTextStatus)
        TextView txBodyTextStatus;
        @BindView(R.id.txCountComment)
        TextView txCountComment;
        @BindView(R.id.txCountLike)
        TextView txCountLike;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
