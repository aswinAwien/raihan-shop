package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.Notification;

import java.util.ArrayList;

/**
 * Created by Mohamad on 2/3/2015.
 */
public class NotificationAdapter extends ArrayAdapter<Notification> {
    private Context context;
    private LayoutInflater inflater;

    public NotificationAdapter(Context context, ArrayList<Notification> list) {
        super(context, R.layout.itemlist_notification, list);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.itemlist_notification,null);
            holder = new ViewHolder();
            holder.icon = (ImageView) convertView.findViewById(R.id.iv_notif_icon);
            holder.timestamp = (TextView) convertView.findViewById(R.id.tvTimestamp);
            holder.title = (TextView) convertView.findViewById(R.id.tvNotification);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Notification notification = getItem(position);
        holder.title.setText(notification.title);
        if(notification.is_read == 1) {
            holder.title.setTextColor(0xff6e6e6e);
            holder.title.setTypeface(Typeface.DEFAULT);
        }
        holder.timestamp.setText(notification.date);



        switch (notification.type) {
            case "1":
                break;
            case "2":
                break;
            case "3":
                int id3[] = {R.drawable.ic_notifikasi_naiklevel_pink,R.drawable.ic_notifikasi_naiklevel_grey};
                holder.icon.setImageResource(id3[notification.is_read]);
                break;
            case "4":
                int id4[] = {R.drawable.ic_notifikasi_order_pink,R.drawable.ic_notifikasi_order_grey};
                holder.icon.setImageResource(id4[notification.is_read]);
                break;
            case "5":
                int id5[] = {R.drawable.ic_notifikasi_komisi_pink,R.drawable.ic_notifikasi_komisi_grey};
                holder.icon.setImageResource(id5[notification.is_read]);
                break;
            case "6":
                int id6[] = {R.drawable.ic_notifikasi_member_pink,R.drawable.ic_notifikasi_member_grey};
                holder.icon.setImageResource(id6[notification.is_read]);
                break;
            case "7":
                break;
            case "8":
                int id8[] = {R.drawable.ic_notifikasi_ultah_pink,R.drawable.ic_notifikasi_ultah_grey};
                holder.icon.setImageResource(id8[notification.is_read]);
                break;
            case "9":
                int id9[] = {R.drawable.ic_notifikasi_naiklevel_pink,R.drawable.ic_notifikasi_naiklevel_grey};
                holder.icon.setImageResource(id9[notification.is_read]);
                break;
            case "10":
                int[] id10 = {R.drawable.ic_notifikasi_retur_pink, R.drawable.ic_notifikasi_retur_grey};
                holder.icon.setImageResource(id10[notification.is_read]);
                break;
            case "11":
                int id11[] = {R.drawable.ic_notifikasi_naiklevel_pink,R.drawable.ic_notifikasi_naiklevel_grey};
                holder.icon.setImageResource(id11[notification.is_read]);
                break;
            case "12":
                int id12[] = {R.drawable.ic_notifikasi_naiklevel_pink,R.drawable.ic_notifikasi_naiklevel_grey};
                holder.icon.setImageResource(id12[notification.is_read]);
                break;
            default:
                break;
        }

        return convertView;
    }

    private class ViewHolder{
        ImageView icon;
        TextView title;
        TextView timestamp;
    }
}