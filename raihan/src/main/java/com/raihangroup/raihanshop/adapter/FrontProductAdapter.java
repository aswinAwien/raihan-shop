package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.CategoryListActivity;
import com.raihangroup.raihanshop.model.Category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FrontProductAdapter extends RecyclerView.Adapter<FrontProductAdapter.FrontProductCategoryViewHolder>{
    private List<Category> lisOfCategory;
    private Context context;

    public FrontProductAdapter(List<Category> lisOfCategory, Context context) {
        this.lisOfCategory = lisOfCategory;
        this.context = context;
    }

    @NonNull
    @Override
    public FrontProductCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parent_category_product, null, false);
        return new FrontProductCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FrontProductCategoryViewHolder holder, final int position) {
        final Category category = lisOfCategory.get(position);
        holder.tvItemParentCategory.setText(category.name);
        if(category.id==2){
            holder.ivLogoParentCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_muslim));
        }else if(category.id == 1){
            holder.ivLogoParentCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_hijab));
        }else if(category.id == 3){
            holder.ivLogoParentCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_muslim));
        }else if(category.id == 4){
            holder.ivLogoParentCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_anak2));
        }else if(category.id == 5){
            holder.ivLogoParentCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_aksesoris));
        }else {
            Toast.makeText(context, "nothing here", Toast.LENGTH_SHORT).show();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryListActivity.class);
                intent.putExtra("category_name",category.name);
                intent.putExtra("category_id",category.id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lisOfCategory.size();
    }


    public class FrontProductCategoryViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.iv_logoParentCategory)
        ImageView ivLogoParentCategory;
        @BindView(R.id.tv_itemParentCategory)
        TextView tvItemParentCategory;

        public FrontProductCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
