package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.TimeAgo;
import com.raihangroup.raihanshop.model.ChatItem;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.ArrayList;

public class ChatReplyAdapter extends ArrayAdapter<ChatItem> {
    private Context context;
    private LayoutInflater inflater;
    private ImageTagFactory imageTagFactory;
    private final static int ME = 0;
    private final static int OTHER = 1;
    private final static int PUSH = 2;
    private TimeAgo timeAgo;

    public ChatReplyAdapter(Context context, ArrayList<ChatItem> list) {
        super(context, R.layout.itemlist_feedback_reply, list);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.bg_popup);
        imageTagFactory.setErrorImageId(R.drawable.bg_popup);
        timeAgo = new TimeAgo(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ChatItem chatReply = getItem(position);
        if (convertView == null) {
            holder = new ViewHolder();
            if (getItemViewType(position) == ME) {
                convertView = inflater.inflate(R.layout.itemlist_feedback_sent, null);
            } else if (getItemViewType(position) == OTHER) {
                convertView = inflater.inflate(R.layout.itemlist_feedback_reply, null);
            } else if (getItemViewType(position) == PUSH) {
                convertView = inflater.inflate(R.layout.itemlist_push_message, null);
                holder.tvProductName = (TextView) convertView.findViewById(R.id.tvProductName);
            }
            holder.tvFrom = (TextView) convertView.findViewById(R.id.tvFrom);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tvMessage);
            holder.ivImage = (ImageView) convertView.findViewById(R.id.ivImageChat);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (getItemViewType(position) == PUSH) {
            holder.tvProductName.setText(chatReply.product_name);
        }
        holder.tvFrom.setText(chatReply.fullname);
        holder.tvDate.setText(timeAgo.dayOfWeeks(chatReply.date));
        holder.tvMessage.setText(chatReply.message);
        holder.ivImage.setVisibility(View.GONE);
        holder.ivImage.setImageDrawable(null);

        if (!chatReply.image.isEmpty()) {
            holder.ivImage.setVisibility(View.VISIBLE);
            if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
                holder.ivImage.setTag(imageTagFactory.build(
                        Constants.BASE_URL + chatReply.image, context));
                ImageLoaderApplication.getImageLoader().getLoader().load(holder.ivImage);
            }
        } else {
            holder.ivImage.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        ChatItem chatReply = getItem(position);
        if (chatReply.id_member.equals(String.valueOf(PrefStorage.instance.getUserId()))) {
            return ME;
        } else {
            if (!chatReply.id_product.equals("0")) {
                return PUSH;
            } else {
                return OTHER;
            }
        }
    }

    private class ViewHolder {
        TextView tvFrom;
        TextView tvDate;
        TextView tvMessage;
        TextView tvProductName;
        ImageView ivImage;
    }
}