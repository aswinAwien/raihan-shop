package com.raihangroup.raihanshop.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.Display;

import com.raihangroup.raihanshop.fragments.ImgFragment;
import com.raihangroup.raihanshop.fragments.PhotoViewFragment.Photo;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.model.Image;
import com.raihangroup.raihanshop.model.MImage;
import com.raihangroup.raihanshop.model.Product;

import java.util.ArrayList;

	public class ImageVpAdapter extends FragmentPagerAdapter {

	private Context ctx;
	private String[] imageUrl;
	private Display display;
	private Product product;
	private Photo[] photo;
	
	public ImageVpAdapter(FragmentManager fm, Context ctx, String[] urlsImage, Product p) {
		super(fm);
		this.ctx = ctx;

		this.imageUrl = urlsImage;
		display = ((Activity)ctx).getWindowManager().getDefaultDisplay();
		product = p;
		ArrayList<Photo> list = new ArrayList<Photo>();
		for(MImage m: product.getImages()){
			list.add(new Photo(Constants.UPLOAD_URL +m.getImage(), product.getName(), null));
		}
		photo = list.toArray(new Photo[0]);
	}

//	@Override
//	public Object instantiateItem(ViewGroup container, int position) {
//		RelativeLayout rl = new RelativeLayout(ctx);
//		
//		ImageView imageView = new ImageView(ctx);  
//		imageView.setBackgroundResource(R.drawable.sample_5);
//		
//		RelativeLayout.LayoutParams ivLayout = new RelativeLayout.LayoutParams
//				(display.getWidth()*70/100, display.getHeight()*40/100);
//		ivLayout.addRule(RelativeLayout.CENTER_IN_PARENT);
//		
//		imageView.setLayoutParams(ivLayout);
//		
//		rl.addView(imageView);
//		
//		container.addView(rl,0);  
//
//		return rl;
//	}
	@Override
	public int getCount() {
		return product.getImages().size();
	}

	@Override
	public Fragment getItem(int i) {
		ImgFragment fg = new ImgFragment();
		Bundle b = new Bundle();
		b.putInt("index", i);
		b.putSerializable("photos", photo);
		fg.setArguments(b);
		return fg;
	}
}
