package com.raihangroup.raihanshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.raihangroup.raihanshop.ImageLoaderApplication;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.activity.SelfPerformanceActivity;
import com.raihangroup.raihanshop.helper.CircleImageView;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.SelfPerformance;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SelfPerformanceAdapter extends ArrayAdapter<SelfPerformance> {

    private Context ctx;
    private int res;
    private List<SelfPerformance> listSPerf;
    private int mode;
    private ImageTagFactory imageTagFactory;
    private LayoutInflater inflater;

    public SelfPerformanceAdapter(Context context, int resource,
                                  List<SelfPerformance> objects, int mode) {
        super(context, resource, objects);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.ctx = context;
        this.res = resource;
        this.listSPerf = objects;
        this.mode = mode;
        imageTagFactory = ImageTagFactory.newInstance(context, R.drawable.img_default_user);
        imageTagFactory.setErrorImageId(R.drawable.img_default_user);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(res, parent, false);

            holder = new ViewHolder();
            holder.llSp = (LinearLayout) convertView.findViewById(R.id.llPs);
            holder.llTts = (LinearLayout) convertView.findViewById(R.id.llTts);

            holder.tvSPTanggal = (TextView) convertView.findViewById(R.id.tvSPTanggal);
            holder.tvSPJumBarang = (TextView) convertView.findViewById(R.id.tvSPJumBarang);
            holder.tvSPNominalBarang = (TextView) convertView.findViewById(R.id.tvSPNominalBarang);
            holder.tvSPVolume = (TextView) convertView.findViewById(R.id.tvSPVolume);

            holder.tvSPid = (TextView) convertView.findViewById(R.id.tvSpId);
            holder.tvSPLevel = (TextView) convertView.findViewById(R.id.tvSpLevel);
            holder.ivProfile = (CircleImageView) convertView.findViewById(R.id.imageView);
            holder.tvMentor = (TextView) convertView.findViewById(R.id.tvMentor);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SelfPerformance item = listSPerf.get(position);

        if (mode == SelfPerformanceActivity.MODE_PS) {
            holder.llSp.setVisibility(View.VISIBLE);
            holder.llTts.setVisibility(View.GONE);
            holder.tvMentor.setVisibility(View.GONE);
            holder.tvSPTanggal.setText(item.getDate_time());
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.getDate_time());
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm", new Locale("in"));
                holder.tvSPTanggal.setText(sdf.format(d));

            } catch (ParseException e) {
                Log.d("exception", String.valueOf(e));
            }
            holder.tvSPJumBarang.setText(item.getProduct());
            holder.tvSPNominalBarang.setText(Utility.formatPrice(item.getPrice()));
            holder.ivProfile.setVisibility(View.GONE);
        } else if (mode == SelfPerformanceActivity.MODE_TTS) {
            holder.llSp.setVisibility(View.GONE);
            holder.llTts.setVisibility(View.VISIBLE);
            if (item.getIs_mentor() == 0) {
                holder.tvMentor.setVisibility(View.GONE);
            }else{
                holder.tvMentor.setVisibility(View.VISIBLE);
            }
            holder.tvSPTanggal.setTextColor(Color.rgb(236, 0, 134));
            holder.tvSPTanggal.setText(item.getFullname());
            holder.tvSPid.setText(String.valueOf(item.getId()));
            holder.tvSPLevel.setText(item.getLevel());
            if (ImageLoaderApplication.getImageLoader().getLoader() != null) {
                holder.ivProfile.setTag(imageTagFactory.build(
                        Constants.BASE_URL + item.getImage(), ctx));
                ImageLoaderApplication.getImageLoader().getLoader().load(holder.ivProfile);
            }
        }
        float d = item.getVolume();
        if (d == (int) d) {
            holder.tvSPVolume.setText(String.format("%d", (int) d));
        } else {
            holder.tvSPVolume.setText(String.valueOf(d));
        }
        return convertView;
    }

    static class ViewHolder {
        LinearLayout llSp;
        LinearLayout llTts;

        TextView tvSPTanggal;
        TextView tvSPJumBarang;
        TextView tvSPNominalBarang;
        TextView tvSPVolume;

        TextView tvSPid;
        TextView tvSPLevel;
        CircleImageView ivProfile;
        TextView tvMentor;
    }

}
