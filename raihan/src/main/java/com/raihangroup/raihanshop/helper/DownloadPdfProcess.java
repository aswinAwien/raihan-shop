package com.raihangroup.raihanshop.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DownloadPdfProcess {
    public static final int MAX_PROGRESS = 100;
    private Context context;
    private boolean isOpenPdfAfterDownloading;

    private final static String TAG = "Download PDF";

    public void performDownload(Context context, String fileName,
                                String urlString, boolean isOpenPdfAfterDownloading) {
        this.context = context;
        this.isOpenPdfAfterDownloading = isOpenPdfAfterDownloading;

        new DownloadPdfTask(fileName).execute(urlString);
    }

    private class DownloadPdfTask extends AsyncTask<String, Integer, Void> {

        private String writtenFilePath;
        private ProgressDialog progDialog;

        public DownloadPdfTask(String fileName) {
            writtenFilePath = Environment.getExternalStorageDirectory()
                    + "/" + fileName;

            // set progress dialog
            progDialog = new ProgressDialog(context);
            progDialog.setTitle("Downloading Pdf");
            progDialog.setMessage("Please wait...");
            progDialog.setIndeterminate(false);
            progDialog.setCanceledOnTouchOutside(false);
            progDialog.setMax(MAX_PROGRESS);
            progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

            progDialog.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    File file = new File(writtenFilePath);
                    boolean isDeleted = file.delete();
                    Log.d("deleted?", "" + isDeleted);

                    DownloadPdfTask.this.cancel(true);
                }
            });
        }

        @Override
        protected void onPreExecute() {
            progDialog.show();

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... args) {
            String urlString = args[0];

            try {
                DefaultHttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost(urlString);

                HttpResponse rp = client.execute(request);
                int fileLength = Integer.parseInt((rp.getHeaders("Content-Length"))[0].getValue());
                InputStream instream = rp.getEntity().getContent();

                OutputStream output = new FileOutputStream(writtenFilePath);
                byte[] tmp = new byte[2048];
                long total = 0;
                int count;
                while ((count = instream.read(tmp)) != -1) {
                    total += count;
                    // publishing the progress....
                    publishProgress((int) (total * MAX_PROGRESS / fileLength));
                    //LogMe.d("Unduh", "" + (int) (total * 100 / fileLength));
                    output.write(tmp, 0, count);
                }

                output.flush();
                output.close();
            } catch (IOException ioe) {
                Log.d("exception", String.valueOf(ioe));
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progDialog.setProgress(progress[0]);
            if (progress[0] == MAX_PROGRESS) {
                progDialog.dismiss();

                if (isOpenPdfAfterDownloading) {
                    File file = new File(writtenFilePath);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(intent);

                    Log.d(TAG, "open pdf " + writtenFilePath);
                    Utility.toast(context, "Download berhasil. Membuka PDF...");
                }
            }
        }

    } // end of task
}
