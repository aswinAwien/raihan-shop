package com.raihangroup.raihanshop.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * storage untuk misc things. sepertinya nama fungsinya sudah cukup
 * representatif
 *
 * @author badr
 */
public class PrefStorage {


    public static PrefStorage instance;
    private Context context;
    private SharedPreferences prefs;

    private final static String MP_PREFS = "com.bi.raihan.prefs";
    private final static String KEY_USER_CLASS = "com.bi.raihan.prefs.class";
    private final static String KEY_USER_ID = "com.bi.raihan.prefs.iduser";
    private final static String KEY_USER_NAME = "com.bi.raihan.prefs.name";
    private final static String KEY_USER_JSON = "com.bi.raihan.prefs.json";
    private final static String KEY_CATEGORY_JSON = "com.bi.raihan.prefs.cat.json";
    private final static String KEY_CART_ITEMS = "com.bi.raihan.prefs.jumlahCart";
    private final static String KEY_STOK = "com.bi.raihan.prefs.stok";
    private final static String KEY_UNREAD_NEW_PRODUCT_AMOUNT = "UnreadNewProductAmount";
    private final static String KEY_I_UNDERSTAND_TOUR_APP = "IUndestandTourApp";
    private final static String KEY_UNREAD_MESSAGES = "UnreadMessages";
    private final static String KEY_UNREAD_NOTIF = "general";
    private final static String KEY_UNREAD_FEEDBACK = "feedback";
    private final static String KEY_UNREAD_UPLINE = "upline";
    private final static String KEY_UNREAD_DOWNLINE = "downline";
    private final static String KEY_UNREAD_BOOK = "booking";
    private final static String KEY_UNREAD_ORDER = "order";
    private final static String KEY_MUTE_NOTIFICATION = "mute";
    private final static String KEY_IS_LOGGED_IN = "isLoggedIn";
    private final static String KEY_PERMISSION = "isPermitted";
    public static final String KEY_SECTION_MODE = "section_mode";

    private final static String KEY_USER_GCM_ID = "com.bi.raihan.prefs.id_gcm";


    public static PrefStorage newInstance(Context context) {
        if (instance == null) {
            instance = new PrefStorage(context);
        }
        return instance;
    }

    public PrefStorage(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences(MP_PREFS, 0);
    }

    // set methods

    public void setUser(int id, String name, String kelas) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(KEY_USER_NAME, name);
        prefsEditor.putString(KEY_USER_CLASS, kelas);
        prefsEditor.putInt(KEY_USER_ID, id);
        prefsEditor.commit();
    }

    public void setSectionMode(int sectionMode){
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_SECTION_MODE,sectionMode);
        prefsEditor.commit();
    }
    public void setKeyPermission(boolean isPermitted){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(KEY_PERMISSION,isPermitted);
        editor.commit();
    }


    public void setKeyIsLoggedIn(boolean isLoggedIn){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(KEY_IS_LOGGED_IN,isLoggedIn);
        editor.commit();
    }

    public void setUserData(String json) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(KEY_USER_JSON, json);
        prefsEditor.commit();
    }

    public void setCategoriesJSON(String json) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(KEY_CATEGORY_JSON, json);
        prefsEditor.commit();
    }

    public void setGCMid(String id) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(KEY_USER_GCM_ID, id);
        prefsEditor.commit();
    }

    public void setUnreadNewProductAmount(int amount) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_NEW_PRODUCT_AMOUNT, amount);
        prefsEditor.commit();
    }

    public void setStok(boolean enabled) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(KEY_STOK, enabled);
        prefsEditor.commit();
    }

    public void setIUnderstandTourAppStatus(boolean isUnderstood) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(KEY_I_UNDERSTAND_TOUR_APP, isUnderstood);
        prefsEditor.commit();
    }

    public void setUnreadMessages(int unreadMessages) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_MESSAGES, unreadMessages);
        prefsEditor.commit();
    }

    public void setUnreadNotification(int unreadNotification) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_NOTIF, unreadNotification);
        prefsEditor.commit();
    }

    public void setUnreadFeedback(int unreadFeedback) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_FEEDBACK, unreadFeedback);
        prefsEditor.commit();
    }

    public void setUnreadUpline(int unreadUpline) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_UPLINE, unreadUpline);
        prefsEditor.commit();
    }

    public void setUnreadDownline(int unreadDownline) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_DOWNLINE, unreadDownline);
        prefsEditor.commit();
    }

    public void setMuteNotification(boolean mute) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(KEY_MUTE_NOTIFICATION, mute);
        prefsEditor.commit();
    }

    public void setUnreadBooking(int unreadBooking) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_BOOK, unreadBooking);
        prefsEditor.commit();
    }

    public void setUnreadOrder(int unreadOrder) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_UNREAD_ORDER, unreadOrder);
        prefsEditor.commit();
    }


    // get methods

    public String getGCMid() {
        return prefs.getString(KEY_USER_GCM_ID, "");
    }

    public String getKelas() {
        return prefs.getString(KEY_USER_CLASS, "unavailable");
    }

    public int getUserId() {
        return prefs.getInt(KEY_USER_ID, -1);
    }

    public String getUserData() {
        return prefs.getString(KEY_USER_JSON, null);
    }

    public int getCartAmount() {
        return prefs.getInt(KEY_CART_ITEMS, 0);
    }

    public int getUnreadNewProductAmount() {
        return prefs.getInt(KEY_UNREAD_NEW_PRODUCT_AMOUNT, 0);
    }

    public int getSectionMode(){
        return prefs.getInt(KEY_SECTION_MODE,1);
    }

    public boolean getPermission(){
        return prefs.getBoolean(KEY_PERMISSION,false);
    }

    public boolean getIsLoggedIn(){
        return prefs.getBoolean(KEY_IS_LOGGED_IN,false);
    }

    public String getCategoriesJSON() {
        return prefs.getString(KEY_CATEGORY_JSON, null);
    }

    public void changeCart(int amount) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        int tmp = getCartAmount() + amount;
        prefsEditor.putInt(KEY_CART_ITEMS, tmp < 0 ? 0 : tmp);
        prefsEditor.commit();
    }

    public void resetCart() {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(KEY_CART_ITEMS, 0);
        prefsEditor.commit();
    }

    public boolean getStokStat() {
        return prefs.getBoolean(KEY_STOK, false);
    }

    public boolean getUnderstandingOfTourApp() {
        // default value is true, it means show tour app
        return prefs.getBoolean(KEY_I_UNDERSTAND_TOUR_APP, false);
    }

    public int getUnreadMessages() {
        return prefs.getInt(KEY_UNREAD_MESSAGES, 0);
    }

    public int getUnreadNotification() {
        return prefs.getInt(KEY_UNREAD_NOTIF, 0);
    }

    public int getUnreadFeedback() {
        return prefs.getInt(KEY_UNREAD_FEEDBACK, 0);
    }

    public int getUnreadUpline() {
        return prefs.getInt(KEY_UNREAD_UPLINE, 0);
    }

    public int getUnreadDownline() {
        return prefs.getInt(KEY_UNREAD_DOWNLINE, 0);
    }

    public boolean getMuteNotification() {
        return prefs.getBoolean(KEY_MUTE_NOTIFICATION, false);
    }

    public int getUnreadBooking() {
        return prefs.getInt(KEY_UNREAD_BOOK, 0);
    }

    public int getUnreadOrder() {
        return prefs.getInt(KEY_UNREAD_ORDER, 0);
    }


    /**
     * clear ALL the storage.
     */
    public void clear() {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        String gcm_id = getGCMid();
        //clear
        prefsEditor.clear();

        //tetap pasang GCM ID
        prefsEditor.putString(KEY_USER_GCM_ID, gcm_id);
        prefsEditor.commit();
    }
}
