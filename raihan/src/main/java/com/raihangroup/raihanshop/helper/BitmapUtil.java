package com.raihangroup.raihanshop.helper;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.IOException;

import static android.graphics.Bitmap.Config;

/**
 * @author fikr4n
 */
public final class BitmapUtil {

    private BitmapUtil(){}

    public static long getOneEighthMemoryBytes() {
        /* Note: In this example, one eighth of the application memory is allocated for our cache.
           On a normal/hdpi device this is a minimum of around 4MB (32/8). A full screen GridView
           filled with images on a device with 800x480 resolution would use around 1.5MB
           (800*480*4 bytes), so this would cache a minimum of around 2.5 pages of images in memory.
           https://developer.android.com/training/displaying-bitmaps/cache-bitmap.html
         */

        // Use 1/8th of the available memory for this memory cache.
        return Runtime.getRuntime().maxMemory() / 8;
    }

    public static Bitmap decodeFileScaled(int targetW, int targetH, String path) {
        /* Adapted from http://developer.android.com/training/camera/photobasics.html */

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);

        // Determine how much to scale down the image
        int scaleFactor = Math.min(bmOptions.outWidth / targetW, bmOptions.outHeight / targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        return BitmapFactory.decodeFile(path, bmOptions);
    }

    public static Bitmap decodeByteArrayScaled(int targetW, int targetH, byte[] data, int offset, int length) {
        /* Adapted from http://developer.android.com/training/camera/photobasics.html */

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, offset, length, bmOptions);

        // Determine how much to scale down the image
        int scaleFactor = Math.min(bmOptions.outWidth / targetW, bmOptions.outHeight / targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        return BitmapFactory.decodeByteArray(data, offset, length, bmOptions);
    }

    /**
     * Get image orientation metadata. This method returns 360 if fail to get.
     *
     * @param context  context
     * @param imageUri URI
     * @return orientation in degrees
     */
    public static int getOrientation(Context context, Uri imageUri) {
        /* Adapted from http://stackoverflow.com/a/8661569/1197317 */
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(imageUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        int result = 360;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                result = cursor.getInt(0);
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Rotate bitmap. The passed bitmap is returned if degrees is 0 or 360, otherwise the rotated
     * bitmap is returned but the passed bitmap will be recycled and must not be used anymore.
     *
     * @param bitmap  bitmap
     * @param degrees angle of rotation
     * @return rotated bitmap or the original bitmap
     */
    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        if (degrees == 0f || degrees == 360f) {
            return bitmap;
        } else {
            Matrix matrix = new Matrix();
            matrix.setRotate(degrees);
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
    }

    /**
     * Get image orientation metadata. This method returns
     * {@link android.media.ExifInterface#ORIENTATION_UNDEFINED} if fail to get.
     *
     * @param filename file path
     * @return orientation of {@code ExifInterface.ORIENTATION_*} constants
     */
    public static int getExifOrientation(String filename) {
        /* Adapted from http://stackoverflow.com/a/20480741/1197317 */
        try {
            ExifInterface exif = new ExifInterface(filename);
            return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            return ExifInterface.ORIENTATION_UNDEFINED;
        }
    }

    /**
     * Rotate bitmap. The passed bitmap is returned if rotation is normal or undefined, otherwise
     * the rotated bitmap is returned but the passed bitmap will be recycled and must not be used anymore.
     *
     * @param bitmap          bitmap
     * @param exifOrientation orientation of {@code ExifInterface.ORIENTATION_*} constants
     * @return rotated bitmap or the original bitmap
     */
    public static Bitmap rotateByExif(Bitmap bitmap, int exifOrientation) {
        /* Adapted from http://stackoverflow.com/a/20480741/1197317 */
        Matrix matrix = new Matrix();
        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return bmRotated;
    }

    public static Bitmap fixOrientation(Context context, Bitmap bitmap, Uri bitmapUri) {
        int degrees = getOrientation(context, bitmapUri);
        if (degrees == 360 && "file".equals(bitmapUri.getScheme())) {
            String filename = bitmapUri.getPath();
            return rotateByExif(bitmap, getExifOrientation(filename));
        } else {
            return rotate(bitmap, degrees);
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

}

