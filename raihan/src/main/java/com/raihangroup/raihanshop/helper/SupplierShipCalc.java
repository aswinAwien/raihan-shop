package com.raihangroup.raihanshop.helper;

import android.util.Log;

import com.raihangroup.raihanshop.model.CartItem;

public class SupplierShipCalc implements Cloneable {

	private String supplierId;
	/** Price of shipping service. */
	private int servicePrice;
	/** Id of shipping service method. */ 
	private int serviceMethodId;
	/** Total weight of cart items in this supplier.
	 *  The weight unit here is in grams. 
	 */
	private int totalWeightInGr;
	
	private final static String TAG = "SHIP PRICE CALC";
	
	public SupplierShipCalc(String supplierId) {
		this.supplierId = supplierId;
	}
	
	public void addCartItemWeight(CartItem ci) {
		totalWeightInGr += (ci.getWeight() * ci.getQty());
	}
	
	private int calculateAdjustmentShippingWeightInKg() {
		int n = totalWeightInGr<1200 ? 1 : 
			(int)Math.ceil((float)(totalWeightInGr - 200)/1000);
		
		Log.d(TAG, "calculate adjusment shipping weight...");
		Log.d(TAG, "result: " + n + " kg for supplier id " + supplierId);
		
		return n;
	}
	
	public int calculateShippingPrice() {
		Log.d(TAG, "calculate shipping price...");
		Log.d(TAG, "service cost: " + servicePrice);
		
		int shippingPrice = servicePrice * calculateAdjustmentShippingWeightInKg();
		if(serviceMethodId == Constants.POS_SHIP_METHOD_ID) {
			shippingPrice = servicePrice;
		}
		Log.d(TAG, "result: shipping price " + shippingPrice);
		
		return shippingPrice;
	}
	
	// setter methods 
	
	public void setServicePrice(int servicePrice) {
		this.servicePrice = servicePrice;
	}
	
	public void setServiceMethodId(int serviceMethodId) {
		this.serviceMethodId = serviceMethodId;
	}
	
	// getter methods

	public String getSupplierId() {
		return supplierId;
	}

	public int getTotalWeightInGr() {
		return totalWeightInGr;
	}

	public int getServicePrice() {
		return servicePrice;
	}

	// cloneable method
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
