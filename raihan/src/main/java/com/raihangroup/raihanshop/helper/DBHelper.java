package com.raihangroup.raihanshop.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.raihangroup.raihanshop.model.MessageObj;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * dbhelper using sqliteasset. bisa dizip juga. pretty cool
 * @author badr
 */
public class DBHelper extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "raihan";
	private static final int DATABASE_VERSION = 2;
	
	private final static String TAG = "DB Helper";
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		setForcedUpgradeVersion(DATABASE_VERSION);
	}
	
	public Cursor findProvince(String key) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT count(prov) as _id, prov FROM zona WHERE prov LIKE ? GROUP BY prov",
				new String[] {Utility.isNullOrBlank(key) ? "%" : "%"+key+"%"});

		c.moveToFirst();
		return c;
	}
	
	public Cursor findCityByProvince(String prov
			, String key) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT count(kota) as _id, kota FROM zona WHERE kota LIKE" +
				" ? AND prov = ? " +"GROUP BY kota",
				new String[] {Utility.isNullOrBlank(key) ? "%" : "%"+key+"%", prov});
	
		c.moveToFirst();
		return c;
	}
	
	public Cursor findKecaByCityProv(String prov, String kota
			, String key) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT count(keca) as _id, keca FROM zona " +
				"WHERE keca LIKE ? AND prov = ? AND kota = ?" +
				"GROUP BY keca",new String[] {Utility.isNullOrBlank(key) ? "%" : "%"+key+"%", prov, kota});
	
		c.moveToFirst();
		return c;
	}
	
	public int findIdKeca(String key) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT count(keca) as _id, keca FROM zona " +
				"WHERE keca = ? GROUP BY keca",new String[] {key});
	
		c.moveToFirst();
		
		int ret = Constants.NOT_FOUND_DEFAULT;
		try {
			ret = c.getInt(0);
		} catch(Exception e){}
		
		return ret;
	}
	
	public Cursor initCursor(String key) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT * FROM zona WHERE keca = ?",
				new String[] {key});
	
		c.moveToFirst();
		return c;
	}
	
	// about bank info
	
	public Cursor findBanks(String key) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT _id, nama FROM banks WHERE nama LIKE ? GROUP BY nama",
				new String[] {Utility.isNullOrBlank(key) ? "%" : "%"+key+"%"});

		c.moveToFirst();
		return c;
	}
	
	public String findBankName(int id) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c  = db.rawQuery("SELECT _id, nama FROM banks WHERE _id = ?",
				new String[] {String.valueOf(id)});

		String ret = null;
		if (c.moveToFirst()) {
			ret = c.getString(1);
		}
		c.close();
		
		return ret;
	}
	
	public int findIdBank(String bank) {
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor c  = db.rawQuery("SELECT _id FROM banks WHERE nama = ?",
				new String[] {bank});
		c.moveToFirst();
		
		int ret = -1;
		try {
			ret = c.getInt(0);
			c.close();
		} catch (Exception e) {}
		return ret;
	}
	
//	public int findIdProvince(String province) {
//		SQLiteDatabase db = getReadableDatabase();
//		
//		Cursor c  = db.rawQuery("SELECT _id FROM provinsi WHERE title = ?",
//				new String[] {province});
//		c.moveToFirst();
//		
//		int ret = -1;
//		try {
//			ret = c.getInt(0);
//			c.close();
//		} catch (Exception e) {}
//		return ret;
//	}
	
//	public Cursor findCityByIdProvince(int id, String key) {
//		SQLiteDatabase db = getReadableDatabase();
//		Cursor c  = db.rawQuery("SELECT _id, id_provinsi, title FROM kota WHERE title LIKE ? AND id_provinsi = ? " +
//				"ORDER BY title",new String[] {Utility.isNullOrBlank(key) ? "%" : "%"+key+"%", String.valueOf(id)});
//
//		c.moveToFirst();
//		return c;
//	}
//	public int findIdCity(String city) {
//		SQLiteDatabase db = getReadableDatabase();
//		
//		Cursor c  = db.rawQuery("SELECT _id FROM kota WHERE title = ?",
//				new String[] {city});
//		c.moveToFirst();
//		
//		int ret = -1;
//		try {
//			ret = c.getInt(0);
//			c.close();
//		} catch (Exception e) {}
//		return ret;
//	}
	
//	public Cursor findKecaByIdCity(int id, String key) {
//		SQLiteDatabase db = getReadableDatabase();
//		Cursor c  = db.rawQuery("SELECT _id, id_kota, title FROM kecamatan WHERE title LIKE ? AND id_kota = ? " +
//				"ORDER BY title",new String[] {Utility.isNullOrBlank(key) ? "%" : "%"+key+"%", String.valueOf(id)});
//
//		c.moveToFirst();
//		return c;
//	}
	
//	public int findIdKeca(String kecamatan) {
//		SQLiteDatabase db = getReadableDatabase();
//		
//		Cursor c  = db.rawQuery("SELECT _id FROM kecamatan WHERE title = ?",
//				new String[] {kecamatan});
//		c.moveToFirst();
//		
//		int ret = -1;
//		try {
//			ret = c.getInt(0);
//			c.close();
//		} catch (Exception e) {}
//		return ret;
//	}
	
	// about messages
	
	public List<MessageObj> getAllMessages() {
		if(isMessageDbEmpty()) return null;
		
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor c = db.rawQuery("SELECT * FROM message ORDER BY id DESC", null);
		c.moveToFirst();
		
		List<MessageObj> msgs = new ArrayList<MessageObj>();
		do {
			// see column name on sqlite file on assets
			MessageObj mo = new MessageObj();
			mo.setId(c.getInt(1));
			mo.setTitle(c.getString(2));
			mo.setMessage(c.getString(3));
			mo.setImage(c.getString(4));
			mo.setTimestamp(c.getString(5));
			mo.setStatus(c.getInt(6));
			
			if(mo.getId() == 0) continue; // ignore message with id 0
			msgs.add(mo);
		} while(c.moveToNext());
		
		c.close();
		
		return msgs;
	}
	
	public List<MessageObj> getUnreadMessages() {
		if(isMessageDbEmpty()) return null;
		
		SQLiteDatabase db = getReadableDatabase();
		
		// status 0 means it is unread
		Cursor c  = db.rawQuery("SELECT * FROM message WHERE status = 0", null);
		c.moveToFirst();
		
		List<MessageObj> msgs = new ArrayList<MessageObj>();
		do {
			// see column name on sqlite file on assets
			MessageObj mo = new MessageObj();
			mo.setId(c.getInt(1));
			mo.setTitle(c.getString(2));
			mo.setMessage(c.getString(3));
			mo.setImage(c.getString(4));
			mo.setTimestamp(c.getString(5));
			mo.setStatus(c.getInt(6));
			
			if(mo.getId() == 0) continue; // ignore message with id 0
			msgs.add(mo);
		} while(c.moveToNext());
		
		c.close();
//		super.close();
		
		return msgs;
	}
	
	public void clearMessage() {
		SQLiteDatabase db = getWritableDatabase();
		db.delete("message", null, null);
		super.close();
	}
	
	public boolean isMessageDbEmpty() {
		SQLiteDatabase db = getReadableDatabase();
		
		boolean isEmpty = false;
		
		Cursor c = db.rawQuery("SELECT COUNT(*) FROM message", null);
		
		if(c.moveToFirst()) {
			int messageNumber = c.getInt(0);
			if(messageNumber == 0) isEmpty = true;
			
			Log.d(TAG, "message in db " + messageNumber);
		} else {
			isEmpty = true;
		}
		
		c.close();
		super.close();
		
		return isEmpty;
	}
	
	public int insertNewMessages(List<MessageObj> list) {
		SQLiteDatabase db = getWritableDatabase();

		for(MessageObj mo : list) {
			ContentValues cv = new ContentValues();
			cv.put("id", mo.getId());
			cv.put("title", mo.getTitle());
			cv.put("message", mo.getMessage());
			cv.put("image", mo.getImage());
			cv.put("timestamp", mo.getTimestamp());
			cv.put("status", mo.getStatus());
			
			Cursor c = db.rawQuery("SELECT COUNT(*) FROM message WHERE id = ?", 
					new String[]{String.valueOf(mo.getId())});
			
			if(c.moveToNext() && c.getInt(0) != 0) {
				// it means the message is exist before
				Log.d(TAG, "need not to insert message with id " + mo.getId() + ", count in db " + c.getInt(0));
			} else {
				long s = db.insert("message", null, cv);
				Log.d(TAG, "insert message with id " + mo.getId() + ", " + s);
			}
			c.close();
		}
//		super.close();
		return list.size();
	}
	
	public int changeStatReadMessage(int messageId) {
		SQLiteDatabase db = getWritableDatabase();
	
		ContentValues cv = new ContentValues();
		cv.put("status", 1);
		
		int aff = db.update("message", cv, "id = " + messageId, null);
		
//		super.close();
		return aff;
	}
	
	public int syncAllMessages(List<MessageObj> list) {
		clearMessage(); // make sure it's empty
		return insertNewMessages(list);
	}
}
