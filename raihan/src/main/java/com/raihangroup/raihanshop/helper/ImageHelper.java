package com.raihangroup.raihanshop.helper;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Mohamad on 2/25/2015.
 */
public class ImageHelper {

    public static final int QUALITY = 86;
    public static final int MAX_SIZE = 1280;

    public static String getRealPathFromURI(Uri uri, Context ctx) {
        String result;
        Cursor cursor = ctx.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static Uri getMediaStorageUri() {
        String state = Environment.getExternalStorageState();
        if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;
        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }

    public static File getImageFile(String imagePath, Context ctx) {
        File image = null;
        Bitmap mBitmap = BitmapFactory.decodeFile(imagePath);

        final double MAX_SIZE = 512;
        double scaledWidth = 0;
        double scaledHeight = 0;
        image = new File(imagePath);

        if (mBitmap.getWidth() > MAX_SIZE || mBitmap.getHeight() > MAX_SIZE) {
            Log.d("resize image",
                    mBitmap.getWidth() + "x" + mBitmap.getHeight());
            if (mBitmap.getWidth() > mBitmap.getHeight()) {
                scaledWidth = MAX_SIZE;
                scaledHeight = MAX_SIZE * mBitmap.getHeight()
                        / mBitmap.getWidth();
            } else {
                scaledHeight = MAX_SIZE;
                scaledWidth = MAX_SIZE * mBitmap.getWidth()
                        / mBitmap.getHeight();
            }
            mBitmap = Bitmap.createScaledBitmap(mBitmap,
                    (int) scaledWidth, (int) scaledHeight, true);
            mBitmap = BitmapUtil.fixOrientation(ctx, mBitmap,Uri.fromFile(image));
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(image);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, out);
            } catch (Exception e) {
                Log.d("exception", String.valueOf(e));
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    Log.d("exception", String.valueOf(e));
                }
            }
        }
        return image;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
