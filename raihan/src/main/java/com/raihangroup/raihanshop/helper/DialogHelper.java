package com.raihangroup.raihanshop.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogHelper {

	public static void showMessageDialog(Context context, String title, 
			String message, String neutralButtonText, DialogInterface.OnClickListener neutralButtonListener) {
		
    	AlertDialog.Builder ad = new AlertDialog.Builder(context)
    		.setTitle(title)
    		.setMessage(message)
    		.setNeutralButton(neutralButtonText, neutralButtonListener);
    	AlertDialog alert = ad.create();
		alert.show();
	}
	
	public static void showMessageDialog(Context context, String title,
			String message, String neutralButtonText, DialogInterface.OnClickListener neutralButtonListener,
			DialogInterface.OnCancelListener cancelListener) {
		
    	AlertDialog.Builder ad = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(neutralButtonText, neutralButtonListener)
				.setCancelable(false);
        AlertDialog alert = ad.create();
        alert.setOnCancelListener(cancelListener);
        alert.show();
	}
	
	public static void showYesNoDialog(Context context, String title, String message, 
			String positiveButtonText, DialogInterface.OnClickListener positiveButtonListener,
			String negativeButtonText, DialogInterface.OnClickListener negativeButtonListener) {
		
    	AlertDialog.Builder ad = new AlertDialog.Builder(context)
    		.setTitle(title)
    		.setMessage(message)
    		.setPositiveButton(positiveButtonText, positiveButtonListener)
    		.setNegativeButton(negativeButtonText, negativeButtonListener);
    	AlertDialog alert = ad.create();
		alert.show();
	}
}
