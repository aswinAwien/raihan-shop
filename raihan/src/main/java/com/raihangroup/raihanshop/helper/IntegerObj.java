package com.raihangroup.raihanshop.helper;

public class IntegerObj {
	private int o;
	
	public IntegerObj(int i) {
		o = i;
	}
	
	public int add (int i) {
		o+=i;
		return o;
	}
	
	public int red (int i) {
		o-=i;
		return o;
	}

	public int getO() {
		return o;
	}

	public void setO(int o) {
		this.o = o;
	}
}
