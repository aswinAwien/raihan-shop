package com.raihangroup.raihanshop.helper;

/**
 * kelas berisi konstanta..
 */
public class Constants {

    // for tour app page
    public static final int TOUR_PAGE_ID_PRODUCT_CATALOGUE = 0;
    public static final int TOUR_PAGE_ID_CART = 1;
    public static final int TOUR_PAGE_ID_PRODUCT_BOOKING = 2;
    public static final int TOUR_PAGE_ID_PAYMENT_CONFIRM = 3;
    public static final int TOUR_PAGE_ID_DELIVERY_CONFIRM = 4;
    public static final int TOUR_PAGE_ID_HOW_TO_BOOKING_1 = 5;
    public static final int TOUR_PAGE_ID_HOW_TO_BOOKING_2 = 6;
    public static final int TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_1 = 7;
    public static final int TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_2 = 8;
    public static final int TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_3 = 9;
    public static final int TOUR_PAGE_ID_HOW_TO_CONFIRM_PAYMENT_4 = 10;
    public static final int TOUR_PAGE_ID_HOW_TO_CONFIRM_DELIVERY_1 = 11;
    public static final int TOUR_PAGE_ID_HOW_TO_CONFIRM_DELIVERY_2 = 12;

    // for tour list contents
    public static final int TOUR_LIST_ID_MAIN = 1;
    public static final int TOUR_LIST_ID_BOOKING = 2;
    public static final int TOUR_LIST_ID_PAYMENT = 3;
    public static final int TOUR_LIST_ID_DELIVERY = 4;

    //JNE stuff
    public static final String JNE_API_URL = "http://api.jne.co.id:8889/tracing/raihanshop/";
    public static final String JNE_API_KEY = "cce54293f99f3b5dca02f6736ca1a7e6";
    public static final String JNE_API_USERNAME = "RAIHANSHOP";

    public static final String URL_JNE_GET_DEST = JNE_API_URL + "dest/key/";
    public static final String URL_JNE_GET_ORGN = JNE_API_URL + "origin/key/";
    public static final String URL_JNE_GET_FARE = JNE_API_URL + "price/";

    //URL stuff
    public static final String BASE_URL = "https://raihanshop.com/";
//    public static final String BASE_URL = "http://dev.raihanshop.com/index.php/api/util";
//    public static final String BASE_URL = "http://dev2.raihanshop.com/";
//    public static final String BASE_URL = "http://192.168.1.6//raihanshop-fi/";

    public static final String API_URL = "index.php/api/";
    public static final String MAIN_URL = BASE_URL + API_URL;
    public static final String UPLOAD_URL = BASE_URL + "v2/upload/product/";
    public static final String MESSAGE_IMG_URL = BASE_URL + "v2/upload/message/";

    //API stuff
    public static final String URL_LOGIN_API = MAIN_URL + "login";
    public static final String URL_LUPAPASS_API = MAIN_URL + "forgotpassword";
    public static final String URL_EDPASS_API = MAIN_URL + "forgotpassword/edit";
    public static final String URL_REGISTER_API = MAIN_URL + "register";
    public static final String URL_REFERAL_API = MAIN_URL + "referal";
    public static final String URL_BOOK_API = MAIN_URL + "book";
    public static final String URL_GETBOOK_API = MAIN_URL + "book/list";
    public static final String URL_GET_NEW_PRODUCTS_API = MAIN_URL + "product/new";
    public static final String URL_GET_PRODUCTS_API = MAIN_URL + "product";
    public static final String URL_GET_PRODUCTS_CAT_API = MAIN_URL + "product/category";
    public static final String URL_SHIPPING_API = MAIN_URL + "shipping_cost";
    public static final String URL_PRODDETAIL_API = MAIN_URL + "product/detail";
    public static final String URL_PRODSEARCH_API = MAIN_URL + "product/search";
    public static final String URL_ORDER_API = MAIN_URL + "order";
    public static final String URL_GETORDER_API = MAIN_URL + "order/list";
    public static final String URL_PERFSUM_API = MAIN_URL + "performance";
    public static final String URL_SPERF_API = MAIN_URL + "performance/self";
    public static final String URL_TPERF_API = MAIN_URL + "performance/team";
    public static final String URL_TTSPERF_API = MAIN_URL + "performance/tts";
    public static final String URL_HOME_INFO_API = MAIN_URL + "performance/dashboard";
    public static final String URL_GETCART_API = MAIN_URL + "cart";
    public static final String URL_ADDCART_API = MAIN_URL + "cart/add";
    public static final String URL_EDCART_API = MAIN_URL + "cart/edit";
    public static final String URL_RMCART_API = MAIN_URL + "cart/remove";
    public static final String URL_JCART_API = MAIN_URL + "cart/total";
    public static final String URL_ADMGETRESERVED_API = MAIN_URL + "cart/reserved";
    public static final String URL_MESSAGE_ALL_API = MAIN_URL + "message";
    public static final String URL_MESSAGE_NEWEST_API = MAIN_URL + "message/newest";
    public static final String URL_READMESSAGE_API = MAIN_URL + "message/read";
    public static final String URL_CATEGORY_LIST_API = MAIN_URL + "product/list_category";
    public static final String URL_CATEGORY_PARENT_LIST_API = MAIN_URL + "product/category_parent";
    public static final String URL_VERSION_APP_API = MAIN_URL + "util";
    public static final String URL_SET_PRODUCTS_READ_API = MAIN_URL + "product/read";
    public static final String URL_SUPPLIER_LIST_API = MAIN_URL + "product/list_supplier";
    public static final String URL_SUPPLIER_DETAIL_API = MAIN_URL + "product/supplier";
    public static final String URL_SUPPLIER_PROFILE_API = MAIN_URL + "profile/supplier";
    public static final String URL_SUPPLIER_REVIEW_API = MAIN_URL + "profile/supplier_review";
    public static final String URL_SUPPLIER_PERFORMANCE_API = MAIN_URL + "profile/supplier_performance";
    public static final String URL_USER_PROFILE_API = MAIN_URL + "profile";
    public static final String URL_USER_PROFILE_STATUS_API = MAIN_URL + "status/displaystatusbyid";
    public static final String URL_EDIT_USER_PROFILE_API = MAIN_URL + "profile/edit";
    public static final String URL_CITY_API = MAIN_URL + "location/city";
    public static final String URL_SUB_DISTRICT_API = MAIN_URL + "/location/sub_district";
    public static final String URL_PROVINCE_API = MAIN_URL + "location/province";
    public static final String URL_FEEDBACK_LIST_API = MAIN_URL + "message/list_feedback";
    public static final String URL_FEEDBACK_REPLY_LIST_API = MAIN_URL + "message/list_feedback_reply";
    public static final String URL_POST_FEEDBACK_API = MAIN_URL + "message/submit_feedback";
    public static final String URL_POST_REPLY_FEEDBACK_API = MAIN_URL + "message/submit_feedback_reply";
    public static final String URL_LIST_SUPPLIER_FOR_RATING_API = MAIN_URL + "profile/list_supplier_by_order";
    public static final String URL_RATE_SUPPLIER_API = MAIN_URL + "profile/rate_supplier";
    public static final String URL_COMPLAIN_DROPDOWN_API = MAIN_URL + "message/get_complain_dropdown";
    public static final String URL_LIST_GROUP_CHAT = MAIN_URL + "chat/room";
    public static final String URL_REPLY_CHAT = MAIN_URL + "chat/reply";
    public static final String URL_GET_NOTIFICATION = MAIN_URL + "notification";
    public static final String URL_READ_NOTIFICATION = MAIN_URL + "notification/read";
    public static final String URL_LOCK_CART = MAIN_URL + "cart/lock_order";
    public static final String URL_UPDATE_STATUS = MAIN_URL + "status/updatestatus";
    public static final String URL_SELLING_USER = MAIN_URL + "performance/money_detail";

    public static final String URL_GET_FRIEND = MAIN_URL + "/friends/getfriends";

    public static final String INTENT_PRODUCT_ID = "com.bi.raihan.prodid";
    public static final String INTENT_PRODUCT_DETAIL = "com.bi.raihan.proddetail";
    public static final String INTENT_USER_OBJ = "com.bi.raihan.userobj";
    public static final String INTENT_CART_2_BOOK = "com.bi.raihan.cart2book";
    public static final String INTENT_CART_ADMIN_RESELLERID = "com.bi.raihan.idResellerforAdmin";
    public static final String INTENT_BOOK_2_ORD = "com.bi.raihan.book2order";

    public static final String INTENT_ALTERNATE_SENDER = "com.bi.raihan.altersender";
    public static final String INTENT_PAYMENT_INFO = "com.bi.raihan.paymentinfo";
    public static final String INTENT_COMISSION_INFO = "com.bi.raihan.komissiinfo";
    public static final String INTENT_REKENINGOWNER_INFO = "com.bi.raihan.yangpunyarekeninginfo";
    public static final String INTENT_NOREK_INFO = "com.bi.raihan.nomorrekeninginfo";
    public static final String INTENT_BANK_INFO = "com.bi.raihan.bankInfo";
    public static final String INTENT_JUMLAH_PAYMENT = "com.bi.raihan.jumlahPaid";
    public static final String INTENT_DATE_PAID = "com.bi.raihan.kapanKirimUang";

    public static final String INTENT_ONGKIR_TOTAL_INFO = "com.bi.raihan.totalongkir";
    public static final String INTENT_RELOAD_HOME = "com.bi.raihan.reloadbooklah";

    public static final String INTENT_ORDER_NUMBER = "com.bi.raihan.nomerorderah";
    public static final String INTENT_PERFORMANCE_MONTH = "com.bi.raihan.bulanperformance";
    public static final String INTENT_PERFORMANCE_YEAR = "com.bi.raihan.yearperformance";
    public static final String INTENT_PERFORMANCE_ID = "com.bi.raihan.useridperformance";
    public static final String INTENT_SPERFORMANCE_PS_START = "com.bi.raihan.psstartself";
    public static final String INTENT_SPERFORMANCE_PS_END = "com.bi.raihan.psendself";
    public static final String INTENT_TPERFORMANCE_CLASS = "com.bi.raihan.kelasJabatan";
    public static final String INTENT_SELFPERFORMANCE_MODE = "com.bi.raihan.selfperformancemode";
    public static final String PACKAGE_NAME_PROVIDER = "com.bi.raihan.provider";

    public static final String INTENT_CHANGEDETAIL_PRODDETAIL_MODE = "com.bi.raihan.gantinominaldiprodctdetail";
    public static final String INTENT_CHANGEDETAIL_PRODDETAIL_ID = "com.bi.raihan.gantinominalidbook";
    public static final String INTENT_CHANGEDETAIL_PRODDETAIL_CL = "com.bi.raihan.gantinominaldiprodctdetail.color";
    public static final String INTENT_CHANGEDETAIL_PRODDETAIL_SZ = "com.bi.raihan.gantinominaldiprodctdetail.size";
    public static final String INTENT_CHANGEDETAIL_PRODDETAIL_JM = "com.bi.raihan.gantinominaldiprodctdetail.jumlah";
    public static final String INTENT_CHANGEDETAIL_PRODDETAIL_STR = "com.bi.raihan.jenisganti";

    public static final String INTENT_NOTIFICATION_OBJ = "com.bi.raihan.objeknotifikasi";
    public static final String INTENT_MESSAGE_OBJ = "com.bi.raihan.objekpesan";

    public static final String INTENT_FORGET_P = "com.bi.raihan.forgetpass";

    public static final String INTENT_BOOKING_NAME = "com.bi.raihan.bookingName";

    public static final String INTENT_CONFIRMATION_FROM = "com.bi.raihan.confirmationFrom";
    public static final String INTENT_RETURN_PRODUCT = "com.bi.raihan.ReturnProductActivity";
    public static final String INTENT_REVIEW_WITH_RATING = "com.bi.raihan.ReviewWithRatingActivity";

    public static final String BUNDLE_PERF_MONTH = "com.bi.raihan.bundlebulan";
    public static final String BUNDLE_PERF_YEAR = "com.bi.raihan.bundletahun";


    //constants in projects
    public static final int NOT_FOUND_DEFAULT = -404;
    public static final int ACTIVITY_BACK_SO_SAVE = -1;

    public static final int GROUP_UPLINE = 1;
    public static final int GROUP_DOWNLINE = 2;

    public static final String GCM_PROJECT_ID = "148546213268";
    public static final int NOTIF_ID_TEST = 1;

    public static final int RELOAD_BOOK = 1;
    public static final int RELOAD_ORD = 2;
    public static final int RELOAD_PRODUCT_LIST = 4;

    public static final int HTTP_RESPONSE_OK = 200;
    public static final int HTTP_RESPONSE_INTERNALSERVERERROR = 500;
    public static final int HTTP_RESPONSE_NETWORKERROR = 101;
    public static final int HTTP_RESPONSE_PAGENOTFOUND = 404;

    public static final String STATUS_UNCONFIRMED = "0";
    public static final String STATUS_CONFIRMED = "1";

    public static final int NOTIFICATION_AFTER_REGISTER = 1;
    public static final int NOTIFICATION_BROADCAST_INBOX = 2;
    public static final int NOTIFICATION_PERFORMANCE = 3;
    public static final int NOTIFICATION_ORDER = 4;
    public static final int NOTIFICATION_FEE = 5;
    public static final int NOTIFICATION_MEMBER = 6;
    public static final int NOTIFICATION_FEEDBACK_REPLY = 7;
    public static final int NOTIFICATION_BIRTHDAY = 8;
    public static final int NOTIFICATION_VOLUME_REQUIRED = 9;
    public static final int NOTIFICATION_RETUR_ORDER = 10;
    public static final int NOTIFICATION_KOMISI = 11;
    public static final int NOTIFICATION_GET_FEE = 12;


    /**
     * Special case for shipping method id.
     */
    public static final int POS_SHIP_METHOD_ID = 4;
    public static final String USER_ID_PERFORMANCE = "userIdPerformance";
    public static final String REGISTER_LINKS = "registerLink";

    //new api
    public static String URL_GET_FRIENDS = "http://raihanshop.com/index.php/api/friends/getfriends";
}