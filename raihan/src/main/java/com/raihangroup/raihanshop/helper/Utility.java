package com.raihangroup.raihanshop.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.raihangroup.raihanshop.ListMap;
import com.raihangroup.raihanshop.ModelChannel;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.model.Category;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Utility {

    public static Handler mainThread = new Handler(Looper.getMainLooper());

    public static void showDialogError(Context ctx, String msg, String title) {
        new AlertDialog.Builder(ctx)
                .setIcon(android.R.drawable.ic_dialog_alert).setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create().show();
    }

    public static void showDialogSuccess(Context ctx, String msg, String title) {
        new AlertDialog.Builder(ctx).setTitle(title).setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                }).create().show();
    }

    public static View getParentWithTag(View v, int tag) {
        ViewParent p = v.getParent();
        if (p != null && p instanceof View) {
            Object o = ((View) p).getTag(tag);
            if (o != null) {
                return (View) p;
            } else {
                return getParentWithTag((View) p, tag);
            }
        }
        return null;
    }

    public static void snackBar(Context context,CoordinatorLayout coordinatorLayout,String message){
        Snackbar snackbar = Snackbar.make(coordinatorLayout,message,Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public static HashMap<String, List<View>> getViewsID(View mView) {
        ListMap<String, View> result = new ListMap<String, View>();
        if (mView == null) {
            return result;
        }
        return getViewsID(mView, result, mView.getContext().getResources());
    }

    public static ListMap<String, View> getViewsID(View mView, ListMap<String, View> result, Resources resources) {
        if (mView.getId() != View.NO_ID) {
            try {
                result.add(resources.getResourceEntryName(mView.getId()), mView);
            } catch (Exception e) {
            }
        }
        if (mView instanceof ViewGroup && !(mView instanceof AdapterView || mView instanceof ViewPager)) {
            ViewGroup mContainer = (ViewGroup) mView;
            final int mCount = mContainer.getChildCount();

            for (int i = 0; i < mCount; ++i) {
                getViewsID(mContainer.getChildAt(i), result, resources);
            }
        }
        return result;
    }

    public static List<View> getAllView(ViewGroup view) {
        ArrayList<View> viewList = new ArrayList<View>();
        return getAllView(view, viewList);
    }

    private static List<View> getAllView(ViewGroup view, ArrayList<View> viewList) {
        for (int x = 0; x < view.getChildCount(); ++x) {
            View child = view.getChildAt(x);
            if (child instanceof ViewGroup) {
                getAllView((ViewGroup) child, viewList);
            } else {
                viewList.add(child);
            }
        }
        return viewList;
    }

    public interface ReturnInterface<M> {
        void get(M obj);
    }

    public static final String POSITIVE = "Ok";
    public static final String NEGATIVE = "Cancel";

    public static <T extends ModelChannel> Dialog chooseDialog2(Context ctx, String title, final ReturnInterface<T> listener, final T... obj) {
        AlertDialog.Builder ad = new AlertDialog.Builder(ctx);
        if (title != null) ad.setTitle(title);
        ad.setAdapter(ModelChannel.getAdapter(ctx, Arrays.asList(obj)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.get(obj[which]);
                    }
                }
        );
        AlertDialog alert = ad.create();
        alert.show();
        return alert;
    }

    public static void copy(File src, File dst) throws IOException {
        if (!dst.getParentFile().exists()) dst.getParentFile().mkdirs();
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);
        Log.v("Copy:", src + ":" + dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static void saveToGallery(final File filePath, String description, final Context context) {
        ContentValues values = new ContentValues();

        values.put(Images.Media.TITLE, context.getResources().getString(R.string.app_name));
        values.put(Images.Media.DISPLAY_NAME, description);
        values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath.getAbsolutePath());

        context.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isNull(String str) {
        return str == null;
    }

    public static boolean isNullOrBlank(String param) {
        return isNull(param) || param.trim().length() == 0;
    }

    public static String formatPrice(int ff) {
        return "Rp. " + NumberFormat.getInstance(Locale.ITALY).format(ff);
    }

    public static int deformatPrice(String leS) {
        return Integer.parseInt(leS.substring(4).replace(".", ""));
    }

    /**
     * padding (9 -> 09, 12 -> 12) dan balikin ke string
     *
     * @param c int
     * @return string hasil padding
     */
    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + c;
        }
    }

    public static int convertStringtoInt(String str) {
        int value = 0;
        try {
            value = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            value = -1;
        }
        return value;
    }

    public static String getDateTimeNow() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        String today = year + "-" + Utility.pad(month) + "-"
                + Utility.pad(day) + " " + Utility.pad(hour) + ":"
                + Utility.pad(minute) + ":" + Utility.pad(second);
        return today;
    }

    public static long getNowInLong() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static String toSHA1(String text) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();

        return convertToHex(sha1hash);
    }

    public static String toMD5(String text) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());

        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    /**
     * diambil dari project MKIshell
     */
    public static Uri getCameraUri(Activity activity) {
        String[] projection = {
                MediaStore.Images.Thumbnails._ID, // The columns we want
                MediaStore.Images.Thumbnails.IMAGE_ID,
                MediaStore.Images.Thumbnails.KIND,
                MediaStore.Images.Thumbnails.DATA};
        String selection = MediaStore.Images.Thumbnails.KIND + "=" + // Select
                // only
                // mini's
                MediaStore.Images.Thumbnails.MINI_KIND;
        String sort = MediaStore.Images.Thumbnails._ID + " DESC";

        // At the moment, this is a bit of a hack, as I'm returning ALL images,
        // and just taking the latest one. There is a better way to narrow this
        // down I think with a WHERE clause which is currently the selection
        // variable
        Cursor myCursor = activity.getContentResolver().query(
                MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, projection,
                selection, null, sort);

        long imageId = 0l;
        long thumbnailImageId = 0l;
        String thumbnailPath = "";

        try {
            myCursor.moveToFirst();
            imageId = myCursor
                    .getLong(myCursor
                            .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.IMAGE_ID));
            thumbnailImageId = myCursor.getLong(myCursor
                    .getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID));
            thumbnailPath = myCursor.getString(myCursor
                    .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
        } finally {
            myCursor.close();
        }

        // Create new Cursor to obtain the file Path for the large image
        String[] largeFileProjection = {MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA};

        String largeFileSort = MediaStore.Images.ImageColumns._ID + " DESC";
        myCursor = activity.managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                largeFileProjection, null, null, largeFileSort);
        String largeImagePath = "";

        try {
            myCursor.moveToFirst();

            // This will actually give yo uthe file path location of the image.
            largeImagePath = myCursor.getString(myCursor
                    .getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA));
        } finally {
            myCursor.close();
        }

        // These are the two URI's you'll be interested in. They give you a
        // handle to the actual images
        Uri uriLargeImage = Uri.withAppendedPath(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                String.valueOf(imageId));

        Uri uriThumbnailImage = Uri.withAppendedPath(
                MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                String.valueOf(thumbnailImageId));

        return uriLargeImage;
    }

    public static int generateIdCatChooser(Category c) {
        return 12340 + c.id;
    }

    public static int decodeIdCatChooser(int id) {
        return id - 12340;
    }

    public static String idKelastoName(int id) {
        String tmp = "";
        switch (id) {
            case 0:
                tmp = "Personal Sales (PS)";
                break;
            case 1:
                tmp = "Bintang 1";
                break;
            case 2:
                tmp = "Bintang 2";
                break;
            case 3:
                tmp = "Manager";
                break;
            case 4:
                tmp = "Senior Manager";
                break;
            case 5:
                tmp = "Director";
                break;
            case 6:
                tmp = "Head Director";
                break;
            case 7:
                tmp = "Gold Director";
                break;
            default:
                break;
        }

        return tmp;
    }

    public static int dipToPx(int dip, Resources res) {
        return
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, res.getDisplayMetrics());
    }

    public static int spToPx(int sp, Resources res) {
        return
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, res.getDisplayMetrics());
    }

    public static boolean isAdmin() {
        try {
            return PrefStorage.instance.getKelas().equals("Admin");
        } catch (Exception e) {
            return false;
        }

    }


    public static String toTitleCase(String str) {
        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    /**
     * Clear focus from view.
     *
     * @param context
     * @param v
     */
    public static void clearSoftInputFromView(Context context, View v) {
        // dismiss the keyboard
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        // clear focus
        v.clearFocus();
    }
}
