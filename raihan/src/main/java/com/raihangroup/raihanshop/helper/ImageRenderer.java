package com.raihangroup.raihanshop.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

// to render image
public class ImageRenderer extends AsyncTask<String, String, Bitmap> {
	ImageView pict;
	Intent intent;
	private ProgressDialog pDialog;
	private Bitmap bitmap;
    Uri imageUri;
	public ImageRenderer(ImageView bmImage, Intent intent) {
		this.intent = intent;
		this.pict = bmImage;
	}

    public ImageRenderer(ImageView bmImage, Intent intent, String imagepath) {
        this.intent = intent;
        this.pict = bmImage;
        imageUri = Uri.fromFile(new File(imagepath));
    }

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(pict.getContext());
		pDialog.setMessage("Loading Image...\nPlease Wait");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Bitmap doInBackground(String... params) {
        Bitmap result =null;
		try {
			WindowManager wm = (WindowManager) pict.getContext()
					.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			float scale = (float)display.getWidth() / (float)pict.getWidth();
			bitmap = decodeSampledBitmapFromStream(
                    pict.getContext(), intent.getData(), display.getWidth(),
                    (int) (scale * pict.getHeight()));
             result = BitmapUtil.fixOrientation(pict.getContext(),bitmap,imageUri);
			Log.d("debug", "1 = " + bitmap);

		} catch (Exception e) {
			Log.d("Error asynctask", "" + e);
		}
		return result;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		pDialog.dismiss();
		Log.d("debug", "result = " + result);
		if (result != null) {
			pict.setImageBitmap(result);
            pict.setVisibility(View.VISIBLE);
		}
	}

    public static Bitmap decodeSampledBitmapFromStream(Context context, Uri uri,
                                                       int reqWidth, int reqHeight) throws IOException {
        InputStream is = context.getContentResolver()
                .openInputStream(uri);

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        is.close();
        is = context.getContentResolver()
                .openInputStream(uri);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);
        is.close();
        return bitmap;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                                        int reqHeight) { // BEST QUALITY MATCH

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.d("debug", "" + width);
        Log.d("debug", "" + height);
        Log.d("debug", "" + reqHeight);
        Log.d("debug", "" + reqWidth);

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}
