package com.raihangroup.raihanshop.helper;

/**
 * Created by ocit on 9/16/15.
 */
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.raihangroup.raihanshop.model.Person;
import com.linkedin.android.spyglass.mentions.Mentionable;
import com.linkedin.android.spyglass.tokenization.QueryToken;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple class to get suggestions from a JSONArray (represented as a file on disk), which can then
 * be mentioned by the user by tapping on the suggestion.
 */
public  abstract class MentionsLoader<T extends Mentionable> {

    protected T[] mData;
    private static final String TAG = MentionsLoader.class.getSimpleName();

    private List<Person> data;

    public MentionsLoader(List<Person> data) {
        this.data = data;
        mData = loadData(getArray(data));

    }

    public abstract T[] loadData(String[] arr);

    // Returns a subset
    public List<T> getSuggestions(QueryToken queryToken) {
        String prefix = queryToken.getKeywords().toLowerCase();
        List<T> suggestions = new ArrayList<>();
        if (mData != null) {
            for (T suggestion : mData) {
                String name = suggestion.getSuggestiblePrimaryText().toLowerCase();
                if (name.startsWith(prefix)) {
                    suggestions.add(suggestion);
                }
//                System.out.println(name);
            }
        }
        return suggestions;
    }

    public String[] getArray(List<Person> data){
        String[] read = new String[0];
        for(int i = 0; i < data.size(); i++){
            read[i] = data.get(i).fullname;
            System.out.println(read[i]);
        }
        return read;
    }
}