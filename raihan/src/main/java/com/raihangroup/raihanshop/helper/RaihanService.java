package com.raihangroup.raihanshop.helper;

import com.raihangroup.raihanshop.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RaihanService {
    @FormUrlEncoded
    @POST("post")
    Call<Product> getProductList(@Field("mode") int mode,@Field("page")int page);
}
