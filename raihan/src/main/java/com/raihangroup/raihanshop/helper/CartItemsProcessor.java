package com.raihangroup.raihanshop.helper;

import android.util.Log;

import com.raihangroup.raihanshop.model.CartItem;

import java.util.HashMap;
import java.util.List;

public class CartItemsProcessor {

	public static final String TAG = "Cart Items Proc";
	
	/**
	 * Collect weight from each cart item in associated Supplier Shipping Calculation (SSC) 
	 * instance. After collection process, we get unique total weight per supplier id.
	 * @param listOfCartItems, list of cart item
	 * @param isSetServiceShipping, if true, set service cost to SSC instance
	 * @return
	 */
	public static HashMap<String, SupplierShipCalc> collectWeightPerSupplierId(
			List<CartItem> listOfCartItems, boolean isSetServiceShipping) {
		HashMap<String, SupplierShipCalc> hm = new HashMap<String, SupplierShipCalc>();
		for(CartItem ci : listOfCartItems) {
			Log.d(TAG, "cart: id " + ci.id_supplier + ", name " + ci.getName()
					+ ", price " + ci.getPrice() + ", qty " + ci.getQty()
					+ ", weight " + ci.getWeight() + ", shipping cost " + ci.shipping_cost);
			
			if(hm.containsKey(ci.id_supplier)) {
				// update data in hash map
				SupplierShipCalc sscFromHm = hm.get(ci.id_supplier);
				sscFromHm.addCartItemWeight(ci);
			} else {
				// add new data to hash map
				SupplierShipCalc ssc = new SupplierShipCalc(ci.id_supplier);
				if(isSetServiceShipping) {
					ssc.setServicePrice(ci.shipping_cost);
					ssc.setServiceMethodId(ci.shipping_method_id);
				}
				ssc.addCartItemWeight(ci);
				
				hm.put(ci.id_supplier, ssc);
			}
		}
		
		Log.d(TAG, hm.size() + " supplier ship calculation objects");
		return hm;
	}
}
