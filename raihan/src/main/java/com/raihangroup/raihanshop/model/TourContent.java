package com.raihangroup.raihanshop.model;

/**
 * Created by Alh on 4/20/15.
 */
public class TourContent {

    public int tourId;
    public String title;
    public String description;
    public int imageRes;

    public TourContent(int tourId, String title, String description, int imageRes) {
        this.tourId = tourId;
        this.title = title;
        this.description = description;
        this.imageRes = imageRes;
    }
}
