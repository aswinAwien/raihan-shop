package com.raihangroup.raihanshop.model;

public class SubKoordinat {
	
	private int id_sub_sub_coordinate; //either ini
	private int id_reseller; //atau ini
	private int current_ps;
	private int target_ps;
	private String label;
	private String label_target;
	private int current_state;
	private int target_state;
	
	public int getId_reseller() {
		return id_reseller;
	}
	public void setId_reseller(int id_reseller) {
		this.id_reseller = id_reseller;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getLabel_target() {
		return label_target;
	}
	public void setLabel_target(String label_target) {
		this.label_target = label_target;
	}
	public int getCurrent_state() {
		return current_state;
	}
	public void setCurrent_state(int current_state) {
		this.current_state = current_state;
	}
	public int getTarget_state() {
		return target_state;
	}
	public void setTarget_state(int target_state) {
		this.target_state = target_state;
	}
	public int getCurrent_ps() {
		return current_ps;
	}
	public void setCurrent_ps(int current_ps) {
		this.current_ps = current_ps;
	}
	public int getTarget_ps() {
		return target_ps;
	}
	public void setTarget_ps(int target_ps) {
		this.target_ps = target_ps;
	}
	public int getId_sub_sub_coordinate() {
		return id_sub_sub_coordinate;
	}
	public void setId_sub_sub_coordinate(int id_sub_sub_coordinate) {
		this.id_sub_sub_coordinate = id_sub_sub_coordinate;
	}
}
