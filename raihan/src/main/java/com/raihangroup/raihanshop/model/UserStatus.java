package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ocit on 8/11/15.
 */

public class UserStatus implements Parcelable {
    public String id;
    public String id_creator;
    public String name_creator;
    public String content;
    public String created;
    public String updated;
    public String image;

    public UserStatus(Parcel in) {
        String[] data = new String[7];
        in.readStringArray(data);

        this.id = data[0];
        this.id_creator = data[1];
        this.name_creator = data[2];
        this.content = data[3];
        this.created = data[4];
        this.updated = data[5];
        this.image = data[6];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeStringArray(new String[] { id, id_creator, name_creator,
                content, created, updated, image });
    }

    public static final Creator<UserStatus> CREATOR = new Parcelable.Creator<UserStatus>() {

        @Override
        public UserStatus createFromParcel(Parcel source) {
            return new UserStatus(source);
        }

        @Override
        public UserStatus[] newArray(int size) {
            return new UserStatus[size];
        }
    };

}