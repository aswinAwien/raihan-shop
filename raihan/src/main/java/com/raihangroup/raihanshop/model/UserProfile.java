package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserProfile implements Parcelable {
	public String id;
	public String id_level;
	public String referal;
	public String email;
	public String password;
	public String identity_number;
	public String fullname;
	public String shop_name;
	public String birth_date;
	public String birth_place;
	public String phonenumber;
	public String job;
	public String id_bank;
	public String bank_name;
	public String account_bank;
	public String address;
	public String province;
	public String city;
	public String sub_district;
	public String zipcode;
	public String level_updated;
	public String level_updated_status;
	public String status;
	public String created;
	public String last_updated;
	public String deleted;
	public String gcm_regid;
	public String total_sales;
	public String is_marketing;
	public String level_name;
	public String province_name;
	public String city_name;
	public String sub_district_name;
    public String image;
	
	public UserProfile(Parcel in) {
		String[] data = new String[34];
		in.readStringArray(data);
		
		this.id = data[0];
		this.id_level = data[1];
		this.referal = data[2];
		this.email = data[3];
		this.password = data[4];
		this.identity_number = data[5];
		this.fullname = data[6];
		this.shop_name = data[7];
		this.birth_date = data[8];
		this.birth_place = data[9];
		this.phonenumber = data[10];
		this.job = data[11];
		this.id_bank = data[12];
		this.bank_name = data[13];
		this.account_bank = data[14];
		this.address = data[15];
		this.province = data[16];
		this.city = data[17];
		this.sub_district = data[18];
		this.zipcode = data[19];
		this.level_updated = data[20];
		this.level_updated_status = data[21];
		this.status = data[22];
		this.created = data[23];
		this.last_updated = data[24];
		this.deleted = data[25];
		this.gcm_regid = data[26];
		this.total_sales = data[27];
		this.is_marketing = data[28];
		this.level_name = data[29];
		this.province_name = data[30];
		this.city_name = data[31];
		this.sub_district_name = data[32];
        this.image = data[33];
	}

	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int arg1) {
		out.writeStringArray(new String[] {
				 id, id_level, referal, email, password, identity_number, fullname, 
				 shop_name, birth_date, birth_place, phonenumber, job, id_bank, bank_name, 
				 account_bank, address, province, city, sub_district, zipcode, level_updated, 
				 level_updated_status, status, created, last_updated, deleted, gcm_regid, total_sales, 
				 is_marketing, level_name, province_name, city_name, sub_district_name, image
		});
	}

	public static final Creator<UserProfile> CREATOR = new Parcelable.Creator<UserProfile>() {

		@Override
		public UserProfile createFromParcel(Parcel source) {
			return new UserProfile(source);
		}

		@Override
		public UserProfile[] newArray(int size) {
			return new UserProfile[size];
		}
	};
}
