package com.raihangroup.raihanshop.model;

public class FeedbackReply {

	public String id;
	public String feedback_id;
	public String id_from;
	public String id_to;
    public String id_admin;
    public String id_supplier;
	public String message;
    public String image;
	public int status;
	public String date;
}
