package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.support.annotation.NonNull;
import com.linkedin.android.spyglass.mentions.Mentionable;
import com.linkedin.android.spyglass.tokenization.QueryToken;

import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ocit on 9/14/15.
 */

public class Friend implements Mentionable {

    private final String mName;

    public Friend(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    // --------------------------------------------------
    // Mentionable Implementation
    // --------------------------------------------------

    @NonNull
    @Override
    public String getTextForDisplayMode(MentionDisplayMode mode) {
        switch (mode) {
            case FULL:
                return mName;
            case PARTIAL:
            case NONE:
            default:
                return "";
        }
    }

    @Override
    public MentionDeleteStyle getDeleteStyle() {
        return MentionDeleteStyle.PARTIAL_NAME_DELETE;
    }

    @Override
    public int getSuggestibleId() {
        return mName.hashCode();
    }

    @Override
    public String getSuggestiblePrimaryText() {
        return mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
    }

    public Friend(Parcel in) {
        mName = in.readString();
    }

    public static final Parcelable.Creator<Friend> CREATOR
            = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };

    // --------------------------------------------------
    // Friend Class (loads friends from JSON file)
    // --------------------------------------------------


    public static class FriendLoader extends FriendMentionsLoader<Friend> {
        private static final String TAG = Kota.CityLoader.class.getSimpleName();

        public FriendLoader(List<Person> data) {
            super(data);
        }

        @Override
        public Friend[] loadData(JSONArray arr) {
            Friend[] data = new Friend[arr.length()];
            try {
                for (int i = 0; i < arr.length(); i++) {
                    data[i] = new Friend(arr.getString(i));
                }
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception while parsing city JSONArray", e);
            }
            return data;
        }
    }
}

/**
 * Simple class to get suggestions from a JSONArray (represented as a file on disk), which can then
 * be mentioned by the user by tapping on the suggestion.
 */
abstract class FriendMentionsLoader<T extends Mentionable> {

    protected T[] mData;
    private static final String TAG = MentionsLoader.class.getSimpleName();

    public FriendMentionsLoader(List<Person> data) {
        returnJSONArray(data);
    }

    public abstract T[] loadData(JSONArray arr);

    // Returns a subset
    public List<T> getSuggestions(QueryToken queryToken) {
        String prefix = queryToken.getKeywords().toLowerCase();
        List<T> suggestions = new ArrayList<>();
        if (mData != null) {
            for (T suggestion : mData) {
                String name = suggestion.getSuggestiblePrimaryText().toLowerCase();
                if (name.startsWith(prefix)) {
                    suggestions.add(suggestion);
                }
            }
        }
        return suggestions;
    }

    private JSONArray returnJSONArray(List<Person> arr) {
        JSONArray jsonArray = new JSONArray();
        for(int i =0; i<arr.size(); i++) {
            jsonArray.put("@"+arr.get(i).fullname);
            System.out.println(arr.get(i).fullname);
        }
        System.out.println(jsonArray.toString());
        mData = loadData(jsonArray);
        return jsonArray;
    }
}