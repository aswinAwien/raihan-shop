package com.raihangroup.raihanshop.model;

import java.util.List;

public class SupplierShippingCost {
	
	/** Id of supplier. */
	public String supplier_id;
	/** Location name of supplier. */
	public String location;
	/** Full name of supplier. */
	public String fullname;
	public List<ServiceShippingCost> shipping;
}
