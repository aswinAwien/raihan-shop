package com.raihangroup.raihanshop.model;

public class City {

	public String id;
	public String id_province;
	public String title;

	public City(String id, String id_province, String title) {
		this.id = id;
		this.id_province = id_province;
		this.title = title;
	}

	@Override
	public String toString() {
		return title;
	}
}
