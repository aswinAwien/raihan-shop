package com.raihangroup.raihanshop.model;

import java.util.List;

/**
 * Dashboard info. Used by gson (dashboard performance).
 * @author Alh
 *
 */
public class Home {
	/** Current PS. */
	public float ps;
	public float target_ps;
	/** Current TTS. */
	public float tts;
	public float target_tts;
	public String level;
	public String next_level;
	public int commission;
	public MemberPerformance highest;
	public MemberPerformance lowest;
	public List<Update> updates;
	public List<Product> products;
    public int downline_count;
}
