package com.raihangroup.raihanshop.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Product implements Serializable{
	private static final long serialVersionUID = 4353707736694976495L;

	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("stock")
	@Expose
	private List<Stock> stock;
	@SerializedName("prices")
	@Expose
	private List<Price> prices;
	@SerializedName("images")
	@Expose
	private List<MImage> images;
	@SerializedName("category")
	@Expose
	private String category;
	@SerializedName("commission")
	@Expose
	private int commission;
	@SerializedName("fee")
	@Expose
	private int fee;
	@SerializedName("volume")
	@Expose
	private String volume;
	@SerializedName("location")
	@Expose
	private String location;
	@SerializedName("updated_time")
	@Expose
	private String updated_time;
	@SerializedName("is_new")
	@Expose
	private int is_new;
	@SerializedName("is_read")
	@Expose
	public int is_read; // only in new product
	public float product_quality;
	@SerializedName("fullname")
	@Expose
	public String fullname;
	@SerializedName("id_supplier")
	@Expose
	public int id_supplier;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Stock> getStock() {
		return stock;
	}
	public void setStock(List<Stock> stock) {
		this.stock = stock;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getUpdated_time() {
		return updated_time;
	}
	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Price> getPrices() {
		return prices;
	}
	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}
	public int getCommission() {
		return commission;
	}
	public void setCommission(int commission) {
		this.commission = commission;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<MImage> getImages() {
		return images;
	}
	public void setImages(List<MImage> images) {
		this.images = images;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public int getIs_new() {
		return is_new;
	}
	public void setIs_new(int is_new) {
		this.is_new = is_new;
	}
}

//public class Product implements Serializable {
//	private static final long serialVersionUID = 4353707736694976495L;
//
//	@SerializedName("id")
//	@Expose
//	private String id;
//	@SerializedName("name")
//	@Expose
//	private String name;
//	@SerializedName("supplier")
//	@Expose
//	private Object supplier;
//	@SerializedName("fullname")
//	@Expose
//	private Object fullname;
//	@SerializedName("id_supplier")
//	@Expose
//	private Integer idSupplier;
//	@SerializedName("category")
//	@Expose
//	private String category;
//	@SerializedName("category_array")
//	@Expose
//	private CategoryArray categoryArray;
//	@SerializedName("description")
//	@Expose
//	private String description;
//	@SerializedName("stock")
//	@Expose
//	private List<Stock> stock = null;
//	@SerializedName("price")
//	@Expose
//	private String price;
//	@SerializedName("prices")
//	@Expose
//	private List<Price> prices = null;
//	@SerializedName("images")
//	@Expose
//	private List<Image> images = null;
//	@SerializedName("image1")
//	@Expose
//	private String image1;
//	@SerializedName("image2")
//	@Expose
//	private String image2;
//	@SerializedName("image3")
//	@Expose
//	private Object image3;
//	@SerializedName("image4")
//	@Expose
//	private Object image4;
//	@SerializedName("image5")
//	@Expose
//	private Object image5;
//	@SerializedName("image6")
//	@Expose
//	private Object image6;
//	@SerializedName("commission")
//	@Expose
//	private String commission;
//	@SerializedName("fee")
//	@Expose
//	private String fee;
//	@SerializedName("volume")
//	@Expose
//	private String volume;
//	@SerializedName("location")
//	@Expose
//	private String location;
//	@SerializedName("updated_time")
//	@Expose
//	private String updatedTime;
//	@SerializedName("is_read")
//	@Expose
//	private Integer isRead;
//	@SerializedName("is_new")
//	@Expose
//	private Integer isNew;
//
//	public float product_quality;
//
//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public Object getSupplier() {
//		return supplier;
//	}
//
//	public void setSupplier(Object supplier) {
//		this.supplier = supplier;
//	}
//
//	public Object getFullname() {
//		return fullname;
//	}
//
//	public void setFullname(Object fullname) {
//		this.fullname = fullname;
//	}
//
//	public Integer getIdSupplier() {
//		return idSupplier;
//	}
//
//	public void setIdSupplier(Integer idSupplier) {
//		this.idSupplier = idSupplier;
//	}
//
//	public String getCategory() {
//		return category;
//	}
//
//	public void setCategory(String category) {
//		this.category = category;
//	}
//
//	public CategoryArray getCategoryArray() {
//		return categoryArray;
//	}
//
//	public void setCategoryArray(CategoryArray categoryArray) {
//		this.categoryArray = categoryArray;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	public List<Stock> getStock() {
//		return stock;
//	}
//
//	public void setStock(List<Stock> stock) {
//		this.stock = stock;
//	}
//
//	public String getPrice() {
//		return price;
//	}
//
//	public void setPrice(String price) {
//		this.price = price;
//	}
//
//	public List<Price> getPrices() {
//		return prices;
//	}
//
//	public void setPrices(List<Price> prices) {
//		this.prices = prices;
//	}
//
//	public List<Image> getImages() {
//		return images;
//	}
//
//	public void setImages(List<Image> images) {
//		this.images = images;
//	}
//
//	public String getImage1() {
//		return image1;
//	}
//
//	public void setImage1(String image1) {
//		this.image1 = image1;
//	}
//
//	public String getImage2() {
//		return image2;
//	}
//
//	public void setImage2(String image2) {
//		this.image2 = image2;
//	}
//
//	public Object getImage3() {
//		return image3;
//	}
//
//	public void setImage3(Object image3) {
//		this.image3 = image3;
//	}
//
//	public Object getImage4() {
//		return image4;
//	}
//
//	public void setImage4(Object image4) {
//		this.image4 = image4;
//	}
//
//	public Object getImage5() {
//		return image5;
//	}
//
//	public void setImage5(Object image5) {
//		this.image5 = image5;
//	}
//
//	public Object getImage6() {
//		return image6;
//	}
//
//	public void setImage6(Object image6) {
//		this.image6 = image6;
//	}
//
//	public String getCommission() {
//		return commission;
//	}
//
//	public void setCommission(String commission) {
//		this.commission = commission;
//	}
//
//	public String getFee() {
//		return fee;
//	}
//
//	public void setFee(String fee) {
//		this.fee = fee;
//	}
//
//	public String getVolume() {
//		return volume;
//	}
//
//	public void setVolume(String volume) {
//		this.volume = volume;
//	}
//
//	public String getLocation() {
//		return location;
//	}
//
//	public void setLocation(String location) {
//		this.location = location;
//	}
//
//	public String getUpdatedTime() {
//		return updatedTime;
//	}
//
//	public void setUpdatedTime(String updatedTime) {
//		this.updatedTime = updatedTime;
//	}
//
//	public Integer getIsRead() {
//		return isRead;
//	}
//
//	public void setIsRead(Integer isRead) {
//		this.isRead = isRead;
//	}
//
//	public Integer getIsNew() {
//		return isNew;
//	}
//
//	public void setIsNew(Integer isNew) {
//		this.isNew = isNew;
//	}
//
//}


