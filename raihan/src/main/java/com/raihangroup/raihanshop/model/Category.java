package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Category implements Serializable, Parcelable {
	
	private static final long serialVersionUID = -2551005909905946892L; // ????
	
	public int id;
	public String name;
	public String description;
	
	public Category(Parcel in) {
		this.id = in.readInt();
		this.name = in.readString();
		this.description = in.readString();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeString(name);
		out.writeString(description);
	}
	
	public static final Creator<Category> CREATOR = new Parcelable.Creator<Category>() {

		@Override
		public Category createFromParcel(Parcel source) {
			return new Category(source);
		}

		@Override
		public Category[] newArray(int size) {
			return new Category[size];
		}

	};
}
