package com.raihangroup.raihanshop.model;

import java.util.List;

public class PerformanceObject {
	
	private String month;
	private int year;
	private String level;
	private float ps;
	private float tts;
	private int target_ps;
	private List<SubKoordinat> sub_coordinate;
	private int commission;
	private int fee;
	
	public int all_commission_this_month;
	public int all_commission_before;
	public int last_month_fee;
	public float last_month_vol;
	public int this_month_fee;
	public float this_month_vol;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month; tts = 0.6f;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public List<SubKoordinat> getSub_coordinate() {
		return sub_coordinate;
	}
	public void setSub_coordinate(List<SubKoordinat> sub_coordinate) {
		this.sub_coordinate = sub_coordinate;
	}
	public int getCommission() {
		return commission;
	}
	public void setCommission(int commission) {
		this.commission = commission;
	}
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public float getPs() {
		return ps;
	}
	public void setPs(float ps) {
		this.ps = ps;
	}
	public int getTarget_ps() {
		return target_ps;
	}
	public void setTarget_ps(int target_ps) {
		this.target_ps = target_ps;
	}
	public float getTts() {
		return tts;
	}
	public void setTts(float tts) {
		this.tts = tts;
	}
}
