package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CartItem implements Parcelable {
    private int id;
    private int id_product;
    private String color;
    private String size;
    private String name;
    private String image1;
    private int price;
    private int qty;
    private int weight;
    private int commission;
    private String location;
    private String resi;
    /**
     * Service shipping cost.
     */
    public int shipping_cost; // appears only on booking order
    /**
     * Id of shipping method e.g. REG, YES, POS.
     */
    public int shipping_method_id; // appears only on booking order
    /**
     * Status Delivery
     */
    public String status;
    /**
     * Confirmation Status
     */
    public String is_confirmed;
    public String is_rated;
    public String id_supplier;

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getIs_rated() {
        return is_rated;
    }

    public void setIs_rated(String is_rated) {
        this.is_rated = is_rated;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getResi() {
        return resi;
    }

    public void setResi(String resi) {
        this.resi = resi;
    }

    public CartItem() {
    }

    public CartItem(Parcel in) {
        int[] dataI = new int[2];
        in.readIntArray(dataI);
        this.id = dataI[0];
        this.id_product = dataI[1];

        String[] data = new String[8];
        in.readStringArray(data);
        this.color = data[0];
        this.size = data[1];
        this.name = data[2];
        this.image1 = data[3];
        this.location = data[4];
        this.resi = data[5];
        this.id_supplier = data[6];
        this.status = data[7];

        dataI = new int[6];
        in.readIntArray(dataI);
        this.price = dataI[0];
        this.qty = dataI[1];
        this.weight = dataI[2];
        this.commission = dataI[3];
        this.shipping_cost = dataI[4];
        this.shipping_method_id = dataI[5];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeIntArray(new int[]{id, id_product});

        out.writeStringArray(new String[]{
                color, size, name, image1, location, resi, id_supplier, status
        });

        out.writeIntArray(new int[]{price, qty, weight, commission, shipping_cost, shipping_method_id});
    }

    public static final Creator<CartItem> CREATOR = new Parcelable.Creator<CartItem>() {

        @Override
        public CartItem createFromParcel(Parcel source) {
            return new CartItem(source);
        }

        @Override
        public CartItem[] newArray(int size) {
            return new CartItem[size];
        }

    };

}
