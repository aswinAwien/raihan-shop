package com.raihangroup.raihanshop.model;

/**
 * Member performance. Used by gson (dashboard performance).
 * @author Alh
 *
 */
public class MemberPerformance {
	/** Current PS. */
	public float ps;
	public String fullname;
	public String id_member;
}
