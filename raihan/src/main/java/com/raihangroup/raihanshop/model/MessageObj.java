package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MessageObj implements Parcelable{
	private int id;
	private String title;
	private String message;
	private String image;
	private String timestamp;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public MessageObj() {}
	
	public MessageObj(Parcel in) {
		this.id = in.readInt();
		this.title = in.readString();
		this.message = in.readString();
		this.image = in.readString();
		this.timestamp = in.readString();
		this.status = in.readInt();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeString(title);
		out.writeString(message);
		out.writeString(image);
		out.writeString(timestamp);
		out.writeInt(status);
	}
	
	public static final Creator<MessageObj> CREATOR = new Parcelable.Creator<MessageObj>() {

		@Override
		public MessageObj createFromParcel(Parcel source) {
			return new MessageObj(source);
		}

		@Override
		public MessageObj[] newArray(int size) {
			return new MessageObj[size];
		}

	};
	
}
