package com.raihangroup.raihanshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

public class Price implements Serializable {
	private static final long serialVersionUID = -215288452704101280L;
	@SerializedName("size")
	@Expose
	private String size;
	@SerializedName("price")
	@Expose
	private int price;
	@SerializedName("weight")
	@Expose
	private int weight;

	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
}


//public class Price {
//
//	@SerializedName("size")
//	@Expose
//	private String size;
//	@SerializedName("buy_price")
//	@Expose
//	private String buyPrice;
//	@SerializedName("price")
//	@Expose
//	private String price;
//	@SerializedName("tenant_price")
//	@Expose
//	private String tenantPrice;
//	@SerializedName("weight")
//	@Expose
//	private String weight;
//
//	public String getSize() {
//		return size;
//	}
//
//	public void setSize(String size) {
//		this.size = size;
//	}
//
//	public String getBuyPrice() {
//		return buyPrice;
//	}
//
//	public void setBuyPrice(String buyPrice) {
//		this.buyPrice = buyPrice;
//	}
//
//	public String getPrice() {
//		return price;
//	}
//
//	public void setPrice(String price) {
//		this.price = price;
//	}
//
//	public String getTenantPrice() {
//		return tenantPrice;
//	}
//
//	public void setTenantPrice(String tenantPrice) {
//		this.tenantPrice = tenantPrice;
//	}
//
//	public String getWeight() {
//		return weight;
//	}
//
//	public void setWeight(String weight) {
//		this.weight = weight;
//	}
//
//}




//package com.bi.raihan.model;
//
//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;
//
