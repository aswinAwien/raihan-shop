package com.raihangroup.raihanshop.model;

public class UserRsvAdmin {
	private int id_member;
	private String fullname;
	
	public int getId_member() {
		return id_member;
	}
	public void setId_member(int id_member) {
		this.id_member = id_member;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	@Override
	public String toString() {
		return fullname;
	}

}
