package com.raihangroup.raihanshop.model;

public class SupplierDetail {
	public String id;
	public String id_bank;
	public String fullname;
	public String location_id;
	public String address;
	public String phonenumber;
	public String email;
	public String company;
	public String zipcode;
	public String account_number;
	public String account_owner;
	public String created;
	public String last_updated;
	public double quality;
	public double service;
	public int rating;
	public String location;
}
