package com.raihangroup.raihanshop.model;

public class ServiceShippingCost {
	
	/** Id of shipping service. */
	public int id;
	/** Shipping service name (e.g., REG, POS, YES). */ 
	public String services;
	/** Shipping price. */
	public int price;
	/** Estimation shipping time in days. */
	public String etd;
}
