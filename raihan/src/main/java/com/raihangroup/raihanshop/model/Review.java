package com.raihangroup.raihanshop.model;

public class Review {
    public int id;
    public int id_order;
    public int id_order_item;
    public int id_supplier;
    public int id_member;
    public float quality;
    public float service;
    public String testimonial;
    public String date;
    public int id_product;
    public String member_name;
    public String member_image;
    public String product_name;
    public String product_image;
}
