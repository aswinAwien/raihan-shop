package com.raihangroup.raihanshop.model;

/**
 * Created by Mohamad on 5/19/2015.
 */
public class SubDistrictJNE {
    public SubDistrict[] detail;

    public class SubDistrict {
        public String code;
        public String label;

        public SubDistrict(String code, String label) {
            this.code = code;
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

}
