package com.raihangroup.raihanshop.model;

/**
 * Created by Mohamad on 2/3/2015.
 */
public class Notification {
    public static String[] notificationType = {"Register","Broadcast","Performance",};

    public String type;
    public String title;
    public int is_read;
    public String date;
    public int id_member;
}
