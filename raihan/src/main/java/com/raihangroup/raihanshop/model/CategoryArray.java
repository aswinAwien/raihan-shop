
package com.raihangroup.raihanshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryArray {

    @SerializedName("133")
    @Expose
    private String _133;
    @SerializedName("143")
    @Expose
    private String _143;
    @SerializedName("8")
    @Expose
    private String _8;
    @SerializedName("100")
    @Expose
    private String _100;
    @SerializedName("159")
    @Expose
    private String _159;
    @SerializedName("64")
    @Expose
    private String _64;
    @SerializedName("98")
    @Expose
    private String _98;
    @SerializedName("152")
    @Expose
    private String _152;
    @SerializedName("10")
    @Expose
    private String _10;
    @SerializedName("18")
    @Expose
    private String _18;
    @SerializedName("97")
    @Expose
    private String _97;
    @SerializedName("13")
    @Expose
    private String _13;
    @SerializedName("91")
    @Expose
    private String _91;

    public String get133() {
        return _133;
    }

    public void set133(String _133) {
        this._133 = _133;
    }

    public String get143() {
        return _143;
    }

    public void set143(String _143) {
        this._143 = _143;
    }

    public String get8() {
        return _8;
    }

    public void set8(String _8) {
        this._8 = _8;
    }

    public String get100() {
        return _100;
    }

    public void set100(String _100) {
        this._100 = _100;
    }

    public String get159() {
        return _159;
    }

    public void set159(String _159) {
        this._159 = _159;
    }

    public String get64() {
        return _64;
    }

    public void set64(String _64) {
        this._64 = _64;
    }

    public String get98() {
        return _98;
    }

    public void set98(String _98) {
        this._98 = _98;
    }

    public String get152() {
        return _152;
    }

    public void set152(String _152) {
        this._152 = _152;
    }

    public String get10() {
        return _10;
    }

    public void set10(String _10) {
        this._10 = _10;
    }

    public String get18() {
        return _18;
    }

    public void set18(String _18) {
        this._18 = _18;
    }

    public String get97() {
        return _97;
    }

    public void set97(String _97) {
        this._97 = _97;
    }

    public String get13() {
        return _13;
    }

    public void set13(String _13) {
        this._13 = _13;
    }

    public String get91() {
        return _91;
    }

    public void set91(String _91) {
        this._91 = _91;
    }

}