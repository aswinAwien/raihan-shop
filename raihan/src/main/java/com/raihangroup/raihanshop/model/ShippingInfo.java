package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ShippingInfo implements Parcelable{
	private String name;
	private String email;
	private String address;
	private String province;
	private String city;
	private String zipcode;
	private String phone;
	private String shipping_method_id;
	private String sub_district;
	private String resi;
    private String alternate_sender;
    private String code;

	private int province_id;
	private int city_id;
	private int subdistrict_id;

	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getShipping_method_id() {
		return shipping_method_id;
	}
	public void setShipping_method_id(String shipping_method_id) {
		this.shipping_method_id = shipping_method_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
    public String getCode() {return code;}
    public void setCode(String code) { this.code = code; }
    public int getProvince_id() {
		return province_id;
	}
	public void setProvince_id(int province_id) {
		this.province_id = province_id;
	}
	public int getCity_id() {
		return city_id;
	}
	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}
	public int getSubdistrict_id() {
		return subdistrict_id;
	}
	public void setSubdistrict_id(int subdistrict_id) {
		this.subdistrict_id = subdistrict_id;
	}
	public String getSub_district() {
		return sub_district;
	}
	public void setSub_district(String sub_district) {
		this.sub_district = sub_district;
	}
	public String getResi() {
		return resi;
	}
	public void setResi(String resi) {
		this.resi = resi;
	}
    public String getAlternate_sender() {
        return alternate_sender;
    }
    public void setAlternate_sender(String alternate_sender) {
        this.alternate_sender = alternate_sender;
    }

    public ShippingInfo() {}

	public ShippingInfo(Parcel in) {
		String[] data = new String[12];
		in.readStringArray(data);

		this.name = data[0];
		this.email = data[1];
		this.address = data[2];
		this.province = data[3];
		this.city = data[4];
		this.zipcode = data[5];
		this.phone = data[6];
		this.shipping_method_id = data[7];
		this.sub_district = data[8];
		this.resi = data[9];
		this.alternate_sender = data[10];
        this.code = data[11];
		int[] daint = new int[3];
		in.readIntArray(daint);

		this.province_id = daint[0];
		this.city_id = daint[1];
		this.subdistrict_id = daint[2];
	}

	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int arg1) {
		out.writeStringArray(new String[] {
				name, email, address, province, city, zipcode, phone, shipping_method_id,
				sub_district, resi, alternate_sender, code
		});
		out.writeIntArray(new int[] {
				province_id, city_id, subdistrict_id
		});
	}

	public static final Creator<ShippingInfo> CREATOR = new Parcelable.Creator<ShippingInfo>() {

		@Override
		public ShippingInfo createFromParcel(Parcel source) {
			return new ShippingInfo(source);
		}

		@Override
		public ShippingInfo[] newArray(int size) {
			return new ShippingInfo[size];
		}
	};


}
