package com.raihangroup.raihanshop.model;

/**
 * Created by ocit on 9/3/15.
 */
public class SellingUser {

    private String ps, pt, comission, omset;

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getPt() {
        return pt;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }

    public String getComission() {
        return comission;
    }

    public void setComission(String comission) {
        this.comission = comission;
    }

    public String getOmset() {
        return omset;
    }

    public void setOmset(String omset) {
        this.omset = omset;
    }

}
