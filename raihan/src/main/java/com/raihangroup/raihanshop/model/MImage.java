package com.raihangroup.raihanshop.model;

import java.io.Serializable;

public class MImage implements Serializable {
	private static final long serialVersionUID = -3645714219592578527L;
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
