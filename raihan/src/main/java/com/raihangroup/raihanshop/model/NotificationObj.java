package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationObj implements Parcelable {
	private String type;
	private String title;
	private String content;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	// PARCELABLE STUFF
	public NotificationObj(){}
	
	public NotificationObj(Parcel in) {
		String[] data = new String[3];
		in.readStringArray(data);
		
		this.type = data[0];
		this.title = data[1];
		this.content = data[2];
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {
		
		out.writeStringArray(new String[] {
				type, title, content
		});
		
	}
	
	public static final Creator<NotificationObj> CREATOR = new Parcelable.Creator<NotificationObj>() {

		@Override
		public NotificationObj createFromParcel(Parcel source) {
			return new NotificationObj(source);
		}

		@Override
		public NotificationObj[] newArray(int size) {
			return new NotificationObj[size];
		}

	};
	
}
