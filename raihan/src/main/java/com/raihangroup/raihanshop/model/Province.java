package com.raihangroup.raihanshop.model;

public class Province {

	public String id;
	public String title;
	
	public Province(String id, String title) {
		this.id = id;
		this.title = title;
	}
	
	@Override
	public String toString() {
		return title;
	}
}