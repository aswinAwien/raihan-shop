package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class BookReference implements Parcelable{
	
	private String booking_id;
	private List<CartItem> products;
	private ShippingInfo shipping;
	private String customer;
	
	public ShippingInfo getShipping() {
		return shipping;
	}
	public void setShipping(ShippingInfo shipping) {
		this.shipping = shipping;
	}
	public String getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(String booking_id) {
		this.booking_id = booking_id;
	}
	public List<CartItem> getProducts() {
		return products;
	}
	public void setProducts(List<CartItem> products) {
		this.products = products;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
	public BookReference(){}
	
	public BookReference(Parcel in) {
		this.booking_id = in.readString();
		this.customer = in.readString();
		this.products = new ArrayList<CartItem>();
		
		in.readList(products, CartItem.class.getClassLoader());
		shipping = (ShippingInfo) in.readValue(ShippingInfo.class.getClassLoader());
		
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(booking_id);
		out.writeString(customer);
		out.writeList(products);
		out.writeValue(shipping);
	}

	public static final Creator<BookReference> CREATOR = new Parcelable.Creator<BookReference>() {

		@Override
		public BookReference createFromParcel(Parcel source) {
			return new BookReference(source);
		}

		@Override
		public BookReference[] newArray(int size) {
			return new BookReference[size];
		}

	};
}
