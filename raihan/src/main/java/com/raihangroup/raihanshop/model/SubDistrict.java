package com.raihangroup.raihanshop.model;

public class SubDistrict {
	
	public String id;
	public String id_city;
	public String title;
	
	public SubDistrict(String id, String id_city, String title) {
		this.id = id;
		this.id_city = id_city;
		this.title = title;
	}
	
	@Override
	public String toString() {
		return title;
	}
}
