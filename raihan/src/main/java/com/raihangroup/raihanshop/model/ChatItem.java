package com.raihangroup.raihanshop.model;

/**
 * Created by Mohamad on 1/27/2015.
 */
public class ChatItem {
    public String group_id;
    public String id_member;
    public String fullname;
    public String message;
    public String image;
    public String date;
    public String id_product;
    public String product_name;
}
