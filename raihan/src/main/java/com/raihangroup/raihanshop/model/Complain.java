package com.raihangroup.raihanshop.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohamad on 1/22/2015.
 */
public class Complain {
    public String id_order;
    public List<Item> items = new ArrayList<Item>();

    @Override
    public String toString() {
        return id_order;
    }

    public static class Item{
        public int id_order_items;
        public String title;

        @Override
        public String toString() {
            return title;
        }
    }

}