package com.raihangroup.raihanshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

public class Stock implements Serializable {

	private static final long serialVersionUID = -3897531734386600445L;
	@SerializedName("color")
	@Expose
	private String color;
	@SerializedName("size")
	@Expose
	private String size;
	@SerializedName("stock")
	@Expose
	private int stock;

	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}

}
//public class Stock {
//
//	@SerializedName("color")
//	@Expose
//	private String color;
//	@SerializedName("size")
//	@Expose
//	private String size;
//	@SerializedName("stock")
//	@Expose
//	private String stock;
//
//	public String getColor() {
//		return color;
//	}
//
//	public void setColor(String color) {
//		this.color = color;
//	}
//
//	public String getSize() {
//		return size;
//	}
//
//	public void setSize(String size) {
//		this.size = size;
//	}
//
//	public String getStock() {
//		return stock;
//	}
//
//	public void setStock(String stock) {
//		this.stock = stock;
//	}
//
//}
//package com.bi.raihan.model;
//
//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;
//
