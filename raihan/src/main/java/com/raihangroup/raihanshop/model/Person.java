package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ocit on 9/16/15.
 */
public class Person implements Parcelable {
    public String id, fullname, image;

    public Person(Parcel in) {
        String[] data = new String[3];
        in.readStringArray(data);

        this.id = data[0];
        this.fullname = data[1];
        this.image = data[2];
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeStringArray(new String[] { id, fullname, image });
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

}
