package com.raihangroup.raihanshop.model;


public class SelfPerformance {
	//TS
	private int id;
	private String date_time;
	private String product;
	private int price;
	private float volume;
    private String image;
	private String fullname;
	private String level;
    private int is_mentor;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	public float getVolume() {
		return volume;
	}
	public void setVolume(float volume) {
		this.volume = volume;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
    public String getImage() { return image; }
    public void setImage(String image) { this.image = image; }
    public int getIs_mentor() { return is_mentor; }
    public void setIs_mentor(int is_mentor) { this.is_mentor = is_mentor; }
}
