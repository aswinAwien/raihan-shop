package com.raihangroup.raihanshop.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;
import com.linkedin.android.spyglass.mentions.Mentionable;
import org.json.JSONArray;
import com.linkedin.android.spyglass.tokenization.QueryToken;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by ocit on 9/14/15.
 */

public class Kota implements Mentionable {

    private final String mName;

    public Kota(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    // --------------------------------------------------
    // Mentionable Implementation
    // --------------------------------------------------

    @NonNull
    @Override
    public String getTextForDisplayMode(MentionDisplayMode mode) {
        switch (mode) {
            case FULL:
                return mName;
            case PARTIAL:
            case NONE:
            default:
                return "";
        }
    }

    @Override
    public MentionDeleteStyle getDeleteStyle() {
        // Note: Cities do not support partial deletion
        // i.e. "San Francisco" -> DEL -> ""
        return MentionDeleteStyle.PARTIAL_NAME_DELETE;
    }

    @Override
    public int getSuggestibleId() {
        return mName.hashCode();
    }

    @Override
    public String getSuggestiblePrimaryText() {
        return mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
    }

    public Kota(Parcel in) {
        mName = in.readString();
    }

    public static final Parcelable.Creator<Kota> CREATOR
            = new Parcelable.Creator<Kota>() {
        public Kota createFromParcel(Parcel in) {
            return new Kota(in);
        }

        public Kota[] newArray(int size) {
            return new Kota[size];
        }
    };

    // --------------------------------------------------
    // CityLoader Class (loads cities from JSON file)
    // --------------------------------------------------

    public static class CityLoader extends MentionsLoader<Kota> {
        private static final String TAG = CityLoader.class.getSimpleName();

        public CityLoader(List<Person> data) {
            super(data);
        }

        @Override
        public Kota[] loadData(JSONArray arr) {
            Kota[] data = new Kota[arr.length()];
            try {
                for (int i = 0; i < arr.length(); i++) {
                    data[i] = new Kota(arr.getString(i));
                }
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception while parsing city JSONArray", e);
            }
            return data;
        }
    }

}

/**
 * Simple class to get suggestions from a JSONArray (represented as a file on disk), which can then
 * be mentioned by the user by tapping on the suggestion.
 */
abstract class MentionsLoader<T extends Mentionable> {

    protected T[] mData;
    private static final String TAG = MentionsLoader.class.getSimpleName();

    public MentionsLoader(List<Person> data) {
        returnJSONArray(data);
    }

    public abstract T[] loadData(JSONArray arr);

    // Returns a subset
    public List<T> getSuggestions(QueryToken queryToken) {
        String prefix = queryToken.getKeywords().toLowerCase();
        List<T> suggestions = new ArrayList<>();
        if (mData != null) {
            for (T suggestion : mData) {
                String name = suggestion.getSuggestiblePrimaryText().toLowerCase();
                if (name.startsWith(prefix)) {
                    suggestions.add(suggestion);
                }
            }
        }
        return suggestions;
    }

    private JSONArray returnJSONArray(List<Person> arr) {
        JSONArray jsonArray = new JSONArray();
        for(int i =0; i<arr.size(); i++) {
            jsonArray.put("@"+arr.get(i).fullname);
            System.out.println(arr.get(i).fullname);
        }
        System.out.println(jsonArray.toString());
        mData = loadData(jsonArray);
        return jsonArray;
    }
}