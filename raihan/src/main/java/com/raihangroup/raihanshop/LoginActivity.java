package com.raihangroup.raihanshop;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.raihangroup.raihanshop.activity.ForgetPasActivity;
import com.raihangroup.raihanshop.activity.HomeActivity;
import com.raihangroup.raihanshop.activity.MainActivity;
import com.raihangroup.raihanshop.activity.RegisterActivity;
import com.raihangroup.raihanshop.activity.RegisterWebViewActivity;
import com.raihangroup.raihanshop.helper.Constants;
import com.raihangroup.raihanshop.helper.Helper;
import com.raihangroup.raihanshop.helper.PrefStorage;
import com.raihangroup.raihanshop.helper.Utility;
import com.raihangroup.raihanshop.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private AQuery aq;
    private ProgressDialog progDialog;
    private String gcm;
    private String regid;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        progDialog = new ProgressDialog(this);
        progDialog.setTitle("Login");
        progDialog.setMessage("Mohon Tunggu..");

        TextView versionNameTextView = (TextView) findViewById(R.id.tvErsion);
        versionNameTextView.setText(Helper.getAppVersionName(this));

        aq = new AQuery(this);
        aq.id(R.id.btnLogin).clicked(this, "loginClicked");
        aq.id(R.id.btnRegist).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
//                String url = "https://www.raihanshop.com/v2/index.php/register";
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
            }
        });
        PrefStorage.newInstance(this);

        button = (Button)findViewById(R.id.btnLogin);
        aq.id(R.id.tvLupa).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgetPasActivity.class);
                i.putExtra(Constants.INTENT_FORGET_P, ForgetPasActivity.FORGET);
                startActivity(i);
            }
        });

        if (checkPlayServices()) {
//            gcm = FirebaseInstanceId.getInstance().getId();
            regid = PrefStorage.instance.getGCMid();
            if (Utility.isNullOrBlank(regid)) {
                Utility.toast(this, "Registering GCM..");
                Log.i(TAG, "registering gcm .... " );
                registerInBackground();
            }
        }
//        else {
//			Utility.showDialogError(this, "No Google Play Service", "Galat");
//		}

        // belum login
        if (PrefStorage.instance.getUserId() == Constants.NOT_FOUND_DEFAULT) {
            Utility.toast(this, "Please Login");
            Log.d("debug", "deleting db... (the hard way) " + deleteDatabase("raihan"));
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                if (gcm == null) {
                    gcm = FirebaseInstanceId.getInstance().getId();
                    PrefStorage.instance.setGCMid(gcm);
                }
//                    regid = gcm.register(Constants.GCM_PROJECT_ID);
//                    PrefStorage.instance.setGCMid(regid);
                msg = "Sukses mendaftar GCM!";
                return msg;
            }

            protected void onPostExecute(String result) {
                Utility.toast(LoginActivity.this, result);
                if (!result.equals("Sukses mendaftar GCM!")) {
                    new AlertDialog.Builder(LoginActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert).setTitle("Error")
                            .setMessage(result + ".\nRestart Aplikasi.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    finish();
                                }
                            }).create().show();
                }
            }
        }.execute(null, null, null);
    }

    // view clicked method

    public void loginClicked(View button) throws Exception {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", aq.id(R.id.etLogin).getText().toString());
        params.put("password",
                Utility.toMD5(aq.id(R.id.etPassword).getText().toString()));
        if (Utility.isNullOrBlank(PrefStorage.instance.getGCMid())) {
            Utility.showDialogError(this, "Device ini belum dapat digunakan", "Error");
            return;
        }
        params.put("gcm_regid", PrefStorage.instance.getGCMid());
        PrefStorage.newInstance(this).setKeyIsLoggedIn(true);

        aq.progress(progDialog).ajax(Constants.URL_LOGIN_API, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (status.getCode() == Constants.HTTP_RESPONSE_OK) {
                    try {
                        if (object.getString("success").equals("false")) {
                            switch (object.getInt("error")) {
                                case 1: //salah komb
                                    Utility.toast(LoginActivity.this, "Username atau password salah");
                                    break;
                                case 2: //GCM udah ada
                                    Utility.toast(LoginActivity.this, "Device ini telah terdaftar atas akun lain");
                                    break;
                            }
                        } else {
                            Gson gs = new Gson();
                            User now = gs.fromJson(object.toString(), User.class);

                            PrefStorage.instance.setUser(now.getId(), now.getFullname(), now.getRole());
                            PrefStorage.instance.setUserData(object.toString());

                            Utility.toast(getApplicationContext(), "Selamat Datang, " + now.getFullname());
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);

                            i.putExtra(Constants.INTENT_USER_OBJ, now);
                            startActivity(i);
                            finish();
                        }
                    } catch (JSONException e) {
                    }
                } else if (status.getCode() == Constants.HTTP_RESPONSE_NETWORKERROR) {
                    Utility.toast(LoginActivity.this, "Kesalahan Jaringan");
                }
            }
        });
    }

    // private support method

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        Constants.HTTP_RESPONSE_PAGENOTFOUND).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }
}
