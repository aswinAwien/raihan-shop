// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChartPerformanceAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ChartPerformanceAdapter.ViewHolder target;

  @UiThread
  public ChartPerformanceAdapter$ViewHolder_ViewBinding(ChartPerformanceAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.tvLevelName = Utils.findRequiredViewAsType(source, R.id.tv_level_name, "field 'tvLevelName'", TextView.class);
    target.tvSubtitle = Utils.findRequiredViewAsType(source, R.id.tv_subtitle, "field 'tvSubtitle'", TextView.class);
    target.piePSchart = Utils.findRequiredViewAsType(source, R.id.piePSchart, "field 'piePSchart'", PieChart.class);
    target.piePTchart = Utils.findRequiredViewAsType(source, R.id.piePTchart, "field 'piePTchart'", PieChart.class);
    target.tvKomisi = Utils.findRequiredViewAsType(source, R.id.tv_komisi, "field 'tvKomisi'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChartPerformanceAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvLevelName = null;
    target.tvSubtitle = null;
    target.piePSchart = null;
    target.piePTchart = null;
    target.tvKomisi = null;
  }
}
