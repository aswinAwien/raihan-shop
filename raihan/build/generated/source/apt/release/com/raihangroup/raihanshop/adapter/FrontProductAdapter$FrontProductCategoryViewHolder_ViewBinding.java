// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FrontProductAdapter$FrontProductCategoryViewHolder_ViewBinding implements Unbinder {
  private FrontProductAdapter.FrontProductCategoryViewHolder target;

  @UiThread
  public FrontProductAdapter$FrontProductCategoryViewHolder_ViewBinding(FrontProductAdapter.FrontProductCategoryViewHolder target,
      View source) {
    this.target = target;

    target.ivLogoParentCategory = Utils.findRequiredViewAsType(source, R.id.iv_logoParentCategory, "field 'ivLogoParentCategory'", ImageView.class);
    target.tvItemParentCategory = Utils.findRequiredViewAsType(source, R.id.tv_itemParentCategory, "field 'tvItemParentCategory'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FrontProductAdapter.FrontProductCategoryViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivLogoParentCategory = null;
    target.tvItemParentCategory = null;
  }
}
