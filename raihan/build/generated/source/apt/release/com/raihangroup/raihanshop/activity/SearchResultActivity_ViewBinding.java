// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchResultActivity_ViewBinding implements Unbinder {
  private SearchResultActivity target;

  @UiThread
  public SearchResultActivity_ViewBinding(SearchResultActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SearchResultActivity_ViewBinding(SearchResultActivity target, View source) {
    this.target = target;

    target.svProduct = Utils.findRequiredViewAsType(source, R.id.sv_product, "field 'svProduct'", SearchView.class);
    target.pbList = Utils.findRequiredViewAsType(source, R.id.pb_list, "field 'pbList'", ProgressBar.class);
    target.toolbarSearch = Utils.findRequiredViewAsType(source, R.id.toolbar_search, "field 'toolbarSearch'", Toolbar.class);
    target.resultProductList = Utils.findRequiredViewAsType(source, R.id.result_product_list, "field 'resultProductList'", PullToRefreshListView.class);
    target.tvMessageTitle = Utils.findRequiredViewAsType(source, R.id.tvMessageTitle, "field 'tvMessageTitle'", TextView.class);
    target.tvMessageContent = Utils.findRequiredViewAsType(source, R.id.tvMessageContent, "field 'tvMessageContent'", TextView.class);
    target.llInfo = Utils.findRequiredViewAsType(source, R.id.llInfo, "field 'llInfo'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SearchResultActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.svProduct = null;
    target.pbList = null;
    target.toolbarSearch = null;
    target.resultProductList = null;
    target.tvMessageTitle = null;
    target.tvMessageContent = null;
    target.llInfo = null;
  }
}
