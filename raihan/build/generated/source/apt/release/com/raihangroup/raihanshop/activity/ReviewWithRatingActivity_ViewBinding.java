// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReviewWithRatingActivity_ViewBinding implements Unbinder {
  private ReviewWithRatingActivity target;

  @UiThread
  public ReviewWithRatingActivity_ViewBinding(ReviewWithRatingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ReviewWithRatingActivity_ViewBinding(ReviewWithRatingActivity target, View source) {
    this.target = target;

    target.toobalReviewRating = Utils.findRequiredViewAsType(source, R.id.toobal_review_rating, "field 'toobalReviewRating'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ReviewWithRatingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalReviewRating = null;
  }
}
