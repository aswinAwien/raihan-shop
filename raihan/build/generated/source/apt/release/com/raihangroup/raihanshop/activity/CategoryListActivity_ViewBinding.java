// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoryListActivity_ViewBinding implements Unbinder {
  private CategoryListActivity target;

  @UiThread
  public CategoryListActivity_ViewBinding(CategoryListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CategoryListActivity_ViewBinding(CategoryListActivity target, View source) {
    this.target = target;

    target.toolbarCategoryList = Utils.findRequiredViewAsType(source, R.id.toolbar_category_list, "field 'toolbarCategoryList'", Toolbar.class);
    target.recyclerListCategory = Utils.findRequiredViewAsType(source, R.id.recycler_list_category, "field 'recyclerListCategory'", RecyclerView.class);
    target.pg = Utils.findRequiredViewAsType(source, R.id.pg, "field 'pg'", ProgressBar.class);
    target.imageView2 = Utils.findRequiredViewAsType(source, R.id.imageView2, "field 'imageView2'", ImageView.class);
    target.tvMessageTitle = Utils.findRequiredViewAsType(source, R.id.tvMessageTitle, "field 'tvMessageTitle'", TextView.class);
    target.tvMessageContent = Utils.findRequiredViewAsType(source, R.id.tvMessageContent, "field 'tvMessageContent'", TextView.class);
    target.llInfo = Utils.findRequiredViewAsType(source, R.id.llInfo, "field 'llInfo'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoryListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbarCategoryList = null;
    target.recyclerListCategory = null;
    target.pg = null;
    target.imageView2 = null;
    target.tvMessageTitle = null;
    target.tvMessageContent = null;
    target.llInfo = null;
  }
}
