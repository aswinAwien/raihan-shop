// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewStatusListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ViewStatusListAdapter.ViewHolder target;

  @UiThread
  public ViewStatusListAdapter$ViewHolder_ViewBinding(ViewStatusListAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.imgProfileStatus = Utils.findRequiredViewAsType(source, R.id.imgProfileStatus, "field 'imgProfileStatus'", CircleImageView.class);
    target.txNameProfileStatus = Utils.findRequiredViewAsType(source, R.id.txNameProfileStatus, "field 'txNameProfileStatus'", TextView.class);
    target.txSubTimeStatus = Utils.findRequiredViewAsType(source, R.id.txSubTimeStatus, "field 'txSubTimeStatus'", TextView.class);
    target.txBodyTextStatus = Utils.findRequiredViewAsType(source, R.id.txBodyTextStatus, "field 'txBodyTextStatus'", TextView.class);
    target.txCountComment = Utils.findRequiredViewAsType(source, R.id.txCountComment, "field 'txCountComment'", TextView.class);
    target.txCountLike = Utils.findRequiredViewAsType(source, R.id.txCountLike, "field 'txCountLike'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewStatusListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgProfileStatus = null;
    target.txNameProfileStatus = null;
    target.txSubTimeStatus = null;
    target.txBodyTextStatus = null;
    target.txCountComment = null;
    target.txCountLike = null;
  }
}
