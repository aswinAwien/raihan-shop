// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FrontMainFragment_ViewBinding implements Unbinder {
  private FrontMainFragment target;

  @UiThread
  public FrontMainFragment_ViewBinding(FrontMainFragment target, View source) {
    this.target = target;

    target.viewPagerFront = Utils.findRequiredViewAsType(source, R.id.view_pager_front, "field 'viewPagerFront'", ViewPager.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FrontMainFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewPagerFront = null;
  }
}
