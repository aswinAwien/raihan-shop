// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListMemberActivity_ViewBinding implements Unbinder {
  private ListMemberActivity target;

  @UiThread
  public ListMemberActivity_ViewBinding(ListMemberActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListMemberActivity_ViewBinding(ListMemberActivity target, View source) {
    this.target = target;

    target.toobalListMember = Utils.findRequiredViewAsType(source, R.id.toobal_list_member, "field 'toobalListMember'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListMemberActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalListMember = null;
  }
}
