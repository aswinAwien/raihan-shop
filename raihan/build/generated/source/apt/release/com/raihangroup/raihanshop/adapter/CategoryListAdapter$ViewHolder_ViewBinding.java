// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoryListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CategoryListAdapter.ViewHolder target;

  @UiThread
  public CategoryListAdapter$ViewHolder_ViewBinding(CategoryListAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.tvCategory = Utils.findRequiredViewAsType(source, R.id.tvCategory, "field 'tvCategory'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoryListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvCategory = null;
  }
}
