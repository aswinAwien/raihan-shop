// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductDetailActivity_ViewBinding implements Unbinder {
  private ProductDetailActivity target;

  @UiThread
  public ProductDetailActivity_ViewBinding(ProductDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductDetailActivity_ViewBinding(ProductDetailActivity target, View source) {
    this.target = target;

    target.toolbarDetail = Utils.findRequiredViewAsType(source, R.id.toolbar_detail, "field 'toolbarDetail'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbarDetail = null;
  }
}
