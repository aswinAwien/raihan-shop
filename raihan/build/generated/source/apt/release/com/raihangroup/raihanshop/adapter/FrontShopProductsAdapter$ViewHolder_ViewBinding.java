// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FrontShopProductsAdapter$ViewHolder_ViewBinding implements Unbinder {
  private FrontShopProductsAdapter.ViewHolder target;

  @UiThread
  public FrontShopProductsAdapter$ViewHolder_ViewBinding(FrontShopProductsAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.ivNewestProduct = Utils.findRequiredViewAsType(source, R.id.iv_newestProduct, "field 'ivNewestProduct'", ImageView.class);
    target.tvNewestProductName = Utils.findRequiredViewAsType(source, R.id.tv_newestProductName, "field 'tvNewestProductName'", TextView.class);
    target.tvNewestProductPrice = Utils.findRequiredViewAsType(source, R.id.tv_newestProductPrice, "field 'tvNewestProductPrice'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FrontShopProductsAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivNewestProduct = null;
    target.tvNewestProductName = null;
    target.tvNewestProductPrice = null;
  }
}
