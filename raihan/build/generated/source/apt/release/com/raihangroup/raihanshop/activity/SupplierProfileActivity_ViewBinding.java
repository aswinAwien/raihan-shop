// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SupplierProfileActivity_ViewBinding implements Unbinder {
  private SupplierProfileActivity target;

  @UiThread
  public SupplierProfileActivity_ViewBinding(SupplierProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SupplierProfileActivity_ViewBinding(SupplierProfileActivity target, View source) {
    this.target = target;

    target.tbVpSupplier = Utils.findRequiredViewAsType(source, R.id.tb_vp_supplier, "field 'tbVpSupplier'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SupplierProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tbVpSupplier = null;
  }
}
