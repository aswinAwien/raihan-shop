// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReturnProductActivity_ViewBinding implements Unbinder {
  private ReturnProductActivity target;

  @UiThread
  public ReturnProductActivity_ViewBinding(ReturnProductActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ReturnProductActivity_ViewBinding(ReturnProductActivity target, View source) {
    this.target = target;

    target.toobalReturnProduct = Utils.findRequiredViewAsType(source, R.id.toobal_return_product, "field 'toobalReturnProduct'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ReturnProductActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalReturnProduct = null;
  }
}
