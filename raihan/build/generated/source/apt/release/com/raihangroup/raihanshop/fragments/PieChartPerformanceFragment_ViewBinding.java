// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PieChartPerformanceFragment_ViewBinding implements Unbinder {
  private PieChartPerformanceFragment target;

  @UiThread
  public PieChartPerformanceFragment_ViewBinding(PieChartPerformanceFragment target, View source) {
    this.target = target;

    target.pieLevelChart = Utils.findRequiredViewAsType(source, R.id.pieLevelChart, "field 'pieLevelChart'", PieChart.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PieChartPerformanceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.pieLevelChart = null;
  }
}
