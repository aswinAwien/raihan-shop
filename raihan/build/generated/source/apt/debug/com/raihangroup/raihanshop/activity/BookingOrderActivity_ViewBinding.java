// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BookingOrderActivity_ViewBinding implements Unbinder {
  private BookingOrderActivity target;

  @UiThread
  public BookingOrderActivity_ViewBinding(BookingOrderActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BookingOrderActivity_ViewBinding(BookingOrderActivity target, View source) {
    this.target = target;

    target.toobalBookingOrder = Utils.findRequiredViewAsType(source, R.id.toobal_booking_order, "field 'toobalBookingOrder'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BookingOrderActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalBookingOrder = null;
  }
}
