// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileActivity_ViewBinding implements Unbinder {
  private ProfileActivity target;

  @UiThread
  public ProfileActivity_ViewBinding(ProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileActivity_ViewBinding(ProfileActivity target, View source) {
    this.target = target;

    target.appbarProfile = Utils.findRequiredViewAsType(source, R.id.appbar_profile, "field 'appbarProfile'", AppBarLayout.class);
    target.toolbarProfile = Utils.findRequiredViewAsType(source, R.id.toolbar_profile, "field 'toolbarProfile'", Toolbar.class);
    target.rvStatusDisplay = Utils.findRequiredViewAsType(source, R.id.rv_status_display, "field 'rvStatusDisplay'", RecyclerView.class);
    target.tvOmset = Utils.findRequiredViewAsType(source, R.id.tvOmset, "field 'tvOmset'", TextView.class);
    target.tvKomisi = Utils.findRequiredViewAsType(source, R.id.tvKomisi, "field 'tvKomisi'", TextView.class);
    target.tvPSendiri = Utils.findRequiredViewAsType(source, R.id.tvPSendiri, "field 'tvPSendiri'", TextView.class);
    target.tvPTim = Utils.findRequiredViewAsType(source, R.id.tvPTim, "field 'tvPTim'", TextView.class);
    target.tvProfileId = Utils.findRequiredViewAsType(source, R.id.tv_profile_id, "field 'tvProfileId'", TextView.class);
    target.coordinatorLayout = Utils.findRequiredViewAsType(source, R.id.coordinatorLayout, "field 'coordinatorLayout'", CoordinatorLayout.class);
    target.financialDisplay = Utils.findRequiredViewAsType(source, R.id.financial_display, "field 'financialDisplay'", LinearLayout.class);
    target.tvProfileAddress = Utils.findRequiredViewAsType(source, R.id.tv_profile_address, "field 'tvProfileAddress'", TextView.class);
    target.materialupTitleContainer = Utils.findRequiredViewAsType(source, R.id.materialup_title_container, "field 'materialupTitleContainer'", LinearLayout.class);
    target.cairkanBtn = Utils.findRequiredViewAsType(source, R.id.cairkan_btn, "field 'cairkanBtn'", Button.class);
    target.postStatus = Utils.findRequiredViewAsType(source, R.id.post_status, "field 'postStatus'", FloatingActionButton.class);
    target.emptyView = Utils.findRequiredViewAsType(source, R.id.emptyView, "field 'emptyView'", TextView.class);
    target.tvProfileName = Utils.findRequiredViewAsType(source, R.id.tv_profile_name, "field 'tvProfileName'", TextView.class);
    target.tvProfileTitle = Utils.findRequiredViewAsType(source, R.id.tv_profile_title, "field 'tvProfileTitle'", TextView.class);
    target.materialupProfileImage = Utils.findRequiredViewAsType(source, R.id.materialup_profile_image, "field 'materialupProfileImage'", CircleImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.appbarProfile = null;
    target.toolbarProfile = null;
    target.rvStatusDisplay = null;
    target.tvOmset = null;
    target.tvKomisi = null;
    target.tvPSendiri = null;
    target.tvPTim = null;
    target.tvProfileId = null;
    target.coordinatorLayout = null;
    target.financialDisplay = null;
    target.tvProfileAddress = null;
    target.materialupTitleContainer = null;
    target.cairkanBtn = null;
    target.postStatus = null;
    target.emptyView = null;
    target.tvProfileName = null;
    target.tvProfileTitle = null;
    target.materialupProfileImage = null;
  }
}
