// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderPaymentActivity_ViewBinding implements Unbinder {
  private OrderPaymentActivity target;

  @UiThread
  public OrderPaymentActivity_ViewBinding(OrderPaymentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderPaymentActivity_ViewBinding(OrderPaymentActivity target, View source) {
    this.target = target;

    target.toobalOrderPayment = Utils.findRequiredViewAsType(source, R.id.toobal_order_payment, "field 'toobalOrderPayment'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderPaymentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalOrderPayment = null;
  }
}
