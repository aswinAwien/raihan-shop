// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelfPerformanceActivity_ViewBinding implements Unbinder {
  private SelfPerformanceActivity target;

  @UiThread
  public SelfPerformanceActivity_ViewBinding(SelfPerformanceActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SelfPerformanceActivity_ViewBinding(SelfPerformanceActivity target, View source) {
    this.target = target;

    target.toobalSelfPerformance = Utils.findRequiredViewAsType(source, R.id.toobal_self_performance, "field 'toobalSelfPerformance'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SelfPerformanceActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalSelfPerformance = null;
  }
}
