// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoggedinDrawer_ViewBinding implements Unbinder {
  private LoggedinDrawer target;

  @UiThread
  public LoggedinDrawer_ViewBinding(LoggedinDrawer target, View source) {
    this.target = target;

    target.homeProfileImage = Utils.findRequiredViewAsType(source, R.id.homeProfileImage, "field 'homeProfileImage'", CircleImageView.class);
    target.tvNameHome = Utils.findRequiredViewAsType(source, R.id.tvNameHome, "field 'tvNameHome'", TextView.class);
    target.bLogout = Utils.findRequiredViewAsType(source, R.id.bLogout, "field 'bLogout'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoggedinDrawer target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.homeProfileImage = null;
    target.tvNameHome = null;
    target.bLogout = null;
  }
}
