// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RatingListActivity_ViewBinding implements Unbinder {
  private RatingListActivity target;

  @UiThread
  public RatingListActivity_ViewBinding(RatingListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RatingListActivity_ViewBinding(RatingListActivity target, View source) {
    this.target = target;

    target.toobalListMessage = Utils.findRequiredViewAsType(source, R.id.toobal_list_message, "field 'toobalListMessage'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RatingListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalListMessage = null;
  }
}
