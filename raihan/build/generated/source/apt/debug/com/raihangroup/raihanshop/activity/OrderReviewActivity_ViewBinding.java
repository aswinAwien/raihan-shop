// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderReviewActivity_ViewBinding implements Unbinder {
  private OrderReviewActivity target;

  @UiThread
  public OrderReviewActivity_ViewBinding(OrderReviewActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderReviewActivity_ViewBinding(OrderReviewActivity target, View source) {
    this.target = target;

    target.toobalOrderReview = Utils.findRequiredViewAsType(source, R.id.toobal_order_review, "field 'toobalOrderReview'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderReviewActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalOrderReview = null;
  }
}
