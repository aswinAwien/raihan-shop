// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivity_ViewBinding implements Unbinder {
  private RegisterActivity target;

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target, View source) {
    this.target = target;

    target.textInputLayoutEtRPass = Utils.findRequiredViewAsType(source, R.id.textInputLayoutEtRPass, "field 'textInputLayoutEtRPass'", TextInputLayout.class);
    target.textInputLayoutEtRPassConf = Utils.findRequiredViewAsType(source, R.id.textInputLayoutEtRPassConf, "field 'textInputLayoutEtRPassConf'", TextInputLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.textInputLayoutEtRPass = null;
    target.textInputLayoutEtRPassConf = null;
  }
}
