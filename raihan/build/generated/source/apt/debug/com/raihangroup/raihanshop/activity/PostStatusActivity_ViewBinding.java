// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import com.raihangroup.raihanshop.helper.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PostStatusActivity_ViewBinding implements Unbinder {
  private PostStatusActivity target;

  @UiThread
  public PostStatusActivity_ViewBinding(PostStatusActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PostStatusActivity_ViewBinding(PostStatusActivity target, View source) {
    this.target = target;

    target.toolbarPost = Utils.findRequiredViewAsType(source, R.id.toolbar_post, "field 'toolbarPost'", Toolbar.class);
    target.appbarPost = Utils.findRequiredViewAsType(source, R.id.appbar_post, "field 'appbarPost'", AppBarLayout.class);
    target.civPost = Utils.findRequiredViewAsType(source, R.id.civ_post, "field 'civPost'", CircleImageView.class);
    target.etPost = Utils.findRequiredViewAsType(source, R.id.et_post, "field 'etPost'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PostStatusActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbarPost = null;
    target.appbarPost = null;
    target.civPost = null;
    target.etPost = null;
  }
}
