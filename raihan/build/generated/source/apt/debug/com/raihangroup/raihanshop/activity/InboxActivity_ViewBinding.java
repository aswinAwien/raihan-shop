// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InboxActivity_ViewBinding implements Unbinder {
  private InboxActivity target;

  @UiThread
  public InboxActivity_ViewBinding(InboxActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public InboxActivity_ViewBinding(InboxActivity target, View source) {
    this.target = target;

    target.toobalInbox = Utils.findRequiredViewAsType(source, R.id.toobal_inbox, "field 'toobalInbox'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InboxActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toobalInbox = null;
  }
}
