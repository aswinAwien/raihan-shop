// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PerformanceChartActivity_ViewBinding implements Unbinder {
  private PerformanceChartActivity target;

  @UiThread
  public PerformanceChartActivity_ViewBinding(PerformanceChartActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PerformanceChartActivity_ViewBinding(PerformanceChartActivity target, View source) {
    this.target = target;

    target.tabLayoutPerformance = Utils.findRequiredViewAsType(source, R.id.tabLayout_performance, "field 'tabLayoutPerformance'", TabLayout.class);
    target.viewpagePerformance = Utils.findRequiredViewAsType(source, R.id.viewpage_performance, "field 'viewpagePerformance'", ViewPager.class);
    target.toolbarPerformance = Utils.findRequiredViewAsType(source, R.id.toolbar_performance, "field 'toolbarPerformance'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PerformanceChartActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tabLayoutPerformance = null;
    target.viewpagePerformance = null;
    target.toolbarPerformance = null;
  }
}
