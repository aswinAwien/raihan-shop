// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GridCategoryAdapter$ViewHolder_ViewBinding implements Unbinder {
  private GridCategoryAdapter.ViewHolder target;

  @UiThread
  public GridCategoryAdapter$ViewHolder_ViewBinding(GridCategoryAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.imgCategory = Utils.findRequiredViewAsType(source, R.id.img_category, "field 'imgCategory'", ImageView.class);
    target.textCategory = Utils.findRequiredViewAsType(source, R.id.text_category, "field 'textCategory'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GridCategoryAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgCategory = null;
    target.textCategory = null;
  }
}
