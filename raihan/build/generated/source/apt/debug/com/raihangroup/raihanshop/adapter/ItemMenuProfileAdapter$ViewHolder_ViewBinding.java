// Generated code from Butter Knife. Do not modify!
package com.raihangroup.raihanshop.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.raihangroup.raihanshop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ItemMenuProfileAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ItemMenuProfileAdapter.ViewHolder target;

  @UiThread
  public ItemMenuProfileAdapter$ViewHolder_ViewBinding(ItemMenuProfileAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.btnListMenuImg = Utils.findRequiredViewAsType(source, R.id.btn_list_menu_img, "field 'btnListMenuImg'", ImageView.class);
    target.btnListMenuTv = Utils.findRequiredViewAsType(source, R.id.btn_list_menu_tv, "field 'btnListMenuTv'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ItemMenuProfileAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnListMenuImg = null;
    target.btnListMenuTv = null;
  }
}
